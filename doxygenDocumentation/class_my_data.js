var class_my_data =
[
    [ "MyData", "class_my_data.html#a0ae2b758f0d0a2b10b25050407cbcc75", null ],
    [ "MyData", "class_my_data.html#aceac6ee2340c0ccb1b71991fbb0b5f88", null ],
    [ "~MyData", "class_my_data.html#aa8a130b5af3bf2a8188ac24eda2b826b", null ],
    [ "AddEdgeList", "class_my_data.html#a3a01a5511f7bc5f851c13812c0305cfb", null ],
    [ "AddNodeList", "class_my_data.html#a8e682ffe91095285fbfe19fb0240e8cd", null ],
    [ "Append", "class_my_data.html#ae59a17d889e6d1edd90f77ad4ee0adc8", null ],
    [ "GetEdgeByID", "class_my_data.html#a459d6a3b1984c4eb5f6bf0770771b016", null ],
    [ "GetEdgeList", "class_my_data.html#a50454da420d006524cf09d1577b4bf07", null ],
    [ "GetEdgeList", "class_my_data.html#acc16096e5e8ad0ef27d053e18dc11ff6", null ],
    [ "GetEdgesByNodeID", "class_my_data.html#ae0ee75c9c999297d8422e461847d7360", null ],
    [ "GetNodeByID", "class_my_data.html#a37bcd40a792ffeabb8d48e4677e96ea2", null ],
    [ "GetNodeList", "class_my_data.html#a092a5262cad3644a1ccb759124c5b118", null ],
    [ "GetNodeList", "class_my_data.html#a24d1fc3de9cc9b75324fdb968e151df7", null ],
    [ "GetUniqueNodeID", "class_my_data.html#aa2e0ebc527f4cfa734dd6b492bae2d67", null ],
    [ "operator=", "class_my_data.html#a319e269c40a21df08dff851ccc6406b6", null ],
    [ "RemoveAndDeleteAll", "class_my_data.html#a2a1a56ef495f140db15823b30b463ca6", null ],
    [ "RemoveAndDeleteEdge", "class_my_data.html#a74d8be1392b3873ea73c8206f9c365c1", null ],
    [ "RemoveAndDeleteNode", "class_my_data.html#a374a189a34e600263eecf214e186e387", null ]
];