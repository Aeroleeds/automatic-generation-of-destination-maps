var searchData=
[
  ['basicprovider',['BasicProvider',['../class_basic_provider.html',1,'']]],
  ['bb',['bb',['../class_option_file.html#adfee3d9b4238595aa270d425f0a16d1c',1,'OptionFile']]],
  ['boolvaluetoindex',['boolValueToIndex',['../class_edge.html#a0203795bf71b9bb7400bb55fbff4f7cc',1,'Edge']]],
  ['borderlinecontrol',['BorderLineControl',['../class_border_line_control.html',1,'']]],
  ['boundingbox',['BoundingBox',['../struct_bounding_box.html',1,'BoundingBox'],['../struct_bounding_box.html#a6e401c4da5839950f1f30c8b8c4d1208',1,'BoundingBox::BoundingBox()'],['../struct_bounding_box.html#a63576f59f3c259553ec59785c8680ae7',1,'BoundingBox::BoundingBox(long double pLat, long double nLat, long double pLong, long double nLong)'],['../struct_bounding_box.html#a8dca1095bfeb6edff5723824af1407c0',1,'BoundingBox::BoundingBox(const BoundingBox &amp;other)']]],
  ['box',['Box',['../struct_box.html',1,'Box'],['../struct_box.html#af3838d4507107e9e67ddec97aa7c97eb',1,'Box::Box()']]]
];
