var searchData=
[
  ['intersectiononsegment',['IntersectionOnSegment',['../class_line.html#a7ebd6ff4a385ee378b760a02e7fc4771',1,'Line']]],
  ['intersectionpoint',['IntersectionPoint',['../class_line.html#a50ce4cabcf8d62b7c40d964de6d78de0',1,'Line::IntersectionPoint(Line other, unsigned int usedID)'],['../class_line.html#aa0ef5e1ddce53bfb30420dd10f72d6ad',1,'Line::IntersectionPoint(Node *p, unsigned int usedID)']]],
  ['intersects',['Intersects',['../struct_box.html#a26b1f382b50ed4d0ebf24f8be77e4c4f',1,'Box']]],
  ['isfalseintersection',['IsFalseIntersection',['../class_false_intersection_helper.html#a5213cf52608e0f23df9f62421c39c47b',1,'FalseIntersectionHelper']]],
  ['ishigherorder',['IsHigherOrder',['../class_edge.html#accb911dec2ead4b8590aa87c72563776',1,'Edge']]],
  ['isinside',['IsInside',['../struct_bounding_box.html#a0448e9780582e55d21a828a2ad395041',1,'BoundingBox::IsInside(double lat, double lon)'],['../struct_bounding_box.html#a5145335082950eba3948db1d13f68ad0',1,'BoundingBox::IsInside(Node *n)']]],
  ['islinkroad',['IsLinkRoad',['../class_edge.html#a8e9a66603c14bd24ecc2a279f3e7177a',1,'Edge']]],
  ['isoneway',['IsOneWay',['../class_edge.html#a4382e6e06e240537c483c5a5570b5835',1,'Edge']]],
  ['isonside',['IsOnSide',['../class_line.html#a9cb80634baba3ea49f584fd1dd8fc192',1,'Line']]],
  ['isringroad',['IsRingRoad',['../class_edge.html#a38e08068650d6c052f7ddf3c04c064f0',1,'Edge::IsRingRoad()'],['../class_node.html#a8b079a38d1c6d0333b1b7fed7c0a28e1',1,'Node::IsRingRoad()']]],
  ['isroundabout',['IsRoundabout',['../class_edge.html#a435286445e23bdb80cc50a8d115b74dc',1,'Edge::IsRoundabout()'],['../class_node.html#af78a4dbbce38b2ebf08e4b839d252cac',1,'Node::IsRoundabout()']]],
  ['istaken',['IsTaken',['../class_edge.html#a63828bf3dbbb9ae4cc3ca629a7411a9d',1,'Edge::IsTaken()'],['../class_node.html#aeb4e6d29e5b233fbda3ac759b23be9ab',1,'Node::IsTaken()']]],
  ['istraversable',['IsTraversable',['../class_edge.html#af0e250093ea36ad62d25a6337d76b89f',1,'Edge']]]
];
