var searchData=
[
  ['boundingbox',['BoundingBox',['../struct_bounding_box.html#a6e401c4da5839950f1f30c8b8c4d1208',1,'BoundingBox::BoundingBox()'],['../struct_bounding_box.html#a63576f59f3c259553ec59785c8680ae7',1,'BoundingBox::BoundingBox(long double pLat, long double nLat, long double pLong, long double nLong)'],['../struct_bounding_box.html#a8dca1095bfeb6edff5723824af1407c0',1,'BoundingBox::BoundingBox(const BoundingBox &amp;other)']]],
  ['box',['Box',['../struct_box.html#af3838d4507107e9e67ddec97aa7c97eb',1,'Box']]]
];
