var class_debug_options =
[
    [ "DebugSettings", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4", [
      [ "Parndorf", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4aad99854959c74647b8339ff82bf8bf6d", null ],
      [ "Bad_Brueckenau", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a88268b790330449752c60c9abf319fbb", null ],
      [ "Kiskunfelegyhaza", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a9874d2cb8b9f55fd3bfa274690d52a05", null ],
      [ "Oftringen", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a1da925e5da0fd21699aa39d1c2cfec30", null ],
      [ "Chase", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a625506af3b32eee6ed597a43e092a5f0", null ],
      [ "Dodoma", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4af36d5b806b243fb3e20f38d1031d60ed", null ],
      [ "PleasantPoint", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4aafabcb4c533751bc0d255184a2e1d8a2", null ],
      [ "Paide", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a6b1bb449e587af5ca04a7d71a4ecccb2", null ],
      [ "Belatai", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a9e13d3b51cbce0959536ad2f69134d43", null ],
      [ "Bollnas", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a94401b6bf70db3081313bfedf0ea951e", null ],
      [ "StGeorge", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a4887c3207f6da93e666b197efbea64be", null ],
      [ "Odessa", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4ac237664785632aba2e82dddd257c2e3d", null ],
      [ "Princeton", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a43ad2cc7a23560ac6ff82ba4af4eea0f", null ],
      [ "Charleston", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4a3e114646700018655512887991986b8a", null ],
      [ "NoDebug", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4ac52398091040f4ba75087aa5424758ea", null ],
      [ "numDebugValues", "class_debug_options.html#a07e011753cc0555c62d43b645172c3c4aff6dabb6cd57222c9817f3aaa1a6431a", null ]
    ] ],
    [ "DebugOptions", "class_debug_options.html#a84a54a78fff942340a059d241b2bf160", null ],
    [ "~DebugOptions", "class_debug_options.html#a2b72a68e9e005d6824be9c0e9947a262", null ]
];