var class_vec2 =
[
    [ "Vec2", "class_vec2.html#ae47b7f36ca21a470bf0d96e3339d490a", null ],
    [ "Vec2", "class_vec2.html#a13febfac139ecb0acf244603e2d4fc49", null ],
    [ "~Vec2", "class_vec2.html#a26104e95e4a87c889510b37fe760b8fb", null ],
    [ "DotProduct", "class_vec2.html#a8d7bcea4f39eb429f597ae372598bdc6", null ],
    [ "GetAngle", "class_vec2.html#a227c5ab64206898461b8297baf8e6787", null ],
    [ "GetAngleRadians", "class_vec2.html#a8f77ce10e9709673654b9462d600334b", null ]
];