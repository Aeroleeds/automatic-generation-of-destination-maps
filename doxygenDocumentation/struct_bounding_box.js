var struct_bounding_box =
[
    [ "BoundingBox", "struct_bounding_box.html#a6e401c4da5839950f1f30c8b8c4d1208", null ],
    [ "BoundingBox", "struct_bounding_box.html#a63576f59f3c259553ec59785c8680ae7", null ],
    [ "BoundingBox", "struct_bounding_box.html#a8dca1095bfeb6edff5723824af1407c0", null ],
    [ "IsInside", "struct_bounding_box.html#a0448e9780582e55d21a828a2ad395041", null ],
    [ "IsInside", "struct_bounding_box.html#a5145335082950eba3948db1d13f68ad0", null ],
    [ "toString", "struct_bounding_box.html#a8fee83d6eee1c6c4e94e8140bccb5a7b", null ],
    [ "negLat", "struct_bounding_box.html#af13c752af412cb1805bbf59fe1928a1e", null ],
    [ "negLong", "struct_bounding_box.html#ac8cd984c9e14b198b61472d1ec0d71a5", null ],
    [ "posLat", "struct_bounding_box.html#a0b4a0848cf2a3cb87321d2792aed46ae", null ],
    [ "posLong", "struct_bounding_box.html#a3488ab95835be83adc23dad2c54dac06", null ]
];