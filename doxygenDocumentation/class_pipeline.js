var class_pipeline =
[
    [ "Pipeline", "class_pipeline.html#aca6dcaa79d9658b2bd878b6975d651ce", null ],
    [ "~Pipeline", "class_pipeline.html#ae273add0ea7490c0ca4c13cb3683ec6f", null ],
    [ "DisplaySubProg", "class_pipeline.html#a003e3d06b27e2f79991af55fed90cd18", null ],
    [ "DoWork", "class_pipeline.html#a3da53338fb0b37b43a385ed3067a1a58", null ],
    [ "ResultReady", "class_pipeline.html#a67ce9ec96cafb6ad719095b265913698", null ],
    [ "run", "class_pipeline.html#ac6bf0607a8a6af598ef3c207a848a807", null ],
    [ "SetBroker", "class_pipeline.html#a841ce44821c61a6b5134fc3d9804f64f", null ],
    [ "SetUp", "class_pipeline.html#a52f9a99ccd56857281e20587735e5d20", null ]
];