var class_main_window =
[
    [ "MainWindow", "class_main_window.html#a8b244be8b7b7db1b08de2a2acb9409db", null ],
    [ "~MainWindow", "class_main_window.html#ae98d00a93bc118200eeef9f9bba1dba7", null ],
    [ "ApplyChanges", "class_main_window.html#a5deb486e2acaf3efec5003edf6cea5e7", null ],
    [ "DisplayProgress", "class_main_window.html#a08b641f76b2ba6f9fe2bedb80f25dfea", null ],
    [ "focusInEvent", "class_main_window.html#ae7c76e9879cf5cf6073b0f3186e8a901", null ],
    [ "HandleResults", "class_main_window.html#a11b84378ce8155d9024bf1fcdf0320dd", null ],
    [ "JavaScriptEventMarker", "class_main_window.html#a18c842d9c2a7cf1ae174fc614cdbbf18", null ],
    [ "JavaScriptEventRectangle", "class_main_window.html#a2640e49791c21bf4fae8f40f8b62326f", null ]
];