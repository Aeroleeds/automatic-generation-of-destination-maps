var class_xapi_data_provider =
[
    [ "XapiDataProvider", "class_xapi_data_provider.html#a4e1d34b5a9082555d48e76d8b937bd61", null ],
    [ "~XapiDataProvider", "class_xapi_data_provider.html#a854082e149f404f88e139df75d5eb660", null ],
    [ "GetData", "class_xapi_data_provider.html#a4fdbde0170ec1b5fde28067915790153", null ],
    [ "SetAPIKey", "class_xapi_data_provider.html#a73da043e8982beb99fc6a57d21bebb68", null ],
    [ "SetServer", "class_xapi_data_provider.html#ac63997af798ab6b9366117be0cac13e5", null ],
    [ "SettingsLoaded", "class_xapi_data_provider.html#a413af130cca0d534249e2606212bda6b", null ],
    [ "SetUp", "class_xapi_data_provider.html#af080172ae54ca7c0c3be4fcd1a3ff3c0", null ],
    [ "ToString", "class_xapi_data_provider.html#a4242fd955cbafe3331831f138bcc9f12", null ]
];