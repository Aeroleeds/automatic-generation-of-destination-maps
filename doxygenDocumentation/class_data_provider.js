var class_data_provider =
[
    [ "DataProvider", "class_data_provider.html#a2f8baa3781218c74e4ea387cb19ce3a4", null ],
    [ "~DataProvider", "class_data_provider.html#a7af043ed7315011021223a3135df56f2", null ],
    [ "GetData", "class_data_provider.html#a9a04e469a433e55bb8b6f49671887ca4", null ],
    [ "SetAPIKey", "class_data_provider.html#a68fa8f033fb77e61503f0cb1c12864e2", null ],
    [ "SetServer", "class_data_provider.html#a047ae284a8147125a4dd74ec9215b663", null ],
    [ "SettingsLoaded", "class_data_provider.html#a11f3564abf3eb66287673b777a1cdaa7", null ],
    [ "SetUp", "class_data_provider.html#a291db0c758b109b8b77960305484c8bf", null ],
    [ "ToString", "class_data_provider.html#a803795a2b844ba497fba4ac959d72b31", null ]
];