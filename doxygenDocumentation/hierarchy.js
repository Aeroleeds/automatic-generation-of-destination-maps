var hierarchy =
[
    [ "BorderLineControl", "class_border_line_control.html", null ],
    [ "BoundingBox", "struct_bounding_box.html", null ],
    [ "Box", "struct_box.html", null ],
    [ "CostFunction", "class_cost_function.html", null ],
    [ "CostTerm", "class_cost_term.html", [
      [ "AreaControlTerm", "class_area_control_term.html", null ],
      [ "FalseIntersectionTerm", "class_false_intersection_term.html", null ],
      [ "MinimumLengthTerm", "class_minimum_length_term.html", null ],
      [ "OrientationTerm", "class_orientation_term.html", null ],
      [ "RelativeLengthTerm", "class_relative_length_term.html", null ],
      [ "RelativeOrientationTerm", "class_relative_orientation_term.html", null ],
      [ "RelativeRelativeLengthTerm", "class_relative_relative_length_term.html", null ]
    ] ],
    [ "DataProvider", "class_data_provider.html", [
      [ "BasicProvider", "class_basic_provider.html", null ],
      [ "XapiDataProvider", "class_xapi_data_provider.html", null ]
    ] ],
    [ "DebugOptions", "class_debug_options.html", null ],
    [ "Dijkstra", "class_dijkstra.html", null ],
    [ "Edge", "class_edge.html", null ],
    [ "EventBroker", "class_event_broker.html", null ],
    [ "FalseIntersectionHelper", "class_false_intersection_helper.html", null ],
    [ "LayoutMove", "class_layout_move.html", null ],
    [ "Line", "class_line.html", null ],
    [ "MainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "MyData", "class_my_data.html", null ],
    [ "Node", "class_node.html", null ],
    [ "OptionFile", "class_option_file.html", null ],
    [ "PipelinePart", "class_pipeline_part.html", [
      [ "Clipper", "class_clipper.html", null ],
      [ "DataGrabber", "class_data_grabber.html", null ],
      [ "DataWallE", "class_data_wall_e.html", null ],
      [ "GeometrySimplifier", "class_geometry_simplifier.html", null ],
      [ "MotorwayTurnRemover", "class_motorway_turn_remover.html", null ],
      [ "PrintXML", "class_print_x_m_l.html", null ],
      [ "RampRemover", "class_ramp_remover.html", null ],
      [ "Renderer", "class_renderer.html", null ],
      [ "RoadExtender", "class_road_extender.html", null ],
      [ "RoadLayout", "class_road_layout.html", null ],
      [ "RoadMerger", "class_road_merger.html", null ],
      [ "RoadStumper", "class_road_stumper.html", null ],
      [ "RoundAboutKiller", "class_round_about_killer.html", null ],
      [ "RoundaboutTaker", "class_roundabout_taker.html", null ],
      [ "TraversableRoute", "class_traversable_route.html", null ],
      [ "VisibilityRingCreator", "class_visibility_ring_creator.html", null ]
    ] ],
    [ "PointInSpace", "struct_point_in_space.html", null ],
    [ "QDialog", null, [
      [ "ProgressDialog", "class_progress_dialog.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "OutputBroker", "class_output_broker.html", null ]
    ] ],
    [ "QThread", null, [
      [ "Pipeline", "class_pipeline.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "AdvancedSettings", "class_advanced_settings.html", null ],
      [ "TermWeightWindow", "class_term_weight_window.html", null ]
    ] ],
    [ "RatioHelper", "class_ratio_helper.html", null ],
    [ "VisibilityRingCreator::RingSamples", "struct_visibility_ring_creator_1_1_ring_samples.html", null ],
    [ "RoadLayoutData", "class_road_layout_data.html", null ],
    [ "RoundaboutHelper::Roundabout", "struct_roundabout_helper_1_1_roundabout.html", null ],
    [ "RoundaboutHelper", "class_roundabout_helper.html", null ],
    [ "Vec2", "class_vec2.html", null ],
    [ "XmlHelper", "class_xml_helper.html", null ]
];