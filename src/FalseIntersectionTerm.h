#pragma once
#include "costterm.h"
#include "FalseIntersectionHelper.h"

///CostTerm to prevent edges from intersecting where there is no intersection and to punish edges that lie too close to one another
/**	Makes sure edges do not intersect even though they should not
	and to keep at least a distance of 8mm inbetween edges
	(see: 4.3.2. #1)
*/
class FalseIntersectionTerm :
	public CostTerm
{
public:
	///Computes the real-life to map distance factor and sets the minimum distance between edges
	FalseIntersectionTerm(void);
	~FalseIntersectionTerm(void);

	//the stuff we inherited from our costTerm
	///Evaluates the Edge against all other edges in the tempData for possible intersections without any intersections, if that is not the case the closeness of all two edges are evaluated
	/**	Will automatically return DBL_MAX in case of a found false intersection
		Else it will return a value depending on whether the closest edge to e is closer than 8mm (on the output map)*/
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///The FalseIntersectionTerm has no postcost, will always return 0
	double PostCost();
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	double GetWeight(int suffix);
private:
	//@Deprecated
	//double GetShortestDistance(Edge * edge, Edge * otherEdge);
	///Returns the shortest distance between the nodes of the two edges
	/**	The returned distance will be in metres (real-world distance)*/
	double GetSimpleShortestDistance(Edge * edge, Edge * otherEdge);
	///The helping object to determine if intersections occur where they should not
	FalseIntersectionHelper help;
	///The distance that is deemed to be the minimal (map) distance between two edges (8mm)
	double controlDist;
	///The computed factor to convert the real-life distance (metres) to map distance (in mm)
	/**	Is computed once using the output width and height as well as the distance covered by the BoundingBox of the area of interest*/
	double rlToMapFactor;
	///Needed to allow exisiting false intersections to persist
	/**	This is needed if OptionFile::allowExistingFalseIntersections is true
		If not needed, this will remain unfilled	*/
	std::map<unsigned int, std::set<unsigned int>> knownFalseIntersections;
	///Needed to know whether the knownFalseIntersections map has already been initialised
	bool knownFalseSecsInit;
	///Method to analyse a MyData object for existing false intersections as well as building the eIDToNeighbours map to easily access neighbouring edges
	void InitKnownFalseInsecs(MyData * data);
	///Map saving which edge has which neighbours, slight runtime improvements, for we no longer compute the neighbours at each execution
	std::map<unsigned int, std::set<unsigned int>> eIDToNeighbours;
};

