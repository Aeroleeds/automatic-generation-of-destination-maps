#include "StdAfx.h"
#include "ProgressDialog.h"


ProgressDialog::ProgressDialog(QWidget * parent) : QDialog(parent)
{
	ui.setupUi(this);
	ui.ProgressLabel->setText("");
	ui.SubProgressLabel->setText("");
}


ProgressDialog::~ProgressDialog(void)
{
}



void ProgressDialog::SetMainText(QString text){
	ui.ProgressLabel->setText(text);
	//and clean the sub progress
	ui.SubProgressLabel->setText(QString());
}

void ProgressDialog::SetSubText(QString text){
	ui.SubProgressLabel->setText(text);
}

