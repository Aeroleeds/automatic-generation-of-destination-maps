#pragma once
#include "dataprovider.h"
#include <sstream>

///DataProvider for xapi-compatible servers that do not need a key
/**	Sends a xapi request to the given server w/out appending anything
	Make sure the server is usable w/xapi and up and running
	Which server is used can be determined via the SETTINGS.xml and the serverkey as set in the AdvancedSettings window*/
class BasicProvider :
	public DataProvider
{
public:
	BasicProvider(std::string settingsKey);
	virtual ~BasicProvider(void);
	///will grab osm data and the result into a tinyxml2 document for further usage
	/**	The key and value pair restrict the output to certain nodes and ways that fulfill the given criteria
		(e.g. the commonly used pair in this algorithm are of the "highway=xyz" type, to restrict the output to only motorways, primary roads, or similar)
		The BoundingBox limits the output to a certain area of interest, this area is the one being portayed in the output
		After grabbing the data it will be loaded as tinyxml2::XMlDocument and returned*/
	virtual tinyxml2::XMLDocument * GetData(std::string key, std::string value, BoundingBox bb);
	///Not yet implemented way of switching the data privider
	/**	the way of providing the data provider w/a server to reach for
		will return true if the server is up and running, false if we encountered an issue along the way*/
	virtual bool SetServer(std::string serverUrl);
	///Get a string representation of the data provider
	virtual std::string ToString();
	///Returns true if all the settings have been loaded correctly
	virtual bool SettingsLoaded();
private:
	///the url to be reached for the data
	std::string serverUrl;
	///Our object to gain a connection to our server and obtain all the necessary results
	cURLpp::Easy easy;
};

