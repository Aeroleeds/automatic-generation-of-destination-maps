#include "StdAfx.h"
#include "LayoutMove.h"
#include "Line.h"

LayoutMove::LayoutMove(void)
{
	srand(time(NULL));
}

LayoutMove::LayoutMove(unsigned int randomSeed){
	srand(randomSeed);
}

LayoutMove::~LayoutMove(void)
{
}


void LayoutMove::DoLayoutMove(RoadLayoutData * data){
	/*
		First, let's get all the necessary random values
	*/
	//get a random Node
	int nodeIndex = rand() % data->GetNodeList().size();
	Node * node = data->GetNodeList().at(nodeIndex);
	//fetch the directions for the line through our node, and create the line object
	double directionX, directionY;
	directionX = (double) rand() / RAND_MAX + 0.001;
	directionY = (double) rand() / RAND_MAX + 0.001;
	//and potentially make them negative
	int neg = rand() % 2;
	directionX = (neg == 0 ? directionX : - directionX);
	neg = rand() % 2;
	directionY = (neg == 0 ? directionY : - directionY);

	Line line = Line(node, directionX, directionY);
	//choose a side randomly (either -1 or 1, so we need some mapping)
	int side = rand() % 2;
	side = (side == 0 ? -1 : 1);
	//get our scale factor (from 0.95 to 1.05)
	double scaleFactor = ((double) (rand() % 11 + 95)) / 100;
	while(scaleFactor == 1){
		scaleFactor = ((double) (rand() % 11 + 95)) / 100;
	}
	//and get our method of scaling (true equals radial scaling, false orthogonal scaling)
	bool scalingType = (rand() % 2 == 1);
	//std::cout << node->GetNodeID() << " " << directionX << " " << directionY << std::endl << scaleFactor << std::endl << scalingType << std::endl;
	/*
		Then apply our move w/the defined settings
	*/
	std::vector<Node *> nodeList = data->GetNodeList();
	int size = nodeList.size();
	for(int i = 0; i < size; i++){
		//if the node is on the wrong side of our line, continue
		if(!line.IsOnSide(side, nodeList.at(i))){
			continue;
		}
		Node * temp = nodeList.at(i);
		//the scaling should be usable in mercator projection
		//TODO: make the scaling ways own classes for reusability
		if(scalingType){
			//get the original paths and multiply them with our factor
			double newDirectionX = (temp->GetLon() - node->GetLon()) * scaleFactor;
			double newDirectionY = (temp->GetLat() - node->GetLat()) * scaleFactor;
			temp->SetLon(node->GetLon() + newDirectionX);
			temp->SetLat(node->GetLat() + newDirectionY);
		}else{
			//we can use temp's nodeID here, for it will not be used for anything at all, but the construction of nodes (possibly make Nodes without IDs possible, not sure whether I want that though)
			//we scale the path the Node has in a right angle away from our line
			Node * intersection = line.IntersectionPoint(temp, temp->GetNodeID());
			double newDirectionX = (temp->GetLon() - intersection->GetLon()) * scaleFactor;
			double newDirectionY = (temp->GetLat() - intersection->GetLat()) * scaleFactor;
			temp->SetLon(intersection->GetLon() + newDirectionX);
			temp->SetLat(intersection->GetLat() + newDirectionY);
			delete intersection;
		}
		//SnapToBorder(temp);
	}
	//then recompute the distance values to be used by the cost function for each edge (for the distance has surely changed after moving all them nodes around

	std::vector<Edge *> elist = data->GetEdgeList();
	for(unsigned int i = 0; i < elist.size(); i++){
		elist.at(i)->RecomputeDistance();
	}
}

/*
void LayoutMove::SnapToBorder(Node * node){
	if(node->GetLat() < OptionFile::bb.negLat){
		node->SetLat(OptionFile::bb.negLat);
	}else if(node->GetLat() > OptionFile::bb.posLat){
		node->SetLat(OptionFile::bb.posLat);
	}
	if(node->GetLon() < OptionFile::bb.negLong){
		node->SetLon(OptionFile::bb.negLong);
	}else if(node->GetLon() > OptionFile::bb.posLong){
		node->SetLon(OptionFile::bb.posLong);
	}
}*/
