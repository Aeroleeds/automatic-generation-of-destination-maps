#pragma once
#include "pipelinepart.h"

///Optional PipelinePart to remove streets that are only part of the layout so one can turn on motorways
/**	When normal streets are needed in addition to possible motorway_links in order to change the direction
	then these streets are part of the layout, even though they do not contribute anything to potential drivers
	The MotorwayTurnRemover removes such streets
	This removes clutter and reduces the size of to be processed Edges and Nodes in the later steps*/
class MotorwayTurnRemover :
	public PipelinePart
{
public:
	MotorwayTurnRemover(void);
	~MotorwayTurnRemover(void);

	///Removes the streets that are not connected to the rest of the layout and are only there for the ability to turn on motorways
	/**	Checks the val > 2 nodes of motorways, if the edges going away from it do not connect to another part of the motorway
		and the sum of the distance of these streets is smaller than some threshold all concerning edges are removed*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();

private:
	///Checks for unneccessary edges going out from Node node
	/**	Will return a vector of Edge IDs if they are deemed unneccessary, else will return an empty vector*/
	std::vector<unsigned int> ComputeUnneccessaryEdges(Node * node, MyData * data);
	///Returns true if none of the edges at the node are motorway edges
	bool NoMotorwayEdges(Node * node, MyData * data);
	///
//	std::vector<Node *> GetNeighbouringNodes(Node * node, MyData * data);
	///Returns the next steps nodes going out from Node node that are on a motorway edge
	/**	These nodes are specially considered for the ComputeUnneccessaryEdges() method*/
	std::vector<Node *> GetMotorwayNodes(Node * node, unsigned int steps, MyData * data);
};

