#include "StdAfx.h"
#include "Vec2.h"

//PI definition according to cplusplus.com
#define PI 3.14159265

Vec2::Vec2(double lo, double la){
	double length = this->GetLength(lo, la);
	this->lon = lo / length;
	this->lat = la / length;}

Vec2::Vec2(Node * start, Node * end){
	this->lon = end->GetLon() - start->GetLon();
	this->lat = end->GetLat() - start->GetLat();
	double length = this->GetLength(lon, lat);
	lon = lon / length;
	lat = lat / length;
}

Vec2::~Vec2(void){

}

//returns the dot product of the two vectors
double Vec2::DotProduct(Vec2 otherVec){
	return this->lat * otherVec.lat + this->lon * otherVec.lon;
}

//returns the angle in degrees
double Vec2::GetAngle(Vec2 otherVec){
	//since acos(..) returns the angle in radians, we multiply it, to get degrees
	return  this->GetAngleRadians(otherVec) * 180.0 / PI;;
}

double Vec2::GetAngleRadians(Vec2 otherVec){
	double dot = this->DotProduct(otherVec);
	//through weird neuroscience the dotproduct sometimes return 1.00000000002, which will result in a bad outcome therefore the min (we possibly need to make sure we dont get -1.0000001)
	dot = dot > 1.0 ? 1.0 : dot;
	dot = dot < -1.0 ? -1.0 : dot;	
	return std::acos(dot);
	//return std::atan2(this->lat * otherVec.lat, this->lon * otherVec.lon);
}

double Vec2::GetLength(double lo, double la){
	return std::sqrt(lo * lo + la * la);
}