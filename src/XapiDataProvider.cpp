#include "StdAfx.h"
#include "XapiDataProvider.h"
#include <fstream>

XapiDataProvider::XapiDataProvider(std::string settingsKey) : DataProvider(settingsKey)
{
	this->easy = cURLpp::Easy();

	this->serverUrl = std::string();
	this->apiKey = std::string();
	/*
		setup the necessary stuff for the xapi data provider
	*/
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err;
	err = doc->LoadFile(std::string("SETTINGS.xml").c_str());
	if(err == 0){
		//the first child elem is the root node, so we need to look through the firstchild's firstchild's elems
		tinyxml2::XMLElement * elem = doc->RootElement()->FirstChildElement();
		while(elem != 0 && std::string(elem->Name()) != settingsKey){
			elem = elem->NextSiblingElement();
		}
		if(elem != 0){
			this->serverUrl = elem->Attribute("url");
			this->apiKey = elem->Attribute("key");
		}
	}

}


XapiDataProvider::~XapiDataProvider(void)
{
}

tinyxml2::XMLDocument * XapiDataProvider::GetData(std::string key, std::string value, BoundingBox bb){
	//first we build our request
	std::string request = "";
	request.append(this->serverUrl);
	request.append("way[").append(key).append("=").append(value).append("]");
	request.append(bb.toString());
	request.append("?key=").append(this->apiKey);
	//awesome view of hoertenford
	//easy.setOpt(cURLpp::Options::Url("http://open.mapquestapi.com/xapi/api/0.6/way[highway=motorway][bbox=14.325381861007395,46.6138032822627,14.46271096256441,46.666839639314205]"));
	//and get the information associated with our key value pair
	easy.setOpt(cURLpp::Options::Url(request));
	/*try{
		easy.perform();
	}catch(cURLpp::RuntimeError e){
		std::cout << "Exception with performing request.";
		return false;
	}*/
	//then write it into a string
	std::ostringstream oss;
	
	//std::ofstream os("buffer.xml", std::ofstream::out);
	curlpp::options::WriteStream ws(&oss);


	easy.setOpt(ws);
	easy.perform();
	//os.close();
	std::string result = oss.str();
	//that will then be parsed into an tinyxml document (which will later be parsed into a MyData object)
	tinyxml2::XMLDocument * xmlDoc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err = xmlDoc->Parse(result.c_str());
//	tinyxml2::XMLError err = xmlDoc->LoadFile("buffer.xml");
	if(err == tinyxml2::XML_SUCCESS){
		return xmlDoc;
	}else return nullptr;
}

//serverurl should end with .../0.6/
bool XapiDataProvider::SetServer(std::string serverUrl){
	//TODO: make check whether server is reachable
	this->serverUrl = serverUrl;
	return true;
}
std::string XapiDataProvider::ToString(){
	return std::string("Xapi Data Provider");
}

void XapiDataProvider::SetUp(){
	//TODO: make this go away
	//pretty damn hardcoded solution
	this->apiKey = std::string();
}

void XapiDataProvider::SetAPIKey(std::string key){
	this->apiKey = key;
}

bool XapiDataProvider::SettingsLoaded(){
	return apiKey != std::string() && serverUrl != std::string();
}
