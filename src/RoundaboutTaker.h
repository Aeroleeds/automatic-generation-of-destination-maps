#pragma once
#include "pipelinepart.h"

///Takes all Edges and Nodes of a Roundabout that has at least one taken Node
/**	If the Roundabout is not simply followed straight ahead, all leaving Edges of the Roundabout are taken as well
	In case of the Roundabout being traversed straight ahead, no context is taken*/
class RoundaboutTaker :
	public PipelinePart
{
public:
	RoundaboutTaker(void);
	~RoundaboutTaker(void);

	///Takes all edges and nodes leading to and from already taken roundabouts, unless the Roundabout is simply going straight forward, in which case no tails are kept
	/**	i.e. if a part of a roundabout is taken already, this simply takes ALL edges from inside the roundabout and stumps of edges leading out of the roundabout
		The exception being if the roundabout is only taken to stay on the same road, where no context information is taken in order to not clutter the map with tails
		If the OptionFile::useRoundaboutOptimiser is set to false, this will not do anything
		(i.e. PrepDoc can safely be called even when no special treatment is desired)*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();

private:
	///Set the Roundabout that the Node node is part of as taken
	/**	Automatically decides whether context is taken or not*/
	void TakeRoundabout(Node * node, MyData * data);
	//std::vector<Edge *> GetLeavingEdges(Edge * roundEdge, MyData * data);
	//Returns the next val != 2 Node when following the Edge edge from the start node
	//Node * GetNextValThree(Edge * edge, Node * start, MyData * data);
	//Returns the next Edge from Node node that is not the prevEdge and is either a roundabout Edge or not a roundabout Edge depending on the shouldBeRoundabout
	//Edge * GetNextEdge(Node * node, Edge * prevEdge, bool shouldBeRoundabout, MyData * data);
	///Returns the first val three node it encounters and takes all Edges along the way
	Node * TakeEdgesUntilValThree(Node * node, Edge * edge, MyData * data);
};

