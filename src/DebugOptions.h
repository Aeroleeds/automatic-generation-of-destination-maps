#pragma once
#include "stdafx.h"

///Provides us with the premade settings to be applied with the AdvancedSettings window
/**	A premade setting contains values for the BoundingBox, the destinations position (only lat/lon pair, not a Node) and the output height and width
*/
class DebugOptions
{
public:
	DebugOptions(void);
	virtual ~DebugOptions(void);
	///The enumerations used to distiniguish between the possible premade-settings
	enum DebugSettings{Parndorf, Bad_Brueckenau, Kiskunfelegyhaza, Oftringen, Chase, Dodoma, PleasantPoint, Paide, Belatai, Bollnas, StGeorge, Odessa, Princeton, Charleston, NoDebug, numDebugValues};
	///Contains string representations of all the enums described above	
	static std::string debugSettingsStrings[];
	///The value of the currently selected DebugSettings enum
	static DebugSettings chosenDebugSettings;
	///Sets all the values associated with the DebugSetting in the OptionFile
	/**	Set values are:
			BoundingBox: posLat, posLon, negLat, negLon
			Destination: Lat, Lon
			Output: width, height*/
	static void SetDebugSettings(DebugSettings set);

};

