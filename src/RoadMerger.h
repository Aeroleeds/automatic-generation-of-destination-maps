#pragma once
#include "pipelinepart.h"
/*
	Will merge parallel roads that are close to one another and share their name into a single edge
	this mostly helps w/reducing the cluster @ highways
	see: 4.2.1
*/
///Will merge parallel roads that are close to one another and share their name into a single Edge
/**	Reduces clutter at the motorways without losing information vital to navigating the data
	Only edges within 200metres of one another are considered for being merged*/
class RoadMerger : public PipelinePart
{
public:
	RoadMerger(void);
	~RoadMerger(void);
	/*
		Make sure the streetnames of the optionfile contain all the taken streets
	*/
	///Merges the parallel roads into a single one
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();

private:
	///Returns an Edge that is both parallel to the Edge e and shares the same name and StreetType as Edge e, or a nullptr if none exist
	Edge * FindNearbyParallelEdge(MyData * data, Edge * e, Node * n);
	///Returns true, if the edge contains exactly two non-link-road edges
	bool OnlyTwoRealEdges(Node * node, MyData * data);
	///Returns whether the Node n lies on the Edge e
	bool WithinBounds(Node * n, Edge * e);
	///will merge all the nodes in otherNodes into the firstNode (this includes re-linking of the edges to the new Node) this will DELETE all nodes in othernodes! (from the data set and the memory)
	/**	Additionally adjusts the position to reflect the positions in the otherNodes by averaging them*/
	Node * MergeAndDeleteOtherNodes(Node * firstNode, std::vector<Node *> otherNodes, MyData * data);
	//will merge the Edges between first and second Node along the street given by the streetname 
	//@Deprecated
	//void MergeEdges(Node * firstNode, Node * secondNode, Edge * edgeOne, Edge * edgeTwo, MyData * data);
	//finds the next merged node along the two edges and returns it; returns null if none is found
	//@Deprecated
	//Node * FindLinkedAndMergedNodes(Node * firstNode, Edge * edgeOne, Edge * edgeTwo, std::vector<Node *> mergedNodes, MyData * data);
	///will look for the next val != 2 node along the edge
	Node * GetNextNode(Node * start, Edge * edge, MyData * data);
	///Collapses the two Edges eOne and eTwo into a single Edge
	/**make sure the edge one belongs to node one, and edge two to node two*/
	void CollapseEdges(Node * one, Node * two, Edge * eOne, Edge * eTwo, MyData * data);
	///inserts the node n into the edge e, thereby splitting the edge e in two parts
	/** IMPORTANT NOTE: the edge passed as e WILL be deleted by this method, do not call the pointer again!*/
	void SplitEdge(Edge * e, Node * n, MyData * data);
	///Gets the distance traveled to the next val != 2 node
	unsigned int GetDistanceToNextNode(Node * start, Edge * toTravel, MyData * data);
	///a map to get all edges with the given name
	std::map<std::string, std::vector<Edge *>> nameToEdge;
};

