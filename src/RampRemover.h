#pragma once
#include "PipelinePart.h"
/// PipelinePart for removing ramps (mainly) leading from/to highways and replacing them with simple intersections
/** instead of the ramps a simple intersection is created (makes the map easier to read)
	for reference: the 3rd paragraph of 4.2.1.*/
class RampRemover :
	public PipelinePart
{
public:
	RampRemover(void);
	~RampRemover(void);
	///Removes all link roads in the MyData set and replaces them with a simple junction, in case the two connected streets intersect, or with a shortest connecting edge, created from the neighbours of the connected nodes, if that is not the case
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	//determines whether the street type associated with Edge e is a link road type
	/*More specifically, searches the corresponding string value of the streettype for "_link"
		returns true if such a part is found within the string, false else
		The string values used for comparison are stored in the stdafx header (streetStrings array)*/
	//bool IsLinkRoad(Edge * e);
	///Follows the specified edge away from the base node steps times and return all edges encountered along the way
	/**	Checks for the next edge of the given streetname and adds that one to the list, then checks for edges of the same name again and so forth
		The returned vector does not necessarily contain steps edges, for the road may end earlier*/
	std::vector<Edge *> FollowEdge(int steps, Edge * toFollow, Node * base, MyData * data);
	//DEPRECATED
	//void RemoveRampAndMergeStreets(Node * intersection, Edge * intersecOne, Edge * intersecTwo, std::pair<Node *, Node*> startAndEndNode, MyData * data);
	///Returns pairs of all nodes connected with a link road
	/**	Follows all link roads of the Mydata object in both directions and returns all found pairs of nodes that are connected via link_roads
		May contain duplicates*/
	std::vector<std::pair<Node *, Node *>> GetStartAndEndNodes(MyData * data);
	///Will insert the node in between the edges start and end nodes resulting in the original edge being replaced by the two newly constructed edges in the MyData object, returns the id of the edge to be deleted
	/**	Make sure the node is not already in place (we do not want multiple nodes at intersection points)
		The new edges will carry on the properties of the parameter edge (e.g. streetname, streettype, ...)
		The old edge will be deleted by this method, do not call it again
		If the node is not already part of the MyData object, then it will be added to it by this method*/
	unsigned int SplitEdge(Edge * edge, Node * node, MyData * data);
	///Checks whether we already inserted a node at the position where the param node lies, if so it returns true
	/**	Takes the absolute value of the difference between the nodes lat/lon values and all the AlreadyInsertedNodes lat/lon values
		if the diff is below a certain threshold, it is considered to be already inserted, and thus true is returned
		returns false if AlreadyInsertedNodes is empty (as intended)*/
	bool AlreadyConnected(Node * node, std::vector<Node *> AlreadyInsertedNodes);
	//bool SameCoordinates(Node * n, Node * m);
	///Returns false if one of the outgoing edges of the node is not a link edge
	/**A node without edges will thus return true*/
	bool OnlyLinkEdges(unsigned int nodeID, MyData * data);
	///Checks whether any of the edges starting in node connect to the otherNode
	bool AlreadyConnected(Node * node, Node * otherNode, MyData * data);
	//DEPRECATED
	//bool AlreadyConnected(Edge * edge, Edge * otherEdge);
	///Returns another Edge of the same type and w/the same name as the param edge that originates in the node, or a nullptr if none is found
	Edge * GetNextEdge(Edge * edge, Node * node, MyData * data);
	///Returns all Nodes along that given path, until the # of steps is too large
	/** Edge edge is the one that will be followed, and Node node is the node on the starting side of the edge (the edge will be followed along edge aways from node)
		The first next edge will contain edge->GetOtherNode(node) */
	std::vector<Node *> GetNextNodes(Edge * edge, Node * node, MyData * data, unsigned int steps);
	///Follows outgoing, non-link-road edges that contain the Node node and returns the Nodes encountered along the way
	std::vector<Node *> BuildNodeSet(Node * node, MyData * data);
	///Computes the shortest path between two nodesets created from the surrounding of the two Nodes in the pair and returns a corresponding edge
	/**	First calls the BuildNodeSet method on each Node to retain the surroundings, and then checks where the shortest edge is to be placed
		For this all combinations of neighbouring nodes are tried, and the shortest pair of these two nodesets is connected*/
	Edge * GetShortestConnectingEdge(std::pair<Node *, Node *> nodePair, MyData * data);
	///Deletes all Edge objects of the MyData object that are marked as link road (via the Edge->IsLinkRoad() method)
	void DeleteLinkEdges(MyData * data);
	///Deletes all Node objects of the MyData object that are marked as link roads (i.e. all nodes which only contain linkroads, or no roads whatsoever)
	void DeleteLinkNodes(MyData * data);
	///Returns whether the given Node lies on the Edge edge
	bool NodeOnEdge(Node * node, Edge * edge);

	//provides a unsigned int that is to be used as nodeID and is not already part of mydata
	/*	Will check for nodes already part of the mydata object, and continue changing the value until we reach a valid one
		We always increment the ID, but take some bigger steps, if the incrementing takes too long to find a usable ID
		the latest used ID is saved and used as starting point for the next query (in order to prevent multiple queries for the same potential IDs)*/
	//unsigned int GetUniqueNodeID(MyData * data);
	//There for performance improvement for the GetUniqueNodeID method
	//unsigned int latestID;
	//std::vector<Node *> newlyCreatedNodes;
	///Keeps track of which edges have already been split, saving the original edge's ID and the two new IDs
	std::map<unsigned int, std::vector<unsigned int>> edgeReplacements;

	
};

