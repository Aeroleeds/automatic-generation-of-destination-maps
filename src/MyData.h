#pragma once
#include <map>
#include <vector>
#include "Node.h"
#include "Edge.h"

///Class to represent the whole data of our system
/**	Contains Node and Edge objects to describe the osm data set
	Basically just holds the nodes and edges and provides some ease of access methods to get the desired parts without iterating over a vector of edges or nodes
	Additionally provides static methods for converting MyData objects to tinyxml documents and vice versa
	MyData objects are to be used for the majority of tasks operating on the whole data set (Exception: RoadLayoutData, for it needs not build the maps for easier access, thus performing better)*/
class MyData
{
public:
	MyData(void);
	MyData(const MyData &data);
	~MyData(void);
	MyData & operator=(const MyData & data);
	/*
		Basic lists containing all the Edges/Nodes, mainly for looping through 'em
		NodeList will not contain duplicates
	*/
	///returns a vector containing all the nodes from the data set in no particular order
	std::vector<Node *> GetNodeList();
	const std::vector<Node *> GetNodeList() const;
	///returns a vector containing all the edges from the data set in no specific order
	std::vector<Edge *> GetEdgeList();
	const std::vector<Edge *> GetEdgeList() const;
	/*
		Ways to get specific Edges/Nodes, either by StreetName or NodeID
		will return nullptrs if id is unavailable
		or empty vectors if name is not in the data
	*/
	///returns the node w/the specified ID
	/**	If no node with given ID is available in the data set, a nullptr is returned
		Note that node IDs are consistent with the data obtained from osm-data providers*/
	Node * GetNodeByID(unsigned int id);
	///returns the edge w/the specified ID
	/**	If no edge with given ID is available in the data set, a nullptr is returned
		Note that the edge ID is NOT equivalent to anything in the classic osm data, and will not be consistent in multiple iterations of executing the algorithm*/
	Edge * GetEdgeByID(unsigned int id);
	///returns a vector of all edges w/the specified streetname
	/**	If no edge w/the streetname is in the data set, it will return an empy vector*/
	//std::vector<Edge *> GetEdgesByStreetName(std::string name);
	///Returns a vector with all edges that go through the Node with the specified ID
	/* *	I.e. all edges that have the specified node as start or end node will be returned
		If the specified ID is not associated with any edges, an empty vector will be returned*/
	std::vector<Edge *> GetEdgesByNodeID(unsigned int id);
	///Will append all the data from the parameter data, to this MyData object
	/**used to add the data from another MyData object to this MyData object (necessary for compositing data from multiple xml files)
		will create deep copies of the edges and nodes (the appended data can safely be deleted)*/
	void Append(MyData * data);
	//ways to add an EdgeList (all containing Nodes will be added aswell, if they are not already in the MyData object), or a Nodelist 
	///Adds all the edges and their containing Nodes to the data set
	/**	Will however not add any duplicates*/
	void AddEdgeList(std::vector<Edge *> edgelist);
	///Adds all the nodes to the data set
	/**	Will not add any duplicates*/
	void AddNodeList(std::vector<Node *> nodelist);
	//methods for access to ringroads
	///gets a set of streetnames along which some edges are considered to be ring roads
	/**	If edges are tagged as ring roads the streetname will be added to this list*/
	//std::set<std::string> GetRingRoads();
	//const std::set<std::string> GetRingRoads() const;
	///simple method to save the streetname associated with an edge considered to be a ring road
	/**	Multiple additions of the same streetname will not matter, since duplicates are automatically handled*/
	//void AddRingRoad(std::string name);
	/*	methods to remove nodes/edges from the dataset
		Will delete them, the calling method needs to never use that pointer again
	*/
	///Removes and deletes the Edge e from the data set (and the heap)
	/**	Will not delete the start and end nodes of the edge
		Always call delete edge before calling delete node (the start and end nodes need to be valid when calling this destructor)
		Will reduce the valence of the contained nodes by one*/
	void RemoveAndDeleteEdge(Edge * e);
	///deletes the node n from the data set and the heap
	/**	Make sure to have already deleted all edges containing this node beforehand*/
	void RemoveAndDeleteNode(Node * n);
	///Removes and deletes all nodes and edges in this MyData object
	void RemoveAndDeleteAll();
	/*
		Convert tinyxml documents to MyData objects
		Will not save them in the current MyData obj, if desired make an additional call to Append
	*/
	///Transforms the xmldocument into a mydata object
	/**	This is not lossless, only information needed for this algorithm are extracted*/
	static MyData * XMLToMyData(tinyxml2::XMLDocument * doc);
	///Transforms the xmldocument into a mydata object, and will assign the default speed to all edges without a maxspeed value
	static MyData * XMLToMyData(tinyxml2::XMLDocument * doc, unsigned int defaultSpeed);
	///will create a xmldocument from a mydata object
	static tinyxml2::XMLDocument * MyDataToXML(MyData * data);
	///maps string values to their corresponding StreetType enums
	static std::map<std::string, StreetType> stringToStreetType;
	///provides a unsigned int that is to be used as nodeID and is not already part of mydata
	/**	Will check for nodes already part of the mydata object, and continue changing the value until we reach a valid one
		We always increment the ID, but take some bigger steps, if the incrementing takes too long to find a usable ID
		the latest used ID is saved and used as starting point for the next query (in order to prevent multiple queries for the same potential IDs)*/
	unsigned int GetUniqueNodeID();

private:
	///The list of Node objects in the data set
	std::vector<Node *> nodeList;
	///The list of Edge objects in the data set
	std::vector<Edge *> edgeList;
	///A map to obtain a Node by its ID
	std::map<unsigned int, Node *> idToNode;
	///Map to obtain an Edge by its ID
	std::map<unsigned int, Edge *> idToEdge;
	///Map to obtain ALL edges starting or ending in the node with the specified ID
	std::map<unsigned int, std::vector<Edge *>> nodeIdToEdge;
	///Map from streetnames to all Edge objects with the given streetname
	//std::map<std::string, std::vector<Edge *>> nameToEdge;
	//Map from streetnames to EdgeIDs
	//std::map<std::string, std::vector<unsigned int>> nameToEdgeID;
	///A set of all streetnames that have some edges that are considered as ring roads
	//std::set<std::string> ringRoads;

	///There for performance improvement for the GetUniqueNodeID method
	unsigned int latestID;

	//some basic helper methods
	///adds the Edge e to parts of the MyData set (NEEDS be called when adding an edge)
	/**	Any Edges to be added to the data set need to call this method
		Will additionally increase the valence of the nodes contained in the edge by one*/
	void AddNodeIDToEdge(Node * n, Edge * e);
	///adds the Node n to the MyData set (NEEDS be called when adding a Node)
	/**	Any Nodes to be added to the data set need to call this method
		If a Node w/the same ID as n is already in the data set, nothing will be saved*/
	void AddIDToNode(Node * n);

	///simple initialiser for the stringToStreetType map converting strings to streettypes
	static std::map<std::string, StreetType> initialiseMap(){
		std::map<std::string, StreetType> temp;
		temp.insert(std::pair<std::string, StreetType>("motorway", motorway));
		temp.insert(std::pair<std::string, StreetType>("motorway_link", motorway_link));
		temp.insert(std::pair<std::string, StreetType>("trunk", trunk));
		temp.insert(std::pair<std::string, StreetType>("trunk_link", trunk_link));
		temp.insert(std::pair<std::string, StreetType>("primary", primary));
		temp.insert(std::pair<std::string, StreetType>("primary_link", primary_link));
		temp.insert(std::pair<std::string, StreetType>("secondary", secondary));
		temp.insert(std::pair<std::string, StreetType>("secondary_link", secondary_link));
		temp.insert(std::pair<std::string, StreetType>("tertiary", tertiary));
		temp.insert(std::pair<std::string, StreetType>("tertiary_link", tertiary_link));
		temp.insert(std::pair<std::string, StreetType>("unclassified", unclassified));
		temp.insert(std::pair<std::string, StreetType>("residential", residential));
		temp.insert(std::pair<std::string, StreetType>("service", service));
		return temp;
	}
};

