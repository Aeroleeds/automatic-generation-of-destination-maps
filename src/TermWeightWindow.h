#ifndef TERMWEIGHTWINDOW_H
#define TERMWEIGHTWINDOW_H

#pragma once
#include <qwidget.h>
#include "ui_TermWeightWindow.h"
#include "stdafx.h"

///Window to adjust the weights associated with the terms during the roadlayout stage
/**	Allows changes to both weights of all CostTerms */
class TermWeightWindow : public QWidget
{
	Q_OBJECT

public:
	TermWeightWindow(void);
	~TermWeightWindow(void);

	///Opens up the TermWeight window
	void ShowSettings();
	///Hides the TermWeight widow (w/out saving any possible changes)
	void HideSettings();

private slots:
	///Saves the changes made in the TermWeight window into the appropriate OptionFile containers
	/**	In case a field is filled with invalid input, no changes apply (not even correctly input ones)
		Only when all input fields are valid, all input is saved
		Input is invalid if
			One of the digits is negative
			One of the fields contains a character that is not a number*/
	void ApplySettings();

private:
	///The ui associated with the TermWeight window
	Ui::TermWeightWindow ui;
	///Validator to make sure double values are passed on to the line edit
	QDoubleValidator valid;
};

#endif //TERMWEIGHTWINDOW_H

