#pragma once
#include "StdAfx.h"
#include <mapnik/map.hpp>
#include <mapnik/layer.hpp>
#include <mapnik/rule.hpp>
#include <mapnik/feature_type_style.hpp>
#include <mapnik/graphics.hpp>
#include <mapnik/datasource_cache.hpp>
#include <mapnik/font_engine_freetype.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik/expression.hpp>
#include <mapnik/color_factory.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/config_error.hpp>

#include "Renderer.h"
#include "mapnik\datasource_cache.hpp"
#include "mapnik\font_engine_freetype.hpp"
//#include "mapnik\cairo_renderer.hpp"
#include <mapnik/map.hpp>
#include <mapnik/load_map.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik\image_data.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/cairo_renderer.hpp>
#include <cairomm/surface.h>
//#include "cairomm\cairomm.h"
//#include "cairo.h"
//#include "cairomm\context.h"
//#include "cairomm\refptr.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
//probably dupes below this comment
/*
	Necessary for testing w/the agg_renderer


#include <mapnik/map.hpp>
#include <mapnik/datasource_cache.hpp>
#include <mapnik/font_engine_freetype.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik/expression.hpp>
#include <mapnik/color_factory.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/config_error.hpp>
#include <iostream>
*/
Renderer::Renderer(void)
{

	//this->m = mapnik::Map(OptionFile::outputWidth, OptionFile::outputHeight);

//	this->m = mapnik::Map(600, 300);
	//this->m.background(mapnik::color("steelblue")); 
	//mapnik::agg_renderer<mapnik::image_32> ren;
	
	//mapnik::Map m(256,256);
	/*mapnik::load_map(m, "dfdfp");
	m.zoom_all();
	mapnik::image_rgba8 im(256,256);
	mapnik::agg_renderer<mapnik::image_rgba8> ren(m, im);
	ren.apply();
	mapnik::save_to_file(im, "the_image.png");
	*/
	this->resHeight = 0;
	this->resWidth = 0;
	this->stylePath = "";

	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err;
	err = doc->LoadFile(std::string("SETTINGS.xml").c_str());
	if(err == 0){
		//the first child elem is the root node, so we need to look through the firstchild's firstchild's elems
		tinyxml2::XMLElement * elem = doc->RootElement()->FirstChildElement();
		while(elem != 0 && std::string(elem->Name()) != "mapnik"){
			std::string name = elem->Name();
			elem = elem->NextSiblingElement();
			name = elem->Name();
		}
		if(elem != nullptr){
			//load the font paths
			tinyxml2::XMLElement * child = elem->FirstChildElement();
			while(child != 0 && std::string(child->Name()) != "fontpaths"){
				child = child->NextSiblingElement();
			}
			//if we found a fontpaths tag
			if(child != 0){
				tinyxml2::XMLElement * font = child->FirstChildElement();
				while(font != 0 && std::string(font->Name()) == "font"){
					this->fontPaths.push_back(font->Attribute("path"));
					font = font->NextSiblingElement();
				}
			}
			//then go looking for the resolution
			child = elem->FirstChildElement();
			while(child != 0 && std::string(child->Name()) != "resolution"){
				child = child->NextSiblingElement();
			}
			//if we found a child tag, set width and height accordingly
			if(child != 0){
				resHeight = child->IntAttribute("height");
				resWidth = child->IntAttribute("width");
			}
			//then go looking for the style path
			child = elem->FirstChildElement();
			while(child != 0 && std::string(child->Name()) != "styleSheet"){
				child = child->NextSiblingElement();
			}
			if(child != 0){
				this->stylePath = child->Attribute("path");
			}
			//then set the plugins paths
			child = elem->FirstChildElement();
			while(child != 0 && std::string(child->Name()) != "plugins"){
				child = child->NextSiblingElement();
			}
			//if we found a fontpaths tag
			if(child != 0){
				tinyxml2::XMLElement * plug = child->FirstChildElement();
				//take all the specified plugs and add them
				while(plug != 0){
					this->pluginPaths.push_back(plug->Attribute("path"));
					plug = plug->NextSiblingElement();
				}
			}
		}
	}


}


Renderer::~Renderer(void)
{
}

std::string Renderer::RenderAndSaveImage(tinyxml2::XMLDocument * xmlDoc){
	//we have to first print the given xml to the RenderMe file
	xmlDoc->SaveFile("RenderMe.osm");
	delete xmlDoc;
	int tempWidth = (this->resWidth == 0 ? 1080 : this->resWidth);
	int tempHeight = (this->resHeight == 0 ? 1920 : this->resHeight);
	try{
		mapnik::Map m(tempWidth, tempHeight);
		//register fonts and plugins
		for(unsigned int i = 0; i < this->fontPaths.size(); i++){
				mapnik::freetype_engine::register_fonts(fontPaths.at(i), false);
		}
		for(unsigned int i = 0; i < this->pluginPaths.size(); i++){
				mapnik::datasource_cache::instance()->register_datasource(this->pluginPaths.at(i));
		}
	/*	mapnik::datasource_cache::instance()->register_datasource("C:\\Users\\Aeroleeds\\Documents\\Visual Studio 2010\\Projects\\GUI_Bac\\GUI_Bac\\bin\\input-Release\\osm.input");
#ifdef (_DEBUG)
		mapnik::datasource_cache::instance()->register_datasource("C:\\Program Files\\Mapnik\\mapnik-v2.1.0-vc100-Win32\\bin\\input-Debug\\osm.input");
#endif

#ifdef (_NDEBUG)
		mapnik::datasource_cache::instance()->register_datasource("C:\\Program Files\\Mapnik\\mapnik-v2.1.0-vc100-Win32\\bin\\input-Release\\osm.input");
#endif
		*/
		/*
		std::string mapnik_dir;
		std::string plugins_dir(mapnik_dir + "/plugins/input/");
		mapnik::datasource_cache::instance()->register_datasources(plugins_dir + "shape");
		*/

		std::vector<std::string> names = mapnik::datasource_cache::plugin_names();
		mapnik::load_map(m, stylePath);
		m.zoom_all();
		/*
			//Testing w/the agg renderer
		mapnik::image_32 buf(tempWidth,tempHeight);
		mapnik::agg_renderer<mapnik::image_32> ren(m,buf);
        ren.apply();

		mapnik::save_to_file<mapnik::image_data_32>(buf.data(),"destinationMap.png","png");
		*/

		//the cairo renderer
		Cairo::RefPtr<Cairo::Surface> surface = Cairo::SvgSurface::create("DestinationMap.svg", m.width(),m.height());
        mapnik::cairo_renderer<Cairo::Surface> svg_render(m, surface);
        svg_render.apply();
		

	}catch(std::exception & ex){
		std::string error = ex.what();
		return std::string("Error during rendering: ").append(error).append(". Check SETTINGS.xml?");
	}
	return std::string("Splendid");
}

std::string Renderer::PrepDoc(MyData * data){
	return this->RenderAndSaveImage(MyData::MyDataToXML(data));
}
std::string Renderer::ToString(){
	return std::string("Renderer");
}

void Renderer::SetUp(){

}
