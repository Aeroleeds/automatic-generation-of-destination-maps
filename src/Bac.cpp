// bac.cpp : Defines the entry point for the console application.
//
/*
#pragma once
#include "stdafx.h"
#include "Pipeline.h"
#include "OptionFile.h"
#include <sstream>

#include <cstdlib>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include <QApplication>
#include <QtWidgets\qapplication.h>

#include "mainwindow.h"

#include "Node.h"
#include "Edge.h"
#include "Dijkstra.h"
#include "MyData.h"

#include "stdafx.h"

#include "RoadLayoutData.h"
#include "FalseIntersectionTerm.h"
#include "RoadMerger.h"
#include "RampRemover.h"
#include "RoadExtender.h"
#include "TraversableRoute.h"
#include "GeometrySimplifier.h"
#include "LayoutMove.h"
#include "RelativeLengthTerm.h"
#include "FalseIntersectionTerm.h"
#include "DataGrabber.h"
#include "XmlHelper.h"
#include "PrintXML.h"
#include "DebugOptions.h"
#include "VisibilityRingCreator.h"
#include "RoadStumper.h"
#include "DataWallE.h"
#include "Vec2.h"
#include "MotorwayTurnRemover.h"
#include "LinkRoadTaker.h"
#include "Renderer.h"
#include "RoadLayout.h"
#include "Clipper.h"
#include "RatioHelper.h"
#include "Vec2.h"
/*#include "XmlHelper.h"
#include "NetworkHelper.h"



int _tmain(int argc, _TCHAR* argv[])
{
	cURLpp::initialize();
	
	XmlHelper helper;
	tinyxml2::XMLDocument * doc = helper.ReadFile("data\\example.xml");
	NetworkHelper netHelper;
	//netHelper.FetchAndSaveResponse("test", "test");
	std::cout << "It worked!" << std::endl;
	system("pause");
	/*
				CLEANUP
	*
	cURLpp::terminate();

	delete doc;
	return 0;
}*

int main(int argc, char *argv[])
{

  char *url = "http://open.mapquestapi.com/xapi/api/0.6/node%5Bamenity=pub%5D%5Bbbox=-77.03299606884731,38.88960821016589,-77.01582993115268,38.8971235910255%5D";
  
  try {
    curlpp::Cleanup cleaner;
    curlpp::Easy request;

    // Setting the URL to retrive.
    request.setOpt(new curlpp::options::Url(url));

    std::cout << request << std::endl;

    // Even easier version. It does the same thing 
    // but if you need to download only an url,
    // this is the easiest way to do it.
    std::cout << curlpp::options::Url(url) << std::endl;
	system("PAUSE");
    return EXIT_SUCCESS;
  }
  catch ( curlpp::LogicError & e ) {
    std::cout << e.what() << std::endl;
  }
  catch ( curlpp::RuntimeError & e ) {
    std::cout << e.what() << std::endl;
  }

  return EXIT_FAILURE;
}
*/
/*
int main(int argc, char *argv[])
{

	QApplication a(argc, argv);
	//GUI_Bac w;
	MainWindow w;
	w.show();
	return a.exec();



	OptionFile::serverKey = "overpass";
	//OptionFile::loadFile = "NewDir\\xapi_nodes.xml";
	OptionFile::saveAs = "gyoer";
	OptionFile::pipeSet = OptionFile::All;

	Pipeline pipe;
	pipe.SetUp();
	pipe.DoWork();
	return 0;
	

	

	//HUGE BREAK, DELETE HERE AND ALL THE WAY AT THE BOTTOM

	PrintXML help;
	//QApplication d(argc, argv);
	/*

	THIS IS THE MAIN LOOP; REMOVE THE COMMENTS!!!


	Pipeline * pipe = new Pipeline();
	pipe->SetUp();
	if(pipe->DoWork()){
		std::cout << "everything worked!" << std::endl;
		std::cin.get();
		return EXIT_SUCCESS;
	}else{
		std::cout << "it did not work...." << std::endl;
		std::cin.get();
		return EXIT_FAILURE;
	}*/

	/**TESTING ANGLES*
	Node * one = new Node(3, 3, 0);
	Node * otherOne = new Node(13.0000000001, 23.000000000001, 0);
	Node * two = new Node(3.2, 3.2, 0);
	Node * three = new Node(12.8, 23.15, 0);
	Node * four = new Node(3, 2.7, 0);
	Node * five = new Node(2.801, 2.881, 0);

	Vec2 vOne(one, two);
	Vec2 vTwo(otherOne, three);
	Vec2 vThree(one, four);
	Vec2 vFour(otherOne, five);

	double angleOne = vOne.GetAngle(vTwo);
	double angleOtherOne = vTwo.GetAngle(vOne);
	double angleTwo = vOne.GetAngle(vThree);
	double angleThree = vOne.GetAngle(vFour);

	/**TESTING ANGLES ---------- END*/
	
	


	//DELETE HERE

/*
	MyData * data = new MyData();

	DebugOptions db;
	db.PrepDoc(data);

	OptionFile::mergeOnNameChange = true;
	//OptionFile::destinationNode = *data->GetNodeByID(334833560);

	RatioHelper ratione;
	OptionFile::bb = ratione.ReconfigureBB(OptionFile::bb, OptionFile::outputWidth, OptionFile::outputHeight);
	
	
	/*

	working pipeline parts from here on (in correct order)
	*/
	/*	DELETE HERE


	DataGrabber grabby;
	grabby.SetUp();
	grabby.PrepDoc(data);
	

	help.SetName("Gmunden");
	help.SetUp();
	//help.PrepDoc(data);
	

//	return 0;

	VisibilityRingCreator create;
	create.SetUp();
	create.PrepDoc(data);

	TraversableRoute travis;
	travis.SetUp();
	travis.PrepDoc(data);
	
	RoadExtender exty;
	exty.SetUp();
	exty.PrepDoc(data);

	//TODO: verify this is where we want the takey

	LinkRoadTaker takey;
	takey.SetUp();
	takey.PrepDoc(data);

	DataWallE ee;
	ee.SetUp();
	ee.PrepDoc(data);
	
	//help.SetName("Debug_italia");
	//help.SetName("TakenStreets");
	help.SetUp();
	help.PrepDoc(data);

	RoadStumper stumpy;
	stumpy.SetUp();
	stumpy.PrepDoc(data);

	RoadMerger mergy;
	mergy.SetUp();
	mergy.PrepDoc(data);

	RampRemover rampy;
	rampy.SetUp();
	rampy.PrepDoc(data);

	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		data->GetEdgeList().at(i)->SetOneWay(false);
	}

	MotorwayTurnRemover moty;
	moty.SetUp();
	moty.PrepDoc(data);

	GeometrySimplifier simply;
	simply.SetUp();
	simply.PrepDoc(data);

	//PrintXML help;
	//help.SetName("Italia_Simplified");
	help.SetUp();
	help.PrepDoc(data);

	Clipper clippy;
	clippy.SetUp();
	clippy.PrepDoc(data);
	
	//help.SetName("Italia_clipped");
	help.SetUp();
	help.PrepDoc(data);
	*/
/*
	RoadLayout layi;
	layi.SetUp();
	layi.PrepDoc(data);
	
	//help.SetName("Italia_Layout");
	help.SetName("THisShouldNotWork");
	help.SetUp();
	help.PrepDoc(data);
	

//	Renderer rend;

	//XmlHelper helper;
	//helper.SaveFile(MyData::MyDataToXML(*data), "test");

	
}