#pragma once
#include "StdAfx.h"
#include "RatioHelper.h"
#include "Edge.h"
#include "OptionFile.h"

double RatioHelper::rlToMapFactor = 0.0;

RatioHelper::RatioHelper(void)
{
}


RatioHelper::~RatioHelper(void)
{
}

bool RatioHelper::EqualEnough(double one, double two){
	//TODO: configure threshold to sth working as intended
	return std::abs(one - two) < 0.01;
}
/*
BoundingBox RatioHelper::ReconfigureBB(BoundingBox orig, int outputWidth, int outputHeight){
	double origRatio = 0;
	if(outputWidth > outputHeight){
		origRatio = (double) outputWidth / (double) outputHeight;
	}else{
		origRatio = (double) outputHeight / (double) outputWidth;
	}
	//then get the current ratio of orig BB
	Node * topLeft = new Node(orig.negLong, orig.posLat, 0);
	Node * topRight = new Node(orig.posLong, orig.posLat, 0);
	Node * botLeft = new Node(orig.negLong, orig.negLat, 0);

	Edge * wide = new Edge(topLeft, topRight, 12, "measureWide");
	Edge * high = new Edge(botLeft, topLeft, 12, "measureHigh");

	double width = wide->GetDistance();
	double height = high->GetDistance();

	double newRatio = 0;
	if(width > height){
		newRatio = width / height;
	}else{
		newRatio = height / width;
	}
	//now compare the ratios and adjust the bb accordingly
	//one of the nodes is to be moved, depending on which side is to be changed, which in turn is determined by checking whether the newRatio is bigger or smaller than the origRatio
	while(!EqualEnough(newRatio, origRatio)){
		Node * toMove = nullptr;
		Edge * changed = nullptr;
		if(newRatio > origRatio){
			toMove = width > height ? botLeft : topRight;
			changed = width > height ? high : wide;
		}else{
			toMove = width > height ? topRight : botLeft;
			changed = width > height ? wide : high;
		}
		if(EqualEnough(toMove->GetLat(), changed->GetOtherNode(toMove)->GetLat())){
			if(toMove->GetLon() < changed->GetOtherNode(toMove)->GetLon()){
				toMove->SetLon(toMove->GetLon() - 0.00001);
			}else toMove->SetLon(toMove->GetLon() + 0.00001);
		}else{
			if(toMove->GetLat() < changed->GetOtherNode(toMove)->GetLat()){
				toMove->SetLat(toMove->GetLat() - 0.00001);
			}else toMove->SetLat(toMove->GetLat() + 0.00001);
		}
		changed->RecomputeDistance();
		//and recompute the ratios
		width = wide->GetDistance();
		height = high->GetDistance();
		if(width > height){
			newRatio = width / height;
		}else{
			newRatio = height / width;
		}
	}
	BoundingBox bb(topLeft->GetLat(), botLeft->GetLat(), topRight->GetLon(), topLeft->GetLon());
	//cleanup
	delete wide;
	delete high;
	delete topLeft;
	delete topRight;
	delete botLeft;

	//compute the rlToMapfactor
	double longerOutput = outputWidth > outputHeight ? outputWidth : outputHeight;
	//longerMap equals the real life distance
	double longerMap = width > height ? width : height;
	
	rlToMapFactor = longerOutput / longerMap ;

	return bb;
}*/

void RatioHelper::ComputeRLFactor(){
	//compute the rlToMapfactor
	double longerOutput = OptionFile::outputWidth > OptionFile::outputHeight ? OptionFile::outputWidth : OptionFile::outputHeight;
	//Get the RL values
	Node * topLeft = new Node(OptionFile::bb.negLong, OptionFile::bb.posLat, 0);
	Node * topRight = new Node(OptionFile::bb.posLong, OptionFile::bb.posLat, 0);
	Node * botLeft = new Node(OptionFile::bb.negLong, OptionFile::bb.negLat, 0);

	Edge * wide = new Edge(topLeft, topRight, 12, "measureWide");
	Edge * high = new Edge(botLeft, topLeft, 12, "measureHigh");

	double width = wide->GetDistance();
	double height = high->GetDistance();
	//longerMap equals the real life distance
	double longerMap = width > height ? width : height;
	rlToMapFactor = longerOutput / longerMap;

	delete wide;
	delete high;
	delete topLeft;
	delete topRight;
	delete botLeft;
}

double RatioHelper::GetRLtoMapFactor(){
	return rlToMapFactor;
}
