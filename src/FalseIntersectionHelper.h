#pragma once
#include "Edge.h"
/// Helper class for determining whether a false intersection occurs between two Edge objects
/**	Additionally provides a function for returning the shortest distance between two Edges (that will be 0 if a false intersection occurs)
	this distance additionally carries information about whether the distance is to the left or right of the first edge (by the sign of the return value)
	used in the FalseIntersectionTerm, as well as in GeometrySimplification Step
	Additionally provides a way of getting the shortest absolute distance between two edges (thereby losing the information whether our edge is to the left or right of the other edge)
*/
class FalseIntersectionHelper
{
public:
	FalseIntersectionHelper(void);
	~FalseIntersectionHelper(void);
	//TODO: possibly need to make an exception for parallel edges
	///will return true if the two edges intersect 
	/** the bool ignoreNeighbours is there to ignore intersections that happen if the two Edges share a Node
		This behaviour is by default set to true and should probably not be false anyway
	*/
	bool IsFalseIntersection(Edge * edge, Edge * otherEdge, bool ignoreNeighbours = true);
	///	returns the shortest directional distance from edge to otherEdge (this is NOT a commutative operation!)
	/**	will NOT check for false intersections so make sure to call IsFalseIntersection(..) first
		this is not commutative (i.e.: GetDistance(one, two) will (in most cases) not equal GetDistance(two, one)
		the distance is not an absolute value, but contains a sign depending on whether otherEdge is to the left or right of edge*/
	double GetDistance(Edge * edge, Edge * otherEdge);
	///Like GetDistance(..), but commutative and returns the absolute value of the shortest distance (i.e. of GetDistance(one, two) and GetDistance(two, one))
	/** help.GetDistance() is computed in a way to check for the smallest distance by comparing the first edge w/the two nodes from the other edge
		(this is done by taking the normal of our first edge, computing the intersection point from our edge and a line through one of the end nodes and the intersection point,
		and then simply computing the distance between the intersection point and our node)
		if we want to get the absolute shortest distance, we will need to do it the other way around as well which is what absolute shortest distance does
		Note that this will not return any information regarding the side where of the edge where the other edge lies (left or right)
	*/
	double GetAbsoluteShortestDistance(Edge * edge, Edge * otherEdge);
	///is a simple little method constructing bounding boxes for the edges for quick and easy dismissal of possible intersections
	/**a true return value does not say anything about them intersecting, this is just for dismissing intesections fast*/
	bool WithinBounds(Edge * edge, Edge * otherEdge);
};

