#include "StdAfx.h"
#include "RoadStumper.h"


RoadStumper::RoadStumper(void)
{
}


RoadStumper::~RoadStumper(void)
{
}

std::string RoadStumper::PrepDoc(MyData * data){
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * node = data->GetNodeList().at(i);
		if(node->GetValence() != 1){
			continue;
		}else if(data->GetEdgesByNodeID(node->GetNodeID()).at(0)->IsRingRoad()){
			continue;
		}else if(data->GetEdgesByNodeID(node->GetNodeID()).at(0)->GetStreetType() == motorway){
			continue;
			//all link roads are dealt with in the RampRemover, leave them alone here
		}else if(data->GetEdgesByNodeID(node->GetNodeID()).at(0)->IsLinkRoad()){
			continue;
		//if the node is at the border, do not remove it!
		}else if(!OptionFile::bb.IsInside(node)){
			continue;
			//TODO: possibly prevent removal from nodes close to the border as well
		}else{
			Edge * edge = data->GetEdgesByNodeID(node->GetNodeID()).at(0);
			while(node->GetValence() == 1){
				unsigned int eID = edge->GetEdgeID();
				unsigned int nID = node->GetNodeID();
				node = edge->GetOtherNode(node);
				if(node->GetValence() > 2){
					break;
				}
				data->RemoveAndDeleteEdge(data->GetEdgeByID(eID));
				data->RemoveAndDeleteNode(data->GetNodeByID(nID));
				//if we somehow end at a val 0 node, delete it and check another edge
				if(node->GetValence() == 0){
					data->RemoveAndDeleteNode(data->GetNodeByID(node->GetNodeID()));
					break;
				}
				edge = data->GetEdgesByNodeID(node->GetNodeID()).at(0);
				if(edge->IsRingRoad()){
					break;
				}
			}
		}
	}
	return std::string("Splendid");
}
std::string RoadStumper::ToString(){
	return std::string("RoadStumper");
}



void RoadStumper::SetUp(){

}


