#pragma once
#include "Node.h"
#include "Edge.h"

///The class containing all the data for the layouting process (kind of a lightweight MyData class)
/**	Is a class similar to the MyData class, albeit w/out all the comfort functions
	contains an edge and a node list
	Is used for during the process of optimizing the layout, we create many variations of our original data
	and constructing all the maps for data we might not take is deemed overhead, so this is just a barebones version
	Can create deep copies of itself
	(used in 4.3.2)
*/
class RoadLayoutData
{
public:
	///will create a RoadLayoutData object containing deep copies of all the nodes and edges
	/**	Do not forget to delete the RoadLayoutData for it contains deep copies of all the nodes and edges passed along to it
		Additionally the RoadLayoutData has no way of adding nodes or edges to it other than pass them on to the constructor*/
	RoadLayoutData(std::vector<Node *> nodes, std::vector<Edge *> edges);
	~RoadLayoutData(void);
	///returns the nodelist saved by the RoadLayoutData
	std::vector<Node *> GetNodeList();
	///returns the edge list saved by the RoadLayoutData
	std::vector<Edge *> GetEdgeList();
	///returns a pointer to a deepcopy of the current layoutdata
	/**	Once again, do not forget to delete any copies of a RoadLayoutData object*/
	RoadLayoutData * GetDeepCopy();
	///Method to be called in order to clean up the roadlayoutdata
	void DeleteAll();


private:
	std::vector<Node *> nodeList;
	std::vector<Edge *> edgeList;
};

