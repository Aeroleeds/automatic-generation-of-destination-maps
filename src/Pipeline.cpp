#pragma once
#include "StdAfx.h"
#include "Pipeline.h"

Pipeline::Pipeline(void)
{
	this->pipeParts = std::vector<PipelinePart *>();
	this->output = nullptr;
	//printXML(this);
}


Pipeline::~Pipeline(void)
{
	delete this->output;
	delete this->data;
}

bool Pipeline::SetUp(){
	//PrintXML printXML;
	printXML.SetName(OptionFile::saveAs);
	OptionFile::updateMidLon();

	//always load some sorta data
	pipeParts.push_back(&dataGrabber);

	switch(OptionFile::pipeSet){
		case OptionFile::All :
			pipeParts.push_back(&printXML);
			pipeParts.push_back(&visRing);
			pipeParts.push_back(&traversRout);
			pipeParts.push_back(&roadTender);
			pipeParts.push_back(&roundTaky);
			pipeParts.push_back(&wallEE);
			pipeParts.push_back(&printXML);
		case OptionFile::Selected :
			//add the roudnaboutkiller here, hope thats where he belongs
			pipeParts.push_back(&kill);
			pipeParts.push_back(&roadStum);
			pipeParts.push_back(&roadMer);
			//TODO: remove this again
			//pipeParts.push_back(&printXML);
			pipeParts.push_back(&ramRem);
			pipeParts.push_back(&moTuRe);
			pipeParts.push_back(&printXML);
			pipeParts.push_back(&geoSim);
			pipeParts.push_back(&printXML);
			pipeParts.push_back(&clip);
			pipeParts.push_back(&printXML);
		case OptionFile::PreLayout :
			pipeParts.push_back(&roadLa);
			pipeParts.push_back(&printXML);
			break;
		case OptionFile::OnlyRender:
			pipeParts.push_back(&lux);
			break;
		//here come special cases
		case OptionFile::OnlyDL :
			pipeParts.push_back(&printXML);
			break;
		case OptionFile::UpToVisRings:
			pipeParts.push_back(&visRing);
			pipeParts.push_back(&wallEE);
			pipeParts.push_back(&printXML);
			break;
		case OptionFile::UpToTravisRoute:
			pipeParts.push_back(&visRing);
			pipeParts.push_back(&traversRout);
			pipeParts.push_back(&wallEE);
			pipeParts.push_back(&printXML);
			break;
	}

	//setting some flags
	OptionFile::mergeOnNameChange = true;

	return true;
}

std::string Pipeline::DoWork(){
	//tinyxml2::XMLDocument * doc = new tinyxml2::XMLDocument();
	//take the data as xmlDoc, and proceed w/the data in MyData format
	data = new MyData(); 
	for(unsigned int i = 0; i < pipeParts.size(); i++){
		//print progress
		if(this->output != nullptr){
			this->output->DisplayProgress(std::string("Entering: ").append(pipeParts.at(i)->ToString()));
		}
		pipeParts.at(i)->SetBroker(this->output);
		pipeParts.at(i)->SetUp();
		std::string out = pipeParts.at(i)->PrepDoc(data);
		if(out != std::string("Splendid")){
			if(this->output != nullptr){
				this->output->DisplayProgress(std::string("Failed at: ").append(pipeParts.at(i)->ToString()));
			}
			
			return std::string("Failed at: ").append(pipeParts.at(i)->ToString()).append("\n").append(out);
		}
	}

	return std::string("Finished Computing!");
}

void Pipeline::DisplaySubProg(std::string text){
	if(this->output != nullptr){
		this->output->DisplayProgress(text, true);
	}
}

void Pipeline::SetBroker(OutputBroker * outputBroker){
	this->output = outputBroker;
}

