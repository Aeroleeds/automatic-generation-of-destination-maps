#pragma once
#include "MyData.h"
#include "OutputBroker.h"
#include "OptionFile.h"
/*
	Our interface for all components of the pipeline
*/
///Interface for all components of the Pipeline
class PipelinePart
{
public:
	PipelinePart();
	virtual ~PipelinePart(void) {};
	///Run the described operation on the mydata set (to be implemented individually by each part)
	/**	returns "Splendid" if everything went according to plan
		a failure description (anything unequal "Splendid") if sth failed (then the pipeline will (should) respond w/a corresponding error message)
	*/
	virtual std::string PrepDoc(MyData * data) = 0;
	///A string representation of the pipelinepart used for identification
	virtual std::string ToString() = 0;
	///Setup must be called before using nay pipelinepart
	/**	May be implemented by a pipeline part if any setup is necessary*/
	virtual void SetUp(){return;};
	///The OutputBroker is the one handling where the output provided to the DisplaySubProg is to be lead
	/**	Its safe to leave the OutputBroker unset, DisplaySubProg will then simply swallow the output to be displayed*/
	virtual void SetBroker(OutputBroker * outputBroker){
		this->output = outputBroker;
	}
	///Method to display progress messages on the sub level of the progress dialog
	/**	Set an OutputBroker first before relying on this method (via SetBroker)*/
	virtual void PrintSubProg(std::string text){
		if(output != nullptr){
			output->DisplayProgress(text, true);
		}
	}
private:
	///Our link to display progress
	OutputBroker * output;
};

