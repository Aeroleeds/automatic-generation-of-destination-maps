#pragma once
#include "stdafx.h"
#include "mainwindow.h"
#include <QThread>
#include <QObject>
#include <QString>

///Class to sent output messages to the mainwindow (which in turn sends them to the progress dialog)
class OutputBroker : public QObject
{
	Q_OBJECT

public:
	OutputBroker(MainWindow * output);
	~OutputBroker(void);
	///Displays the given text in the progress dialog
	/**	if isSubProg is true will display it as sub text and not overwrite the given main progress text*/
	void DisplayProgress(std::string text, bool isSubProg = false);
signals:
	///Displays the given text in the progress dialog (this is the signal connecting this thread to the UI-containing main thread)
	/**	if isSubProg is true will display it as sub text and not overwrite the given main progress text*/	
	void ProgressChangedSignal(QString text, bool isSubProg);
private:
	///The outputwindow where the display operations are sent to
	MainWindow * outputWindow;
	//QMutex mutex;
};

