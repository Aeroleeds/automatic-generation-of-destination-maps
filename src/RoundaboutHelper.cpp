#include "StdAfx.h"
#include "RoundaboutHelper.h"


RoundaboutHelper::RoundaboutHelper(void)
{
}


RoundaboutHelper::~RoundaboutHelper(void)
{
}


RoundaboutHelper::Roundabout RoundaboutHelper::ConstructRoundabout(Edge * e, MyData * data){
	RoundaboutHelper::Roundabout roundy;
	Node * temp = e->GetEndNode();
	Edge * tempEdge = e;
	roundy.nodes.push_back(e->GetStartNode());
	while(*temp != *e->GetStartNode()){
		roundy.edges.push_back(tempEdge);
		roundy.nodes.push_back(temp);
		tempEdge = RoundaboutHelper::GetNextRoundEdge(tempEdge, temp, data);
		if(tempEdge == nullptr){
			return roundy;
		}
		temp = tempEdge->GetOtherNode(temp);
	}
	//we still need to add the last edge to the roundabout
	roundy.edges.push_back(tempEdge);
	return roundy;
}

bool RoundaboutHelper::HasNonRoundaboutEdges(Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(!data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout()) return true;
	}
	return false;
}

///Returns an edge leading from node that is a roundabout edge and not the edge passed on as param
/**	If no such edge is found, a nullptr is returned (even though this should never happen)*/
Edge * RoundaboutHelper::GetNextRoundEdge(Edge * edge, Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(*data->GetEdgesByNodeID(node->GetNodeID()).at(i) == *edge){
			continue;
		}else if(!data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout()){
			continue;
		}else return data->GetEdgesByNodeID(node->GetNodeID()).at(i);
	}
	return nullptr;
}

Edge * RoundaboutHelper::GetNonRoundEdge(Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout()){
			continue;
		}else return data->GetEdgesByNodeID(node->GetNodeID()).at(i);
	}
	return nullptr;
}

