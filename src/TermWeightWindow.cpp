#pragma once
#include "StdAfx.h"
#include "TermWeightWindow.h"
#include "OptionFile.h"

TermWeightWindow::TermWeightWindow(void)
{
	ui.setupUi(this);
	connect(ui.ApplyButton, SIGNAL(clicked()), this, SLOT(ApplySettings()));

	ui.ACTermOne->setText(std::to_string((long double) OptionFile::acOne).c_str());
	ui.ACTermTwo->setText(std::to_string((long double) OptionFile::acTwo).c_str());
	ui.MinLengOne->setText(std::to_string((long double) OptionFile::minLengOne).c_str());
	ui.MinLengTwo->setText(std::to_string((long double) OptionFile::minLengTwo).c_str());
	ui.RelLengOne->setText(std::to_string((long double) OptionFile::relLengOne).c_str());
	ui.RelLengTwo->setText(std::to_string((long double) OptionFile::relLengTwo).c_str());
	ui.RelRelLengOne->setText(std::to_string((long double) OptionFile::relRelLengOne).c_str());
	ui.RelRelLengTwo->setText(std::to_string((long double) OptionFile::relRelLengTwo).c_str());
	ui.OrientTermOne->setText(std::to_string((long double) OptionFile::orientOne).c_str());
	ui.OrientTermTwo->setText(std::to_string((long double) OptionFile::orientTwo).c_str());
	ui.RelOrientTermOne->setText(std::to_string((long double) OptionFile::relOrientOne).c_str());
	ui.RelOrientTermTwo->setText(std::to_string((long double) OptionFile::relOrientTwo).c_str());
	ui.FalseInsecOne->setText(std::to_string((long double) OptionFile::falseInsecOne).c_str());
	ui.FalseInsecTwo->setText(std::to_string((long double) OptionFile::falseInsecTwo).c_str());

	valid.setNotation(QDoubleValidator::StandardNotation);

	ui.ACTermOne->setValidator(&valid);
	ui.ACTermTwo->setValidator(&valid);
	ui.MinLengOne->setValidator(&valid);
	ui.MinLengTwo->setValidator(&valid);
	ui.RelLengOne->setValidator(&valid);
	ui.RelLengTwo->setValidator(&valid);
	ui.RelRelLengOne->setValidator(&valid);
	ui.RelRelLengTwo->setValidator(&valid);
	ui.OrientTermOne->setValidator(&valid);
	ui.OrientTermTwo->setValidator(&valid);
	ui.RelOrientTermOne->setValidator(&valid);
	ui.RelOrientTermTwo->setValidator(&valid);
	ui.FalseInsecOne->setValidator(&valid);
	ui.FalseInsecTwo->setValidator(&valid);

}


TermWeightWindow::~TermWeightWindow(void)
{
}

void TermWeightWindow::ShowSettings(){
	this->show();
}


void TermWeightWindow::HideSettings(){
	this->setVisible(false);
}

void TermWeightWindow::ApplySettings(){
	//read all the stuff from our input fields
	bool correctInput;

	double falseInsecOne, falseInsecTwo;
	double minLengOne, minLengTwo;
	double relLengOne, relLengTwo;
	double orientOne, orientTwo;
	double relOrientOne, relOrientTwo;
	double acOne, acTwo;
	double relRelLengOne, relRelLengTwo;

	
	acOne = ui.ACTermOne->text().toDouble(& correctInput);
	if(!correctInput || acOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	acTwo = ui.ACTermTwo->text().toDouble(& correctInput);
	if(!correctInput|| acTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	relRelLengOne = ui.RelRelLengOne->text().toDouble(& correctInput);
	if(!correctInput || relRelLengOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	relRelLengTwo = ui.RelRelLengTwo->text().toDouble(& correctInput);
	if(!correctInput || relRelLengTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	orientOne = ui.OrientTermOne->text().toDouble(& correctInput);
	if(!correctInput || orientOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	orientTwo = ui.OrientTermTwo->text().toDouble(& correctInput);
	if(!correctInput  || orientTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	relOrientOne = ui.RelOrientTermOne->text().toDouble(& correctInput);
	if(!correctInput || relOrientOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	relOrientTwo = ui.RelOrientTermTwo->text().toDouble(& correctInput);
	if(!correctInput || relOrientTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	falseInsecOne = ui.FalseInsecOne->text().toDouble(& correctInput);
	if(!correctInput || falseInsecOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	falseInsecTwo = ui.FalseInsecTwo->text().toDouble(& correctInput);
	if(!correctInput || falseInsecTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	minLengOne = ui.MinLengOne->text().toDouble(& correctInput);
	if(!correctInput || minLengOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	minLengTwo = ui.MinLengTwo->text().toDouble(& correctInput);
	if(!correctInput || minLengTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	relLengOne = ui.RelLengOne->text().toDouble(& correctInput);
	if(!correctInput || relLengOne < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	relLengTwo = ui.RelLengTwo->text().toDouble(& correctInput);
	if(!correctInput || relLengTwo < 0){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	//disable label
	ui.WarningLabel->setText("");

	//and write all the stuffings into our OptionFile
	OptionFile::acOne = acOne;
	OptionFile::acTwo = acTwo;
	OptionFile::minLengOne = minLengOne;
	OptionFile::minLengTwo = minLengTwo;
	OptionFile::relLengOne = relLengOne;
	OptionFile::relLengTwo = relLengTwo;
	OptionFile::relRelLengOne = relRelLengOne;
	OptionFile::relRelLengTwo = relRelLengTwo;
	OptionFile::orientOne = orientOne;
	OptionFile::orientTwo = orientTwo;
	OptionFile::relOrientOne = relOrientOne;
	OptionFile::relOrientTwo = relOrientTwo;
	OptionFile::falseInsecOne = falseInsecOne;
	OptionFile::falseInsecTwo = falseInsecTwo;

	this->HideSettings();
}
