#include "StdAfx.h"
#include "AreaControlTerm.h"


AreaControlTerm::AreaControlTerm(void)
{
	BoundingBox bb = OptionFile::bb;
	/**	Divide like this:
		_________
		| A|  |B |
		|__|__|__|
		|  |C |  |
		|__|__|__|
		| D|  |E |
		|__|__|__|

		and increase the areas if they are too small
	*/
	
	//NEW SETUP STARTS HERE
	double tempLat = (bb.posLat - bb.negLat) / 3;
	double tempLon = (bb.posLong - bb.negLong) / 3;
	//add the four boxes in the corners
	this->startingBoxes.push_back(BoundingBox(bb.negLat + tempLat, bb.negLat, bb.negLong + tempLon, bb.negLong));
	this->startingBoxes.push_back(BoundingBox(bb.negLat + tempLat, bb.negLat, bb.posLong, bb.posLong - tempLon));
	this->startingBoxes.push_back(BoundingBox(bb.posLat, bb.posLat - tempLat, bb.negLong + tempLon, bb.negLong));
	this->startingBoxes.push_back(BoundingBox(bb.posLat, bb.posLat - tempLat, bb.posLong, bb.posLong - tempLon));
	//and the centre one
	this->startingBoxes.push_back(BoundingBox(bb.posLat - tempLat, bb.negLat + tempLat, bb.posLong - tempLon, bb.negLong - tempLon));

	double steps = 15;
	this->incLat = (bb.posLat - bb.negLat) / steps;
	this->incLon = (bb.posLong - bb.negLong) / steps;
}


AreaControlTerm::~AreaControlTerm(void)
{
}


double AreaControlTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	if(nodes.size() == 0){
		for(unsigned int i = 0; i < tempData->GetNodeList().size(); i++){
			this->nodes.push_back(tempData->GetNodeList().at(i));
		}
	}
	return 0;
}

double AreaControlTerm::PostCost(){
	//divided by 6 would be equal distribution, half that is what we aim for
	unsigned int necessaryNodes = nodes.size() / 12;
	//init the boxes to their starting values
	std::vector<BoundingBox> boxes;
	for(unsigned int i = 0; i < startingBoxes.size(); i++){
		boxes.push_back(BoundingBox(startingBoxes.at(i)));
	}
	bool finished = false;
	//save the indices of the boxes that are already full
	std::set<unsigned int> finishedIndices;
	//inc the penalty for one every time a box is enlargened
	double penalty = 0;
	while(!finished){
		//set finished to true, if we find a box w/out the necessary # of nodes, it will be set to false again
		finished = true;
		//init the vector containing the # of ound nodes w/in the box
		std::vector<unsigned int> nodesPresent;
		for(unsigned int i = 0; i < boxes.size(); i++){
			nodesPresent.push_back(0);
		}
		//go through all non-finished boxes and inc their count accordingly
		for(unsigned int i = 0; i < nodes.size(); i++){
			for(unsigned int x = 0; x < boxes.size(); x++){
				if(finishedIndices.count(x) != 0){
					continue;
				}
				if(boxes.at(x).IsInside(nodes.at(i))){
					nodesPresent.at(x) = nodesPresent.at(x) +1;
				}
			}
		}
		//if a box has the necessary amount, add it to the finished ones and do not penalise
		//else enlargen it and penalise until all boxes contain a valid amount of nodes
		//TODO: verify whether we want to force more nodes into the middle box, if so, add weighing here
		for(unsigned int i = 0; i < boxes.size(); i++){
			if(finishedIndices.count(i) != 0){
				continue;
			}else if(nodesPresent.at(i) >= necessaryNodes){
				finishedIndices.insert(i);
			}
			else{
				boxes.at(i) = this->EnlargenBox(boxes.at(i));
				finished = false;
				penalty++;
			}
		}
	}
	//then look for the number of nodes in each box and penalise
	/*double penalty = 0;
	for(unsigned int i = 0; i < nodesPresent.size(); i++){
		if(nodesPresent.at(i) < necessaryNodes){
			penalty += necessaryNodes - nodesPresent.at(i);
		}
	}*/
	return penalty;
}

BoundingBox AreaControlTerm::EnlargenBox(BoundingBox toSmall){
	return BoundingBox(toSmall.posLat + incLat, toSmall.negLat - incLat, toSmall.posLong + incLon, toSmall.negLong - incLon);
}

void AreaControlTerm::ResetTerm(){
	this->nodes.clear();
}

std::string AreaControlTerm::ToString(){
	return std::string("AreaControlTerm");
}

std::string AreaControlTerm::GetIdentifier(){
	return std::string("AreaControlTerm");
}

double AreaControlTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::acOne;
	case 2:
		return OptionFile::acTwo;
	default:
		return -1;
	}

}
