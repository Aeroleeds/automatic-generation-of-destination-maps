#pragma once
#include "Node.h"
///Class for representing the edges in our data
/**
	Class used for representing ways between nodes, (kinda) equivalent to ways in the osm xml
	references the 2 underlying nodes for reference
	Edges can easily be accessed by using the streetName to edge map in MyData, or simply by using the Edgelist (in MyData as well)
*/
class Edge
{
public:
	///ID to be assigned to the next created edge
	/**This will be done automatically in the constructor, if no ID to be used is provided*/
	static unsigned int nextEdgeID;
	
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> createBoolMap();
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> createIntMap();
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> createStringMap();
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> boolValueToIndex;
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> stringValueToIndex;
	///Currently unused maps to make it easy to read and write additional tags that are deemed interesting, not yet implemented
	static std::map<std::string, unsigned int> intValueToIndex;

	///method providing a quick method to return an edge of the highest order existing in the edge vector passed on as param
	/**In the likely case of multiple edges sharing the same, highest streetType no assertions are made regarding which one will be returned
		although it should generally be the last edge in the list*/
	static Edge * GetHighestOrder(std::vector<Edge *> eList);
	///method providing a quick method to return an edge of the lowest order existing in the edge vector passed on as param
	/**In the likely case of multiple edges sharing the same, lowest streetType no assertions are made regarding which one will be returned
		although it should generally be the last edge in the list*/
	static Edge * GetLowestOrder(std::vector<Edge *> eList);
	
	///all constructors will automatically compute edgeID, distance and Time Travel
	/**	Despite being vital to many algorithms, the streettype is NOT part of the constructor
		Set the streettype manually as soon as possible, please do not use a default value as streettype (i.e. numValues)*/
	Edge(Node * startNode, Node * endNode, unsigned int maxSpeed, std::string streetName);
	///all constructors will automatically compute distance and Time Travel, this one additionally allows to manually set the edge id (mainly for deep copies)
		/**	Despite being vital to many algorithms, the streettype is NOT part of the constructor
		Set the streettype manually as soon as possible, please do not use a default value as streettype (i.e. numValues)*/
	Edge(Node * startNode, Node * endNode, unsigned int maxSpeed, std::string streetName, unsigned int edgeID);
	~Edge(void);

	/*
		GETTER AND SETTER - START
	*/

	/**if it's a one way road, this will be set to true, meaning that one can only travel from startNode to the endNode
	  Take care when using, for the nodes need obviously be in the correct order, if it's a one-way street*/
	bool IsOneWay();
	void SetOneWay(bool newOneWay);
	///Time travel is the time necessary to travel along the edge, in seconds
	double GetTimeTravel();
	void SetTimeTravel(double newTimeTravel);
	///Streetnames
	std::string GetStreetName();
	///Distance between start and end node, in metres (compute w/the wicked formula gotten from the internet TODO: reference implementing class here)
	unsigned int GetDistance();
	void SetDistance(unsigned int newDistance);
	///maxSpeed in m/s
	unsigned int GetMaxSpeed();
	///set the max speed in km/h, it will be transformed to m/s for you
	void SetMaxSpeed(unsigned int newMaxSpeed);
	///Pointers to start and end nodes
	Node * GetStartNode();
	Node * GetEndNode();
	void SetStartNode(Node * newStartNode);
	void SetEndNode(Node * newEndNode);
	///EdgeId that will be statically assigned in the constructor, should be unique
	/**	Multiple edges with the same ID can be created, this is however not intended, and will not happen if the basic constructor is used
		The edge ID is NOT a part of the osm-data, and is arbitrarily assigned by the system
		Loading the same data another time will not guarantee anything about the edge ID, it may (and probably will) be a different edge ID than it was the first time around*/
	unsigned int GetEdgeID();
	///taken is an internal value used to mark edges that are not to be deleted by the DataWallE
	bool IsTaken();
	///sets the edge and both nodes of the edge to the new value
	void SetTaken(bool newTaken);
	///ring road is an internal value to mark the edges that are forming the hierarchical rings around the destination
	bool IsRingRoad();
	///Returns the boolean marking this edge as part of a roundabout-junction
	bool IsRoundabout();
	///Sets the boolean marking this edge as part of a roundabout-junction
	void SetRoundabout(bool newRound);
	void SetRingRoad(bool newRingRoad);
	///Streettype of the edge
	/**	Needs be one of the ones defined in StreetType
		and shall be set as soon as possible*/
	void SetStreetType(StreetType streetType);
	StreetType GetStreetType();
	/*
		GETTER AND SETTER - END
	*/
	///Checks whether the street type associated with this edge is one describing a link road (e.g. motorway_link)
	/**	More precisely: checks the string associated with the StreetType enum for occurence of "_link"
		if "_link" is found within the string, this returns true, else it returns false*/
	bool IsLinkRoad();
	///simple identity check for edges (for more sophisticated methods, use the == operator)
	/**	simply checks whether the two edges IDs are equal
		with correct usage of the system (i.e. letting the system assign the edge IDs) this should be sufficient*/
	bool Equals(Edge otherEdge);
	///more sophisticated identity check for edges
	/**	looks at equality of edge ID, the street name, and of both the start- and end-nodes*/
	bool operator ==( const Edge &other ) const{
		return this->edgeID == other.edgeID && this->streetName == other.streetName && (*this->startNode == *other.startNode) && (*this->endNode == *other.endNode);
	}
	///non-equals is simply the negation of the bool obtained via the == operator
	bool operator !=( const Edge &other ) const{
		return !(*this == other);
	}
	///The smaller operator uses the edgeID as value to sort against
	/**if the edge IDs are equal this will return false (i.e. strictly smaller ids return true)*/
	bool operator < (const Edge &other) const{
		return this->edgeID < other.edgeID;
	}
	///returns true if the streettype of this edge is strictly higher than the one of the other Edge
	/**
	e.g. highwayEdge.IsHigherOrder(residentialEdge) returns true
		highwayEdge.IsHigherOrder(otherHighwayEdge) returns false
	*/
	bool IsHigherOrder(Edge * otherEdge);
	///Returns a string containing the ID of the current edge
	std::string ToString();
	///checks whether the edge can be traversed from the given node
	/**	I.e. will always return true if the edge is NOT a one-way edge
		in case it is a one-way edge, true is returned if the Node n equals the startNode of the edge (for then it is only traversable from start to end node)*/
	bool IsTraversable(Node * n);
	/// returns the node of edge that is not n
	/**	in case n Equals neither of the nodes of the edge, the startnode is returned (no exception or anything fancy here)
	*/
	Node * GetOtherNode(Node * n);
	///will recompute the distance spanned by the two edges, as well as the time taken to travel along it
	/** It is otherwise computed in the constructor automatically, if you however change the position of the contained Nodes, recomputing the distance will give you new, valid values
		Therefore to be used when one of the nodes changes position*/
	void RecomputeDistance();
	///Returns whether one of the informations read from/written to an xml differ between this and other Edge
	/**	If a new Tag is needed (e.g. oneway, roundabout) add it here!	*/
	bool HaveSameTags(Edge * other){
		return this->roundabout == other->IsRoundabout() && this->streetType == other->GetStreetType() && this->streetName == other->GetStreetName() && this->maxSpeed == other->GetMaxSpeed() && this->oneWay == other->IsOneWay();
	}

private:
	///for all the distance computations see: http://www.movable-type.co.uk/scripts/latlong.html
	///Non-implemented alternative for computing distances (deemed unneccessary)
	/**	See: http://www.movable-type.co.uk/scripts/latlong.html */
	unsigned int ComputeDistanceCosines();
	///the formula used to compute the world distance between the start and end node of the edge
	/**	See: http://www.movable-type.co.uk/scripts/latlong.html
		This version is not as accurate as possible, but since we operate on a small area the fault is not that bad
		especially since we change the distance in the layouting stage anyway*/
	unsigned int ComputeDistanceHaversine();
	///Non-implemented alternative for computing distances (deemed unneccessary)
	/**	See: http://www.movable-type.co.uk/scripts/latlong.html */
	unsigned int ComputeDistanceEquirec();
	///Determines the time (in seconds) necessary to travel along the edge
	/**	Simply by dividing the distance by the max speed allowed on the edge
		In case the max speed is set to 0, a speed of 1 is returned (this is only for defaulting purpose, make sure NO maxspeed == 0 is in the system)*/
	double ComputeTimeTravel();
	///Method converting an angle in degrees to an angle in radians
	/**	https://en.wikipedia.org/wiki/Radian#Conversion_between_radians_and_degrees */
	double DegreeToRadians(double angle);

	bool oneWay, taken, ringRoad, roundabout;
	double timeTravel;
	unsigned int distance, maxSpeed;
	unsigned int edgeID;
	std::string streetName;
	StreetType streetType;
	Node * startNode;
	Node * endNode;


};

