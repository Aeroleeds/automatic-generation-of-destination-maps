#pragma once
#include "StdAfx.h"
#include "Node.h"
#include <math.h>
#include "OptionFile.h"

Node::Node(double lon, double lat, unsigned int nodeID){
	//this->lon = lon;
	//this->lat = lat;
	this->SetLat(lat, false);
	this->SetLon(lon, false);
	this->nodeID = nodeID;
	this->valence = 0;
	this->taken = false;
	this->roundabout = false;
	this->ringRoad = false;
}


Node::~Node(void)
{
}

Node & Node::operator=(const Node & node){
	this->nodeID = node.nodeID;
	//this->lat = node.lat;
	//this->lon = node.lon;
	this->SetLat(node.lat, false);
	this->SetLon(node.lon, false);
	this->ringRoad = node.ringRoad;
	this->roundabout = node.roundabout;
	this->taken = node.taken;
	this->valence = node.valence;
	return *this;
}


unsigned int Node::GetNodeID(){return this->nodeID;}
double Node::GetLon(bool isMerc){
	if(isMerc){
		return this->lonX;
	}else{
		return this->lon;
	}
}
void Node::SetLon(double newLon, bool isMerc){
	if(isMerc){
		this->lonX = newLon;
		//this->lon = lonX + OptionFile::midLon;
		this->lon = Node::lonX2lon(lonX);
	}else{
		this->lon = newLon;
		//this->lonX = this->lon - OptionFile::midLon;	
		this->lonX = Node::lon2lonX(lon);
	}
} 
double Node::GetLat(bool isMerc){
	if(isMerc){
		return this->latY;
	}else{
		return this->lat;
	}
}
void Node::SetLat(double newLat, bool isMerc){
	if(isMerc){
		this->latY = newLat;
		//this->lat = asin(tanh(this->latY));
		this->lat = Node::latY2lat(latY);
	}else{
		this->lat = newLat;
		//double si = sin(lat);
		//the normal log function matches the natural logarithm
		//this->latY = 1/2 * log((1 + si) / (1 - si));
		this->latY = Node::lat2latY(lat);
	}
}
unsigned int Node::GetValence(){return this->valence;}
void Node::SetValence(unsigned int newValence){this->valence = newValence;}
bool Node::IsTaken(){return this->taken;}
void Node::SetTaken(bool newTaken){this->taken = newTaken;}
bool Node::IsRingRoad(){return this->ringRoad;}
void Node::SetRingRoad(bool newRingRoad){this->ringRoad = newRingRoad;}
bool Node::IsRoundabout(){return this->roundabout;}
void Node::SetRoundabout(bool newRound){this->roundabout = newRound;}
/*
	reference to the Edges that go through the Node
*
std::vector<Edge *> Node::GetEdges(){return this->edges;}
void Node::AddEdge(Edge* edge){this->edges.push_back(edge);}
*/
bool Node::Equals(Node otherNode){
	return (this->nodeID == otherNode.GetNodeID() ? true : false);
}
std::string Node::ToString(){
	return std::string("Node with ID: ").append(std::to_string((long long unsigned) this->nodeID));
}
