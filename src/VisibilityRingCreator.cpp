#include "StdAfx.h"
#include "VisibilityRingCreator.h"
#include "Line.h"
#include "FalseIntersectionHelper.h"

VisibilityRingCreator::VisibilityRingCreator(void)
{}


VisibilityRingCreator::~VisibilityRingCreator(void)
{
	//then cleanup
	std::vector<Node *> nodes;
	for(unsigned int i = 0; i < compareAgainst.size(); i++){
		if(OptionFile::useExactDestinationForVisRings){

		}else{
			//if the real destination node was used, do not delete it here
			nodes.push_back(compareAgainst.at(i)->GetOtherNode(&OptionFile::destinationNode));
		}
		delete compareAgainst.at(i);
	}
	for(unsigned int i = 0; i < nodes.size(); i++){
		delete nodes.at(i);
	}
}

std::string VisibilityRingCreator::PrepDoc(MyData * data){
	//first off, built sets of edges for each edge type we are interested in (the ones in the set)
	std::set<StreetType> takenType;
	takenType.insert(motorway);
	takenType.insert(trunk);
	takenType.insert(primary);
	takenType.insert(secondary);
	takenType.insert(tertiary);
	//for now, do not take residential visRings
	takenType.insert(unclassified);
	takenType.insert(residential);
	//std::map<StreetType, std::vector<Edge *>> edgeSets;
	std::vector<Edge *> edges = data->GetEdgeList();
	//get the highest streettype
	StreetType typo = tertiary;
	for(unsigned int i = 0; i < edges.size(); i++){
		//smaller values correspond to bigger streettypes
		if(edges.at(i)->GetStreetType() < typo){
			typo = edges.at(i)->GetStreetType();
		}
		//we will not find a streettype bigger than the motorway, so we may stop here
		if(typo == motorway){
			break;
		}
	}
	for(unsigned int i = 0; i < edges.size(); i++){
		//we automatically take all edges of the highest available type and its link road, since they are always part of the destination map
		//if(edges.at(i)->GetStreetType() == motorway){
		if(edges.at(i)->GetStreetType() == typo){
			edges.at(i)->SetTaken(true);
			edges.at(i)->SetRingRoad(true);
		//}else if(edges.at(i)->GetStreetType() == motorway_link){
		//typo + 1 will always be a link road, for we only automatically take edges bigger or equally big than tertiary roads
		}else if(edges.at(i)->GetStreetType() == typo + 1){
			edges.at(i)->SetTaken(true);
		}
		//we however still want to find the samples, for concerning ring roads the highways block smaller roads
		if(takenType.find(edges.at(i)->GetStreetType()) != takenType.end()){
			edgeSets[edges.at(i)->GetStreetType()].push_back(edges.at(i));
		}
	}
	for(StreetType type = motorway; type != numValues; type = (StreetType) (type + 1)){
		if(takenType.count(type) == 0){
			continue;
		}
		if(edgeSets.count(type) == 0){
			continue;
		}
		this->PrintSubProg(std::string("Sampling streettype ").append(streetStrings[type]));
		sampleSets[type] = this->GetSamples(edgeSets.at(type), data);
		/*
		
		//Debug the samples w/out any connecting
		
		std::vector<Edge *> edgars = sampleSets.at(type).edges;
		for(unsigned int i = 0; i < edgars.size(); i++){
			if(edgars.at(i) != nullptr){
				edgars.at(i)->SetTaken(true);
			}
		}*/
		this->PrintSubProg(std::string("Connecting samples of streettype ").append(streetStrings[type]));
		//TODO: enable connections again!!!!
		//SRIOUSLY
		/**
		
		
		
		RIGHT HERE
		
		
		*/
		this->ConnectSamples(sampleSets[type], data);
	}

	return std::string("Splendid");
}

std::string VisibilityRingCreator::ToString(){
	return std::string("VisibilityRings");
}

void VisibilityRingCreator::SetUp(){
	//how many edges we want in between the eight cardinal directions
	double steps = OptionFile::stepsAtVisibilityRings;
	/**
	
		HUGE initialisation of all the edges to test against for samples (maybe should make this more beautiful)
	
	*/
	Node * destination;
	//if exact locations are what's wished for, create a new Node at that exact point (and delete it later on)
	if(OptionFile::useExactDestinationForVisRings){
		destination = new Node(OptionFile::destination.longi, OptionFile::destination.lat, 0);
	}else{
		destination = &OptionFile::destinationNode;
	}
	Edge * west = new Edge(destination, new Node(OptionFile::bb.negLong, destination->GetLat(false), 0), 12, "western low road");
	Edge * east = new Edge(destination, new Node(OptionFile::bb.posLong, destination->GetLat(false), 0), 12, "eastern high road");
	Edge * north = new Edge(destination, new Node(destination->GetLon(false), OptionFile::bb.posLat, 0), 12, "northern frost road");
	Edge * south = new Edge(destination, new Node(destination->GetLon(false), OptionFile::bb.negLat, 0), 12, "south of heaven");
	//add the four in between directions in order to easily introduce new samples in between them
	Edge * northWest = new Edge(destination, new Node(OptionFile::bb.negLong, OptionFile::bb.posLat, 0), 12, "Go west");
	Edge * northEast = new Edge(destination, new Node(OptionFile::bb.posLong, OptionFile::bb.posLat, 0), 12, "Go east");
	Edge * southWest = new Edge(destination, new Node(OptionFile::bb.negLong, OptionFile::bb.negLat, 0), 12, "Go south");
	Edge * southEast = new Edge(destination, new Node(OptionFile::bb.posLong, OptionFile::bb.negLat, 0), 12, "And still south");
	
	compareAgainst.push_back(west); 
	//if we want to add steps edges, we need to divide it by steps + 1
	double stepsize = (OptionFile::bb.posLat - destination->GetLat(false)) / (steps + 1);
	double tempPos = destination->GetLat(false) + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(OptionFile::bb.negLong, tempPos, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(northWest);
	stepsize = (destination->GetLon(false) - OptionFile::bb.negLong) / (steps + 1);
	tempPos = OptionFile::bb.negLong + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(tempPos, OptionFile::bb.posLat, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(north); 
	stepsize = (OptionFile::bb.posLong - destination->GetLon(false)) / (steps + 1);
	tempPos = destination->GetLon(false) + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(tempPos, OptionFile::bb.posLat, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(northEast);
	//stepsize will be neg., for we move down
	stepsize = (destination->GetLat(false) - OptionFile::bb.posLat) / (steps + 1);
	tempPos = OptionFile::bb.posLat + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(OptionFile::bb.posLong, tempPos, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(east);
	//stepsize will be neg., for we move down
	stepsize = (OptionFile::bb.negLat - destination->GetLat(false)) / (steps + 1);
	tempPos = destination->GetLat(false) + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(OptionFile::bb.posLong, tempPos, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(southEast);
	//stepsize will be neg., for we move left
	stepsize = (destination->GetLon(false) - OptionFile::bb.posLong) / (steps + 1);
	tempPos = OptionFile::bb.posLong + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(tempPos, OptionFile::bb.negLat, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}
	
	compareAgainst.push_back(south); 
	//stepsize will be neg., for we move left
	stepsize = (OptionFile::bb.negLong - destination->GetLon(false)) / (steps + 1);
	tempPos = destination->GetLon(false) + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(tempPos, OptionFile::bb.negLat, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	compareAgainst.push_back(southWest);
	stepsize = (destination->GetLat(false) - OptionFile::bb.negLat) / (steps + 1);
	tempPos = OptionFile::bb.negLat + stepsize;
	for(unsigned int i = 0; i < steps; i++){
		compareAgainst.push_back(new Edge(destination, new Node(OptionFile::bb.negLong, tempPos, 0), 12, "in between comparator"));
		tempPos += stepsize;
	}

	/**
			End the initialisation of sample edges
	**/

	for(unsigned int i = 0; i < compareAgainst.size(); i++){
		shortestDistances.push_back(DBL_MAX);
	}
	//do not discount ring roads at all
	dijk.SetFactor(1);
	//make turns scarce
	dijk.SetPenalty(10);
}

VisibilityRingCreator::RingSamples VisibilityRingCreator::GetSamples(std::vector<Edge *> edges, MyData * data){
	Node * destination = &OptionFile::destinationNode;
	RingSamples samples;

	/*we construct eight edges going from the destination directly north/south/east/west, up until we reach the boundary
	  to then check for intersections; the closest intersection of a given dataset in each direction will be considered a ring road*/
	FalseIntersectionHelper help;
	//then loop through all the edges in the data set
	for(unsigned int i = 0; i < edges.size(); i++){
		for(unsigned int j = 0; j < compareAgainst.size(); j++){
			if(help.IsFalseIntersection(edges.at(i), compareAgainst.at(j), false)){
				double dist = this->GetDistance(edges.at(i), compareAgainst.at(j), destination);
				if(dist < shortestDistances.at(j)){
					shortestDistances[j] = dist;
					samples.edges[j] = edges.at(i);
				}
			}
		}
	}
	//note that we do not reset the shortest distances, thus automatically including the "larger roads block smaller ones" part
	return samples;
}

double VisibilityRingCreator::GetDistance(Edge * edge, Edge * otherEdge, Node * node){
	Line one(edge->GetStartNode(), edge->GetEndNode());
	Line two(otherEdge->GetStartNode(), otherEdge->GetEndNode());
	Node * temp = one.IntersectionPoint(two, 0);
	double dlat = node->GetLat() - temp->GetLat();
	double dlon = node->GetLon() - temp->GetLon();
	delete temp;
	double dist = dlat * dlat + dlon * dlon;
	return dist;
}

void VisibilityRingCreator::ConnectSamples(VisibilityRingCreator::RingSamples samples, MyData * data){
	unsigned int startIndex = samples.NextEdgeIndex(-1);
	//if the samples are empty, return w/out doing anything at all
	if(startIndex == UINT_MAX){return;}
	if(samples.edges.at(startIndex)->GetStreetType() == motorway){
		return;
	}
	//if our samples are valid, extend all of them to the next val != 2 node along their way
	unsigned int tempIndex = startIndex;
	do{
		Edge * edge = samples.edges.at(tempIndex);
		Node * temp = edge->GetStartNode();
		while(temp->GetValence() == 2){
			std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
			edge->SetTaken(true);
			edge->SetRingRoad(true);
			edge = *edge == *edges.at(0) ? edges.at(1) : edges.at(0);
			temp = edge->GetOtherNode(temp);
		}
		edge->SetTaken(true);
		edge->SetRingRoad(true);
		//and do the same thing for the endnode (not beautiful, I know)
		temp = edge->GetEndNode();
		while(temp->GetValence() == 2){
			std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
			edge->SetTaken(true);
			edge->SetRingRoad(true);
			edge = *edge == *edges.at(0) ? edges.at(1) : edges.at(0);
			temp = edge->GetOtherNode(temp);
		}
		edge->SetTaken(true);
		edge->SetRingRoad(true);
		tempIndex = samples.NextEdgeIndex(tempIndex);
	}while(tempIndex != startIndex);

	//then start with the connecting

	unsigned int curr = startIndex;
	unsigned int prev;
	bool finished = false;
	while(!finished){
		prev = curr;
		curr = samples.NextEdgeIndex(curr);
		//if(std::abs((int) curr - (int) prev) < 3 && prev != curr){
		if(prev != curr && this->ConnectIt(samples, prev, curr)){
			this->ConnectAndTakeEdges(samples.edges.at(curr), samples.edges.at(prev), data);
		}
		//if theres two blank samples in between prev and curr, try and connect them to the outer ring road
		else{
			StreetType type = samples.edges.at(curr)->GetStreetType();
			bool connectedPrev = false, connectedCurr = false;
			//we simply try and find possible connections to larger roads
			for(StreetType i = (StreetType) (type - 1); i >= 0; i = StreetType(i - 1)){
				if(sampleSets.count(i) == 0){
					continue;
				}
				int prevedd = (prev + 1) % samples.edges.size();
				int curreddge = (curr - 1) % samples.edges.size();
				if(!connectedPrev && sampleSets.at(i).edges.at((prev + 1) % samples.edges.size()) != nullptr){
					this->ConnectAndTakeEdges(sampleSets.at(i).edges.at((prev + 1) % samples.edges.size()), samples.edges.at(prev), data);
					connectedPrev = true;
				}
				if(!connectedCurr && sampleSets.at(i).edges.at((curr - 1) % samples.edges.size()) != nullptr){
					this->ConnectAndTakeEdges(sampleSets.at(i).edges.at((curr - 1) % samples.edges.size()), samples.edges.at(curr), data);
					connectedCurr = true;
				}
				//even if its more than just three in between curr and prev, we will not connect it any further (it shall be connected anyway, since it leads to the previous ring road)
			}
		}
		if(curr == startIndex){finished = true;}
	}
}

void VisibilityRingCreator::ConnectAndTakeEdges(Edge * one, Edge * two, MyData * data){
	//first off, we found the two edges, and they are obviously both taken and ringroads
	one->SetTaken(true);
	two->SetTaken(true);
	one->SetRingRoad(true);
	two->SetRingRoad(true);

	/*		CASE ONE:
		The two edges are on the same street
	if the edges are on the same street, take a shortcut
	*/
	
	if(one->GetStreetName() == two->GetStreetName() && one->GetStreetType() == two->GetStreetType()){
		//we still need to check which direction to go to
		std::vector<Edge *> edges = this->FoundWayToNode(one->GetStartNode(), one, two->GetStartNode(), data);
	/*	if(this->FoundWayToNode(one->GetStartNode(), one, two->GetStartNode(), data)){
			//and if we found a way, follow it, and return
			FollowAndTake(one->GetStartNode(), two->GetStartNode(), one, data);
			return;*/
		//if a legitimate way has been found, take it
		if(edges.size() != 0){
			for(unsigned int i = 0; i < edges.size(); i++){
				edges.at(i)->SetTaken(true);
				edges.at(i)->SetRingRoad(true);
			}
			//then return for our connecting work is done here
			return;
		}
		edges = this->FoundWayToNode(one->GetEndNode(), one, two->GetStartNode(), data);
		//if a legitimate way has been found, take it
		if(edges.size() != 0){
			for(unsigned int i = 0; i < edges.size(); i++){
				edges.at(i)->SetTaken(true);
				edges.at(i)->SetRingRoad(true);
			}
			//then return for our connecting work is done here
			return;
		}	
	}
	/*
			CASE TWO:
		the two edges are not on the same street, but the two streets intersect at some point
	*/
	
	Node * intersec = nullptr;
	Node * origOne = nullptr;
	//see whether we can find an intersection in one direction
	intersec = this->GetJunction(one->GetStartNode(), one, two, data);
	//if we have not found an intersection in the one direction, go look for it in the other one
	if(intersec == nullptr){
		intersec = this->GetJunction(one->GetEndNode(), one, two, data);
		origOne = one->GetEndNode();
	}else{
		origOne = one->GetStartNode();
	}
	//if we found a possible intersection node, try and find it from the other edge as well
	if(intersec != nullptr){
		Node * temp = this->GetJunction(two->GetStartNode(), two, one, data);
		Node * origTwo = two->GetStartNode();
		if(temp == nullptr || (*temp != *intersec)){
			temp = this->GetJunction(two->GetEndNode(), two, one, data);
			origTwo = two->GetEndNode();
		}
		if(temp != nullptr && *temp == *intersec){
			this->FollowAndTake(origOne, intersec, one, data);
			this->FollowAndTake(origTwo, intersec, two, data);
			return;
		}
	}
	/*
			CASE THREE
		neither of the above, try to connect em with the dijkstra
	*/
	bool connected = false;
	//if that did not work, try and connect them w/the dijkstra
	std::vector<Edge *> path = dijk.DijkstraPath(data, one->GetStartNode(), two->GetStartNode());
	Edge * e = new Edge(one->GetStartNode(), two->GetStartNode(), 0, "reference way");
	//TODO: verify the change to 2.5 * the distance works for us
	double ref = e->GetDistance() * 2.5;
	delete e;
	connected = connected || this->CheckAndTakeDijkstraPath(path, data, ref);

	//and connect them again, for with one way roads strange things might happen if we do not go both ways here
	//obviously we change goals, so we reset our dijk
	//dijk.ResetDijkstra();
	path = dijk.DijkstraPath(data, two->GetStartNode(), one->GetStartNode());
	connected = connected || this->CheckAndTakeDijkstraPath(path, data, ref);

	if(!connected){
		//possibly extend the edges if no connection was found
		//TODO: examine and decide accordingly
	}
}

bool VisibilityRingCreator::CheckAndTakeDijkstraPath(std::vector<Edge *> path, MyData * data, double referenceLength){
	//std::vector<Edge *> edges = dijk.DijkToEdges(path, data);
	double currLeng = 0;
	for(unsigned int i = 0; i < path.size() && currLeng <= referenceLength; i++){
		Edge * temp = path.at(i);
		currLeng += temp->GetDistance();
	}
	//if we had to stop because we were going for too long, return
	if(currLeng > referenceLength){
		return false;
	}else{
		//else follow the path again and set everything we passed by as taken and ring road
		for(unsigned int i = 0; i < path.size(); i++){
			Edge * temp = path.at(i);
			temp->SetRingRoad(true);
			temp->SetTaken(true);
//			data->AddRingRoad(temp->GetStreetName());
		}
	}
	return true;
}

void VisibilityRingCreator::FollowAndTake(Node * start, Node * end, Edge * edge, MyData * data){
	bool found = false;
	while(!found){
		edge->SetTaken(true);
		edge->SetRingRoad(true);
//		data->AddRingRoad(edge->GetStreetName());
		if(*start == *end){
			return;
		}
		//get all edges leading from our current node, and find the one to take
		std::vector<Edge *> edges = data->GetEdgesByNodeID(start->GetNodeID());
		Edge * tempEdge = nullptr;
		for(unsigned int i = 0; i < edges.size(); i++){
			if(*edges.at(i) == *edge){
				continue;
			}
			if(edges.at(i)->GetStreetName() == edge->GetStreetName() && edges.at(i)->GetStreetType() == edge->GetStreetType()){
				found = true;
				tempEdge = edges.at(i);
			}
		}
		//this is our little fail-safe part, if we did not find a new edge, this will terminate it for us
		found = !found;
		if(tempEdge == nullptr){
			return;
		}
		edge = tempEdge;
		start = edge->GetOtherNode(start);
	}
}

std::vector<Edge *> VisibilityRingCreator::FoundWayToNode(Node * start, Edge * edge, Node * goal, MyData * data){
	double traveledDist = 0;
	std::string currName = edge->GetStreetName();
	std::vector<Edge *> toReturn;
	toReturn.push_back(edge);
	while(traveledDist < 15000 && start->GetValence() != 0){
		if(*start == *goal){return toReturn;}
		std::vector<Edge *> edges = data->GetEdgesByNodeID(start->GetNodeID());
		bool found = false;
		Edge * tempEdge = nullptr;
		for(unsigned int i = 0; i < edges.size(); i++){
			if(*edges.at(i) == *edge){
				continue;
			}
			if(edges.at(i)->GetStreetName() == currName && edges.at(i)->GetStreetType() == edge->GetStreetType()){
				tempEdge = edges.at(i);
				found = true;
				//we do NOT break here, since we might still find the correct street after finding the way our road is headed
			}
		}
		if(!found){
			toReturn.clear();
			return toReturn;
		}
		//if it were a nullptr, found would have triggered a return already
		edge = tempEdge;
		traveledDist += edge->GetDistance();
		start = edge->GetOtherNode(start);
		toReturn.push_back(edge);
	}
	toReturn.clear();
	return toReturn;
}

Node * VisibilityRingCreator::GetJunction(Node * start, Edge * edge, Edge * otherEdge, MyData * data){
	double traveledDist = 0;
	std::string currName = edge->GetStreetName();
	while(traveledDist < 15000 && start->GetValence() != 0){
		std::vector<Edge *> edges = data->GetEdgesByNodeID(start->GetNodeID());
		bool found = false;
		Edge * tempEdge = nullptr;
		for(unsigned int i = 0; i < edges.size(); i++){
			if(*edges.at(i) == *edge){
				continue;
			}
			if(edges.at(i)->GetStreetName() == otherEdge->GetStreetName() && edges.at(i)->GetStreetType() == otherEdge->GetStreetType()){
				return start;
			}
			if(edges.at(i)->GetStreetName() == currName && edge->GetStreetType() == edges.at(i)->GetStreetType()){
				tempEdge = edges.at(i);
				found = true;
				//we do NOT break here, since we might still find the correct street after finding the way our road is headed
			}
		}
		if(!found){return nullptr;}
		edge = tempEdge;
		traveledDist += edge->GetDistance();
		start = edge->GetOtherNode(start);
	}
	return nullptr;
}

/*
double VisibilityRingCreator::GetDistance(Edge * edge, Node * node){
	//gte the intersectionpoint and compute the distance there
	Line line(edge->GetStartNode(), edge->GetEndNode());
	Node * temp = line.IntersectionPoint(node, 0);
	double dlat = node->GetLat() - temp->GetLat();
	double dlon = node->GetLon() - temp->GetLon();
	delete temp;
	//once again, since we only compare against each other, take the simplest distance measure possible
	return dlat * dlat + dlon * dlon;
}
*/

bool VisibilityRingCreator::ConnectIt(RingSamples samples, unsigned int ind, unsigned int nextInd){
	unsigned int temp = ind;
	for(int i = 0; i < 2; i++){
		temp = (temp + 1) % samples.edges.size();
		if(temp == nextInd){ return true;}
	}
	return false;
}

