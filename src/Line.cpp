#include "StdAfx.h"
#include "Line.h"
//the threshold for values to be considered close enough to zero (damn double arithmetics)
#define THRESHOLD 0.000002

Line::Line(Node * start, Node * end)
{
	this->pointX = start->GetLon();
	this->pointY = start->GetLat();
	this->directionX = end->GetLon() - start->GetLon();
	this->directionY = end->GetLat() - start->GetLat();
	this->normalX = -directionY;
	this->normalY = directionX;
}

Line::Line(Node * start, double directionX, double directionY){
	this->pointX = start->GetLon();
	this->pointY = start->GetLat();
	this->directionX = directionX;
	this->directionY = directionY;
	this->normalX = -this->directionY;
	this->normalY = this->directionX;
}


Line::~Line(void)
{
}

Node * Line::IntersectionPoint(Line other, unsigned int usedID){
	/*	insane line intersections, w/the basic equations
		i.e.:
				point + lambda * direction = other.point + mu * other.direction; resulting in the following huge equation to solve for lambda
	*/
	double lambda = (pointY * other.directionX + other.directionY * other.pointX - other.pointY * other.directionX - other.directionY * pointX)
		/ (directionX * other.directionY - directionY * other.directionX);
	double newX = pointX + directionX * lambda;
	double newY = pointY + directionY * lambda;
	//since we work w/mercator values, we need to compute the corresponding lat/lon values for the node constructor
	return new Node(Node::lonX2lon(newX), Node::latY2lat(newY), usedID);
}

Node * Line::IntersectionPoint(Node * p, unsigned int usedID){
	Line l = Line(p, this->normalX, this->normalY);
	return this->IntersectionPoint(l, usedID);
}

bool Line::IsOnSide(int side, Node * n){
	//can remain w/mercator values, for the whole Line object works w/merc
	double d = (n->GetLon() - pointX) * directionY - (n->GetLat() - pointY) * directionX;
	if((side < 0 && d <= 0 ) || (side > 0 && d >= 0)){ return true;}
	else return false;
}

//TODO: possibly refactor this, since it has nothing to do w/the line class
bool Line::IntersectionOnSegment(Node * intersection, Edge * thisEdge){
	//this check should work w/mercator values
	bool validLon = false;
	bool validLat = false;

	double start = thisEdge->GetStartNode()->GetLon();
	double dir = thisEdge->GetEndNode()->GetLon() - start;
	double sec = intersection->GetLon();
	if(dir == 0){
		validLon = (sec - start <= THRESHOLD);
	}else{
		double lambda = (sec - start) / dir;
		//if the lambda is 0 intersection is our startNode, if 1 it's our endNode, if its inbetween its on the line segment
		validLon = (lambda >= 0 && lambda <= 1);
	}
	//if our first value is already false, the second one will not change anything
	if(!validLon){
		return false;
	}
	//then check whether the node is on the edge in lat direction as well
	start = thisEdge->GetStartNode()->GetLat();
	dir = thisEdge->GetEndNode()->GetLat() - start;
	sec = intersection->GetLat();
	if(dir == 0){
		return (sec - start <= THRESHOLD);
	}else{
		double lambda = (sec - start) / dir;
		return (lambda >= 0 && lambda <= 1);
	}
}
