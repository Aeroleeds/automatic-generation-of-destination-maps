#pragma once
#include "pipelinepart.h"
#include "DataProvider.h"

///PipelinePart to grab the data from a DataProvider and morphing it into a mydata object
/** is the class providing a dataProvider object w/the correct queries to look up
	will then convert the resulting xml into a MyData object as well as compute the ring roads for the data
	Additionally will grab the appropriate settings for the DataProvider from the SETTINGS.xml file and put them in place
	for reference this contains 4.1.1., 4.1.2.
*/
class DataGrabber :
	public PipelinePart
{
public:
	DataGrabber(void);
	~DataGrabber(void);

	///Will grab all necessary data as specified in the OptionFile and return it all in one MyData object
	/**	Retrieves the area of interest and the location of our destination from the OptionFile
		Then obtains the data from a data source (See: DataProvider)
		Data is retrieved in smaller chuncks, delimited by their street type
		In order to make that retrieval possible, the desired DataProvider is filled with the values from the SETTINGS.xml
		Make sure the desired OptionFile::nonUsedType is set correctly*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();

	///will collect all the data of the given streettype within the bounding box defined in the OptionFile and return it in a new MyData object (caller needs to delete it!)
	/** this will additionally map the long/lat values given as input to the nearest node in the first step
		Does NO additional logic apart from that, so will just return the provided xml files as MyData objects
	*/
	MyData * GetData(StreetType	type);
	///will find the nearest node to the destination specified by the lat/lon coordinates
	/** for this we simply create a tiny bounding box around the destination and compute the distance to all nodes within that bounding box
		the closest one being the one we use as destination
		in the rare case that no nodes are in the bounding box, it will automatically enlargen itself until we find at least one node
		(which might result in non-ideal placement of the destination node, but this should be an exceedingly rare case anyway)*/
	//Node * FindDestinationNode(double lat, double lon);
		///will find the nearest node to the destination specified by the lat/lon coordinates within the given data set
	/** Simply checks all nodes for proximity to the lat/lon values and returns the closest one*/
	Node * FindDestinationNodeInData(double lat, double lon, MyData * data);

	//DEPRECATED
	/*		Not the method you're looking for
	USE VISIBILITY RING CREATOR INSTEAD*/
	//void SetRing(Node * destination, MyData * data);
	//will append the toAppend data to the data object (including the ring roads)
	/* this is a fancy method for potentially applying any additional modifications on the data sets
		this was used before the visibility rings have been outsourced from datagrabber, and currently only applies the append() method of mydata
		however still around for any future additions*/
//	void AppendData(MyData * toAppend, MyData * data);
	///DEPRECATED
	/**		Not the method you're looking for
	USE VISIBILITY RING CREATOR INSTEAD*/
	//void ConnectRingRoads(MyData * data);
private:
	//will return the squared distance for comparison only
	/*	Does NOT return a distance measure to be used but for simple greater or smaller comparisons*/
	//double GetDistance(Edge * edge, Node * node);
	///will return the edge connecting the two nodes, or nullptr if none exist
	//Edge * GetCommonEdge(Node * one, Node * two, MyData * data);
	///will build a vector of edges from a vector of nodes (as obtained from the Dijkstra algorithm)
	/**	The returned edges are the ones connecting the nodes from the parameter*/
	//std::vector<Edge *> DijkToEdges(std::vector<Node *> nodes, MyData * data);
	///will check whether the computed dijkstra path is shorter than twice the length of the straight line connecting the two nodes
	//bool ValidPath(std::vector<Edge *> path, Node * start, Node * finish);
	///will be used for link roads because they do not need any ring roads, they will just be added
	//bool IsLinkRoad(Edge * e);
	///Reduces the size of the BoundingBox bb by the set factor
	BoundingBox ShrinkBox(BoundingBox bb, double factor);
	///The DataProvider used to access the data, is set depending on OptionFile::serverKey
	DataProvider * prov;

	//std::vector<std::pair<Edge *, Edge *>> edgePairs;
	//std::map<StreetType, std::vector<Edge *>> typeToRing;
};

