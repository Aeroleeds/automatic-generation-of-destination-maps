#pragma once
#include "costterm.h"

///This CostTerm penalises edges that are considered too small to be easily visible and label-able (for the streetnames)
/**	Every Edge is evaluated with regards to how large they appear in the output map, and penalised if they are too small
	used to penalise edges that are too small (so they are easily visible) */
class MinimumLengthTerm :
	public CostTerm
{
public:
	MinimumLengthTerm(void);
	~MinimumLengthTerm(void);

	//the stuff we inherited from our costTerm
	///GetCost will always return 0, but sort the edges into a map that groups them according to their streetname and their associated nodes
	/**	The real cost comes from the PostCost() method */
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	double GetWeight(int suffix);
private:
	///the distance an edge must have in order to not be penalised
	double minDist;
};

