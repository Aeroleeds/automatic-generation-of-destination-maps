#pragma once
#include "stdafx.h"
#include <vector>

///Basically wrapping all the necessary functions of tinyxml2, and providing them in a manner beneficial to our cause
/**	Not all that necessary, possibly scrapped*/
class XmlHelper
{
	public:
		///read the file from the specified relative filepath; returned document needs be deleted by calling method
		tinyxml2::XMLDocument * ReadFile(const std::string &filePath);
		///save the file under the given name
		/**	Will not delete the doc, this is up to the caller*/
		void SaveFile(tinyxml2::XMLDocument * doc, std::string name);
};