#include "StdAfx.h"
#include "OrientationTerm.h"

OrientationTerm::OrientationTerm(void)
{
	this->alreadyInit = false;
}


OrientationTerm::~OrientationTerm(void)
{
}

double OrientationTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	if(!alreadyInit){
		this->InitMap(data);
	}
	Vec2 vec = Vec2(e->GetStartNode(), e->GetEndNode());
//	Vec2 otherVec = Vec2(data->GetEdgeByID(e->GetEdgeID())->GetStartNode(), data->GetEdgeByID(e->GetEdgeID())->GetEndNode());
	//the deviation in angle equals the angle between the two edges
	double angle = vec.GetAngleRadians(this->eIDToVec.at(e->GetEdgeID()));
	return (angle * angle);
}

void OrientationTerm::InitMap(MyData * data){
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		Edge * e = data->GetEdgeList().at(i);
		this->eIDToVec.insert(std::pair<unsigned int, Vec2>(e->GetEdgeID(), Vec2(e->GetStartNode(), e->GetEndNode())));
	}
	this->alreadyInit = true;
}

//the Orientation has no postCost term, thus return 0
double OrientationTerm::PostCost(){
	return 0;
}

std::string OrientationTerm::ToString(){
	return std::string("Cost Function : Orientation Term");
}

std::string OrientationTerm::GetIdentifier(){
	return std::string("Orientation");
}

double OrientationTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::orientOne;
	case 2:
		return OptionFile::orientTwo;
	default:
		return -1;
	}
}
