#pragma once
#include "PipelinePart.h"
#include "Dijkstra.h"
///PipelinePart to compute and take paths from the visibility rings (as created by the VisibilityRingCreator) to our destination Node
/**	The pathing discounts ring roads along the way, by only taking 70 percent of their time needed to travel along them
	Additionally penalises turns by assessing 10 seconds at each turn*/
class TraversableRoute : public PipelinePart
{
public:
	TraversableRoute(void);
	~TraversableRoute(void);
	///Computes and takes all paths leading from ring roads to our destination Node by using the Dijkstra
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	//SetUp function needs be called before using this Object
	//virtual void SetUp();
private:
	//will return the edge the two nodes share or nullptr if they dont share an edge
	//Edge * GetCommonEdge(Node * one, Node * two, MyData * data);

	//Returns whether this node has only one edge associated with it that is both taken and a ringroad
	//bool IsEndingNode(Node * node, MyData * data);
	///The Dijkstra object used to compute the paths
	Dijkstra dijk;
};

