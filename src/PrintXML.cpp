#include "StdAfx.h"
#include "PrintXML.h"


PrintXML::PrintXML(void)
{
	suffix = 1;
}


PrintXML::~PrintXML(void)
{
}

std::string PrintXML::PrepDoc(MyData * data){
	std::string tempName = name;
	tinyxml2::XMLDocument * doc = MyData::MyDataToXML(data); 
	help.SaveFile(doc, tempName.append("_").append(GetSuffixString()));
	suffix++;
	delete doc;
	return std::string("Splendid");
}

std::string PrintXML::ToString(){
	return std::string("PrintXML");
}

void PrintXML::SetName(std::string fileName){
	this->name = fileName;
}

std::string PrintXML::GetSuffixString(){
/*	switch (suffix){
	case 1:
		return std::string("raw");
	case 2:
		return std::string("cleaned");
	case 3:
		return std::string("simplified");
	case 4:
		return std::string("finished");
	default:*/
		return std::to_string((long long) suffix);
	//}
}

