#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma once
//QT ui stuff
#include <QMainWindow>
#include <QWebView>
#include <QWidget>
#include <QObject>
#include <QSignalMapper>

#include "ui_mainwindow.h"
#include "advancedsettings.h"
#include "stdafx.h"
#include "ProgressDialog.h"
#include "TermWeightWindow.h"
///The MainWindow to be seen on startup
/**	Contains fields for the necessary input as well as a button to start the Pipeline and a button to open the AdvancedSettings and the TermWeightWindow
	Checks the fields for validity before starting the Pipeline and fixes the aspectratio of the BoundingBox and the output width and height with the RatioHelper
	Additionally forwards the progress to the ProgressDialog that is automatically opened when the Pipeline starts*/
class MainWindow : public QMainWindow, public Ui::MainWindow
{
	Q_OBJECT
		
public slots:
	///When the result of our worker thread comes in, we want to close the progression dialog
	/**	Will automatically be called once pipelines result is ready*/
	void HandleResults(const QString &);
	///Will change the text displayed in the progress dialog, if the dialog is currently active
	/**	isSubProg changes whether the text is displayed as main progress point or sub progress point
		If isSubProg is false, potentially still existing sub texts will be cleaned up*/
	void DisplayProgress(QString text, bool isSubProg = false);
	///The slot called when the marker's position has been changed, will deliver the coordinates as QString; Reflects the changes in the input fields of the mainwindow
	Q_INVOKABLE void JavaScriptEventMarker(const QString &coordinates);
	///The slot called when the bounding box is updated, the two strings contain the two edge points of the rectangle (i.e. northeast and southwest corners); Reflects the changes in the input fields of the mainwindow
	Q_INVOKABLE void JavaScriptEventRectangle(const QString &firstPoint, const QString &secondPoint);

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();
	///ApplyChanges is the method to set the fields of the mainwindow to the values currently set in the OptionFile
	/**	Will overwrite current values, if the OptionFile values differ from them
		Is to be called once pre-made settings are used in the advanced settings*/
	void ApplyChanges();
protected:
	///Should call the ApplyChanges() method, does not work however
	void focusInEvent(QFocusEvent *event);
	/// @cond
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	/// @endcond
private slots:
	///Evaluates the input for vadility, starts the algorithm and opens the progression dialog
	void CheckInputAndExecute();
	///Opens the a new window with advanced settings
	/**	See AdvancedSettings*/
	void OpenAdvancedSettings();
	///Loads the webcontent
	void InitialiseMap();
	///Updates the map according to changes made to the line edit
	void UpdateMarker();
	///Updates the bounding box rectangle according to the changes made to the line edit fields
	void UpdateRect();
	///Method to fix the aspect ratio to the bounding boxes' aspect ratio
	/**	The height adjusted value is to be set according to these three options
			-> only the height has been adjusted, set it to 1
			-> only the width has been adjusted, set it to -1
			-> both values have been adjusted, set it to 0 */
	void FixRatio(int heightAdjusted = 0);
private:
	///Parses the QString obtained with the Javascriptevents into a PointInSpace that contains the corresponding values
	PointInSpace ParseMapString(const QString &pos);
	//UI stuff
	///The associated UI
	Ui::MainWindow ui;
	///The associated windows
	AdvancedSettings settings;
	ProgressDialog prog;
	TermWeightWindow term;
	//other stuff
	//Pipeline pipe;
	std::string prev;
//	OutputBroker broke;
	///Contains the Pipeline to be running
	QThread *workerThread;
	///Method to center the map on the rectangle
	void CenterOnRectangle();
	//The view containing the map displayed for the user to interact with
	//QWebView *mapView;
	///Validator to make sure double values are passed on to the line edit
	QDoubleValidator valid;
	///Used to pass args to slots
	QSignalMapper * mapper;
};

#endif // MAINWINDOW_H
