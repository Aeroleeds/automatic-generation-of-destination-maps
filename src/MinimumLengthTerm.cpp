#include "StdAfx.h"
#include "MinimumLengthTerm.h"
#include "OptionFile.h"
#include <algorithm>
#include "RatioHelper.h"

MinimumLengthTerm::MinimumLengthTerm(void)
{
	BoundingBox b = OptionFile::bb;
	//make a diagonal edge, and get its distance
	Node * one = new Node(b.negLong, b.negLat, 0);
	Node * two = new Node(b.posLong, b.posLat, 1);
	Edge * edge = new Edge(one, two, 1, "df");
	/*	fixing the latitude and just using the longitude values we get the distance over the width of the area
		out of this we can compute the equivalent of 25 mm in metres distance
		which is then used to compare the edge length against
	*/
	//TODO: edges are used here the same way osm uses them (i.e. everything between two val != 2 nodes belongs to the same edge)
	//this->minDist = edge->GetDistance() / OptionFile::outputWidth * 25;
	//the diagonal of our output format
	double outputDia = sqrt(OptionFile::outputHeight * OptionFile::outputHeight + OptionFile::outputWidth * OptionFile::outputWidth);
//	this->rlToMapFactor = outputDia / edge->GetDistance();
	this->minDist = 25;
	delete edge;
	delete one;
	delete two;
}


MinimumLengthTerm::~MinimumLengthTerm(void)
{
}

double MinimumLengthTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	
	double currentLength = e->GetDistance() * RatioHelper::GetRLtoMapFactor();
	double ret = currentLength >= minDist ? 0 : minDist - currentLength;
	return ret * ret;
}

std::string MinimumLengthTerm::ToString(){
	return std::string("Cost Function : Minimum Length term");
}

std::string MinimumLengthTerm::GetIdentifier(){
	return std::string("minLength");
}

double MinimumLengthTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		//the weights are reduced to 100, for we compute the mindistance in milimetres and not in centimetres
		//to make this mathematically sound we take the factor 10 that is used to convert the centi- into milimetres from the weights
		//since all we do is sum everyth. up, this should work out just fine
		return OptionFile::minLengOne;
	case 2:
		return OptionFile::minLengTwo;
	default:
		return -1;
	}
}

