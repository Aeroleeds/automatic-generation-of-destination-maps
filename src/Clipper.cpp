#include "StdAfx.h"
#include "Clipper.h"
#include "FalseIntersectionHelper.h"
#include "Line.h"

Clipper::Clipper(void)
{
}


Clipper::~Clipper(void)
{
}


//TODO: REFACTOR
std::string Clipper::PrepDoc(MyData * data){
	BoundingBox bb = OptionFile::bb;
	Node * southEast = new Node(bb.negLong, bb.negLat, 0);
	Node * southWest = new Node(bb.negLong, bb.posLat, 0);
	Node * northEast = new Node(bb.posLong, bb.negLat, 0);
	Node * northWest = new Node(bb.posLong, bb.posLat, 0);
	//the edges to clip against
	Edge * east = new Edge(southEast, northEast, 12, "oregon");
	Edge * north = new Edge(northEast, northWest, 12, "wisconsin");
	Edge * west = new Edge(northWest, southWest, 12, "connecticut");
	Edge * south = new Edge(southWest, southEast, 12, "texas");

	std::vector<Edge *> toClipAgainst;
	toClipAgainst.push_back(east);
	toClipAgainst.push_back(west);
	toClipAgainst.push_back(south);
	toClipAgainst.push_back(north);
	//a vector containing all the edges that lead outside our box
	std::vector<Edge *> toClip;
	//and a vector for all edges that are to be added
	std::vector<Edge *> toAdd;
	//and one for the deletions
	std::vector<unsigned int> toDelete;
	//and check which ones are partially outside
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		//if one node is outside and the other inside, we need to clip it
		if(bb.IsInside(data->GetEdgeList().at(i)->GetStartNode()) != bb.IsInside(data->GetEdgeList().at(i)->GetEndNode())){
			toClip.push_back(data->GetEdgeList().at(i));
			toDelete.push_back(data->GetEdgeList().at(i)->GetEdgeID());
		}
		//if both are outside, delete em
		else if(!bb.IsInside(data->GetEdgeList().at(i)->GetStartNode()) && !bb.IsInside(data->GetEdgeList().at(i)->GetEndNode())){
			toDelete.push_back(data->GetEdgeList().at(i)->GetEdgeID());
		}
	}
	FalseIntersectionHelper help;
	//then clip the ones that are partially outside against the border
	for(unsigned int i = 0; i < toClip.size(); i++){
		Edge * temp = toClip.at(i);
		//clip against all four lines
		for(unsigned int j = 0; j < toClipAgainst.size(); j++){
			if(help.IsFalseIntersection(toClipAgainst.at(j), temp)){
				//get the inside node
				Node * inside = nullptr;
				if(bb.IsInside(temp->GetStartNode())){
					inside = temp->GetStartNode();
				}else{
					inside = temp->GetEndNode();
				}
				Line line(toClipAgainst.at(j)->GetStartNode(), toClipAgainst.at(j)->GetEndNode());
				Node * insec = line.IntersectionPoint(Line(temp->GetStartNode(), temp->GetEndNode()), temp->GetOtherNode(inside)->GetNodeID());
				Edge * edge = nullptr;
				//this is actually not necessary, but lets maintain the order of nodes
				if(*temp->GetStartNode() == *inside){
					edge = new Edge(inside, insec, temp->GetMaxSpeed(), temp->GetStreetName());
				}else{
					edge = new Edge(insec, inside, temp->GetMaxSpeed(), temp->GetStreetName());
				}
				edge->SetStreetType(temp->GetStreetType());
				toAdd.push_back(edge);
				//delete the original edge from the data set
				//data->RemoveAndDeleteEdge(data->GetEdgeByID(temp->GetEdgeID()));
				break; 
			}
		}
	}
	//delete all edges with one or more nodes outside the bounding box (the edges that are both in and outside are replaced by the ones that are just inside)
	for(unsigned int i = 0; i < toDelete.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(toDelete.at(i)));
	}
	//then delete nodes with val == 0
	std::vector<unsigned int> deleteNodes;
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		if(data->GetNodeList().at(i)->GetValence() == 0){
			deleteNodes.push_back(data->GetNodeList().at(i)->GetNodeID());
		}
	}
	for(unsigned int i = 0; i < deleteNodes.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(deleteNodes.at(i)));
	}
	//add the new edges
	data->AddEdgeList(toAdd);

	//Cleanup
	delete east;
	delete west;
	delete south;
	delete north;
	delete southEast;
	delete southWest;
	delete northEast;
	delete northWest;
	//return happiness
	return std::string("Splendid");
}
///Returns a string to identify this PipelinePart
std::string Clipper::ToString(){
	return std::string("Clipper");
}
///SetUp function needs be called before using this Object
void Clipper::SetUp(){

}

void Clipper::MoveNodeToNode(Node * toMove, Node * goal){
	double newDirectionX = (goal->GetLon() - toMove->GetLon()) * 0.98;
	double newDirectionY = (goal->GetLat() - toMove->GetLat()) * 0.98;
	while(!OptionFile::bb.IsInside(toMove)){
		//Clipper uses the merc values
		toMove->SetLon(toMove->GetLon() + newDirectionX);
		toMove->SetLat(toMove->GetLat() + newDirectionY);
	}
}


