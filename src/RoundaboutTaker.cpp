#include "StdAfx.h"
#include "RoundaboutTaker.h"
#include "RoundaboutHelper.h"

RoundaboutTaker::RoundaboutTaker(void)
{
}


RoundaboutTaker::~RoundaboutTaker(void)
{
}


std::string RoundaboutTaker::PrepDoc(MyData * data){
	/*if(!OptionFile::useRoundaboutOptimiser){ return true;}
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(data->GetEdgeList().at(i)->IsRoundabout() && data->GetEdgeList().at(i)->IsTaken()){
			this->TakeRoundabout(data->GetEdgeList().at(i)->GetStartNode(), data);
			this->TakeRoundabout(data->GetEdgeList().at(i)->GetEndNode(), data);
		}

	}*/
	std::vector<RoundaboutHelper::Roundabout> roundies;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(data->GetEdgeList().at(i)->IsRoundabout() && data->GetEdgeList().at(i)->IsTaken()){
			bool alreadyVisited = false;
			//check whether we already know about the roundy containing that edge
			for(unsigned int x = 0; x < roundies.size(); x++){
				if(roundies.at(x).ContainsEdge(data->GetEdgeList().at(i))){
					alreadyVisited = true;
					break;
				}
			}
			if(!alreadyVisited){
				roundies.push_back(RoundaboutHelper::ConstructRoundabout(data->GetEdgeList().at(i), data));
			}
		}
	}
	//then evaluate all roundabouts that we found
	for(unsigned int i = 0; i < roundies.size(); i++){
		// add context by adding all edges of the roundabout
		for(unsigned int x = 0; x < roundies.at(i).edges.size(); x++){
			roundies.at(i).edges.at(x)->SetTaken(true);
		}
		//containers for the nodes which have an edge not belonging to the RA, and one for all unique, taken streetnames
		std::set<std::string> streetNamesFromRoundy;
		std::vector<Node *> leavingNodes;
		for(unsigned int x = 0; x < roundies.at(i).nodes.size(); x++){
			if(RoundaboutHelper::HasNonRoundaboutEdges(roundies.at(i).nodes.at(x), data)){
				//if it has a non-roundabout edge, it is a leavingNodes
				leavingNodes.push_back(roundies.at(i).nodes.at(x));
				if(RoundaboutHelper::GetNonRoundEdge(roundies.at(i).nodes.at(x), data)->IsTaken()){
					//if that leaving edge is additionally taken, insert it to our set
					streetNamesFromRoundy.insert(RoundaboutHelper::GetNonRoundEdge(roundies.at(i).nodes.at(x), data)->GetStreetName());
				}
			}
		}
		//if our set only contains one entry, all leaving streets share the same name (thus we do not add context)
		if(streetNamesFromRoundy.size() == 1){
			continue;
		}else{
			std::map<unsigned int, unsigned int> nodeIDToCount;
			//take all the ways up to their first non val == 2 node
			for(unsigned int x = 0; x < leavingNodes.size(); x++){
				Node * three = this->TakeEdgesUntilValThree(leavingNodes.at(x), RoundaboutHelper::GetNonRoundEdge(leavingNodes.at(x), data), data);
				if(nodeIDToCount.count(three->GetNodeID()) == 0){
					nodeIDToCount.insert(std::pair<unsigned int, unsigned int>(three->GetNodeID(), 1));
				}else{
					nodeIDToCount[three->GetNodeID()] = nodeIDToCount.at(three->GetNodeID()) + 1;
				}
			}
			//then iterate over the map, if a node appeared more than once, add an additional edge at that node (rly important for subsequent algorithms)
			std::map<unsigned int, unsigned int>::iterator it;
			for(it = nodeIDToCount.begin(); it != nodeIDToCount.end(); it ++){
				if(it->second > 1){
					for(unsigned int x = 0; x < data->GetEdgesByNodeID(it->first).size(); x++){
						data->GetEdgesByNodeID(it->first).at(x)->SetTaken(true);
					}
				}
			}
		}
	}

	return std::string("Splendid");
}

Node * RoundaboutTaker::TakeEdgesUntilValThree(Node * node, Edge * edge, MyData * data){
	Node * tempNode = edge->GetOtherNode(node);
	Edge * tempEdge = edge;
	while(tempNode->GetValence() == 2){
		tempEdge->SetTaken(true);
		tempEdge = *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) == * tempEdge ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0);
		tempNode = tempEdge->GetOtherNode(tempNode);
	}
	tempEdge->SetTaken(true);
	return tempNode;
}

/*
std::vector<Edge *> RoundaboutTaker::GetLeavingEdges(Edge * roundEdge, MyData * data){
	std::vector<Edge *> ret;
	if(!roundEdge->IsRoundabout()) return ret;
	Node * temp = roundEdge->GetEndNode();
	Edge * tempEdge = roundEdge;
	while(temp->IsTaken() && *temp != *roundEdge->GetStartNode()){

	}
}*/
/*
Node * RoundaboutTaker::GetNextValThree(Edge * edge, Node * start, MyData * data){
	if(start->GetValence() == 3) return start;
	Node * temp = start;
	Edge * tempEdge = *data->GetEdgesByNodeID(start->GetNodeID()).at(0) == *edge ? data->GetEdgesByNodeID(start->GetNodeID()).at(1) : data->GetEdgesByNodeID(start->GetNodeID()).at(0);
	temp = tempEdge->GetOtherNode(temp);
	while(temp->GetValence() == 2){
		tempEdge = *data->GetEdgesByNodeID(start->GetNodeID()).at(0) == *tempEdge ? data->GetEdgesByNodeID(start->GetNodeID()).at(1) : data->GetEdgesByNodeID(start->GetNodeID()).at(0);
		temp = tempEdge->GetOtherNode(temp);
	}
	return temp;
}
*/
/*
Edge * RoundaboutTaker::GetNextEdge(Node * node, Edge * prevEdge, bool shouldBeRoundabout, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout() == shouldBeRoundabout && *data->GetEdgesByNodeID(node->GetNodeID()).at(i) != *prevEdge) return data->GetEdgesByNodeID(node->GetNodeID()).at(i);
	}
	return nullptr;
}*/

std::string RoundaboutTaker::ToString(){
	return std::string("DataWallE");
}

void RoundaboutTaker::SetUp(){

}

void RoundaboutTaker::TakeRoundabout(Node * node, MyData * data){
	std::vector<Edge *> edges = data->GetEdgesByNodeID(node->GetNodeID());
	for(unsigned int i = 0; i < edges.size(); i++){
		//if an edge from the roundabout node is not yet taken despite being a roundabout edge itself, take it! (and all its neighbouring edges/nodes)
		if(edges.at(i)->IsRoundabout() && !edges.at(i)->IsTaken()){
			edges.at(i)->SetTaken(true);
			//recursively proceed
			TakeRoundabout(edges.at(i)->GetOtherNode(node), data);
		}
		//if the edge is not a roundabout edge take it (we want stumps leading away from our roundabout at all costs)
		else if(!edges.at(i)->IsRoundabout() && !edges.at(i)->IsTaken()){
			edges.at(i)->SetTaken(true);
			//if the way out of the roundabout is not simply a road but some triangle-ish construct, take the edge behind it as well (it will later be merged into a simple edge)
			if(edges.at(i)->GetOtherNode(node)->GetValence() != 2){
				std::vector<Edge *> tempEdges = data->GetEdgesByNodeID(edges.at(i)->GetOtherNode(node)->GetNodeID());
				for(unsigned int x = 0; x < tempEdges.size(); x++){
					tempEdges.at(x)->SetTaken(true);
				}
			}
		}
	}
}


