#pragma once
#include "MyData.h"
#include "Edge.h"
#include "RoadLayoutData.h"
#include "OptionFile.h"
///A CostTerm evaluates a RoadLayoutData set with regards to a specific aspect and assesses a cost for it
class CostTerm
{
public:
	CostTerm(void);
	virtual ~CostTerm(void) {};
	///a string representation of the CostTerm
	virtual std::string ToString() = 0;
	///the identifier is used in the map of the Costfunction (with it the costfunction can add the return values to the right sum)
	/**	Needs be unique among CostTerms */
	virtual std::string GetIdentifier() = 0;

	//all possibly needed other objects need be set for the instance individually, needs be multiplied by the weights
	///Returns the cost associated with the specific Edge e
	/**	The cost is still to be multiplied by the CostTerm's weight */
	virtual double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data) = 0;
	///for any costterms that can only return their cost after all the Edges have been considered, the PostCost method is called after every single Edge has been evaluated with GetCost() (e.g. used in RelativeOrientationTerm)
	/**	Will be called at all CostTerms, so if needed, simply return the appropriate value here (and implement the PostCost method, duh!)
		As with the GetCost() method, results obtained from here need still be multiplied by the weight*/
	virtual double PostCost(){return 0;};
	///returns the weight to be used as multiplier for the given suffix
	/**	If the suffix is invalid (e.g. incremented after the last set of valid weights) -1 is returned */
	virtual double GetWeight(int suffix) = 0;
	///Function used after every computation
	/**	Overwrite if something within the costterm needs resetting
		Needs not be overwritten! */
	virtual void ResetTerm(){return;}
};

