#pragma once
#include "stdafx.h"
#include "advancedsettings.h"
#include "DebugOptions.h"
#include <qfiledialog.h>
#include "EventBroker.h"
#include "OptionFile.h"

AdvancedSettings::AdvancedSettings(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	valid.setNotation(QDoubleValidator::StandardNotation);

	connect(ui.ApplyButton, SIGNAL(clicked()), this, SLOT(ApplySettings()));
	connect(ui.FilePathButton, SIGNAL(clicked()), this, SLOT(OpenFilePicker()));

	for(unsigned int i = 0; i < OptionFile::NumPipeValues; i++){
		if(i == OptionFile::NumPipeValues){
			break;
		}
		ui.PipelineModeBox->addItem(OptionFile::pipelineSetupStrings[i].c_str());
	}
	for(unsigned int i = 0; i < numValues; i++){
		if(i == numValues){
			break;
		}
		ui.UnusedBox->addItem(streetStrings[i].c_str());
	}
	for(unsigned int i = 0; i < DebugOptions::numDebugValues; i++){
		if(i == DebugOptions::numDebugValues){
			break;
		}
		ui.PrefabBox->addItem(DebugOptions::debugSettingsStrings[i].c_str());
	}

	//and set the default values for the combo boxes
	ui.PipelineModeBox->setCurrentIndex(OptionFile::All);
	ui.UnusedBox->setCurrentIndex(service);
	ui.PrefabBox->setCurrentIndex(DebugOptions::NoDebug);
	ui.ServerKeyLine->setText(OptionFile::serverKey.c_str());
	ui.UseExactPositionBox->setChecked(OptionFile::useExactDestinationForVisRings);

	ui.VisRingCountEdit->setText(QString(std::to_string((long double) OptionFile::stepsAtVisibilityRings).c_str()));
	ui.VisRingCountEdit->setValidator(&valid);
}

AdvancedSettings::~AdvancedSettings()
{

}

void AdvancedSettings::ShowSettings(){
	this->show();
	ui.SaveAsLine->setText(OptionFile::saveAs.c_str());
	ui.ServerKeyLine->setText(OptionFile::serverKey.c_str());
}

void AdvancedSettings::HideSettings(){
	this->setVisible(false);
}

void AdvancedSettings::ApplySettings(){
	std::string saveName = ui.SaveAsLine->text().toUtf8().toStdString();
	if(saveName != std::string()){
		OptionFile::saveAs = saveName;
	}
	if(ui.LoadFileCheckBox->isChecked()){
		std::string loadName = ui.LoadFileLine->text().toUtf8().toStdString();
		OptionFile::loadFile = loadName;
	}else{
		//if the load file is not checked, make sure the replace the current loadName w/string(), to prevent loading
		OptionFile::loadFile = std::string();
		ui.LoadFileLine->setText("");
	}
	if(ui.ServerKeyLine->text().toUtf8().toStdString() != std::string()){
		OptionFile::serverKey = ui.ServerKeyLine->text().toUtf8().toStdString();
	}
	OptionFile::nonUsedType = ui.UnusedBox->currentIndex();
	OptionFile::pipeSet = (OptionFile::PipelineSetup) ui.PipelineModeBox->currentIndex();
	if(ui.PrefabBox->currentIndex() != DebugOptions::NoDebug){
		DebugOptions::SetDebugSettings((DebugOptions::DebugSettings)ui.PrefabBox->currentIndex());
		BoundingBox bb;
		bb.posLat = OptionFile::bb.posLat;
		bb.negLat = OptionFile::bb.negLat;
		bb.posLong = OptionFile::bb.posLong;
		bb.negLong = OptionFile::bb.negLong;
		EventBroker::SendRefreshEvent();
	}
	//And read the checkboxes for the experimental cost terms
	OptionFile::useAreaControl = ui.AreaControlBox->isChecked();
	OptionFile::useRelRelLeng = ui.RelRelTermBox->isChecked();
	OptionFile::useExactDestinationForVisRings = ui.UseExactPositionBox->isChecked();
	//if theres a double in the visrings count, apply it, else ignore
	bool correctInput;
	double steps = ui.VisRingCountEdit->text().toDouble(& correctInput);
	if(correctInput && steps >= 0){
		OptionFile::stepsAtVisibilityRings = steps;
	}else{
		//Reset the field to display the current value
		ui.VisRingCountEdit->setText(QString(std::to_string((long double) OptionFile::stepsAtVisibilityRings).c_str()));
	}

	this->hide();
}

void AdvancedSettings::OpenFilePicker(){

	QString path = QFileDialog::getOpenFileName(this, tr("Choose Data File"), QString(), tr("XML Files (*.xml);;All Files (*.*)"));//, "/home", tr("XML Files (*.xml)"));
	ui.LoadFileLine->setText(path);
}

