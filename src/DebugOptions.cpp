#include "StdAfx.h"
#include "DebugOptions.h"
#include "XmlHelper.h"
#include "OptionFile.h"

std::string DebugOptions::debugSettingsStrings[] = {"Parndorf", "Bad Brueckenau - Fahrradmuseun", "Kiskunfélegyháza", "Oftringen - Blumen Schlaefli", "Chase USA", "Dodoma - Tanzania", "Pleasant Point - New Zealand", "Paide - Estonia", "Belatai - India", "Bollnas - Sweden", "St George - Utah", "Odessa - Friday Night Lights", "Princeton", "Charleston - South Carolina Aquarium", "No Premade Settings"};
DebugOptions::DebugSettings DebugOptions::chosenDebugSettings = DebugOptions::numDebugValues;

DebugOptions::DebugOptions(void)
{
}


DebugOptions::~DebugOptions(void)
{
}

void DebugOptions::SetDebugSettings(DebugSettings set){
	switch(set){

	case Parndorf :
		OptionFile::bb.negLong = 16.822;
		OptionFile::bb.negLat = 47.9671;
		OptionFile::bb.posLong = 16.9154;
		OptionFile::bb.posLat = 48.0413;

		OptionFile::destination.lat = 47.995743;
		OptionFile::destination.longi = 16.849078;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;

		break;
	case Bad_Brueckenau:
		OptionFile::bb.negLong = 9.7414;
		OptionFile::bb.negLat = 50.2918;
		OptionFile::bb.posLong = 9.8788;
		OptionFile::bb.posLat = 50.3411;

		OptionFile::destination.lat = 50.303307;
		OptionFile::destination.longi = 9.744447;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Kiskunfelegyhaza:
		OptionFile::bb.negLong = 19.5104;
		OptionFile::bb.negLat = 46.6270;
		OptionFile::bb.posLong = 20.0597;
		OptionFile::bb.posLat = 46.8387;

		OptionFile::destination.lat = 46.717159;
		OptionFile::destination.longi = 19.834059;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Oftringen:
		OptionFile::bb.negLong = 7.8906;
		OptionFile::bb.negLat = 47.2984;
		OptionFile::bb.posLong = 7.9593;
		OptionFile::bb.posLat = 47.3245;

		OptionFile::destination.lat = 47.316106;
		OptionFile::destination.longi = 7.915122;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Chase:
		OptionFile::bb.negLong = -88.28634;
		OptionFile::bb.negLat = 44.65866;
		OptionFile::bb.posLong = -88.01168;
		OptionFile::bb.posLat = 44.7684;

		OptionFile::destination.lat = 44.724040;
		OptionFile::destination.longi = -88.151750;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Dodoma:
		OptionFile::bb.negLong = 35.68861;
		OptionFile::bb.negLat = -6.21659;
		OptionFile::bb.posLong = 35.82594;
		OptionFile::bb.posLat = -6.13979;

		OptionFile::destination.lat = -6.147077;
		OptionFile::destination.longi = 35.723727;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case PleasantPoint:
		OptionFile::bb.negLong = 170.98576;
		OptionFile::bb.negLat = -44.36835;
		OptionFile::bb.posLong = 171.53508;
		OptionFile::bb.posLat = -44.14705;

		OptionFile::destination.lat = -44.198016;
		OptionFile::destination.longi = 171.154584;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Paide:
		OptionFile::bb.negLong = 25.43267;
		OptionFile::bb.negLat = 58.86010;
		OptionFile::bb.posLong = 25.70733;
		OptionFile::bb.posLat = 58.9399;

		OptionFile::destination.lat = 58.882841;
		OptionFile::destination.longi = 25.565114;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Belatai:
		OptionFile::bb.negLong = 79.051376;
		OptionFile::bb.negLat = 24.93963;
		OptionFile::bb.posLong = 80.150009;
		OptionFile::bb.posLat = 25.49871;

		OptionFile::destination.lat = 25.252763;
		OptionFile::destination.longi = 79.563657;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;
		break;
	case Bollnas:
		OptionFile::bb.negLong = 16.322640;
		OptionFile::bb.negLat = 61.33670;
		OptionFile::bb.posLong = 16.45996;
		OptionFile::bb.posLat = 61.3737;

		OptionFile::destination.lat = 61.3643915;
		OptionFile::destination.longi = 16.386313;

		OptionFile::outputWidth = 250;
		OptionFile::outputHeight = 297;

		break;
	case StGeorge:
		OptionFile::bb.negLong = -113.636;
		OptionFile::bb.negLat = 37.0349;
		OptionFile::bb.posLong = -113.492;
		OptionFile::bb.posLat = 37.1756;

		OptionFile::destination.lat = 37.1074;
		OptionFile::destination.longi = -113.546;

		break;
	case Odessa:
		OptionFile::bb.negLong = -102.597;
		OptionFile::bb.negLat = 31.7749;
		OptionFile::bb.posLong = -102.416;
		OptionFile::bb.posLat = 31.909;

		OptionFile::destination.lat = 31.8568;
		OptionFile::destination.longi = -102.519;

		break;
	case Princeton:
		OptionFile::bb.negLong = -74.6885;
		OptionFile::bb.negLat = 40.3219;
		OptionFile::bb.posLong = -74.6191;
		OptionFile::bb.posLat = 40.3962;

		OptionFile::destination.lat = 40.3694;
		OptionFile::destination.longi = -74.6407;

		break;
	case Charleston:
		OptionFile::bb.negLong = -79.9599;
		OptionFile::bb.negLat = 32.7766;
		OptionFile::bb.posLong = -79.9255;
		OptionFile::bb.posLat = 32.8068;

		OptionFile::destination.lat = 32.7904;
		OptionFile::destination.longi = -79.9275;

		break;
	default :
		//do nothing if nothing is to be done
		break;


	}
}
