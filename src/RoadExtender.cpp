#include "StdAfx.h"
#include "RoadExtender.h"


RoadExtender::RoadExtender(void)
{
	//TODO: make sure we need this
	endNodes;
	startNodes;
}


RoadExtender::~RoadExtender(void)
{
}

std::string RoadExtender::ToString(){
	return std::string("RoadExtender");
}

void RoadExtender::SetUp(){

}

std::string RoadExtender::PrepDoc(MyData * data){
	/*
			PART I: extend all taken edges up to 1.500 metres or until their name changes
	*/
	std::vector<Edge *> edgeList;
	std::set<unsigned int> edgesToBeTaken;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(!data->GetEdgeList().at(i)->IsTaken()){
			continue;
		}
		//every already taken road is flagged as ring road for purposes defying the original ring road
		data->GetEdgeList().at(i)->SetRingRoad(true);
		//get ALL edges of given name and streettype at the current node
		std::vector<Edge *> edgesToFollow = this->GetNextEdges(data->GetEdgeList().at(i)->GetStartNode(), data->GetEdgeList().at(i), data);
		for(unsigned int x = 0; x < edgesToFollow.size(); x++){
			//Edge * temp = GetNextEdge(data->GetEdgeList().at(i)->GetStartNode(), data->GetEdgeList().at(i), data);
			Edge * temp = edgesToFollow.at(x);
			if(temp != nullptr && !temp->IsTaken()){
				std::set<unsigned int> tempSet = FollowAndReturnRoad(temp, data->GetEdgeList().at(i)->GetStartNode(), data);
				edgesToBeTaken.insert(tempSet.begin(), tempSet.end());
			//we need not extend link roads, for, if they connect to a valid, taken street, that other street will be extended in their stead, thus preventing weird results
			}else if(temp == nullptr && !(data->GetEdgeList().at(i)->IsLinkRoad())){
				//if no next edge was found, take contextual information
				std::vector<Edge *> edges = data->GetEdgesByNodeID(data->GetEdgeList().at(i)->GetStartNode()->GetNodeID());
				for(unsigned int j = 0; j < edges.size(); j++){
					//THIS NEEDS TO GO


					//TODO: Verify this is what we want!!!!!



					edgesToBeTaken.insert(edges.at(j)->GetEdgeID());
				}
			}
		}
		//the clear should be unncessary, but hey, safety first kids
		edgesToFollow.clear();
		edgesToFollow = this->GetNextEdges(data->GetEdgeList().at(i)->GetEndNode(), data->GetEdgeList().at(i), data);
		for(unsigned int x = 0; x < edgesToFollow.size(); x++){
			Edge * temp = edgesToFollow.at(x);
			//temp = GetNextEdge(data->GetEdgeList().at(i)->GetEndNode(), data->GetEdgeList().at(i), data);
			if(temp != nullptr && !temp->IsTaken()){
				std::set<unsigned int> tempSet = FollowAndReturnRoad(temp, data->GetEdgeList().at(i)->GetEndNode(), data);
				edgesToBeTaken.insert(tempSet.begin(), tempSet.end());
			}else if(temp == nullptr && !(data->GetEdgeList().at(i)->IsLinkRoad())){
				//if no next edge was found, take contextual information
				std::vector<Edge *> edges = data->GetEdgesByNodeID(data->GetEdgeList().at(i)->GetEndNode()->GetNodeID());
				for(unsigned int j = 0; j < edges.size(); j++){
					//THIS NEEDS TO GO


					//TODO: Verify this is what we want!!!!!





					edgesToBeTaken.insert(edges.at(j)->GetEdgeID());
				}
			}
		}
	}

	std::set<unsigned int>::iterator it;
	for(it = edgesToBeTaken.begin(); it != edgesToBeTaken.end(); ++it){
		data->GetEdgeByID(*it)->SetTaken(true);
	}

	//looping through all the nodes, and extending any taken roads coming from taken nodes
	/*
	std::vector<Node *> nlist = data->GetNodeList();
	unsigned int size = nlist.size();
	for(unsigned int i = 0; i < size; i++){
		//TODO: looping through the sets probably is slow, may be a better solution is possible here
		if(!nlist.at(i)->IsTaken() || endNodes.find(nlist.at(i)) != endNodes.end() || startNodes.find(nlist.at(i)) != startNodes.end()){
			continue;
		}
		std::vector<Edge *> edgeList = data->GetEdgesByNodeID(nlist.at(i)->GetNodeID());
		unsigned int edgeListSize = edgeList.size();
		for(unsigned int j = 0; j < edgeListSize; j++){
			if(!edgeList.at(j)->IsTaken()){
				continue;
			}
			//get the last node that is still taken
			Node * currentNode = FollowTakenEdges(nlist.at(i), edgeList.at(j), data);
			//and if its one of the already visited start/endNodes, stop right there
			if(endNodes.find(currentNode) != endNodes.end() || startNodes.find(currentNode) != startNodes.end()){
				continue;
			}else{
				//mark our Node as one of the startNodes
				startNodes.insert(currentNode);
				//then follow our street and take the edges and nodes in it; return the last node
				currentNode = this->FollowAndTakeEdges(currentNode, edgeList.at(j)->GetStreetName(), data);
				//and write the node into our endNodes
				endNodes.insert(currentNode);
			}
		}
	}
	*/
	/*
			PART II: reduce the roads back till we reach an endNode or a ringroad
	*/
	//iterate over all endNodes
	/*
	std::set<Node *>::iterator it;
	for(it = endNodes.begin(); it != endNodes.end(); it++){
		std::vector<Edge *> edgeList = data->GetEdgesByNodeID((*it)->GetNodeID());
		size = edgeList.size();
		//then iterate over all edges leading from our Node, and follow ALL edges that are taken
		for(unsigned int i = 0; i < size; i++){
			if(!edgeList.at(i)->IsTaken()){
				continue;
			}
			Node * n = *it;
			Edge * e = edgeList.at(i);
			//while we have not reached a ringroad or one of the original startnodes keep on deleting edges 
			//(always leave a stub though, in order to show types of junctions, which is why we have our conditions set to the next Node and not the current one)
			while(!e->GetOtherNode(n)->IsRingRoad() || startNodes.find(e->GetOtherNode(n)) == startNodes.end()){
				n->SetTaken(false);
				e->SetTaken(false);
				n = e->GetOtherNode(n);
				e = this->GetNextEdge(n, e, data);
				if(e == nullptr){
					break;
				}
			}
		}
	}
	*/
	return std::string("Splendid");
}

std::set<unsigned int> RoadExtender::FollowAndReturnRoad(Edge * edge, Node * node, MyData * data){
	std::set<unsigned int> toReturn;
	//the max length to follow the road (1.500 meters is the standard way to go)
	unsigned int referenceLength = 1500;
	Node * temp = node;
	for(unsigned int length = 0; length < referenceLength;){
		temp = edge->GetOtherNode(temp);
		length += edge->GetDistance();
		toReturn.insert(edge->GetEdgeID());
		edge = GetNextEdge(temp, edge, data);
		if(edge == nullptr || toReturn.count(edge->GetEdgeID()) != 0){
			break;
		}
	}
	return toReturn;
}

Node * RoadExtender::FollowTakenEdges(Node * n, Edge * e, MyData * data){
	Node * returned = nullptr;
	n = e->GetOtherNode(n);
	std::set<unsigned int> visitedNodes;
	while(returned == nullptr){
		//if we reached a start or end node return it, if we have a circular road, do the same thing 
		if(endNodes.find(n) != endNodes.end() || startNodes.find(n) != startNodes.end() || visitedNodes.count(n->GetNodeID()) != 0){
			returned = n;
		}else{
			visitedNodes.insert(n->GetNodeID());
			e = GetNextEdge(n, e, data);
			//if the road w/this name ends here, return the last node
			if(e == nullptr){
				returned = n;
			//if the next node is not taken, end it here
			}else if(!e->GetOtherNode(n)->IsTaken()){
				returned = n;
			}else{
				n = e->GetOtherNode(n);
			}
		}
	}
	return returned;
}

Node * RoadExtender::FollowAndTakeEdges(Node * n, std::string streetName, MyData * data){
	std::vector<Edge *> edges = data->GetEdgesByNodeID(n->GetNodeID());
	Edge * currentEdge = nullptr;
	unsigned int size = edges.size();
	//go look for the next edge (we look for a specified streetname, and check whether it's set as taken so far, if not, its our road
	for(unsigned int i = 0; i < size; i++){
		if(edges.at(i)->GetStreetName() == streetName && !edges.at(i)->IsTaken()){
			currentEdge = edges.at(i);
			break;			
		}
	}
	std::set<unsigned int> visitedNodes;
	if(currentEdge == nullptr){return n;}
	//save the traveled distance in metres, so we can interrupt our loop after 1500 metres
	unsigned int traveledDistance = 0;
	while(traveledDistance < 1500){
		if(visitedNodes.count(n->GetNodeID()) != 0){
			//return the previous node if we already have been at the current one
			return GetNextEdge(n, currentEdge, data)->GetOtherNode(n);
		}
		//travel along our edge, sum up their distances
		n = currentEdge->GetOtherNode(n);
		traveledDistance += currentEdge->GetDistance();
		//set our nodes and edges to taken
		n->SetTaken(true);
		currentEdge->SetTaken(true);
		//and take the next edge
		currentEdge = GetNextEdge(n, currentEdge, data);
		//if the current road ends here, return the latest node
		if(currentEdge == nullptr){
			return n;
		}
		visitedNodes.insert(n->GetNodeID());
	}
	return n;
}

Edge * RoadExtender::GetNextEdge(Node * n, Edge * e, MyData * data){
	std::vector<Edge *> edges = data->GetEdgesByNodeID(n->GetNodeID());
	unsigned int size = edges.size();
	for(unsigned int i = 0; i < size; i++){
		if(*edges.at(i) == *e){
			continue;
		//if our edge has the same name as the original edge,  but is not equal to our original edge, return it
			///TODO: Verify that the street type addition works as intended (i.e. no "default name" streets extending to lower street types)
		}else if(edges.at(i)->GetStreetName() == e->GetStreetName() && edges.at(i)->GetStreetType() == e->GetStreetType()){
			return edges.at(i);
		}
	}
	//if the street w/the specified name ends here, return it
	return nullptr;
}

std::vector<Edge *> RoadExtender::GetNextEdges(Node * n, Edge * e, MyData * data){
	std::vector<Edge *> toRet;
	std::vector<Edge *> edges = data->GetEdgesByNodeID(n->GetNodeID());
	unsigned int size = edges.size();
	for(unsigned int i = 0; i < size; i++){
		if(*edges.at(i) == *e){
			continue;
		//if our edge has the same name as the original edge,  but is not equal to our original edge, return it
			///TODO: Verify that the street type addition works as intended (i.e. no "default name" streets extending to lower street types)
		}else if(edges.at(i)->GetStreetName() == e->GetStreetName() && edges.at(i)->GetStreetType() == e->GetStreetType()){
			toRet.push_back(edges.at(i));
		}
	}
	//if the street w/the specified name ends here, return it
	return toRet;
}