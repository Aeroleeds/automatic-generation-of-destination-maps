#ifndef ADVANCEDSETTINGS_H
#define ADVANCEDSETTINGS_H

#include <QWidget>
#include "ui_advancedsettings.h"
#include "stdafx.h"

///Window for advanced settings to be set by the user
/**	Made settings are saved in the OptionFile
	Features to be set are:
		The name of the data file to be produced
		The name of a data file to be loaded (instead of downloading the data everytime)
		The severkey to be looked for in the SETTINGS.xml
		The highest not considered type of the algorithm
		Which stages of the pipeline are to be used during the current runthrough
		Whether a premade setting is to be used
		Whether the experimental CostTerms AreaControlTerm and RelativeRelativeLengthTerm are to be used
*/
class AdvancedSettings : public QWidget
{
	Q_OBJECT

public:
	AdvancedSettings(QWidget *parent = 0);
	~AdvancedSettings();
	///Opens up the advancedsettings window
	void ShowSettings();
	///Hides the advancedsettings widow (w/out saving any possible changes)
	void HideSettings();

private slots:
	///Saves the changes made in the advancedsettings window
	void ApplySettings();
	///Opens the file picker dialog for the LoadFile option
	void OpenFilePicker();
private:
	///The ui associated with the advancedsettings window
	Ui::AdvancedSettings ui;
	///Validator to make sure double values are passed on to the line edit
	QDoubleValidator valid;
};

#endif // ADVANCEDSETTINGS_H
