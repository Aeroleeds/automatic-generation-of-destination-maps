#pragma once
#include "StdAfx.h"
#include "Edge.h"

//PI definition according to cplusplus.com
#define PI 3.14159265

unsigned int Edge::nextEdgeID = 1;

std::map<std::string, unsigned int> Edge::createBoolMap(){
	std::map<std::string, unsigned int> mapRet;
	//bool whether it is a oneway edge
	mapRet.insert(std::make_pair("oneway", 0));
	return mapRet;
}

std::map<std::string, unsigned int> Edge::createStringMap(){
	std::map<std::string, unsigned int> mapRet;
	//streetname
	mapRet.insert(std::make_pair("name", 0));
	//alternate reference used as streetname (e.g. "B10")
	mapRet.insert(std::make_pair("ref", 1));
	return mapRet;
}

std::map<std::string, unsigned int> Edge::createIntMap(){
	std::map<std::string, unsigned int> mapRet;
	//streetname
	mapRet.insert(std::make_pair("maxspeed", 0));
	//alternate reference used as streetname (e.g. "B10")
	mapRet.insert(std::make_pair("ref", 1));
	return mapRet;
}

std::map<std::string, unsigned int> Edge::boolValueToIndex = createBoolMap();
std::map<std::string, unsigned int> Edge::stringValueToIndex = createStringMap();
std::map<std::string, unsigned int> Edge::intValueToIndex = createIntMap();


Edge * Edge::GetHighestOrder(std::vector<Edge *> eList){
	Edge * ret = nullptr;
	for(unsigned int i = 0; i < eList.size(); i++){
		if(ret == nullptr){
			ret = eList.at(i);
		}else if(!ret->IsHigherOrder(eList.at(i))){
			ret = eList.at(i);
		}
	}
	return ret;
}

Edge * Edge::GetLowestOrder(std::vector<Edge *> eList){
	Edge * ret = nullptr;
	for(unsigned int i = 0; i < eList.size(); i++){
		if(ret == nullptr){
			ret = eList.at(i);
		}else if(ret->IsHigherOrder(eList.at(i))){
			ret = eList.at(i);
		}
	}
	return ret;
}


Edge::Edge(Node * startNode, Node * endNode, unsigned int maxSpeed, std::string streetname){
	this->startNode = startNode;
	this->endNode = endNode;
	this->maxSpeed = maxSpeed;
	//inc edgeID for the next edge
	//this->edgeID = Edge::nextEdgeID++;
	this->streetName = streetname;
	this->oneWay = false;
	this->ringRoad = false;
	this->taken = false;
	this->roundabout = false;
	this->edgeID = nextEdgeID++;
	//this->distance = this->ComputeDistanceEquirec();
	this->distance = this->ComputeDistanceHaversine();
	this->timeTravel = this->ComputeTimeTravel();
}
Edge::Edge(Node * startNode, Node * endNode, unsigned int maxSpeed, std::string streetName, unsigned int edgeID){
	this->startNode = startNode;
	this->endNode = endNode;
	this->maxSpeed = maxSpeed;
	//inc edgeID for the next edge
	//this->edgeID = Edge::nextEdgeID++;
	this->streetName = streetName;
	this->oneWay = false;
	this->ringRoad = false;
	this->taken = false;
	this->roundabout = false;
	this->edgeID = edgeID;
	//this->distance = this->ComputeDistanceEquirec();
	this->distance = this->ComputeDistanceHaversine();
	this->timeTravel = this->ComputeTimeTravel();

}


Edge::~Edge(void){
	//do NOT delete the nodes here, since one node can be part of many edges, and we do not want to delete them multiple times
	//delete startNode, endNode;
	//decrease the valence of the nodes, for they lose an edge
	//if(!(startNode == nullptr)){
		//startNode->SetValence(startNode->GetValence() -1);
	//}
	//if(!(endNode == nullptr)){
		//endNode->SetValence(endNode->GetValence() -1);
	//}
}

/*if it's a one way road, this will be set to true, meaning that one can only travel from startNode to the endNode
	Take care when using, for the nodes need obviously be in the correct order, if it's a one-way street*/
bool Edge::IsOneWay(){return this->oneWay;}
void Edge::SetOneWay(bool newOneWay){this->oneWay = newOneWay;}
//Time travel is the time necessary to travel along the edge, in seconds
double Edge::GetTimeTravel(){return this->timeTravel;}
void Edge::SetTimeTravel(double newTimeTravel){this->timeTravel = newTimeTravel;}
std::string Edge::GetStreetName(){return this->streetName;}
//Distance between start and end node, in metres (compute w/the wicked formula gotten from the internet TODO: reference implementing class here)
unsigned int Edge::GetDistance(){return this->distance;}
void Edge::SetDistance(unsigned int newDistance){this->distance = newDistance;}
//maxSpeed in m/s
unsigned int Edge::GetMaxSpeed(){return this->maxSpeed;}
///set the max speed in km/h, it will be transformed to m/s for you
void Edge::SetMaxSpeed(unsigned int newMaxSpeed){this->maxSpeed = newMaxSpeed / 3.6;}
//Pointers to start and end nodes
Node * Edge::GetStartNode(){return this->startNode;}
Node * Edge::GetEndNode(){return this->endNode;}
void Edge::SetStartNode(Node * newStartNode){this->startNode = newStartNode;}
void Edge::SetEndNode(Node * newEndNode){this->endNode = newEndNode;}
//EdgeId that will be statically assigned in the constructor
unsigned int Edge::GetEdgeID(){return this->edgeID;}
void Edge::SetStreetType(StreetType streetType){this->streetType = streetType;};
StreetType Edge::GetStreetType(){return this->streetType;}
bool Edge::IsTaken(){return this->taken;}
void Edge::SetTaken(bool newTaken){
this->taken = newTaken;
this->GetStartNode()->SetTaken(newTaken);
this->GetEndNode()->SetTaken(newTaken);}
bool Edge::IsRingRoad(){return this->ringRoad;}
void Edge::SetRingRoad(bool newRingRoad){this->ringRoad = newRingRoad;
this->GetStartNode()->SetRingRoad(newRingRoad);
this->GetEndNode()->SetRingRoad(newRingRoad);}
bool Edge::IsRoundabout(){return this->roundabout;}
//@setroundabout theres an or, since a node is still a roundabout, even if only one of its edges is a roundy (additionally and more importantly, the information whether a node is a roundabout is only used in one very specific place, where this behaviour is benefial)
void Edge::SetRoundabout(bool newRound){this->roundabout = newRound;
this->GetStartNode()->SetRoundabout(newRound || this->GetStartNode()->IsRoundabout());
this->GetEndNode()->SetRoundabout(newRound || this->GetEndNode()->IsRoundabout());
}

bool Edge::Equals(Edge otherEdge){
	return this->edgeID == otherEdge.edgeID;
}
std::string Edge::ToString(){
	return std::string("Edge with ID: ").append(std::to_string((long long unsigned) this->edgeID));
}

//if its a one way edge, check which way it goes (one-way edges always go from first edge to the second edge (i.e. startNode to endNode)
bool Edge::IsTraversable(Node * n){
	return this->IsOneWay() ? n->Equals(*this->startNode) : true;
}

Node * Edge::GetOtherNode(Node * n){
	return this->startNode->Equals(*n) ? this->endNode : this->startNode;
}

unsigned int Edge::ComputeDistanceCosines(){
	//TODO: Implement if wished
	return 1;
}
unsigned int Edge::ComputeDistanceHaversine(){
	double deltaLon = this->DegreeToRadians(endNode->GetLon(false))- this->DegreeToRadians(startNode->GetLon(false));
	double deltaLat = this->DegreeToRadians(endNode->GetLat(false))- this->DegreeToRadians(startNode->GetLat(false));
	double a = (std::sin(deltaLat/2) * std::sin(deltaLat/2)) + std::cos(this->DegreeToRadians(startNode->GetLat(false))) * std::cos(this->DegreeToRadians(endNode->GetLat(false))) * std::sin(deltaLon/2) * std::sin(deltaLon/2);
	double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));
	//6.371km is the mean radius of earth, times thousand to get it as metres
	return (unsigned int) 6371 * c * 1000;
}
//approximate solution, used for small distance; used since we (in comparison to the earth as a whole) use small distances, where this approx works well
unsigned int Edge::ComputeDistanceEquirec(){
	int x = (this->endNode->GetLon() - this->startNode->GetLon()) * std::cos((this->startNode->GetLat() + this->endNode->GetLat()) / 2);
	int y = this->endNode->GetLat() - this->startNode->GetLat();
	//times a thousand for transformation into metres
	return (unsigned int) std::sqrt((long double)(x * x + y * y)) * 1000;
}

//always call ComputeDistance first
double Edge::ComputeTimeTravel(){
	if(maxSpeed == 0){
		return 1;
	}
	//dividing distance (in m) through maxSpeed (in m/s) 
	return (double) this->distance/ (double) this->maxSpeed;
}

double Edge::DegreeToRadians(double angle){
	return angle * PI / 180;
}

void Edge::RecomputeDistance(){
	this->distance = this->ComputeDistanceHaversine();
	this->timeTravel = this->ComputeTimeTravel();
}

bool Edge::IsHigherOrder(Edge * otherEdge){
	//a higher order in my implementation means a lower int value (0 is motorway, 12 is service road)
	return this->streetType < otherEdge->GetStreetType();
}

bool Edge::IsLinkRoad(){
	std::string comp = streetStrings[this->GetStreetType()];
	return comp.find("_link") != std::string::npos;
}




