#pragma once
#include "Edge.h"

///Basic geometric line, constructed from a node and a direction
/**	Used to find the intersection between two edges
	or the intersection of this line and another line constructed from the normal of this line and a node (which can be used to get the normal distance between a point and the line)
	Additionally provides a method to determine whether a node is to the right or left side of the line (necessary for the LayoutMove)*/
class Line
{
public:
	///start and endnodes need be any two, non-identical, nodes 
	/**	the order of start/end node does not matter (i.e. (Line(one, two) results in the same line as Line(two, one))
		The position of the two nodes must differ
		Is equivalent to Line(start, end.x - start.x, end.y - start.y) */
	Line(Node * start, Node * end);
	///a constructor providing us w/a start node and a directional vector for our line
	/** At least one direction needs be != 0 */
	Line(Node * start, double directionX, double directionY);
	~Line(void);
	///get the intersection from this line and the other one, and create a new Node at the intersection point w/the specified ID 
	/**	If the Node is only temporary (i.e. just to find the position of the intersection) any ID can be used
		If the Node is to be added to a MyData set, make sure that ID is not already taken */
	Node * IntersectionPoint(Line other, unsigned int usedID);
	
	///Another way of getting the intersection point between the line and another line created by the Node p and the normal of this line
	/**	If the Node is only temporary (i.e. just to find the position of the intersection) any ID can be used
		If the Node is to be added to a MyData set, make sure that ID is not already taken */
	Node * IntersectionPoint(Node * p, unsigned int usedID);
	///Evaluates whether the Node n is on the specified side of the line
	/**	needed for the moves for the layouting
		side is: 1  for positive d values, -1 for negative d values (Note: nodes directly on the line will return true)
		and the Node is the Node to be checked for position */
	bool IsOnSide(int side, Node * n);
	///finds out if the intersection Node (computed from the IntersectionPoint of this class) is within the bounds of the edge
	/**	make sure the edge passed on as param lies on the current Line object */
	bool IntersectionOnSegment(Node * intersection, Edge * thisEdge);
private:
	///the variables describing the line
	double pointX, pointY, directionX, directionY;
	///Variable to describe the normal of the line
	double normalX, normalY;
};

