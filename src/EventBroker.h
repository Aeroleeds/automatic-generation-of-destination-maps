#pragma once
#include "stdafx.h"
#include "mainwindow.h"

/// Class to sent a message to the MainWindow to update its display of information
/**	Used to write the values of a premade DebugSettings option into the fields of the MainWindow*/
class EventBroker
{
public:
	EventBroker(void);
	~EventBroker(void);
	///Sends a message to the mainwindow to remind it to reload its data from the OptionFile
	static void SendRefreshEvent();
	//the mainwindow where the refreshevent is sent to
	static MainWindow * main;

};

