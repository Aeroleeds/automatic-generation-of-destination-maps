#include "StdAfx.h"
#include "BorderLineControl.h"
#include "OptionFile.h"

BorderLineControl::BorderLineControl(void)
{
	//TODO: tweak threshold
	threshold = 0.0002;
	this->bb = OptionFile::bb;
	//TODO: this only works if posLat and negLat have the same sign)
	double lat = bb.negLat + (bb.posLat - bb.negLat) / 2;
	double lon = bb.negLong + (bb.posLong - bb.negLong )/ 2;
	this->centre = new Node(lon, lat, 0);
}


BorderLineControl::~BorderLineControl(void)
{
	delete centre;
}

void BorderLineControl::ProcessReference(MyData * data){
	this->nodeIDtoLat.clear();
	this->nodeIDtoLon.clear();
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * node = data->GetNodeList().at(i);
		//we compare lat/lon values
		if(std::abs(node->GetLat(false) - OptionFile::bb.negLat) < threshold){
		//we fix nodes at or slightly outside the border
		//if((node->GetLat() - OptionFile::bb.negLat) < 0){
			this->nodeIDtoLat.insert(std::make_pair(node->GetNodeID(), node->GetLat()));
		}else if(std::abs(node->GetLat(false) - OptionFile::bb.posLat) < threshold){
		//}else if((node->GetLat() - OptionFile::bb.posLat) > 0){
			this->nodeIDtoLat.insert(std::make_pair(node->GetNodeID(), node->GetLat()));
		}

		if(std::abs(node->GetLon(false) - OptionFile::bb.negLong) < threshold){
		//if((node->GetLon() - OptionFile::bb.negLong) < 0){
				this->nodeIDtoLon.insert(std::make_pair(node->GetNodeID(), node->GetLon()));
		}else if(std::abs(node->GetLon(false) - OptionFile::bb.posLong) < threshold){
		//}else if((node->GetLon() - OptionFile::bb.posLong) > 0){
			this->nodeIDtoLon.insert(std::make_pair(node->GetNodeID(), node->GetLon()));
		}
		//then add it to the list of outside nodes, if thats the case
		/*if(!bb.IsInside(data->GetNodeList().at(i))){
			this->nodeIDsOutside.insert(data->GetNodeList().at(i)->GetNodeID());
		}*/
	}
}

void BorderLineControl::ApplyBorderLine(RoadLayoutData * data){
	bool needsScaling = false;
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * node = data->GetNodeList().at(i);
		if(nodeIDtoLat.count(node->GetNodeID()) != 0){
			//the values of the bb are in lat/lon, thus false is used
			node->SetLat(nodeIDtoLat[node->GetNodeID()]);
		
			
			
			
			//if the node aditionally moved out of our bb in the other direction, reset it
			/*if(node->GetLon(false) > bb.posLong){
				
				node->SetLon(bb.posLong, false);
			}else if(node->GetLon(false) < bb.negLong){
				node->SetLon(bb.negLong, false);
			}*/

		}
		if(nodeIDtoLon.count(node->GetNodeID()) != 0){
			node->SetLon(nodeIDtoLon[node->GetNodeID()]);
			
			
			
			
			
			/*if(node->GetLat(false) > bb.posLat){
				node->SetLat(bb.posLat, false);
			}else if(node->GetLat(false) < bb.negLat){
				node->SetLat(bb.negLat, false);
			}*/
		}
		//if a node is not supposed to be outside, but is, the layout needs some serious scaling
		//if(this->nodeIDsOutside.count(node->GetNodeID()) == 0){
		//since we clip the data against the boundary all nodes are supposed to be inside the bounding box
		if(!bb.IsInside(node)){
			needsScaling = true;
		}
		//}
	}
	while(needsScaling){
		MoveToCentre(data, 0.99);
		if(!InvalidNodeOutside(data->GetNodeList())){
			needsScaling = false;
			for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
				data->GetEdgeList().at(i)->RecomputeDistance();
			}
		}
	}

}

bool BorderLineControl::InvalidNodeOutside(std::vector<Node *> nodeList){
	for(unsigned int i = 0; i < nodeList.size(); i++){
		if(this->nodeIDsOutside.count(nodeList.at(i)->GetNodeID()) == 0){
			//if we find a node thats not supposed to be outside outside, return true
			if(!bb.IsInside(nodeList.at(i))){
				return true;
			}
		}
	}
	return false;
}

//this isnt really uniformly is it? nodes further away move more than closer nodes
void BorderLineControl::MoveToCentre(RoadLayoutData * tempData, double scale){
	for(unsigned int i = 0; i < tempData->GetNodeList().size(); i++){
		Node * tempNode = tempData->GetNodeList().at(i);
		//dont move outside nodes (they may slip into the bb, which Id prefer not to have happen)
		//if(nodeIDsOutside.count(tempNode->GetNodeID() != 0)){
	//		continue;
//		}
		//if(!OptionFile::bb.IsInside(tempNode)){
			double newDirectionX = (tempNode->GetLon() - centre->GetLon()) * scale;
			double newDirectionY = (tempNode->GetLat() - centre->GetLat()) * scale;
			tempNode->SetLon(centre->GetLon() + newDirectionX);
			tempNode->SetLat(centre->GetLat() + newDirectionY);
			/*if(bb.negLat > tempNode->GetLat()){
				tempNode->SetLat(bb.negLat + 0.01);
			}else if(bb.posLat < tempNode->GetLat()){
				tempNode->SetLat(bb.posLat - 0.01);
			}
			if(bb.negLong > tempNode->GetLon()){
				tempNode->SetLon(bb.negLong + 0.01);
			}else if(bb.posLong < tempNode->GetLon()){
				tempNode->SetLon(bb.posLong - 0.01);
			}*/
		//}
	}
}
