#include "StdAfx.h"
#include "FalseIntersectionHelper.h"
#include "Line.h"

///Struct used in the FalseIntersectionHelper to easily determine if two edges do not intersect by intersecting their Boxes (somewhat like the BoundingBoxes but not to determine whether nodes lie inside, but if they intersect with another Box)
struct Box{
	///The attributes determining the range of the Box
	int lowX, highX, lowY, highY;
	///Constructs a Box from the highest/lowest lat/lon values of the Nodes of the Edge e
	Box(Edge * e){
		lowX = (e->GetStartNode()->GetLon() < e->GetEndNode()->GetLon()) ? e->GetStartNode()->GetLon() : e->GetEndNode()->GetLon();
		lowY = (e->GetStartNode()->GetLat() < e->GetEndNode()->GetLat()) ? e->GetStartNode()->GetLat() : e->GetEndNode()->GetLat();
		
		highX = (e->GetStartNode()->GetLon() < e->GetEndNode()->GetLon()) ? e->GetEndNode()->GetLon() : e->GetStartNode()->GetLon();
		highY = (e->GetStartNode()->GetLat() < e->GetEndNode()->GetLat()) ? e->GetEndNode()->GetLat() : e->GetStartNode()->GetLat();
	};
	///Returns true if this Box is intersecting the other Box
	bool Intersects(Box other){
		//I believe this works
		if(lowX > other.highX){return false;}
		else if(highX < other.lowX){return false;}
		else if(lowY > other.highY){return false;}
		else if(highY < other.lowY){return false;}
		else return true;
	}
};

FalseIntersectionHelper::FalseIntersectionHelper(void)
{
}


FalseIntersectionHelper::~FalseIntersectionHelper(void)
{
}

double FalseIntersectionHelper::GetDistance(Edge * edge, Edge * otherEdge){
	Line line = Line(edge->GetStartNode(), edge->GetEndNode());
	//will contain the Node on the line through which the normal to otherEdge->StartNode lies (thus the distance between this and the startNode equals the normaldist there)
	Node * intersec = line.IntersectionPoint(otherEdge->GetStartNode(), 0);
	double deltaX = intersec->GetLon() - otherEdge->GetStartNode()->GetLon();
	double deltaY = intersec->GetLat() - otherEdge->GetStartNode()->GetLat();
	double shortestDist = sqrt(deltaX * deltaX + deltaY * deltaY);
	delete intersec;
	//then check whether our new Value is smaller than the old one
	intersec = line.IntersectionPoint(otherEdge->GetEndNode(), 0);
	deltaX = intersec->GetLon() - otherEdge->GetEndNode()->GetLon();
	deltaY = intersec->GetLat() - otherEdge->GetEndNode()->GetLat();
	double newShort = sqrt(deltaX * deltaX + deltaY * deltaY);
	delete intersec;
	//it does not matter if we take -1 or 1 as param for IsOnSide(..) as long as we stay consistent with it
	//return the correct value depending on which Node of the edge was shorter
	if(newShort < shortestDist){
		if(line.IsOnSide(1, otherEdge->GetEndNode())){
			return newShort;
		}else return - newShort;
	}else{
		if(line.IsOnSide(1, otherEdge->GetStartNode())){
			return shortestDist;
		}else return - shortestDist;
	}
}

double FalseIntersectionHelper::GetAbsoluteShortestDistance(Edge * edge, Edge * otherEdge){
	double one = std::abs(this->GetDistance(edge, otherEdge));
	double two = std::abs(this->GetDistance(otherEdge, edge));
	return one < two ? one : two;
}

bool FalseIntersectionHelper::IsFalseIntersection(Edge * edge, Edge * otherEdge,  bool ignoreNeighbours){
	//First off, check whether the bounding boxes intersect
	if(!WithinBounds(edge, otherEdge)){return false;}
	//check whether the two edges share a node
	//TODO: experimental feature, see whether it fucks anything up
	if(ignoreNeighbours && (*edge->GetStartNode() == *otherEdge->GetStartNode() || *edge->GetStartNode() == *otherEdge->GetEndNode() || *edge->GetEndNode() == *otherEdge->GetStartNode() || *edge->GetEndNode() == *otherEdge->GetEndNode())){
		return false;
	}
	Line lineOne = Line(edge->GetStartNode(), edge->GetEndNode());
	Node * intersec = lineOne.IntersectionPoint(Line(otherEdge->GetStartNode(), otherEdge->GetEndNode()), 0);
	bool toRet = (lineOne.IntersectionOnSegment(intersec, edge) && lineOne.IntersectionOnSegment(intersec, otherEdge));
	delete intersec;
	return toRet;
}

bool FalseIntersectionHelper::WithinBounds(Edge * edge, Edge * otherEdge){
	return Box(edge).Intersects(Box(otherEdge));
}

