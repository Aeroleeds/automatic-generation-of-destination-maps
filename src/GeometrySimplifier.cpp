#include "StdAfx.h"
#include "GeometrySimplifier.h"
#include "Line.h"
#include "Vec2.h"
#include "FalseIntersectionHelper.h"
#include "Dijkstra.h"

GeometrySimplifier::GeometrySimplifier(void)
{
}


GeometrySimplifier::~GeometrySimplifier(void)
{
}

std::string GeometrySimplifier::PrepDoc(MyData * data){
	//cleanup phase (mostly for weird american highway-links
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * node = data->GetNodeList().at(i);
		std::map<unsigned int, unsigned int> nodeIDToIndex;
		bool reset = true;
		while(reset){
			reset = false;
			std::vector<Edge *> edges = data->GetEdgesByNodeID(node->GetNodeID());
			for(unsigned int x = 0; x < edges.size(); x++){
				if(nodeIDToIndex.count(edges.at(x)->GetOtherNode(node)->GetNodeID()) != 0){
					//if we found dupes, regain the edges, and repeat process
					this->DeleteDuplicates(edges.at(nodeIDToIndex.at(edges.at(x)->GetOtherNode(node)->GetNodeID())), edges.at(x), data);
					reset = true;
					nodeIDToIndex.clear();
					break;
				}
				nodeIDToIndex.insert(std::pair<unsigned int, unsigned int>(edges.at(x)->GetOtherNode(node)->GetNodeID(), x));
			}
		}
	}
	std::vector<Node *> consideredNodes;
	//first off, take all nodes w/valence 2, for they are the ones to be possibly removed
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		//we do not remove our destination node, no matter what
		if(*data->GetNodeList().at(i) == OptionFile::destinationNode){
			continue;
		}
		if(data->GetNodeList().at(i)->GetValence() == 2){
			consideredNodes.push_back(data->GetNodeList().at(i));
		}
	}
	/*then go and build an edge between the two neighbouring nodes and get the distance from that edge to our node
		all the nodes are then sorted according to their distance and later removed from highest distance to lowest distance (if possible)*/
	//std::multimap<unsigned int, Node *> distanceToNode;
	std::multiset<std::pair<unsigned int, Node *>, GeometrySimplifier::comparator> queue; 
	for(unsigned int i = 0; i < consideredNodes.size(); i++){
		//we can directly adress the edges, for we know that we have exactly two entries under the given nodeID
		Node * one = data->GetEdgesByNodeID(consideredNodes.at(i)->GetNodeID()).at(0)->GetOtherNode(consideredNodes.at(i));
		Node * two = data->GetEdgesByNodeID(consideredNodes.at(i)->GetNodeID()).at(1)->GetOtherNode(consideredNodes.at(i));
		//then lets get the distance from the line connecting the two neighbouring nodes to our specified node
		Line line = Line(one, two);
		Node * temp = line.IntersectionPoint(consideredNodes.at(i), 0);
		Edge * e = new Edge(temp, consideredNodes.at(i), 120, "Brewmastered");
		//and let's rank em accordingly, so we can easily check for removal according to their importance
		//distanceToNode.insert(std::pair<unsigned int, Node *>(e->GetDistance(), consideredNodes.at(i)));
		queue.insert(std::pair<unsigned int, Node *>(e->GetDistance(), consideredNodes.at(i)));
		//and cleanup the newly created edge
		delete e;
		delete temp;
	}
	//then we iteratively remove the nodes while we can
	//std::multimap<unsigned int, Node*>::const_reverse_iterator rit;
	//for (std::multimap<unsigned int, Node*>::const_reverse_iterator rit = distanceToNode.rbegin(); rit != distanceToNode.rend(); ++rit){
	while(!queue.empty()){
		bool remove = false;
		//get the highest ranked node
		Node * toCheck = queue.begin()->second;
		//and erase it from our queue
		queue.erase(queue.begin());
		//check for removal condition here //TODO: await michael's email...
		//get the angle at the node to be removed (coming from the two neighbouring nodes)
		std::vector<Edge *> neighbourEdges = data->GetEdgesByNodeID(toCheck->GetNodeID());
		//we construct the vecs so one leads to the toCheck node while the other starts in the toCheck node
		//for there are wrong angles if both vecs lead to or from toCheck
		Vec2 first(neighbourEdges.at(0)->GetOtherNode(toCheck), toCheck); 
		Vec2 second(toCheck, neighbourEdges.at(1)->GetOtherNode(toCheck));
		double ang = second.GetAngle(first);
		//turns out the paper was right considering the fact that angles < 130 deg need be removed (the acos seems to return 180 - theAngle, which explains this behaviour (I think))
		//or not?!?!?! HELP!
		//if(first.GetAngle(second) < this->GetComparisonAngle(toCheck)){

		/*
			HERE COMES THE ANGLE COMPARISON
		*/

		if(first.GetAngle(second) < this->GetComparisonAngle(toCheck)){
			remove = true;
		}
		//then remove it
		if(remove){
			//we can directly adress the edges, for we know that we have exactly two entries under the given nodeID
			Node * one = data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0)->GetOtherNode(toCheck);
			Node * two = data->GetEdgesByNodeID(toCheck->GetNodeID()).at(1)->GetOtherNode(toCheck);
			//create the new edge between the two neighbours of toCheck
			Edge * edge = new Edge(one, two, data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0)->GetMaxSpeed() ,data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0)->GetStreetName());
			edge->SetStreetType(data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0)->GetStreetType());
			//in order to detect false intersections correctly, we need to add the edge before checking
			std::vector<Edge *> eList;
			eList.push_back(edge);
			data->AddEdgeList(eList);
			//if the edge introduces a false intersection, revert the process
			if(this->IntroducesFalseIntersection(edge, data)){
				data->RemoveAndDeleteEdge(data->GetEdgeByID(edge->GetEdgeID()));
				continue;
			}

			//then remove all the edges to ToCheck and the node toCheck itself
			data->RemoveAndDeleteEdge(data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0));
			//we remove the one @ 0 again, for the edge fromerly @ 1 is now @ 0 since we removed the old 0
			data->RemoveAndDeleteEdge(data->GetEdgesByNodeID(toCheck->GetNodeID()).at(0));
			data->RemoveAndDeleteNode(data->GetNodeByID(toCheck->GetNodeID()));
			//then insert the new edge into the MyData set
			/*std::vector<Edge *> eList;
			eList.push_back(edge);
			data->AddEdgeList(eList);*/
		}
	}
	//cleanup potentially unconnected parts
	Dijkstra dijk;
	std::vector<unsigned int> toDeleteEdges;
	//leads to all unconnected val != 2 nodes
	std::vector<Node *> unconnectedNodes = dijk.GetUnconnectedNodes(data, data->GetNodeByID(OptionFile::destinationNode.GetNodeID()));
	for(unsigned int i = 0; i < unconnectedNodes.size(); i++){
		std::vector<Edge *> toRem = data->GetEdgesByNodeID(unconnectedNodes.at(i)->GetNodeID());
		//follow all edges until another val != 2 node is reached, and add their IDs to the toDelete vector
		for(unsigned int x = 0; x < toRem.size(); x++){
			toDeleteEdges.push_back(toRem.at(x)->GetEdgeID());
			Node * tempNode = toRem.at(x)->GetOtherNode(unconnectedNodes.at(i));
			Edge * tempEdge = toRem.at(x);
			//keep looking while we are at intermediate nodes
			while(tempNode->GetValence() == 2){
				std::vector<Edge *> dgs = data->GetEdgesByNodeID(tempNode->GetNodeID());
				tempEdge = (*tempEdge == *dgs.at(0) ? dgs.at(1) : dgs.at(0));
				tempNode = tempEdge->GetOtherNode(tempNode);
				toDeleteEdges.push_back(tempEdge->GetEdgeID());
			}
		}
	}
	//delete all edges
	for(unsigned int i = 0; i < toDeleteEdges.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(toDeleteEdges.at(i)));
	}
	//and delete all the nodes w/out any edges
	std::vector<Node *> nodes = data->GetNodeList();
	for(unsigned int i = 0; i < nodes.size(); i++){
		if(nodes.at(i)->GetValence() == 0) data->RemoveAndDeleteNode(data->GetNodeByID(nodes.at(i)->GetNodeID()));
		//actually unnecessary, but safety first kids
		nodes.at(i) = nullptr;
	}

	return std::string("Splendid");
}
std::string GeometrySimplifier::ToString(){
	return std::string("GeometrySimplifier");
}

void GeometrySimplifier::SetUp(){

}

unsigned int GeometrySimplifier::DeleteDuplicates(Edge * eOne, Edge * eTwo, MyData * data){
	if(eOne->GetStreetType() < eTwo->GetStreetType()){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(eTwo->GetEdgeID()));
		unsigned int ret = eTwo->GetEdgeID();
		eTwo = nullptr;
		return ret;
	}else{
		data->RemoveAndDeleteEdge(data->GetEdgeByID(eOne->GetEdgeID()));
		unsigned int ret = eOne->GetEdgeID();
		eOne = nullptr;
		return ret;
	}
}

double GeometrySimplifier::GetComparisonAngle(Node * n){
	//do some fancy stuff for determining the distance to our destination node
	//for ease of implementation we could just take a defined distance (e.g. 30 km) in which the angle is 130
	Edge * e = new Edge(n, &OptionFile::destinationNode, 12, "way home");
	//TODO: possibly these values need to be flipped
	//Test feature; smaller angle for closer streets
	//return e->GetDistance() < 1500 ? 95 : 130;
	//test the new angles
	double toReturn =  e->GetDistance() < 1500 ? 130 : 95;
	delete e;
	return toReturn;
}

bool GeometrySimplifier::IntroducesFalseIntersection(Edge * edge, MyData * data){
	FalseIntersectionHelper help;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(*edge == *data->GetEdgeList().at(i)){
			continue;
		}
		if(help.IsFalseIntersection(edge, data->GetEdgeList().at(i))){
			return true;
		}
	}
	return false;
}

