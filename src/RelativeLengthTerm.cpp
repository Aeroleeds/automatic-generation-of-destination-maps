#include "StdAfx.h"
#include "RelativeLengthTerm.h"
#include <algorithm>

RelativeLengthTerm::RelativeLengthTerm(void)
{
}


RelativeLengthTerm::~RelativeLengthTerm(void)
{
}

double RelativeLengthTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	double sum = 0;
	bool found = false;
	//we always sum up the values for the current edge and all other edges (apart from the already visited ones)
	//[e.g. if we are @edge 2 we do not need to consider the rel. length of edge 2 and edge 1 since they have been covered while we were @edge 1]
	for(unsigned int i = 0; i < tempData->GetEdgeList().size(); i++){
		//the relations for all edges w/indices < the one of e have already been considered and need no further computing
	/*	if(!found){
		if(*tempData->GetEdgeList().at(i) == *e){
			found = true;
		}
			continue;
		}*/
		double r0, r, square;
		//the original distance of both e and the tempedge
		double oEDist = data->GetEdgeByID(e->GetEdgeID())->GetDistance();
		double oTDist = data->GetEdgeByID(tempData->GetEdgeList().at(i)->GetEdgeID())->GetDistance();
		//and in case the first layouting step reduced edges quite a lot, prevent them from being 0
		oEDist = oEDist > 0.00001 ? oEDist : 0.00001;
		oTDist = oTDist > 0.00001 ? oTDist : 0.00001;
		//this should work for the edgelist in data should have the exact same size as the edgelist in the roadlayoutdata
		/*r0 = (data->GetEdgeByID(e->GetEdgeID())->GetDistance() > data->GetEdgeByID(tempData->GetEdgeList().at(i)->GetEdgeID())->GetDistance()) ? 
			(data->GetEdgeByID(e->GetEdgeID())->GetDistance() / data->GetEdgeByID(tempData->GetEdgeList().at(i)->GetEdgeID())->GetDistance()) :
			(data->GetEdgeByID(tempData->GetEdgeList().at(i)->GetEdgeID())->GetDistance() / data->GetEdgeByID(e->GetEdgeID())->GetDistance());*/
		r0 = oEDist > oTDist ? (oEDist / oTDist) : (oTDist / oEDist);
		//r = (e->GetDistance() > tempData->GetEdgeList().at(i)->GetDistance()) ?
/*		r = (data->GetEdgeByID(e->GetEdgeID())->GetDistance() > data->GetEdgeByID(tempData->GetEdgeList().at(i)->GetEdgeID())->GetDistance()) ?
			(e->GetDistance() / tempData->GetEdgeList().at(i)->GetDistance()) :
			(tempData->GetEdgeList().at(i)->GetDistance() / e->GetDistance());*/
		double nEDist = e->GetDistance();
		double nTDist = tempData->GetEdgeList().at(i)->GetDistance();
		nEDist = nEDist > 0.00001 ? nEDist : 0.00001;
		nTDist = nTDist > 0.00001 ? nTDist : 0.00001;
		r = oEDist > oTDist ? (nEDist / nTDist) : (nTDist / nEDist);
		square = (r0 - r)/r;
		sum += square * square;
	}
	return sum;
}

//the RelativeLength has no postCost term, thus return 0
/*double RelativeLengthTerm::PostCost(){
	return 0;
}
*/

std::string RelativeLengthTerm::ToString(){
	return std::string("Cost Function : Relative Length Term");
}

std::string RelativeLengthTerm::GetIdentifier(){
	return std::string("RelativeLength");
}

double RelativeLengthTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::relLengOne;
	case 2:
		return OptionFile::relLengTwo;
		//return 0;
	default:
		return -1;
	}
}
