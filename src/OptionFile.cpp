#include "StdAfx.h"
#include "OptionFile.h"

/*
	setup static stuff w/base values
*/
std::string OptionFile::dataPath = std::string();
BoundingBox OptionFile::bb = BoundingBox();
double OptionFile::midLon = 0;
PointInSpace OptionFile::destination = PointInSpace();
std::string OptionFile::imagePath = std::string();
std::string OptionFile::serverLocation = std::string();
std::string OptionFile::styleSheetPath = std::string();
double OptionFile::straightSmallerAngle = 160.0;
double OptionFile::straightBiggerAngle = 200.0;
//set to awful default values, to be later replaced w/user input
Node OptionFile::destinationNode(0, 0, 0);
//TODO: delete this probably
std::set<std::string> OptionFile::streetNames;
//output defaults to A4 (is in milimetres)
double OptionFile::outputWidth = 210;
double OptionFile::outputHeight = 297;
//this is actually an enum, however it wont comple that way, so we save it as int instead
int OptionFile::nonUsedType = service;

double OptionFile::stepsAtVisibilityRings = 6;
unsigned int OptionFile::stepsAtNeighbourBuilding = 20;
bool OptionFile::mergeOnNameChange = false;
//bool OptionFile::takeAllLinks = false;
bool OptionFile::doNotMergeDefaultNames = false;
bool OptionFile::useRoundaboutOptimiser = true;
bool OptionFile::allowExistingFalseIntersections = true;
bool OptionFile::useAreaControl = false;
bool OptionFile::useRelRelLeng = false;
bool OptionFile::useExactDestinationForVisRings = true;

std::string OptionFile::saveAs = "default";
std::string OptionFile::serverKey = "overpass";
std::string OptionFile::loadFile = std::string();

OptionFile::PipelineSetup OptionFile::pipeSet = All;
std::string OptionFile::pipelineSetupStrings[] = { "All parts", "Already selected data", "Pre-layout data", "Only download", "Up to visibility rings", "Up to traversable routes", "Only render"};

int OptionFile::renderSuffix = 0;

//the weight stuff
double OptionFile::falseInsecOne = 100, OptionFile::falseInsecTwo = 100;
double OptionFile::minLengOne = 1000, OptionFile::minLengTwo = 1000;
double OptionFile::relLengOne = 1, OptionFile::relLengTwo = 1;
double OptionFile::orientOne = 1000, OptionFile::orientTwo = 5000;
double OptionFile::relOrientOne = 0, OptionFile::relOrientTwo = 500;
double OptionFile::acOne = 500, OptionFile::acTwo = 300;
double OptionFile::relRelLengOne = 100, OptionFile::relRelLengTwo = 100;

OptionFile::OptionFile(void)
{
}


OptionFile::~OptionFile(void)
{
}

