#include "StdAfx.h"
#include "DataGrabber.h"
#include "FalseIntersectionHelper.h"
#include "Line.h"
#include "Dijkstra.h"
#include "XapiDataProvider.h"
#include "BasicProvider.h"

//the increments in which we search for the next closest node to the destination point (it should actually not need more than one increment)
#define WiggleIncrements 0.001

DataGrabber::DataGrabber(void)
{
	//if the desired server is using the xapi key
	if(OptionFile::serverKey == "xapi"){
		prov = new XapiDataProvider(OptionFile::serverKey);
	//if no special treatment is necessary for this serverKey
	}else{
		prov = new BasicProvider(OptionFile::serverKey);
	}
}


DataGrabber::~DataGrabber(void)
{
}


std::string DataGrabber::PrepDoc(MyData * data){
	//If the data is to be taken from a file on the system, load it
	if(OptionFile::loadFile != std::string()){
		tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
		tinyxml2::XMLError err;
		err = doc->LoadFile(OptionFile::loadFile.c_str());
		if(err != 0){
			std::string toRet = doc->GetErrorStr1();
			delete doc;
			return toRet;
		}
		data->Append(MyData::XMLToMyData(doc));
		delete doc;
	}
	//if a provider is needed, check whether it's been loaded correctly
	else if(!this->prov->SettingsLoaded()){
		return std::string("The provider was unable to load the settings correctly. Is everything entered properly in the SETTINGS.xml?");
	}

	//if theres already values in the dataset, we do nothing after getting the destination node
	if(data->GetNodeList().size() != 0 || data->GetEdgeList().size() != 0){
		this->PrintSubProg("Data already loaded, skipping data grabber");
		OptionFile::destinationNode = *this->FindDestinationNodeInData(OptionFile::destination.lat, OptionFile::destination.longi, data);
		//there is no need to load anymore data, so return right now
		return std::string("Splendid");
	}
	
	//the data object should not yet be set to any value, so we init it
	for(StreetType type = (StreetType) 0; type < numValues; type = (StreetType) (type + 1)){
		if(type == OptionFile::nonUsedType){
			break;
		}
		MyData * temp = GetData(type);
		if(temp == nullptr){
			return false;
		}
		this->PrintSubProg(std::to_string((long long) temp->GetNodeList().size()).append(" nodes and ").append(std::to_string((long long)temp->GetEdgeList().size())).append(" edges."));
		data->Append(temp);
		delete temp;
	}
	//Then go find the destination node
	OptionFile::destinationNode = *this->FindDestinationNodeInData(OptionFile::destination.lat, OptionFile::destination.longi, data);
	//after writing all the values into our pointer, return our success
	return std::string("Splendid");
}

std::string DataGrabber::ToString(){
	return std::string("DataGrabber");
}

void DataGrabber::SetUp(){
	prov->SetUp();
}


MyData * DataGrabber::GetData(StreetType type){
	//std::cout << "Get data of street type " << streetStrings[type] << std::endl;
	this->PrintSubProg(std::string("Get data of street type ").append(streetStrings[type]));
	//make sure the payload is not too big, therefore make multiple requests depending on the streettype
	tinyxml2::XMLDocument * doc = prov->GetData("highway", streetStrings[type], OptionFile::bb);
	if(doc == nullptr){
		BoundingBox temp = OptionFile::bb;
		for(unsigned int i = 1; i < 8; i++){
		//	std::cout << "Trying for the " << std::to_string((long long)i) << "th time with a smaller area..." << std::endl;
			switch(i){
			case 1:
				this->PrintSubProg(std::string("Trying for the ").append(std::to_string((long long) i)).append("st time with a smaller area"));
				break;
			case 2:
				this->PrintSubProg(std::string("Trying for the ").append(std::to_string((long long) i)).append("nd time with a smaller area"));
				break;
			case 3:
				this->PrintSubProg(std::string("Trying for the ").append(std::to_string((long long) i)).append("rd time with a smaller area"));
				break;
			default:
				this->PrintSubProg(std::string("Trying for the ").append(std::to_string((long long) i)).append("th time with a smaller area"));
				break;
			}
			//this->PrintSubProg(std::string("Trying for the ").append(std::to_string((long long) i)).append("th time with a smaller area"));
			//use a base factor of 3/4
			temp = ShrinkBox(temp, 0.75);
			doc = prov->GetData("highway", streetStrings[type], temp);
			if(doc != nullptr){
				break;
			}
		}
	}
	if(doc == nullptr){return nullptr;}
	
	this->PrintSubProg(std::string("Parse data"));
	//write it into a data object, delete the xml doc, and return the MyData object
	MyData * data = MyData::XMLToMyData(doc, defaultSpeed[type]);


	//then fix some values (i.e. fill in default values where non are provided) (maxspeed)
	//we should have fixed the value with the new parsing method, therefore this is not part of the canon as of now
	return data;
}

BoundingBox DataGrabber::ShrinkBox(BoundingBox bb, double factor){
	BoundingBox tempBox = bb;
	double temp;
	//take a part of the distance to each border
	temp = OptionFile::destination.longi - tempBox.negLong;
	tempBox.negLong = OptionFile::destination.longi - temp * factor;
	temp = tempBox.posLong - OptionFile::destination.longi; 
	tempBox.posLong = OptionFile::destination.longi + temp * factor;
	//and the lat values in the same manner
	temp = OptionFile::destination.lat - tempBox.negLat;
	tempBox.negLat = OptionFile::destination.lat - temp * factor;
	temp = tempBox.posLat - OptionFile::destination.lat;
	tempBox.posLat = OptionFile::destination.lat + temp * factor;

	return tempBox;
}


Node * DataGrabber::FindDestinationNodeInData(double lat, double lon, MyData * data){
	Node * res = nullptr;
	//go looking for the node w/the shortest distance to our lat/lon values
	double shortestDist = DBL_MAX;
	for(unsigned int j = 0; j < data->GetNodeList().size(); j++){
		Node * tempNode = data->GetNodeList().at(j);
		//we need to compare w/the lat/lon values, for the destination's coordinates are in lat/lon
		double dlon = tempNode->GetLon(false) - lon;
		double dlat = tempNode->GetLat(false) - lat;
		//no need to sqrt anything, since we only compare them against each other (the squared distance will thus suffice)
		double dist = dlon * dlon + dlat * dlat;
		if(shortestDist > dist){
			res = tempNode;
			shortestDist = dist;
		}
	}
	return res;

}
/*
void DataGrabber::SetRing(Node * destination, MyData * data){
	std::vector<Edge *> compareAgainst;
	std::vector<double> shortestDistances;
	std::vector<Edge *> takenEdges;

	//we construct four edges going from the destination directly north/south/east/west, up until we reach the boundary
	 //to then check for intersections; the closest intersection of a given dataset in each direction will be considered a ring road
	FalseIntersectionHelper help;
	Edge * west = new Edge(destination, new Node(OptionFile::bb.negLong, destination->GetLat(), 0), 12, "western low road");
	Edge * east = new Edge(destination, new Node(OptionFile::bb.posLong, destination->GetLat(), 0), 12, "eastern high road");
	Edge * north = new Edge(destination, new Node(destination->GetLon(), OptionFile::bb.posLat, 0), 12, "northern frost road");
	Edge * south = new Edge(destination, new Node(destination->GetLon(), OptionFile::bb.negLat, 0), 12, "heaven");
	//possibility for more samples for our ring road, leave it at 4 for now
/*	Edge * northWest = new Edge(destination, new Node(OptionFile::bb.negLong, OptionFile::bb.posLat, 0), 12, "south of heaven");
	Edge * northEast = new Edge(destination, new Node(OptionFile::bb.posLong, OptionFile::bb.posLat, 0), 12, "south of heaven");
	Edge * southWest = new Edge(destination, new Node(OptionFile::bb.negLong, OptionFile::bb.negLat, 0), 12, "south of heaven");
	Edge * southEast = new Edge(destination, new Node(OptionFile::bb.posLong, OptionFile::bb.negLat, 0), 12, "south of heaven");*/
/*	compareAgainst.push_back(west);
	//compareAgainst.push_back(northWest);
	compareAgainst.push_back(north);
//	compareAgainst.push_back(northEast);
	compareAgainst.push_back(east);
//	compareAgainst.push_back(southEast);
	compareAgainst.push_back(south);
	//compareAgainst.push_back(southWest);
	/*
	for(unsigned int i = 0; i < compareAgainst.size(); i++){
		shortestDistances.push_back(DBL_MAX);
		takenEdges.push_back(nullptr);
	}
	//then loop through all the edges in the data set
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		for(unsigned int j = 0; j < compareAgainst.size(); j++){
			if(help.IsFalseIntersection(data->GetEdgeList().at(i), compareAgainst.at(j))){
				double dist = GetDistance(data->GetEdgeList().at(i), destination);
				if(dist < shortestDistances.at(j)){
					shortestDistances[j] = dist;
					takenEdges[j] = data->GetEdgeList().at(i);
				}
			}
		}
	}
	//then get the current streettype
	StreetType currentType = numValues;
	//And a vector containing all the non-nullptr edges
	std::vector<Edge *> nonNullEdges;
	for(unsigned int i = 0; i < takenEdges.size(); i++){
		if(takenEdges.at(i) == nullptr){
			continue;
		}
		data->AddRingRoad(takenEdges.at(i)->GetStreetName());
		currentType = takenEdges.at(i)->GetStreetType();
		nonNullEdges.push_back(takenEdges.at(i));
	}
	if(currentType != numValues){
		this->typeToRing.insert(std::pair<StreetType, std::vector<Edge *>>(currentType, nonNullEdges));
	}
}
*/
/*
void DataGrabber::AppendData(MyData * toAppend, MyData * data){
	/****

	INCREASE THE SIZE OF THE CONTAINERS BY toAppend.GetEdgelist.size()
	might?! SAVE AGES OF RUNTIME


	*/
	//lets just reserve space for 80000 edges, and 80000 nodes (that should be about whats necessary...)
	//we can call this multiple times for it will only do something if the current reserve is smaller than the set one
	//data->GetEdgeList().reserve(80000);
	//data->GetNodeList().reserve(80000);
	//data->Append(toAppend);
	//and we simply add all the ring roads from toAppend, since ringRoads is a set and possible duplicates do not matter
	/*
		outsourced to visibility ring creator

	std::set<std::string>::iterator it;
	for(it = toAppend->GetRingRoads().begin(); it != toAppend->GetRingRoads().end(); ++it){
		data->AddRingRoad(*it);
	}*/
//}
/*
double DataGrabber::GetDistance(Edge * edge, Node * node){
	Line line(edge->GetStartNode(), edge->GetEndNode());
	Node * temp = line.IntersectionPoint(node, 0);
	double dlat = node->GetLat() - temp->GetLat();
	double dlon = node->GetLon() - temp->GetLon();
	//once again, since we only compare against each other, take the simplest distance measure possible
	return dlat * dlat + dlon * dlon;
}

void DataGrabber::ConnectRingRoads(MyData * data){
	Dijkstra dijk;
	//we make traveling along the ring roads quite cheap for now
	dijk.SetFactor(0.3);
	StreetType type = motorway;
	//will contain all the paths connecting our ring roads
	std::vector<std::vector<Edge *>> paths;
	while(type != numValues){
		if(this->typeToRing.count(type) == 0){
			continue;
		}
		//set all edges from ring roads as ring roads
		std::vector<Edge *> edges = typeToRing.at(type);
		for(unsigned int j = 0; j < edges.size(); j++){
			for(unsigned int i = 0; i < data->GetEdgesByStreetName(edges.at(j)->GetStreetName()).size(); i++){
				data->GetEdgesByStreetName(edges.at(j)->GetStreetName()).at(j)->SetRingRoad(true);
			}
		}
		//setting the street type to the next value
		type = (StreetType) (type + 1);
	}
	//cycle through the map again
	type = motorway;
	while(type != numValues){
		if(this->typeToRing.count(type) == 0){
			continue;
		}
		//we get the rings in clockwise order, starting from west
		std::vector<Edge *> rings = typeToRing.at(type);
		Edge * prev = nullptr;
		Edge * current = nullptr;
		for(unsigned int i = 0; i < rings.size(); i++){
			current = rings.at(i);
			if(prev != nullptr){
				//get the path taken to the other edge in edge form
				//reset the dijkstra algorithm every time, since the goal changes (potentially) everytime
				dijk.ResetDijkstra();
				std::vector<Edge *> tempEdges = dijk.DijkstraPath(data, prev->GetEndNode(), current->GetStartNode()), data;
				//and make sure it is not too long to be taken
				if(ValidPath(tempEdges, prev->GetEndNode(), current->GetStartNode())){
					paths.push_back(tempEdges);
				}
			}
			prev = current;
		}
		//setting the street type to the next value
		type = (StreetType) (type + 1);
	}
	//Thus obtaining all the paths for our ring roads
	//Next up we strip all the ring road tags we have so far applied, and reapply them only to those streets that are within the real ring road (as computed by the dijkstra)
	std::set<std::string>::iterator it;
	for(it = data->GetRingRoads().begin(); it != data->GetRingRoads().end(); ++it){
		for(unsigned int j = 0; j < data->GetEdgesByStreetName(*it).size(); j++){
			//if we reach a motorway, do not strip it of its tag, motorways are always taken and always ring roads
			if(data->GetEdgesByStreetName(*it).at(j)->GetStreetType() == motorway){
				continue;
			}
			data->GetEdgesByStreetName(*it).at(j)->SetRingRoad(false);
		}
	}
	//reapply the ring road to all edges in the path (we needed to apply it first, so our dijkstra could work w/the reduced penalty for driving across ring roads
	for(unsigned int i = 0; i < paths.size(); i++){
		for(unsigned int j = 0; j < paths.at(i).size(); j++){
			paths[i][j]->SetRingRoad(true);
			paths[i][j]->SetTaken(true);
		}
	}
	//and were finally done
}
*/
/*
Edge * DataGrabber::GetCommonEdge(Node * one, Node * two, MyData * data){
	std::vector<Edge *> edgesOne = data->GetEdgesByNodeID(one->GetNodeID());
	std::vector<Edge *> edgesTwo = data->GetEdgesByNodeID(two->GetNodeID());
	unsigned int sizeOne = edgesOne.size();
	unsigned int sizeTwo = edgesTwo.size();
	for(unsigned int i = 0; i < sizeOne; i++){
		for(unsigned int j = 0; i < sizeTwo; j++){
			if(edgesOne.at(i) == edgesTwo.at(j)){
				return edgesOne.at(i);
			}
		}
	}
	return nullptr;
}
*/
/*
std::vector<Edge *> DataGrabber::DijkToEdges(std::vector<Node *> nodes, MyData * data){
	std::vector<Edge *> edges;
	Node * current = nullptr;
	Node *prev = nullptr;
	//loop through all the nodes, and get the edges connecting them to return a series of edges instead of a series of nodes
	for(unsigned int i = 0; i < nodes.size(); i++){
		current = nodes.at(i);
		if(prev != nullptr && current != nullptr){
			Edge * temp = GetCommonEdge(prev, current, data);
			if(temp != nullptr){
				edges.push_back(temp);
			}
		}
		prev = current;
	}
	return edges;
}
*/
/*
bool DataGrabber::ValidPath(std::vector<Edge *> path, Node * start, Node * finish){
	//make an edge, so the distance gets computed
	Edge * ref = new Edge(start, finish, 12, "reference only");
	//and sum up the distances of the path
	double sum = 0;
	for(unsigned int i = 0; i < path.size(); i++){
		sum += path.at(i)->GetDistance();
	}
	//if the path is not longer than twice the direct connection, take it
	bool valid = (ref->GetDistance() * 2 >= sum);
	delete ref;
	return valid;
}
*/
//TODO: rewrite to use the IsLinkRoad of the edge class
/*bool DataGrabber::IsLinkRoad(Edge * e){
	std::string comp = streetStrings[e->GetStreetType()];
	//std::size_t found = comp.find("_link");
	return comp.find("_link") != std::string::npos;
}*/
