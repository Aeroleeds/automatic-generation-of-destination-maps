#pragma once
#include "pipelinepart.h"

/// PipelinePart to delete nodes that do not contribute much to the overall layout of our data
/**	improves visibility and makes the following algorithms faster by reducing the amount of data to be processed
	All nodes w/valence == 2 are in danger of being removed, if they do not represent a sharp bend (those shall be preserved)
	for reference see: 4.2.2.
	The threshold for when to remove a node differs depending on how close the node is to our destination
	Nodes in close proximity to the destination are more likely to be retained
	Will only correctly work if a valid value has been set for the OptionFile::destinatonNode
*/
class GeometrySimplifier :
	public PipelinePart
{
public:
	GeometrySimplifier(void);
	~GeometrySimplifier(void);
	
	///Removes all val == 2 nodes from the data set if they do not represent a sharp bend
	/**	These nodes are deleted from the heap, please do not use pointers obtained before applying the geometry simplifer
		The necessity of the nodes is comptued by evaluating the angle at the node to be deleted
		deleted nodes will be replaced by an edge directly connecting their two neighbouring nodes*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	///There to determine which angle to compare against, when checking for possible removal
	/**	As nodes closer to the destination are more often retained, this computes the distance between the node n and the destination
		depending upon the distance between these two the appropriate comparison angle is returned */
	double GetComparisonAngle(Node * n);
	///Returns true if the proposed Edge edge introduces a false intersection with one of the MyData Edges
	/**	Edges that introduce a false intersection are obviously not taken in the simplification process*/
	bool IntroducesFalseIntersection(Edge * edge, MyData * data);
	///Deletes the lesser version of the two edges, or the first one if no lesser version exists; returns the id of the removed edge
	unsigned int DeleteDuplicates(Edge * eOne, Edge * eTwo, MyData * data);
	///simple comparator to order pairs of distances and nodes according to their distance
	struct comparator{
		///returns true if the value of the given pair is strictly greater than the value of the second pair
		/**	For we want to check for removal of the more impactful nodes first*/
		bool operator()(const std::pair<unsigned, Node *>& one, const std::pair<unsigned, Node *>& two){
			return one.first > two.first;
		}
	};
};

