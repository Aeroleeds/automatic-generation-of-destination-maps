#pragma once
#include "PipelinePart.h"

///PipelinePart deleting all nodes and edges that are not marked as taken
/**	used to clean up all the non-taken data, just before 4.2 (i.e. just after all the necessary data has been set as taken, and before the layout is computed)
	speeds up all future operations and claryfies the data for navigating purposes*/
class DataWallE : public PipelinePart
{
public:
	DataWallE(void);
	~DataWallE(void);
	///Deletes all non-taken Edges and Nodes
	/**	Will delete them from the Heap, so do not use any pointers that might have been deleted
		Instead try reobtaining the information using its ID and the MyData set (if really necessary)*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	};

