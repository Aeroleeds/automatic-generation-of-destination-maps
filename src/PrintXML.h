#pragma once
#include "pipelinepart.h"
#include "XmlHelper.h"

///PipelinePart to print an xml version of the current MyData object
/**	The printed XML is valid OSM-data and may be used with tools like the JOSM-Editor*/
class PrintXML :
	public PipelinePart
{
public:
	PrintXML(void);
	~PrintXML(void);

	///saves an XML File containing the data passed on as MyData object
	/**	Will write it under the given name in the executing dir
		if no name has been set via the SetName() method, it will assume a default name
		can be called multiple times w/a single name, for a suffix will be appended
		e.g.: myFile_1.xml, myFile_2.xml, .... myFile_322.xml, etc.
	*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	/// specifies the filename for the files to be saved
	/**	sets the name to be used for saving xml files during the current run0
		printing multiple files with the same name will result in multiple files w/a suffix index
		the suffix is added automatically w/out the user doing anything
	*/
	void SetName(std::string fileName);

private:
	///Maps the suffix to a string
	/**	Currently unused but can be used to output more specific results using the suffix string (e.g. instead of Tatooine_4, one can map it to Tatooine_SimplifedData*/
	std::string GetSuffixString();
	///The name under which the printed file can be found
	std::string name;
	///A suffix that is increased after every print to allow multiple print operations w/out always changing the name
	unsigned int suffix;
	///The helper used to simplify the task
	XmlHelper help;
};

