#pragma once
#include <vector>
#include "Node.h"
#include "MyData.h"

///Basic implementation of the dijkstra path search on a myData object
/** contains a couple quirks for this specific case
	1:	a possible factor that is multiplied w/the time it takes to travel along ring roads
		here only used w/values < 1, thus providing a cleaner network of streets
		(the resulting routes are possibly a bit slower than w/out it, but easier to navigate)
	2:	A penalty for taking turns
		once again for providing an easier-to-navigate end result
		(1 and 2 can be turned off by setting the factor to 1, and the penalty to 0)
	*/
class Dijkstra
{
public:
	///base constructor will provide default values for penalty and factor, please adjust at will
	Dijkstra(void);
	~Dijkstra(void);
	///computes the vector of nodes that form the shortest way from the start to the goal node (within the data)
	/**	a common dijkstra path search, w/a couple of different points, namely:
			a factor for making the travels on ring roads cheaper
			a way to trace the way back to the start point
			a fixed penalty for taking turns (in seconds)
		returns the path to be taken in a vector
	*/
	std::vector<Edge *> DijkstraPath(MyData * data, Node * start, Node * goal);
	///Computes the full dijkstra paths starting at the goal and connects all nodes in toConnect to the goal
	/**	Used during TraversableRoute
		Will set the edges to be traveled as taken*/
	void ConnectAllToGoal(MyData * data, Node * goal, std::vector<Node *> toConnect);
	///Returns all Nodes in the MyData object, that are not connected to the goal node (weird stuff might happen w/the visibility rings, thus creating unconnected components)
	std::vector<Node *> GetUnconnectedNodes(MyData * data, Node * goal);
	///factor to be applied to the travel time along ring roads
	void SetFactor(double f);
	///penalty is applied for any turns along the way
	/** Default value here is 10 seconds per turn*/
	void SetPenalty(int p);
	///return the factor to be multiplied w/the travel time along ring roads
	double GetFactor(Edge * e);
	//convenience function to morph the result of a DijkstraPath into a vector of Edges instead of nodes
	/*Undefined behaviour if the nodes passed along are not directly from the DijkstraPath(...) method
		Don't do it kids!*/
	//std::vector<Edge *> DijkToEdges(std::vector<Node *> nodes, MyData * data);
	//Dijkstra uses time saving measures that need be reset before every use
	/*	It however must NOT be reset when maintaining the same goal Node *
		This cleans up all the data used to improve performance for multiple requests for paths with the same goal*/
	//void ResetDijkstra();
private:
	/**	checks the angle between the preceeding edge and the following edge, computing the angle between the two (see: Vec2 for details)
		if the angle is inbetween two given values (see optionfile for the value) a penalty will be returned
	*/
	int GetPenalty(Edge * prevEdge, Edge * nextEdge);
	//returns the first edge found that contains the two nodes, or a nullptr if no such edge was found
	//Edge * GetCommonEdge(Node * one, Node * two, MyData * data);
	///the factor to be applied to travel time along ring roads
	double factor;
	///the penalty to be applied for turns
	int penalty;
	///The map used for optimisation
	std::map<unsigned int, double> nodeToGoal;
	///simple comparator to order pairs of distances and nodes according to their distance
	struct comparator{
		///returns true if the double value of the given pair is strictly smaller than the double value of the second pai
		bool operator()(const std::pair<double, Node *>& one, const std::pair<double, Node *>& two){
			return one.first < two.first;
		}
	};
	///The data returned and used by the Dijkstra algorithm
	struct dijkData{
		///the status works as 0 .... unvisited, 1 .... in queue, 2 .... already visited
		std::map<unsigned int, unsigned int> idToStatus;
		///The distance traveled to reach that node from the starting point
		std::map<unsigned int, double> idToDist;
		///the previous edge ID for the node with the specified ID
		/**	Used to recreate the path to be taken*/
		std::map<unsigned int, unsigned int> idToPred;
	};
	///The result of queries for the next edge along the given path
	struct WayQuery{
		///Creates a Query containing the weight and the IDs of the given Node and Edge
		WayQuery(double weigh, Node * node, Edge * edge){
			this->wayWeight = weigh;
			this->resultingNodeID = node->GetNodeID();
			this->leadingEdgeID = edge->GetEdgeID();
		}
		///Creates a WayQuery with the weight and the two IDs
		WayQuery(double weigh, unsigned int nodeID, unsigned int edgeID){
			this->wayWeight = weigh;
			this->resultingNodeID = nodeID;
			this->leadingEdgeID = edgeID;
		}
		///Contains the sum of weights for traveling along that specific path
		double wayWeight;
		///Contains the ID of the val != 2 node that this edge lead to
		unsigned int resultingNodeID;
		///Contains the last EdgeID that lead to that node
		unsigned int leadingEdgeID;
	};
	///Query for the next node along the path of the leadingEdge starting at the start node
	/**	The prevEdge is necessary for assessing penalties for turns
		The goal node is to be set if the algorithm shall not completely build the road network, but instead stop at finding a specific goal
		If such a goal node is set, the returned node id is not necessarily a val != 2 node, for it might be the goal node aswell*/
	WayQuery GetWeightAndNode(Node * start, Edge * leadingEdge, Edge * prevEdge, MyData * data, Node * goal = nullptr);
	///Computes the full dijkstra data starting with the start node
	/**	In case the goal node is set, will stop computing once the shortest way to that goal node has been found*/
	dijkData FullDijkstraPath(MyData * data, Node * start, Node * goal = nullptr);
	///Follows the path described by the dijkData from the start node and sets the dijks.IDToPred at the start node to 0
	/**	Since the path will not changed anymore we set the dijk.idToPred to 0 so we do not follow the same road more than once
		(since the algorithm stops at idTOPreds that are 0)*/
	void FollowAndTakeEdges(Node * start, Dijkstra::dijkData dijk, MyData * data);
	///Follow the edge starting at start until a val != 2 node is reached and take all edges found along the way
	Node * FollowAndTakeEdge(Node * start, Edge * edge, MyData * data);
	///Legacy function to produce the path as vector of nodes
	std::vector<Edge *> GetEdges(Node * start, Node * goal, Dijkstra::dijkData dijk, MyData * data);
};

