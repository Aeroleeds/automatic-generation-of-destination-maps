#include "StdAfx.h"
#include "RoadLayout.h"


RoadLayout::RoadLayout(void)
{
	//initialised = false;
}


RoadLayout::~RoadLayout(void)
{
}

std::string RoadLayout::PrepDoc(MyData * data){
	func.SetUp(data);
	//temp debugging method
	//func.SwitchWeights();
	blc.ProcessReference(data);
	RoadLayoutData * takenData = new RoadLayoutData(data->GetNodeList(), data->GetEdgeList());
	//shrink the layout into our borders
	//blc.ApplyBorderLine(takenData);
	double currentCost = func.GetEnergy(data, takenData);
	double originalCost = currentCost;
	double oldCost = currentCost;
	bool finished = false;
	bool secondStage = false;
	for(int i = 1; !finished; i++){
		//we create a new copy of our data and perform a layoutmove on it that is getting evaluated
		RoadLayoutData * tempData = takenData->GetDeepCopy();
		mover.DoLayoutMove(tempData);
		//in theory we should evaluate first, then apply the border line, this seems kinda non-sensical to me
		double tempCost = func.GetEnergy(data, tempData);
		//and snap the border nodes back to the border after applying the layout move
		//Lets try the official method of first evaluating, then shrinkings
		blc.ApplyBorderLine(tempData);
		//if we achieved a better result, delete the old one and keep working with the new one
		if(tempCost < currentCost){
			currentCost = tempCost;
			takenData->DeleteAll();
			delete takenData;
			takenData = tempData;
		}else{
			tempData->DeleteAll();
			delete tempData;
		}
		//every 500ndreth time we check whether there is enough improvement to keep going
		if(i % 500 == 0){
			double cost = currentCost / oldCost;
			if(cost >= 0.99){
				//if we have less than a one percent improvement, switch the weights (or terminate if that was the last set of available weights)
				finished = !func.SwitchWeights();
				//if we keep on working with the new weights, we have to set a new OldCost to compare against, and a new currentCost to check for improvement
				if(!finished){
					oldCost = func.GetEnergy(data, takenData);
					originalCost = oldCost;
					currentCost = oldCost;
					secondStage = true;
					this->PrintSubProg(std::string("Reached second stage of layouting"));
				}
				
			}else{
				if(!secondStage){
					this->PrintSubProg(std::string("Original cost: ").append(std::to_string((long double) originalCost)).append("\nCurrent cost: ").append(std::to_string((long double) currentCost)));
				}else{
					this->PrintSubProg(std::string("Stage 2 cost: ").append(std::to_string((long double) originalCost)).append("\nCurrent cost: ").append(std::to_string((long double) currentCost)));
				}
			}
			oldCost = currentCost;
			//break;
		}
	}
	//after we did all of this, reconstruct a MyData object to return and delete the old one, that was kept for reference
	//	delete data;
	//data = new MyData();
	data->RemoveAndDeleteAll();
	data->AddNodeList(takenData->GetNodeList());
	//We have to set the nodes valence to 0, for the already have a valid valence value, but since we still add the edges we end up with twice the valence (is that even english?!)
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		data->GetNodeList().at(i)->SetValence(0);
	}
	data->AddEdgeList(takenData->GetEdgeList());
	return std::string("Splendid");
}
std::string RoadLayout::ToString(){
	return std::string("Road Layout");
}

void RoadLayout::SetUp(){

}
