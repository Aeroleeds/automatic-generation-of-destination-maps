#include "StdAfx.h"
#include "RampRemover.h"
#include "FalseIntersectionHelper.h"
#include "Line.h"
#include <algorithm>

RampRemover::RampRemover(void)
{
	//this->latestID = 1;
}


RampRemover::~RampRemover(void)
{
}

std::string RampRemover::PrepDoc(MyData * data){
	std::vector<std::pair<Node *, Node *>> startAndEndNodes = this->GetStartAndEndNodes(data); 
	//after finding our pairs of start and end nodes we delete all the link edges
	//we do not need them anymore and it will make testing easier (for we need not account for link_edges)
	this->DeleteLinkEdges(data);
	//the delete link nodes method is actually quite fancy for the task here, since we could simply delete all nodes with valence == 0
	this->DeleteLinkNodes(data);
	//first off, we check if the two roads that are linked together intersect one another, if that's the case, we simply move a node to the interesction point, and split the other edge in two @ the intersection point
	std::vector<Node *> AlreadyInsertedNodes;
	//the vector with edges to be considered for induction into the mydata object
	std::vector<Edge *> possibleEdges;
	/*	save the edges to be split along a certain node
		to the splitting AFTER all the computations*/
	std::vector<Node *> splitAgainst;
	std::vector<Edge *> edgesSplitOne;
	std::vector<Edge *> edgesSplitTwo;
	//therefore we go along both edges in both direction and check for possible intersections between em
	for(unsigned int i = 0; i < startAndEndNodes.size(); i++){
		//we build two vectors of edges (one for each connected road)
		std::vector<Edge *> edgesOne, edgesTwo;
		std::vector<Edge *> temp = data->GetEdgesByNodeID(startAndEndNodes.at(i).first->GetNodeID());
		for(unsigned int j = 0; j < temp.size(); j++){
			std::vector<Edge *> followed = FollowEdge(15, temp.at(j), startAndEndNodes.at(i).first, data);
			edgesOne.insert(edgesOne.end(), followed.begin(), followed.end());
		}
		//do it a second time, but this time build the edgesTwo vector, so we can check for intersection between the two vectors
		temp = data->GetEdgesByNodeID(startAndEndNodes.at(i).second->GetNodeID());
		for(unsigned int j = 0; j < temp.size(); j++){
			std::vector<Edge *> followed = FollowEdge(15, temp.at(j), startAndEndNodes.at(i).second, data);
			edgesTwo.insert(edgesTwo.end(), followed.begin(), followed.end());
		}
		//then loop through the followed edges again, in order to find intersections in between the two edge vectors
		FalseIntersectionHelper help;
		bool merged = false;
		for(unsigned int j = 0; j < edgesOne.size() && !merged; j++){
			for(unsigned int k = 0; k < edgesTwo.size() && !merged; k++){
				if(help.IsFalseIntersection(edgesOne.at(j), edgesTwo.at(k))){
					Line lineOne = Line(edgesOne.at(j)->GetStartNode(), edgesOne.at(j)->GetEndNode());
					//the intersection node is computed(it will be deleted later on in the process)
					Node * intersec = lineOne.IntersectionPoint(Line(edgesTwo.at(k)->GetStartNode(), edgesTwo.at(k)->GetEndNode()), 0);

					//this->RemoveRampAndMergeStreets(intersec, edgesOne.at(j), edgesTwo.at(k), startAndEndNodes.at(i), data);
					if(!this->AlreadyConnected(intersec, AlreadyInsertedNodes)){
						//get a new node that possesses a valid node id
						//nodes need be initialised with the lat/lon values
						Node * newNode = new Node(intersec->GetLon(false), intersec->GetLat(false), data->GetUniqueNodeID());
						//and add our newly created node to the list of nodes we already inserted (to prevent quadruple nodes at the same point)
						AlreadyInsertedNodes.push_back(newNode);
						
						splitAgainst.push_back(newNode);
						edgesSplitOne.push_back(edgesOne.at(j));
						edgesSplitTwo.push_back(edgesTwo.at(k));
					}
					delete intersec;
					merged = true;
				}
			}
		}
		//if the two roads do NOT intersect as such, connect them with one of the other methods
		if(!merged){
			//make sure we take the shortest possible edge here
			Edge * e = this->GetShortestConnectingEdge(startAndEndNodes.at(i), data);
			
			possibleEdges.push_back(e);
			merged = true;
		}
	}

	std::vector<unsigned int> splitEdgesToDelete;

	for(unsigned int i = 0; i < splitAgainst.size(); i++){
		Edge * one = edgesSplitOne.at(i);
		Edge * two = edgesSplitTwo.at(i);
		//in case a single edge is to be split more than once, special treatment is required
		splitEdgesToDelete.push_back(this->SplitEdge(edgesSplitOne.at(i), splitAgainst.at(i), data));
		splitEdgesToDelete.push_back(this->SplitEdge(edgesSplitTwo.at(i), splitAgainst.at(i), data));
	}
	//And delete all obsolete edges
	for(unsigned int i = 0; i < splitEdgesToDelete.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(splitEdgesToDelete.at(i)));
	}

	//then connect the possible edges, if they are not already connected
	for(unsigned int i = 0; i < possibleEdges.size(); i++){
		if(!AlreadyConnected(possibleEdges.at(i)->GetStartNode(), possibleEdges.at(i)->GetEndNode(), data) && (*possibleEdges.at(i)->GetStartNode() != * possibleEdges.at(i)->GetEndNode())){
			std::vector<Edge *> tempList;
			tempList.push_back(possibleEdges.at(i));
			data->AddEdgeList(tempList);
		}else{
			delete possibleEdges.at(i);
		}
	}

	//introduced a huge clean up phase here, where all the edges belonging to a link road are deleted in a second loop through our system
	//immensely improved FULL CONSISTENCY
	std::vector<unsigned int> toRem;
	std::vector<unsigned int> toRemNodes;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(data->GetEdgeList().at(i)->IsLinkRoad()){
			toRem.push_back(data->GetEdgeList().at(i)->GetEdgeID());
			/*if(OnlyLinkEdges(data->GetEdgeList().at(i)->GetStartNode()->GetNodeID(), data)){
				toRemNodes.push_back(data->GetEdgeList().at(i)->GetStartNode()->GetNodeID());
			}
			if(OnlyLinkEdges(data->GetEdgeList().at(i)->GetEndNode()->GetNodeID(), data)){
				toRemNodes.push_back(data->GetEdgeList().at(i)->GetEndNode()->GetNodeID());
			}*/
		}
	}
	for(unsigned int i = 0; i < toRem.size(); i++){
		//if we accidentally push back the same id twice the MyData should handle it	
		data->RemoveAndDeleteEdge(data->GetEdgeByID(toRem.at(i)));
	}
	//and delete all those edges (slight runtime improvement if we do it after killing the link_edges, for nodes w/out any edges are deleted in the fast lane)
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		if(OnlyLinkEdges(data->GetNodeList().at(i)->GetNodeID(), data)){
			toRemNodes.push_back(data->GetNodeList().at(i)->GetNodeID());
		}
	}
	for(unsigned int i = 0; i < toRemNodes.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(toRemNodes.at(i)));
	}
	return std::string("Splendid");
}
std::string RampRemover::ToString(){
	return std::string("RampRemover");
}

void RampRemover::SetUp(){
//	this->latestID = 1;
	//this->newlyCreatedNodes.clear();
	this->edgeReplacements.clear();
}
/*
bool RampRemover::IsLinkRoad(Edge * e){
	//TODO: get the logic for the streettypes going!
	std::string test = streetStrings[e->GetStreetType()];
	return test.find("_link") != std::string::npos;
}*/

//method to follow a single edge an return all the edges leading from it w/the same street name
std::vector<Edge *> RampRemover::FollowEdge(int steps, Edge * toFollow, Node * base, MyData * data){
	std::vector<Edge *> edges;
	Edge * tempEdge = toFollow;
	Node * tempNode = base;
	edges.push_back(tempEdge);
	for(int i = 0; i < steps; i++){
		tempNode = tempEdge->GetOtherNode(tempNode);
		std::vector<Edge *> otherEd = data->GetEdgesByNodeID(tempNode->GetNodeID());
		//checks whether we found another edge w/the same street name from the current edge (if we do not find an edge, we return the current vector)
		bool found = false;
		for(unsigned int j = 0; j < otherEd.size(); j++){
			if(*otherEd.at(j) == *tempEdge){
				continue;
			}else if(otherEd.at(j)->GetStreetName() != tempEdge->GetStreetName()){
				continue;
			}else{
				found = true;
				tempEdge = otherEd.at(j);
				edges.push_back(tempEdge);
				break;
			}
		}
		//if we did not find another edge along the given path, return the path we found thus far
		if(!found){return edges;}
	}
	return edges;
}
//we delete the link road in between the start and end nodes, then insert a junction node on the two specified edges

/*void RampRemover::RemoveRampAndMergeStreets(Node * intersection, Edge * intersecOne, Edge * intersecTwo, std::pair<Node *, Node*> startAndEndNode, MyData * data){
	//get the series of edges between start and end node
	std::vector<Edge *> edges;
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(startAndEndNode.first->GetNodeID()).size(); i++){
		if(this->IsLinkRoad(data->GetEdgesByNodeID(startAndEndNode.first->GetNodeID()).at(i))){
			//in case we already followed a road, that turned out to not be the one we're looking for, delete the current edge pointers (might be if a single node has two different link roads)
			edges.clear();
			Node * tempNode = startAndEndNode.first;
			Edge * tempEdge = data->GetEdgesByNodeID(startAndEndNode.first->GetNodeID()).at(i);
			edges.push_back(tempEdge);
			tempNode = tempEdge->GetOtherNode(tempNode);
			while(tempNode->GetValence() == 2){
				tempEdge = (*data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) == *tempEdge ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0));
				tempNode = tempEdge->GetOtherNode(tempNode);
				edges.push_back(tempEdge);
			}
			//if we have not reached our precious end point, check the next link and hope that it gets us there
			if(*tempNode != *startAndEndNode.second){
				continue;
			}
			//Delete all the linkage stuff, first off, get all the nodes to be deleted
			std::vector<Node *> nodes;
			for(unsigned int j = 0; j < edges.size(); j++){
				if(*edges.at(j)->GetStartNode() != *startAndEndNode.first && *edges.at(j)->GetStartNode() != *startAndEndNode.second){
					nodes.push_back(edges.at(j)->GetStartNode());
				}
			}
			//then delete them and the edges 
			for(unsigned int j = 0; j < edges.size(); j++){
				data->RemoveAndDeleteEdge(edges.at(j));
			}
			for(unsigned int j = 0; j < nodes.size(); j++){
				data->RemoveAndDeleteNode(nodes.at(j));
			}
			//move the first end node to our intersection point
			intersecOne->GetEndNode()->SetLat(intersection->GetLat());
			intersecOne->GetEndNode()->SetLon(intersection->GetLon());
			intersecOne->RecomputeDistance();
			// and split the other edge @ the first end node
			std::vector<Edge *> toAdd;
			toAdd.push_back(new Edge(intersecTwo->GetStartNode(), intersecOne->GetEndNode(), intersecTwo->GetMaxSpeed(), intersecTwo->GetStreetName()));
			toAdd.at(toAdd.size() - 1)->SetStreetType(intersecTwo->GetStreetType());
			toAdd.push_back(new Edge(intersecOne->GetEndNode(), intersecTwo->GetEndNode(), intersecTwo->GetMaxSpeed(), intersecTwo->GetStreetName()));
			toAdd.at(toAdd.size() - 1)->SetStreetType(intersecTwo->GetStreetType());
			//delete intersecOne;
			//add the two new edges and delete the one edge
			data->RemoveAndDeleteEdge(intersecTwo);
			data->AddEdgeList(toAdd);
		}
	}
}

bool RampRemover::SameCoordinates(Node * n, Node * m){
	return n->GetLat() == m->GetLat() && n->GetLon() == m->GetLon();
}
*/
bool RampRemover::OnlyLinkEdges(unsigned int nodeID, MyData * data){
	std::vector<Edge *> eList = data->GetEdgesByNodeID(nodeID);
	for(unsigned int i = 0; i < eList.size(); i++){
		if(!(eList.at(i)->IsLinkRoad())){
			return false;
		}
	}
	return true;
}


Edge * RampRemover::GetNextEdge(Edge * edge, Node * node, MyData * data){
	std::vector<Edge *> eList = data->GetEdgesByNodeID(node->GetNodeID());
	for(unsigned int i = 0; i < eList.size(); i++){
		if(*eList.at(i) == *edge){
			continue;
		}else if(eList.at(i)->GetStreetName() != edge->GetStreetName()){
			continue;
		}else if(eList.at(i)->GetStreetType() != edge->GetStreetType()){
			continue;
		}//if we find an edge with the correct name and street type, return the otherNode of that edge
		else{
			return eList.at(i);
		}
	}
	//if none was found, return a nullptr as def value
	return nullptr;
}

std::vector<Node *> RampRemover::BuildNodeSet(Node * node, MyData * data){
	std::vector<Node *> ret;
	ret.push_back(node);
	std::vector<Edge *> eList = data->GetEdgesByNodeID(node->GetNodeID());
	/*Edge * toFollow = nullptr;
	for(unsigned int i = 0; i < eList.size(); i++){
		//we do not want to follow link roads
		if(eList.at(i)->IsLinkRoad()){
			continue;
		}
		if(toFollow == nullptr){
			toFollow = eList.at(i);
		}else if(!toFollow->IsHigherOrder(eList.at(i)) && eList.at(i)->GetStreetName() != ""){
			toFollow = eList.at(i);
		}
	}
	//if only link roads lead from our node (actually should not happen, but what do I know)
	if(toFollow == nullptr){
		return ret;
	}*/
	//we then follow the street from our toFollow edge in all directions for x steps
	for(unsigned int i = 0; i < eList.size(); i++){
		if(eList.at(i)->IsLinkRoad()) continue;
		else{
			//calling it w/a zero, since we did not yet perform a step of the algorithm
			std::vector<Node *> nds = this->GetNextNodes(eList.at(i), node, data, 0);
			ret.insert(ret.end(), nds.begin(), nds.end());
		}

	}
	return ret;
	/*
	//first get the same street leading into the other direction and write it into the otherFollow edge
	Edge * otherFollow = nullptr;
	eList = data->GetEdgesByNodeID(node->GetNodeID());
	for(unsigned int i = 0; i < eList.size(); i++){
		if(*eList.at(i) == *toFollow){
			continue;
		}
		else if(eList.at(i)->GetStreetName() == toFollow->GetStreetName() && eList.at(i)->GetStreetType() == toFollow->GetStreetType()){
			otherFollow = eList.at(i);
			break;
		}
	}
	Node * tempNode = toFollow->GetOtherNode(node);
	Edge * tempEdge = toFollow;
	ret.push_back(tempNode);
	for(unsigned int i = 0; i < OptionFile::stepsAtNeighbourBuilding; i++){
		tempEdge = GetNextEdge(tempEdge, tempNode, data);
		if(tempEdge == nullptr){
			break;
		}
		tempNode = tempEdge->GetOtherNode(tempNode);
		ret.push_back(tempNode);
	}
	//and then follow our street in the other direction (if there is an edge going there)
	if(otherFollow != nullptr){
		Node * tempNode = otherFollow->GetOtherNode(node);
		Edge * tempEdge = otherFollow;
		ret.push_back(tempNode);
		for(unsigned int i = 0; i < OptionFile::stepsAtNeighbourBuilding; i++){
			tempEdge = GetNextEdge(tempEdge, tempNode, data);
			if(tempEdge == nullptr){
				break;
			}
			tempNode = tempEdge->GetOtherNode(tempNode);
			ret.push_back(tempNode);
		}
	}
	*/
	return ret;
}

std::vector<Node *> RampRemover::GetNextNodes(Edge * edge, Node * node, MyData * data, unsigned int steps){
	std::vector<Node *> toRet;
	Edge * tempEdge = edge; 
	Node * tempNode = edge->GetOtherNode(node);
	toRet.push_back(tempNode);
	for(; steps < OptionFile::stepsAtNeighbourBuilding; steps++){
		Edge * nextEdge = nullptr;
		std::vector<Edge *> edges = data->GetEdgesByNodeID(tempNode->GetNodeID());
		for(unsigned int i = 0; i < edges.size(); i++){
			if(edges.at(i)->GetStreetName() != edge->GetStreetName()) continue;
			else if(*edges.at(i) == *tempEdge) continue;
			else if(nextEdge == nullptr){
				nextEdge = edges.at(i);
			}else{
				//if the path splits up, follow the other path as well
				std::vector<Node *> dgs = GetNextNodes(edges.at(i), tempNode, data, steps);
				toRet.insert(toRet.end(), dgs.begin(), dgs.end());
			}
		}
		//check whether we found a new way, if not, end the loop
		if(nextEdge != nullptr){
			tempEdge = nextEdge;
			tempNode = tempEdge->GetOtherNode(tempNode);
			toRet.push_back(tempNode);
		}else break;
	}
	return toRet;
}

Edge * RampRemover::GetShortestConnectingEdge(std::pair<Node *, Node *> nodePair, MyData * data){
	std::vector<Node *> nodesOne = this->BuildNodeSet(nodePair.first, data);
	std::vector<Node *> nodesTwo = this->BuildNodeSet(nodePair.second, data);
	//compare every node from set A with every node from set B
	//yes, that is slow, however it will not occur that often; these are not the runtime improvements you're looking for
	Edge * shorty = nullptr;
	for(unsigned int i = 0; i < nodesOne.size(); i++){
		for(unsigned int j = 0; j < nodesTwo.size(); j++){
			//we name the connecting street as blank, in order to certainly provide a non-false name without getting sophisticated
			Edge * tempEdge = new Edge(nodesOne.at(i), nodesTwo.at(j), 12, "");
			if(shorty == nullptr){
				shorty = tempEdge;
			//if our new edge is shorter, delete the old one
			}else if(shorty->GetDistance() > tempEdge->GetDistance()){
				delete shorty;
				shorty = tempEdge;
			}else{
				//else delete the new one and try again
				delete tempEdge;
			}
		}
	}
	//then set the streettype accordingly
	std::vector<Edge *> leadCandidates = data->GetEdgesByNodeID(nodePair.first->GetNodeID());
	//leadCandidates.insert(leadCandidates.end(), data->GetEdgesByNodeID(nodePair.second->GetNodeID()).begin(), data->GetEdgesByNodeID(nodePair.second->GetNodeID()).end());
	std::vector<Edge *> leadCandidatesTwo = data->GetEdgesByNodeID(nodePair.second->GetNodeID());
	leadCandidates.insert(leadCandidates.end(), leadCandidatesTwo.begin(), leadCandidatesTwo.end());
	//Edge * lead = Edge::GetHighestOrder(data->GetEdgesByNodeID(nodePair.first->GetNodeID()));
	Edge * lead = Edge::GetLowestOrder(leadCandidates);
	//and adjust shorty accordingly
	shorty->SetStreetType(lead->GetStreetType());
	shorty->SetMaxSpeed(lead->GetMaxSpeed());
	return shorty;
}

bool RampRemover::AlreadyConnected(Node * node, Node * otherNode, MyData * data){
	std::vector<Edge *> eList = data->GetEdgesByNodeID(node->GetNodeID());
	for(unsigned int i = 0; i < eList.size(); i++){
		if(*eList.at(i)->GetOtherNode(node) == *otherNode){
			return true;
		}
	}
	return false;
}
/*
bool RampRemover::AlreadyConnected(Edge * edge, Edge * otherEdge){
	if(* edge->GetStartNode() == *otherEdge->GetStartNode()){
		return true;
	}
	else if(* edge->GetEndNode() == *otherEdge->GetEndNode()){
		return true;
	}
	return false;
}*/
/*
unsigned int RampRemover::GetUniqueNodeID(MyData * data){
	//increase the latestID by one (the latest ID is obviously already taken)
	latestID++;
	for(unsigned int i = 1; data->GetNodeByID(this->latestID) != nullptr; i++){
		if(i % 6 == 0){
			latestID += 50;
		}else{
			latestID++;
		}
	}
	return latestID;
}*/

void RampRemover::DeleteLinkEdges(MyData * data){
	std::vector<unsigned int> toRem;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(data->GetEdgeList().at(i)->IsLinkRoad()){
			toRem.push_back(data->GetEdgeList().at(i)->GetEdgeID());
		}
	}
	for(unsigned int i = 0; i < toRem.size(); i++){
		//if we accidentally push back the same id twice the MyData should handle it	
		data->RemoveAndDeleteEdge(data->GetEdgeByID(toRem.at(i)));
	}
	
}
void RampRemover::DeleteLinkNodes(MyData * data){
	std::vector<unsigned int> toRemNodes;
	//and delete all those edges (slight runtime improvement if we do it after killing the link_edges, for nodes w/out any edges are deleted in the fast lane)
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		if(OnlyLinkEdges(data->GetNodeList().at(i)->GetNodeID(), data)){
			toRemNodes.push_back(data->GetNodeList().at(i)->GetNodeID());
		}
	}
	for(unsigned int i = 0; i < toRemNodes.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(toRemNodes.at(i)));
	}
}

std::vector<std::pair<Node *, Node *>> RampRemover::GetStartAndEndNodes(MyData * data){
	std::set<unsigned int> visitedEdges;
	std::vector<std::pair<Node *, Node *>> startAndEndNodes;
	//find all the ramp/links and write the start and end nodes of each link into the vector (as a pair)
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		//just checking whether the specified edge has the street type of a linkroad
		if(!(data->GetEdgeList().at(i)->IsLinkRoad())){
			continue;
		}
		if(visitedEdges.count(data->GetEdgeList().at(i)->GetEdgeID()) != 0){
			continue;
		}
		//if we passed all the checks, follow the edge outward in both directions until we reach the point where we connect w/a normal street
		Edge * tempEdge = data->GetEdgeList().at(i);
		Node * tempNode = tempEdge->GetStartNode();
		visitedEdges.insert(tempEdge->GetEdgeID());
		//in order to prevent a faulty state, keep in mind whether we found a next edge
		bool found = false;
		while(tempNode->GetValence() == 2 || (OnlyLinkEdges(tempNode->GetNodeID(), data) && tempNode->GetValence() != 1)){
			found = false;
			//if the valence is greater two but we only have link edges at that node, make sure we do not take already visited ones (or else we may loop here forever)
			if(tempNode->GetValence() > 2){
				std::vector<Edge *> dgs = data->GetEdgesByNodeID(tempNode->GetNodeID());
				for(unsigned int x = 0; x < dgs.size(); x++){
					if(*tempEdge == *dgs.at(x)){
						continue;
					}
					//if(visitedEdges.count(dgs.at(x)->GetEdgeID()) == 0){
					//we follow the edge if it is traversable from that node (ramps are usually one-way edges)
					if(dgs.at(x)->IsTraversable(tempNode)){
						found = true;
						tempEdge = dgs.at(x);
						tempNode = tempEdge->GetOtherNode(tempNode);
						visitedEdges.insert(tempEdge->GetEdgeID());
						break;
					}
				}
			}
			else{
				tempEdge = (*tempEdge == *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0));
				tempNode = tempEdge->GetOtherNode(tempNode);
				visitedEdges.insert(tempEdge->GetEdgeID());
				found = true;
			}
			//if link roads split up, simply follow one of them
			/*while(OnlyLinkEdges(tempNode->GetNodeID(), data) && tempNode->GetValence() != 1){
				tempEdge = (*tempEdge == *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0));
				tempNode = tempEdge->GetOtherNode(tempNode);
				visitedEdges.insert(tempEdge->GetEdgeID());
			}*/
			if(!found) break;
		}
		//if we reach a dead-end node, do not add it to the pairs of start and end nodes (it will be removed at the end)
		if(!found || tempNode->GetValence() == 1){
			continue;
		}
		Node * start = tempNode;
		tempEdge = data->GetEdgeList().at(i);
		tempNode = tempEdge->GetEndNode();
		found = false;
		while(tempNode->GetValence() == 2 || (OnlyLinkEdges(tempNode->GetNodeID(), data) && tempNode->GetValence() != 1)){
			found = false;
			//if the valence is greater two but we only have link edges at that node, make sure we do not take already visited ones (or else we may loop here forever)
			if(tempNode->GetValence() > 2){
				std::vector<Edge *> dgs = data->GetEdgesByNodeID(tempNode->GetNodeID());
				for(unsigned int x = 0; x < dgs.size(); x++){
					if(*tempEdge == *dgs.at(x)){
						continue;
					}
					//we follow the edge if it is traversable from that node (ramps are usually one-way edges)
					if(dgs.at(x)->IsTraversable(tempNode)){
						found = true;
						tempEdge = dgs.at(x);
						tempNode = tempEdge->GetOtherNode(tempNode);
						visitedEdges.insert(tempEdge->GetEdgeID());
						break;
					}
				}
			}
			else{
				tempEdge = (*tempEdge == *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0));	
				tempNode = tempEdge->GetOtherNode(tempNode);
				visitedEdges.insert(tempEdge->GetEdgeID());
				found = true;
			}
			/*while(OnlyLinkEdges(tempNode->GetNodeID(), data) && tempNode->GetValence() != 1){
				tempEdge = (*tempEdge == *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0));
				tempNode = tempEdge->GetOtherNode(tempNode);
				visitedEdges.insert(tempEdge->GetEdgeID());
			}*/
			if(!found) break;
		}
		//if we reach a dead-end node, do not add it to the pairs of start and end nodes (it will be removed at the end)
		if(!found || tempNode->GetValence() == 1){
			continue;
		}
		//and write the pair of end nodes of the ramps into our vector for later use
		startAndEndNodes.push_back(std::pair<Node *, Node *>(start, tempNode));
	}
	return startAndEndNodes;
}


unsigned int RampRemover::SplitEdge(Edge * edge, Node * node, MyData * data){
	//check whether the current edge has already been replaced
	Edge * replaceMe = edge;
	while(this->edgeReplacements.count(replaceMe->GetEdgeID()) != 0){
		//the values of edgeReplacements always have exactly two entries
		Edge * one = data->GetEdgeByID(edgeReplacements.at(replaceMe->GetEdgeID()).at(0));
		Edge * two = data->GetEdgeByID(edgeReplacements.at(replaceMe->GetEdgeID()).at(1));
		//On which edge does our node lie?
		replaceMe = this->NodeOnEdge(node, one) ? one : two;
		//and repeat in case that edge has been replaced aswell
	}
	Edge * tempEdge = new Edge(replaceMe->GetStartNode(), node, replaceMe->GetMaxSpeed(), replaceMe->GetStreetName());
	tempEdge->SetStreetType(replaceMe->GetStreetType());
	//TODO: this shall probably be removed (for I deem it unnecessary)
	tempEdge->SetOneWay(replaceMe->IsOneWay());
	Edge * tempEdgeTwo = new Edge(node, replaceMe->GetEndNode(), replaceMe->GetMaxSpeed(), replaceMe->GetStreetName());
	tempEdgeTwo->SetStreetType(replaceMe->GetStreetType());
	//TODO: this shall probably be removed
	tempEdgeTwo->SetOneWay(replaceMe->IsOneWay());
	//dont delete it, return its id to be deleted later
	//data->RemoveAndDeleteEdge(edge);
	
	//and add the edges to the replace list
	std::vector<unsigned int> replacementEdges;
	replacementEdges.push_back(tempEdge->GetEdgeID());
	replacementEdges.push_back(tempEdgeTwo->GetEdgeID());
	this->edgeReplacements.insert(std::pair<unsigned int, std::vector<unsigned int>>(replaceMe->GetEdgeID(), replacementEdges));

	std::vector<Edge *> eList;
	eList.push_back(tempEdge);
	eList.push_back(tempEdgeTwo);

	data->AddEdgeList(eList);
	return replaceMe->GetEdgeID();
}

bool RampRemover::NodeOnEdge(Node * node, Edge * edge){
	bool validLon = false;
	bool validLat = false;

	double start = edge->GetStartNode()->GetLon();
	double dir = edge->GetEndNode()->GetLon() - start;
	double sec = node->GetLon();
	if(dir == 0){
		validLon = (sec - start == 0);
	}else{
		double lambda = (sec - start) / dir;
		//if the lambda is 0 intersection is our startNode, if 1 it's our endNode, if its inbetween its on the line segment
		validLon = (lambda >= 0 && lambda <= 1);
	}
	//if our first value is already false, the second one will not change anything
	if(!validLon){
		return false;
	}
	//then check whether the node is on the edge in lat direction as well
	start = edge->GetStartNode()->GetLat();
	dir = edge->GetEndNode()->GetLat() - start;
	sec = node->GetLat();
	if(dir == 0){
		return (sec - start == 0);
	}else{
		double lambda = (sec - start) / dir;
		return (lambda >= 0 && lambda <= 1);
	}
}

bool RampRemover::AlreadyConnected(Node * node, std::vector<Node *> AlreadyInsertedNodes){
	for(unsigned int i = 0; i < AlreadyInsertedNodes.size(); i++){
		if(abs(node->GetLat() - AlreadyInsertedNodes.at(i)->GetLat()) < 0.0002 && abs(node->GetLon() - AlreadyInsertedNodes.at(i)->GetLon()) < 0.0002){
			return true;
		}
	}
	return false;
}
