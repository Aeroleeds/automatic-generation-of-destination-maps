#pragma once
#include "MyData.h"
#include "RoadLayoutData.h"
///Class for executing a move on a dataset in an effort to improve the current layout of nodes and ways
/**	The class doing the exact move described in 4.3.3
	Just pass the data along to the DoLayoutMove method, and the data will be modified as specified
	The move is to be evaluated by the cost functions and cost terms
	(for further information on the move, check the DoLayoutMove(..) method*/
class LayoutMove
{
public:
	LayoutMove(void);
	///constructor if a specific random Seeed is desired
	/**mainly for testing purposes, use the base constructor if not testing*/
	LayoutMove(unsigned int randomSeed);
	~LayoutMove(void);
	///performs a move to the passed on dataset, trying to improve the layout (it might as well worsen it)
	/**	Will (pseudo-)randomly choose a couple of settings, namely:
			-> a first Node (of the nodelist from the data object)
			-> a line through that node along a random direction
			-> a scaling factor (between 0.95 and 1.05)
			-> the mode of scaling (radially outward from the first Node, or orthogonally to the line)
		And then apply these settings
		automatically writes the result into the data object passed along; caller needs to create possibly necessary copies of the original state! */
	void DoLayoutMove(RoadLayoutData * data);
//private:
	///quick method to snap any nodes that reach beyond the border back to the border; NOT IN USE
	/**	Replaced by the BorderLineControl */
	//void SnapToBorder(Node * node);
};

