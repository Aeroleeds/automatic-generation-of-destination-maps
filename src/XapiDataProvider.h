#pragma once
#include "dataprovider.h"
#include <curlpp\Easy.hpp>
#include <curlpp\Options.hpp>
#include <curlpp\Exception.hpp>
#include <sstream>

///the class that grabs the data from an XAPI data source that needs a key for use w/the syntax from the mapquestapi)
/**	class to execute queries in the xapi format (i.e., by providing a key, value pair, and a bounding box in which the request is to be executed)
	Grabs the data from an online XAPI providing server with the provided request
*/
class XapiDataProvider :
	public DataProvider
{
public:
	///Contains the key to be looked for in the SETTINGS.xml, all found settings under the given key are loaded during construction
	XapiDataProvider(std::string settingsKey);
	~XapiDataProvider(void);
	///will grab osm data and the result into a tinyxml2 document for further usage
	/**	The key and value pair restrict the output to certain nodes and ways that fulfill the given criteria
		(e.g. the commonly used pair in this algorithm are of the "highway=xyz" type, to restrict the output to only motorways, primary roads, or similar)
		The BoundingBox limits the output to a certain area of interest, this area is the one being portayed in the output
		After grabbing the data it will be loaded as tinyxml2::XMlDocument and returned*/
	tinyxml2::XMLDocument * GetData(std::string key, std::string value, BoundingBox bb);
	///Specifies the server to be accessed for the data
	/**	Example for an all encompassing request: http://open.mapquestapi.com/xapi/api/0.6/way[amenity=pub][bbox=-77.041579,38.885851,-77.007247,38.900881]?key=YOUR_KEY_HERE
		the set serverurl should end with .../0.6/, all the other information is filled out according to the paramaters of GetData and according to the APIkey */
	bool SetServer(std::string serverUrl);
	///Get a string representation of the data provider
	std::string ToString();
	///Needs be called before executing anything else
	void SetUp();
	///Sets the key to grant us access to the data of the mapquest server
	virtual void SetAPIKey(std::string key);
	///Retruns whether both the apikey and the server url have been set
	virtual bool SettingsLoaded();
private:
	///the url to be reached for the data
	std::string serverUrl;
	///the API key used to open sesame
	std::string apiKey;
	//for access to our server
	///Our object to gain a connection to our server and obtain all the necessary results
	cURLpp::Easy easy;
};

