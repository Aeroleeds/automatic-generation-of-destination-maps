#include "StdAfx.h"
#include "MotorwayTurnRemover.h"


MotorwayTurnRemover::MotorwayTurnRemover(void)
{
}


MotorwayTurnRemover::~MotorwayTurnRemover(void)
{
}

std::string MotorwayTurnRemover::PrepDoc(MyData * data){
	std::vector<Edge *> eList = data->GetEdgeList();
	std::vector<unsigned int> toRemove;
	for(unsigned int i = 0; i < eList.size(); i++){
		if(eList.at(i)->GetStreetType() != motorway){
			continue;
		}
		else{
			if(eList.at(i)->GetStartNode()->GetValence() > 2){
				std::vector<unsigned int> tempEdges = this->ComputeUnneccessaryEdges(eList.at(i)->GetStartNode(), data);
				toRemove.insert(toRemove.end(), tempEdges.begin(), tempEdges.end()); 
			}else if(eList.at(i)->GetEndNode()->GetValence() > 2){
				std::vector<unsigned int> tempEdges = this->ComputeUnneccessaryEdges(eList.at(i)->GetEndNode(), data);
				toRemove.insert(toRemove.end(), tempEdges.begin(), tempEdges.end()); 
			}
		}
	}
	std::vector<unsigned int> toRemoveNodes;
	for(unsigned int i = 0; i < toRemove.size(); i++){
		Edge * tempEdge = data->GetEdgeByID(toRemove.at(i));
		if(tempEdge == nullptr){
			continue;
		}
		if(this->NoMotorwayEdges(tempEdge->GetStartNode(), data)){
			toRemoveNodes.push_back(tempEdge->GetStartNode()->GetNodeID());
		}
		if(this->NoMotorwayEdges(tempEdge->GetEndNode(), data)){
			toRemoveNodes.push_back(tempEdge->GetEndNode()->GetNodeID());
		}
		data->RemoveAndDeleteEdge(tempEdge);
	}

	for(unsigned int i = 0; i < toRemoveNodes.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(toRemoveNodes.at(i)));
	}


	return std::string("Splendid");
}
std::string MotorwayTurnRemover::ToString(){
	return std::string("MotorwayTurnRemover");
}
void MotorwayTurnRemover::SetUp(){}

std::vector<unsigned int> MotorwayTurnRemover::ComputeUnneccessaryEdges(Node * node, MyData * data){
	std::vector<unsigned int> toReturn;
	if(NoMotorwayEdges(node, data)){
		return toReturn;
	}
	std::set<unsigned int> visitedNodeIDs, visitedEdgeIDs;
	//TODO: finish up here
	//get the nodes that are functioning as endpoints (the neighbouring motorway nodes are ignored when evaluating for the distance traveled)
	//TODO: set a variable from OptionFile here
	std::vector<Node *> tabooNodes = this->GetMotorwayNodes(node, 5, data); //nabooes
	for(unsigned int i = 0; i < tabooNodes.size(); i++){
		visitedNodeIDs.insert(tabooNodes.at(i)->GetNodeID());
	}
	//then we follow all non-motorway edges from our starting node, adding up their distances
	//if the traveled distance is below a threshold x, the visited edges will be removed, else they will be retained
	unsigned int traveledDistance = 0;
	//a vector containing all the edges to be visited
	std::vector<Edge *> toVisit;
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(data->GetEdgesByNodeID(node->GetNodeID()).at(i)->GetStreetType() == motorway){
			continue;
		}else{
			toVisit.push_back(data->GetEdgesByNodeID(node->GetNodeID()).at(i));
		}
	}
	//in here we look at all the edges, and add all edges leading from both nodes of the 
	for(unsigned int i = 0; i < toVisit.size(); i++){

		if(visitedEdgeIDs.count(toVisit.at(i)->GetEdgeID()) != 0){
			continue;
		}else{
			toReturn.push_back(toVisit.at(i)->GetEdgeID());
			traveledDistance += toVisit.at(i)->GetDistance();
		}
		//look at both the start and end nodes and push the edges from uncharted nodes into our toVisit array, adding the dist of the current edge to the total traveled distance
		if(visitedNodeIDs.count(toVisit.at(i)->GetStartNode()->GetNodeID()) == 0){
			for(unsigned int x = 0; x < data->GetEdgesByNodeID(toVisit.at(i)->GetStartNode()->GetNodeID()).size(); x++){
				if(*data->GetEdgesByNodeID(toVisit.at(i)->GetStartNode()->GetNodeID()).at(x) == *toVisit.at(i)){
					continue;
				}else{
					toVisit.push_back(data->GetEdgesByNodeID(toVisit.at(i)->GetStartNode()->GetNodeID()).at(x));
				}
			}
			visitedNodeIDs.insert(toVisit.at(i)->GetStartNode()->GetNodeID());
		}
		//do the same for the endnode (for each edge only one of these branches shall be visited)
		if(visitedNodeIDs.count(toVisit.at(i)->GetEndNode()->GetNodeID()) == 0){
			for(unsigned int x = 0; x < data->GetEdgesByNodeID(toVisit.at(i)->GetEndNode()->GetNodeID()).size(); x++){
				if(*data->GetEdgesByNodeID(toVisit.at(i)->GetEndNode()->GetNodeID()).at(x) == *toVisit.at(i)){
					continue;
				}else{
					toVisit.push_back(data->GetEdgesByNodeID(toVisit.at(i)->GetEndNode()->GetNodeID()).at(x));
				}
			}
			visitedNodeIDs.insert(toVisit.at(i)->GetEndNode()->GetNodeID());
		}
		//TODO: get a variable from our OPtionFile in here
		//if we meet our end condition before running our of streets to visit, return with our failure (and a therefore empty vector)
		//Note: the distance is in metres
		if(traveledDistance > 1000){
			std::vector<unsigned int> tempReturn;
			return tempReturn;
		}
	}
	return toReturn;
}

bool MotorwayTurnRemover::NoMotorwayEdges(Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(data->GetEdgesByNodeID(node->GetNodeID()).at(i)->GetStreetType() == motorway){
			return false;
		}
	}
	return true;
}

std::vector<Node *> MotorwayTurnRemover::GetMotorwayNodes(Node * node, unsigned int steps, MyData * data){
	std::vector<Node *> toReturn;
	std::vector<Edge *> eList = data->GetEdgesByNodeID(node->GetNodeID());
	for(unsigned int i = 0; i < eList.size(); i++){
		if(eList.at(i)->GetStreetType() != motorway){
			continue;
		}
		Edge * tempEdge = eList.at(i);
		Node * tempNode = node;
		for(unsigned int x = 0; x < steps; x++){
			tempNode = tempEdge->GetOtherNode(tempNode);
			toReturn.push_back(tempNode);
			//the bool is to remain false if no ensuing edges were found
			bool found = false;
			//loop to write a next edge of the motorway streettype into our tempEdge
			for(unsigned int j = 0; j < data->GetEdgesByNodeID(tempNode->GetNodeID()).size(); j++){
				Edge * testEdge = data->GetEdgesByNodeID(tempNode->GetNodeID()).at(j);
				if(* testEdge == *tempEdge){
					continue;
				}else if(testEdge->GetStreetType() != motorway){
					continue;
				}else {
					tempEdge = testEdge;
					found = true;
					break;
				}
			}
			//if there are no more motorways to follow from here, continue with the other edges
			if(!found){
				break;
			}
		}
	}
	//and we add the node itself here
	toReturn.push_back(node);
	return toReturn;
}

