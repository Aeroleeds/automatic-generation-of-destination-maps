#pragma once
#include "pipelinepart.h"
#include "RoundaboutHelper.h"
///Removes Roundabouts and replaces them with a single intersection
class RoundAboutKiller :
	public PipelinePart
{
public:
	RoundAboutKiller(void);
	~RoundAboutKiller(void);
	///Goes through all Roundabouts that are part of the MyData object and replaces them with a simple intersection
	/**	The intersection Node is placed at the middle of the Roundabout
		If there are multiple ramps leading to the same Node they are replaced by a single one (often the case due to one-way restrictions)*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();

	/*struct Roundabout{
		std::vector<Edge *> edges;
		std::vector<Node *> nodes;

		bool ContainsEdge(Edge * edge){
			return std::find(edges.begin(), edges.end(), edge) != edges.end();
		};
	};*/

private:
	//RoundaboutHelper::Roundabout ConstructRoundabout(Edge * e, MyData * data);
	///Sets the lat and lon values of the to be averaged node to the average value of all the nodes in the roundy
	/**	The lat/lon values remain unchanged if roundy has no nodes to be averaged */
	void SetNodeToAverage(Node * toBeAveraged, RoundaboutHelper::Roundabout roundy);
	//Removes all Nodes and Edges that only are part of the roundabout and connects all Nodes that are connected to the outside with the centre Node
	//void SimplifyRoundabout(RoundaboutHelper::Roundabout roundy, MyData * data);
	//Deletes all edges between the node shared by edge one and two and the roundabout, returns the shared node (that is to be connected to the replacing centre node)
	/*	if the two edges do not share a node, a nullptr is returned*/
	//Node * RemoveConnectedEdges(Edge * one, Edge * two, MyData * data);
	///Returns an edge leading from node that is a roundabout edge and not the edge passed on as param
	/**	If no such edge is found, a nullptr is returned (even though this should never happen)*/
	//Edge * GetNextRoundEdge(Edge * edge, Node * node, MyData * data);
	///Replaces the roundabout with a simple intersection, connecting all the nodes in nodes to the newly created intersection node, that is then returned
	/**	All edges of the roundabout will be deleted from the MyData set as will be all nodes that only have connections to other roundabout edges
		(i.e. all nodes of the roundabout with val == 0, AFTER all roundabout edges have been deleted)
		The centre point is set by averaging all nodes from the roundabout and will have a unique ID within the MyData set*/
	Node * ReplaceRoundabout(RoundaboutHelper::Roundabout roundy, std::vector<Node *> nodes, MyData * data);
	//returns true if the name of the Edge edge appears again in the vector of edges
	//Edge * StreetNameOccuringTwice(Edge * edge, std::vector<Edge *> edges);
	/*		
		HERE COMES THE NEW VERSION
	*/
	//bool HasNonRoundaboutEdges(Node * node, MyData * data);
	///Returns the next non-roundabout val != 2 Node when following the Edge edge from the start Node
	Node * GetNextImportantNode(Node * start, Edge * edge, MyData * data);
	//Removes the Edges in between the start and goal Node, starting w/edge
	/*	Deletes them from the MyData set*/
	//void RemoveEdgeInbetween(Node * start, Node * goal, Edge * edge, MyData * data);
	//Get the edgeIDs that lead from the start to the goal Node following the Edge edge
	//std::vector<unsigned int> GetEIDsInbetween(Node * start, Node * goal, Edge * edge, MyData * data);
	//follows the edge from the start node until a val != 2 node is reached and returns the eIDs found on the way
	std::vector<unsigned int> GeteIDs(Node * start, Edge * edge, MyData * data);
};

