#pragma once
#include "StdAfx.h"
#include "MyData.h"
#include "OptionFile.h"

std::map<std::string, StreetType> MyData::stringToStreetType = MyData::initialiseMap();

MyData * MyData::XMLToMyData(tinyxml2::XMLDocument * doc){return MyData::XMLToMyData(doc, 5);}

MyData * MyData::XMLToMyData(tinyxml2::XMLDocument * doc, unsigned int defaultSpeed){
	tinyxml2::XMLElement * root = doc->FirstChildElement();

	std::string t = root->Name();
	//the first child is the <xml> tag followed by the <osm> and the <bound> tag, after that the nodes begin
	tinyxml2::XMLElement * elem = root->FirstChildElement();//doc->FirstChildElement()->FirstChildElement()->FirstChildElement();
	t = elem->Name();
	if(elem == 0){
		//if we do not have a first entry, our result is empty, thus prompting a quick return
		return new MyData();
	}
	/*elem = elem->NextSiblingElement();
	if(elem == 0){
		return new MyData();
	}*/

	while(elem != 0 && std::string(elem->Name()) != "node"){
		elem = elem->NextSiblingElement();
	}
	//start by making up all them nodes
	std::vector<Node *> nodelist;
	std::vector<Edge *> edgelist;
	Node * node;
	double lon, lat;
	unsigned int id;
	if(elem == 0){
		return new MyData();
	}
	//go through all them nodes
	while(elem != 0 && (std::string(elem->Name()) == "node" || std::string(elem->Name()) == "tag")){
		//if we found a tag, go to the next value (we should actually never find a tag)
		if(std::string(elem->Name()) == "tag"){
			elem = elem->NextSiblingElement();
			continue;
		}
		lon = elem->DoubleAttribute("lon");
		lat = elem->DoubleAttribute("lat");
		id = elem->IntAttribute("id");
		//new Node to be deleted after the WHOLE thing is over
		node = new Node(lon, lat, id);
		//Add it to the list that will later be added
		nodelist.push_back(node);
		elem = elem->NextSiblingElement();
	}
	//create the MYDATA obj, and add the list of nodes
	MyData * data = new MyData();
	data->AddNodeList(nodelist);
	//then go looking for the way tags
	while(elem != 0 && std::string(elem->Name()) != "way"){
		elem->NextSiblingElement();
	}
	while(elem != 0){
		//get the tags, so we can directly assign their values to all the nodes
		tinyxml2::XMLElement * temp = elem->FirstChildElement();
		std::string street = "", oneWay, alternateName = "";
		StreetType type = numValues;
		int maxspeed = defaultSpeed;
		bool roundabout = false;
		//we first need to go through all the nd references to get the tag values we want
		while(temp != 0 && std::string(temp->Name()) != "tag"){
			temp = temp->NextSiblingElement();
		}
		while(temp != 0 && std::string(temp->Name()) == "tag"){
			//TODO: make this fail safe (e.g. the maxspeed attrib may contain "50 mph", which would fail miserably here, since we expect only an int)
			//if the k equals name
			if(temp->Attribute("k", "name")){
				street = temp->Attribute("v");
			} 
			if(temp->Attribute("k", "maxspeed")){
				maxspeed = temp->IntAttribute("v");
			}
			if(temp->Attribute("k", "highway")){
				//read the street type, and write it in here
				type = MyData::stringToStreetType.at(temp->Attribute("v"));
			
			}
			if(temp->Attribute("k", "ref")){
				alternateName = temp->Attribute("v");
			}
			if(temp->Attribute("k", "oneway")){
				oneWay = temp->Attribute("v");
				/*
				one way attribs can have the values "yes", "no" and "-1", with -1 meaning that it is a one way road, but not in the direction firstNode -> secondNode
				but the otherway (i.e. only traversable from the second to the first node)
				so if we get a -1 here, we need to flip the order of the nodes inside our created edges
				*/
			}
			if(temp->Attribute("k", "junction")){
				//this will only be true if the value indeed is roundabout
				if(temp->Attribute("v", "roundabout")){
					roundabout = true;
				}
			}
			temp = temp->NextSiblingElement("tag");
		}
		//TODO: We really badly need default values for when no maxspeed is provided (get them from a decent source, probably according to street type)
		//after all the tags, go through the node references
		//the first child of a way node is a nd reference
		temp = elem->FirstChildElement();
		node = data->GetNodeByID(temp->IntAttribute("ref"));
		temp = temp->NextSiblingElement("nd");
		if(temp != 0){
			Node * nodeTwo = data->GetNodeByID(temp->IntAttribute("ref"));
			Edge * edge = nullptr;
			if(street == ""){
				street = alternateName;
			}
			//if it is a reverse one-way road, flip the order of the nodes in the edge
			if(oneWay == "-1"){
				edge = new Edge(nodeTwo, node, maxspeed, street);
				edge->SetOneWay(true);
			}else{
				edge = new Edge(node, nodeTwo, maxspeed, street);
				if(oneWay == "yes"){
					edge->SetOneWay(true);
				}
			}
			if(maxspeed == 0){
				//we should actually never reach this point, just curious
				maxspeed = defaultSpeed;
			}
			if(roundabout){
				edge->SetRoundabout(true);
			}
			edge->SetStreetType(type);
			edgelist.push_back(edge);
			temp = temp->NextSiblingElement("nd");
			//lets start the loop of looking through the node elements
			while(temp != 0){
				node = nodeTwo;
				int myValue = temp->IntAttribute("ref");
				nodeTwo = data->GetNodeByID(temp->IntAttribute("ref"));
				if(oneWay == "-1"){
					edge = new Edge(nodeTwo, node, maxspeed, street);
					edge->SetOneWay(true);
				}else{
					edge = new Edge(node, nodeTwo, maxspeed, street);
					if(oneWay == "yes"){
						edge->SetOneWay(true);
					}
				}
				if(maxspeed == 0){
					//we should actually never reach this point, just curious
					maxspeed = defaultSpeed;
				}
				if(roundabout){
					edge->SetRoundabout(true);
				}
				edge->SetStreetType(type);
				edgelist.push_back(edge);
				temp = temp->NextSiblingElement("nd");
			}
		}
		//and repeat for the next way
		elem = elem->NextSiblingElement("way");
	}
	data->AddEdgeList(edgelist);
	return data;
}
tinyxml2::XMLDocument * MyData::MyDataToXML(MyData * data){
	tinyxml2::XMLDocument * doc = new tinyxml2::XMLDocument();
	//set our root elem, that is an osm tag w/the version and generator taken from our server (possibly should get that info from somewhere, rather than hard-code it here...)
	tinyxml2::XMLElement * root = doc->NewElement("osm");
	doc->InsertFirstChild(root);
	doc->RootElement()->SetAttribute("version", "0.6");
	doc->RootElement()->SetAttribute("generator", "Osmosis SNAPSHOT-r26564");
	//the first child elem is the bounding box elem, that is getting inserted right here
	tinyxml2::XMLElement * bb = doc->NewElement("bound");
	std::string b = std::to_string(OptionFile::bb.negLong) +"," + std::to_string(OptionFile::bb.negLat) +"," + std::to_string(OptionFile::bb.posLong) + ","+std::to_string(OptionFile::bb.posLat);
	bb->SetAttribute("box", b.c_str());
	bb->SetAttribute("origin", "Osmosis SNAPSHOT-r26564");
	// we add the <bound> tag to the <osm> as a child node
	root->InsertEndChild(bb);
	//then go through all the nodes and generate a Node for each one of them
	tinyxml2::XMLElement * node;
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		node = root->InsertEndChild(doc->NewElement("node"))->ToElement();
		node->SetAttribute("id", data->GetNodeList().at(i)->GetNodeID());
		node->SetAttribute("version", 2);
		//always write the lat/lon values here, the output is in osm format, thus should always be in lat/lon format
		node->SetAttribute("lat", data->GetNodeList().at(i)->GetLat(false));
		node->SetAttribute("lon", data->GetNodeList().at(i)->GetLon(false));
	}
	//after we have generated all our nodes, proceed with the edges
	std::set<unsigned int> visitedEdges;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		//first off, make sure we have not already inserted the way containing this edge
		if(visitedEdges.count(data->GetEdgeList().at(i)->GetEdgeID()) != 0){
			continue;
		}//if our edge is simply an inbetween edge (w/two valence 2 nodes) continue, for we will consider this edge when we get here from another edge
		else if(data->GetEdgeList().at(i)->GetStartNode()->GetValence() == 2 && data->GetEdgeList().at(i)->GetEndNode()->GetValence() == 2){
			continue;
		}//and the last case where we continue is if our edge is a one way edge, but our starting node (i.e. the one w/valence != 2) is on the wrong end of the one-way road
		else if(data->GetEdgeList().at(i)->IsOneWay() && data->GetEdgeList().at(i)->GetStartNode()->GetValence() == 2){
			continue;
		}
		//in here we need to follow all valence 2 nodes, and write them into the same way tag (we insert them right after the last node node)
		tinyxml2::XMLElement * edge = doc->NewElement("way")->ToElement();
		//we insert our own edge id here, not the one we originally got from our server
		edge->SetAttribute("id", data->GetEdgeList().at(i)->GetEdgeID());
		edge->SetAttribute("version", 2);
		//I highly doubt that we need the maxSpeed, oneway, etc. tags for rendering.... -.-
		//if we have a one way road, simply enter the start node first and the other Node 2nd
		tinyxml2::XMLElement * tempNode;
		Edge * edPointer;
		Node * nd;
		if(data->GetEdgeList().at(i)->IsOneWay()){
			tempNode = edge->InsertEndChild(doc->NewElement("nd"))->ToElement();
			tempNode->SetAttribute("ref", data->GetEdgeList().at(i)->GetStartNode()->GetNodeID());
			edPointer = data->GetEdgeList().at(i);
			nd = edPointer->GetEndNode();
		}
		//else we start by writing the node with val != 2, then the other node (with possibly val == 2), and continue down the road until we reach another valence != 2 node
		else{
			edPointer = data->GetEdgeList().at(i);
			nd = edPointer->GetStartNode()->GetValence() == 2 ? edPointer->GetEndNode() : edPointer->GetStartNode();
			tempNode = edge->InsertEndChild(doc->NewElement("nd"))->ToElement();
			tempNode->SetAttribute("ref", nd->GetNodeID());
			nd = edPointer->GetOtherNode(nd);
		}
		visitedEdges.insert(edPointer->GetEdgeID());
		//check whether the street type changes as well
		StreetType orig = edPointer->GetStreetType();
		Edge ref = *edPointer;
		//while we have an intermediate node
		while(nd->GetValence() == 2){
			//create the xml node with our new node
			tempNode = edge->InsertEndChild(doc->NewElement("nd"))->ToElement();
			tempNode->SetAttribute("ref", nd->GetNodeID());
			//then change the current edge to the next one
			if(*data->GetEdgesByNodeID(nd->GetNodeID()).at(0) == *edPointer){
				edPointer = data->GetEdgesByNodeID(nd->GetNodeID()).at(1);
			}else edPointer = data->GetEdgesByNodeID(nd->GetNodeID()).at(0);
			//TODO: stop the copy paste here, and make it beautiful
			//if our street changes type in between, finish the current way, and start a new one
			//if(orig != edPointer->GetStreetType() || ref.GetStreetName() != edPointer->GetStreetName() || ref.GetMaxSpeed() != edPointer->GetMaxSpeed() || ref.IsOneWay() != edPointer->IsOneWay()){
			//I do not think we need the orig here necessarily (TODO: verify Im right)
			if(!ref.HaveSameTags(edPointer)){
				tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
				tempNode->SetAttribute("k", "name");
				tempNode->SetAttribute("v", ref.GetStreetName().c_str());
				tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
				tempNode->SetAttribute("k", "oneway");
				if(ref.IsOneWay()){
					tempNode->SetAttribute("v", "yes");
				}else{
					tempNode->SetAttribute("v", "no");
				}
				tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
				tempNode->SetAttribute("k", "maxspeed");
				tempNode->SetAttribute("v", ref.GetMaxSpeed());
				//if the edge is a roundabout
				if(ref.IsRoundabout()){
					tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
					tempNode->SetAttribute("k", "junction");
					//if its not true, we do not land here anyway
					tempNode->SetAttribute("v", "roundabout");
				}
				//and set the street type here
				tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
				tempNode->SetAttribute("k", "highway");
				//important to note that we set the streetType to the original value, since we interrupt it there
				tempNode->SetAttribute("v", streetStrings[orig].c_str());
				//then add our <way> after the last <node>
				root->InsertEndChild(edge);

				/**
					And create a new way tag
				*/
				edge = doc->NewElement("way")->ToElement();
				//we insert our own edge id here, not the one we originally got from our server
				edge->SetAttribute("id", edPointer->GetEdgeID());
				edge->SetAttribute("version", 2);
				//and insert the node again (it is the "junction" between the two roads
				tempNode = edge->InsertEndChild(doc->NewElement("nd"))->ToElement();
				tempNode->SetAttribute("ref", nd->GetNodeID());
				//then reset our reference for streettypes
				orig = edPointer->GetStreetType();
				ref = *edPointer;
			}
			//Original loop continues here
			nd = edPointer->GetOtherNode(nd);
			visitedEdges.insert(edPointer->GetEdgeID());
		}
		//write the last node (the one w/valence != 2)
		tempNode = edge->InsertEndChild(doc->NewElement("nd"))->ToElement();
		tempNode->SetAttribute("ref", nd->GetNodeID());
		//then add the next tags (i.e. streetname)
		tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
		tempNode->SetAttribute("k", "name");
		tempNode->SetAttribute("v", edPointer->GetStreetName().c_str());
		tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
		tempNode->SetAttribute("k", "oneway");
		if(edPointer->IsOneWay()){
			tempNode->SetAttribute("v", "yes");
		}else{
			tempNode->SetAttribute("v", "no");
		}
		tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
		tempNode->SetAttribute("k", "maxspeed");
		tempNode->SetAttribute("v", edPointer->GetMaxSpeed());
		//if the edge is a roundabout
		if(edPointer->IsRoundabout()){
			tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
			tempNode->SetAttribute("k", "junction");
			//if its not true, we do not land here anyway
			tempNode->SetAttribute("v", "roundabout");
		}
		//and set the street type here
		tempNode = edge->InsertEndChild(doc->NewElement("tag"))->ToElement();
		tempNode->SetAttribute("k", "highway");
		tempNode->SetAttribute("v", streetStrings[edPointer->GetStreetType()].c_str());
		//then add our <way> after the last <node>
		root->InsertEndChild(edge);
		//and repeat all that for the other edges
	}
	//we might possibly need to close the osm tag here, but if tinyxml is coded properly, we wont need to
	return doc;
}

MyData::MyData(void){
	this->latestID = 1;
}

MyData::MyData(const MyData &data){
	std::vector<Node *> nodes;
	for(unsigned int i = 0; i < data.GetNodeList().size(); i++){
		Node * temp = new Node(data.GetNodeList().at(i)->GetLon(false), data.GetNodeList().at(i)->GetLat(false), data.GetNodeList().at(i)->GetNodeID());
		temp->SetRingRoad(data.GetNodeList().at(i)->IsRingRoad());
		temp->SetTaken(data.GetNodeList().at(i)->IsTaken());
		nodes.push_back(temp);
	}
	std::vector<Edge *> edges;
	for(unsigned int i = 0; i < data.GetEdgeList().size(); i++){
		Edge * temp = new Edge(data.GetEdgeList().at(i)->GetStartNode(), data.GetEdgeList().at(i)->GetEndNode(), data.GetEdgeList().at(i)->GetMaxSpeed(), data.GetEdgeList().at(i)->GetStreetName(), data.GetEdgeList().at(i)->GetEdgeID());
		temp->SetOneWay(data.GetEdgeList().at(i)->IsOneWay());
		temp->SetRingRoad(data.GetEdgeList().at(i)->IsRingRoad());
		temp->SetStreetType(data.GetEdgeList().at(i)->GetStreetType());
		temp->SetTaken(data.GetEdgeList().at(i)->IsTaken());
		temp->SetRoundabout(data.GetEdgeList().at(i)->IsRoundabout());
		edges.push_back(temp);
	}
	this->AddNodeList(nodes);
	this->AddEdgeList(edges);

	//this->ringRoads = data.GetRingRoads();
}

MyData & MyData::operator=(const MyData & data){
	std::vector<Node *> nodes;
	for(unsigned int i = 0; i < data.GetNodeList().size(); i++){
		Node * temp = new Node(data.GetNodeList().at(i)->GetLon(false), data.GetNodeList().at(i)->GetLat(false), data.GetNodeList().at(i)->GetNodeID());
		temp->SetRingRoad(data.GetNodeList().at(i)->IsRingRoad());
		temp->SetTaken(data.GetNodeList().at(i)->IsTaken());
		nodes.push_back(temp);
	}
	std::vector<Edge *> edges;
	for(unsigned int i = 0; i < data.GetEdgeList().size(); i++){
		Edge * temp = new Edge(data.GetEdgeList().at(i)->GetStartNode(), data.GetEdgeList().at(i)->GetEndNode(), data.GetEdgeList().at(i)->GetMaxSpeed(), data.GetEdgeList().at(i)->GetStreetName(), data.GetEdgeList().at(i)->GetEdgeID());
		temp->SetOneWay(data.GetEdgeList().at(i)->IsOneWay());
		temp->SetRingRoad(data.GetEdgeList().at(i)->IsRingRoad());
		temp->SetStreetType(data.GetEdgeList().at(i)->GetStreetType());
		temp->SetTaken(data.GetEdgeList().at(i)->IsTaken());
		temp->SetRoundabout(data.GetEdgeList().at(i)->IsRoundabout());
		edges.push_back(temp);
	}

	for(unsigned int i = 0; i < this->edgeList.size(); i++){
		delete edgeList.at(i);
	}
	for(unsigned int i = 0; i < this->nodeList.size(); i++){
		delete nodeList.at(i);
	}
	this->edgeList.clear();
	this->idToEdge.clear();
	this->idToNode.clear();
//	this->nameToEdge.clear();
//	this->nameToEdgeID.clear();
	this->nodeIdToEdge.clear();
	this->nodeList.clear();
	//this->ringRoads.clear();

	this->AddNodeList(nodes);
	this->AddEdgeList(edges);

//	this->ringRoads = data.GetRingRoads();
	return *this;
}

MyData::~MyData(void){
	int size = this->edgeList.size();
	for(int i = 0; i < size; i++){
		//deleting the edge 
		delete this->edgeList.at(i);
	}
	this->edgeList.clear();
	size = this->nodeList.size();
	for(int i = 0; i < size; i++){
		//and deleting all them nodes
		delete this->nodeList.at(i);
	}
	this->nodeList.clear();
}
/*
	Basic lists containing all the Edges/Nodes, mainly for looping through 'em
	NodeList will not contain duplicates
*/
std::vector<Node *> MyData::GetNodeList(){return this->nodeList;}
const std::vector<Node *> MyData::GetNodeList() const{return this->nodeList;}
std::vector<Edge *> MyData::GetEdgeList(){return this->edgeList;}
const std::vector<Edge *> MyData::GetEdgeList() const{return this->edgeList;}
/*
	Ways to get specific Edges/Nodes, either by StreetName or NodeID
*/
Node * MyData::GetNodeByID(unsigned int id){
	if(this->idToNode.count(id) != 0){return this->idToNode.at(id);}
	else return nullptr;
}
Edge * MyData::GetEdgeByID(unsigned int id){
	if(this->idToEdge.count(id) != 0){return this->idToEdge.at(id);}
	else return nullptr;
}
/*std::vector<Edge *> MyData::GetEdgesByStreetName(std::string name){
//	if(this->nameToEdge.count(name) != 0){return this->nameToEdge.at(name);}
//	else{
//		return std::vector<Edge *>();
//	}
	std::vector<Edge *> toRet;
	if(this->nameToEdgeID.count(name) != 0){
		//get all edge ids under the given name and add them to the vector
		std::vector<unsigned int> ids = nameToEdgeID.at(name);
		for(unsigned int i = 0; i < ids.size(); i++){
			toRet.push_back(this->idToEdge.at(ids.at(i)));
		}
	}
	return toRet;
}*/

std::vector<Edge *> MyData::GetEdgesByNodeID(unsigned int id){
	if(this->nodeIdToEdge.count(id) != 0){return this->nodeIdToEdge.at(id);}
	else{
		return std::vector<Edge *>();
	}
}
/*
std::set<std::string> MyData::GetRingRoads(){return this->ringRoads;}
const std::set<std::string> MyData::GetRingRoads() const{return this->ringRoads;}

void MyData::AddRingRoad(std::string name){
	this->ringRoads.insert(name);}
	*/
//used to add the data from another MyData object to this MyData object (necessary for compositing data from multiple xml files)
void MyData::Append(MyData * data){
	std::vector<Node *> nodes;
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * temp = new Node(data->GetNodeList().at(i)->GetLon(false), data->GetNodeList().at(i)->GetLat(false), data->GetNodeList().at(i)->GetNodeID());
		temp->SetRingRoad(data->GetNodeList().at(i)->IsRingRoad());
		temp->SetTaken(data->GetNodeList().at(i)->IsTaken());
		nodes.push_back(temp);
	}
	//add the created nodes, so we can reference their pointers in the upcoming edges
	this->AddNodeList(nodes);
	std::vector<Edge *> edges;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		Edge * temp = new Edge(this->GetNodeByID(data->GetEdgeList().at(i)->GetStartNode()->GetNodeID()), this->GetNodeByID(data->GetEdgeList().at(i)->GetEndNode()->GetNodeID()), data->GetEdgeList().at(i)->GetMaxSpeed(), data->GetEdgeList().at(i)->GetStreetName(), data->GetEdgeList().at(i)->GetEdgeID());
		temp->SetOneWay(data->GetEdgeList().at(i)->IsOneWay());
		temp->SetRingRoad(data->GetEdgeList().at(i)->IsRingRoad());
		temp->SetStreetType(data->GetEdgeList().at(i)->GetStreetType());
		temp->SetTaken(data->GetEdgeList().at(i)->IsTaken());
		temp->SetRoundabout(data->GetEdgeList().at(i)->IsRoundabout());
		edges.push_back(temp);
	}
	this->AddEdgeList(edges);
}
//ways to add an EdgeList (all containing Nodes will be added aswell, if they are not already in the MyData object), or a Nodelist
void MyData::AddEdgeList(std::vector<Edge *> edgelist){
	int size = edgelist.size();
	Edge * temp;
	for(unsigned int i = 0; i < edgelist.size(); i++){
		temp = edgelist.at(i);
		//if the current edge is already enlisted, continue (no need to check the nodes, for if the edge is in, the nodes are certainly as well)
		if(this->idToEdge.count(temp->GetEdgeID()) != 0){
			continue;
		}
		//else, check if the nodes are already in the DB, if not, add em
		AddIDToNode(temp->GetStartNode());
		AddIDToNode(temp->GetEndNode());

		//then proceed by saving the edge itself
		this->edgeList.push_back(temp);

		//add it to the list of edges under the given streetname
		/*if(this->nameToEdge.count(temp->GetStreetName()) != 0){
			this->nameToEdge.at(temp->GetStreetName()).push_back(temp);
		}else{
			std::vector<Edge *> v;
			v.push_back(temp);
			nameToEdge.insert(std::pair<std::string, std::vector<Edge *>>(temp->GetStreetName(), v));
		}*/
	/*	if(this->nameToEdgeID.count(temp->GetStreetName()) != 0){
			this->nameToEdgeID.at(temp->GetStreetName()).push_back(temp->GetEdgeID());
		//if there is no entry under that name yet, create a vector for it
		}else{
			std::vector<unsigned int> ids;
			ids.push_back(temp->GetEdgeID());
			nameToEdgeID.insert(std::pair<std::string, std::vector<unsigned int>>(temp->GetStreetName(), ids));
		}*/


		//add it to the list of edges under the nodes
		this->AddNodeIDToEdge(temp->GetStartNode(), temp);
		this->AddNodeIDToEdge(temp->GetEndNode(), temp);

		//and last and least, save it to the idToEdge map
		this->idToEdge.insert(std::pair<unsigned int, Edge*>(temp->GetEdgeID(), temp));
	}
}
void MyData::AddNodeList(std::vector<Node *> nodelist){
	Node * temp;
	for(unsigned int i = 0; i < nodelist.size(); i++){
		temp = nodelist.at(i);
		AddIDToNode(temp);
	}
}

void MyData::AddNodeIDToEdge(Node * n, Edge * e){
	//first off, increase the valence of the node (since a new edge is being added)
	n->SetValence(n->GetValence() + 1);
	//if there's already an edge on the current node, simply push the new edge into the existing vector
	if(this->nodeIdToEdge.count(n->GetNodeID()) != 0){
		this->nodeIdToEdge.at(n->GetNodeID()).push_back(e);
	}else{
		//if not, create the vector, and insert it anew
		std::vector<Edge *> v;
		v.push_back(e);
		this->nodeIdToEdge.insert(std::pair<unsigned int, std::vector<Edge *>>(n->GetNodeID(), v));
	}
}

void MyData::AddIDToNode(Node * n){
	//if the current Node is already in the DB, return
	if(this->idToNode.count(n->GetNodeID()) != 0){return;}
	//if its not in already, add it to both the list and the map
	this->nodeList.push_back(n);
	std::pair<unsigned int, Node *> pairToInsert(n->GetNodeID(), n);
	//this->idToNode.insert(std::pair<unsigned int, Node *>(n->GetNodeID(), n));
	this->idToNode.insert(pairToInsert);
}

void MyData::RemoveAndDeleteEdge(Edge * e){
	//if the edge is not in the data file, return
	if(e == nullptr || idToEdge.count(e->GetEdgeID()) == 0){
		return;
	}

	//remove from edgeList
	//int size = this->edgeList.size();
	for(unsigned int i = 0; i < edgeList.size(); i ++){
		if(edgeList.at(i)->Equals(*e)){
			edgeList.erase(edgeList.begin() + i);
			break;
		}
	}
	//remove from idToEdge map
	idToEdge.erase(e->GetEdgeID());

	//remove edges from the NodeIDToEdge map
	std::vector<Edge *> list = this->GetEdgesByNodeID(e->GetStartNode()->GetNodeID());
	//size = list.size();
	for(unsigned int i = 0; i < list.size(); i ++){
		if(list.at(i)->Equals(*e)){
			list.erase(list.begin() + i);
			nodeIdToEdge[e->GetStartNode()->GetNodeID()] = list;
			break;
		}
	}
	list = this->GetEdgesByNodeID(e->GetEndNode()->GetNodeID());
	//size = list.size();
	for(unsigned int i = 0; i < list.size(); i ++){
		if(list.at(i)->Equals((*e))){
			list.erase(list.begin() + i);
			nodeIdToEdge[e->GetEndNode()->GetNodeID()] = list;
			break;
		}
	}

	//remove from nameToEdge map
	/*list = nameToEdge.at(e->GetStreetName());
	size = list.size();
	for(int i = 0; i < size; i ++){
		if(list.at(i)->Equals(*e)){
			list.erase(list.begin() + i);
			nameToEdge[e->GetStreetName()] = list;
			break;
		}
	}*/
	/*std::vector<unsigned int> ids = nameToEdgeID.at(e->GetStreetName());
	for(unsigned int i = 0; i < ids.size(); i++){
		if(ids.at(i) == e->GetEdgeID()){
			ids.erase(ids.begin() + i);
			nameToEdgeID[e->GetStreetName()] = ids;
			break;
		}
	}*/


	e->GetStartNode()->SetValence(e->GetStartNode()->GetValence() - 1);
	e->GetEndNode()->SetValence(e->GetEndNode()->GetValence() - 1);
	delete e;
}

void MyData::RemoveAndDeleteNode(Node * n){
	//if the node is not in the data file, return
	if(n == nullptr || idToNode.count(n->GetNodeID()) == 0){
		return;
	}

	//remove from idToNode
	idToNode.erase(n->GetNodeID());

	//remove from nodelist
	int size = this->nodeList.size();
	for(int i = 0; i < size; i++){
		if(nodeList.at(i)->Equals(*n)){
			nodeList.erase(nodeList.begin() + i);
			break;
		}
	}

	this->nodeIdToEdge.erase(n->GetNodeID());

	delete n;
}

void MyData::RemoveAndDeleteAll(){
	std::vector<unsigned int> edgeIDs;
	std::vector<unsigned int> nodeIDs;
	for(unsigned int i = 0; i < this->edgeList.size(); i++){
		edgeIDs.push_back(edgeList.at(i)->GetEdgeID());
	}
	for(unsigned int i = 0; i < this->nodeList.size(); i++){
		nodeIDs.push_back(nodeList.at(i)->GetNodeID());
	}

	for(unsigned int i = 0; i < edgeIDs.size(); i++){
		RemoveAndDeleteEdge(this->GetEdgeByID(edgeIDs.at(i)));
	}

	for(unsigned int i = 0; i < nodeIDs.size(); i++){
		RemoveAndDeleteNode(this->GetNodeByID(nodeIDs.at(i)));
	}
	//this->nameToEdge.clear();
	//nameToEdgeID.clear();
}

unsigned int MyData::GetUniqueNodeID(){
	//increase the latestID by one (the latest ID is obviously already taken)
	latestID++;
	for(unsigned int i = 1; this->GetNodeByID(this->latestID) != nullptr; i++){
		if(i % 6 == 0){
			latestID += 50;
		}else{
			latestID++;
		}
	}
	return latestID;
}

