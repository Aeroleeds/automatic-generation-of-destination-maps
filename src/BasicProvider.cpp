#include "StdAfx.h"
#include "BasicProvider.h"


BasicProvider::BasicProvider(std::string settingsKey) : DataProvider(settingsKey)
{
	this->easy = curlpp::Easy();
	this->serverUrl = std::string();
	/*
		setup the necessary stuff for the xapi data provider
	*/
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err;
	err = doc->LoadFile(std::string("SETTINGS.xml").c_str());
	if(err == 0){
		//the first child elem is the root node, so we need to look through the firstchild's firstchild's elems
		tinyxml2::XMLElement * elem = doc->RootElement()->FirstChildElement();
		while(elem != 0 && std::string(elem->Name()) != settingsKey){
			elem = elem->NextSiblingElement();
		}
		if(elem != 0){
			this->serverUrl = elem->Attribute("url");	
		}
	}
}


BasicProvider::~BasicProvider(void)
{
}



tinyxml2::XMLDocument * BasicProvider::GetData(std::string key, std::string value, BoundingBox bb){
	//first we build our request
	std::string request = "";
	request.append(this->serverUrl);
	request.append("way[").append(key).append("=").append(value).append("]");
	request.append(bb.toString());
	//and get the information associated with our key value pair
	easy.setOpt(cURLpp::Options::Url(request));

	//then write it into a string
	std::ostringstream oss;
	curlpp::options::WriteStream ws(&oss);

	easy.setOpt(ws);
	easy.perform();
	std::string result = oss.str();
	//that will then be parsed into an tinyxml document (which will later be parsed into a MyData object)
	tinyxml2::XMLDocument * xmlDoc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err = xmlDoc->Parse(result.c_str());
	if(err == tinyxml2::XML_SUCCESS){
		return xmlDoc;
	}else return nullptr;
}

//serverurl should end with .../0.6/
bool BasicProvider::SetServer(std::string serverUrl){
	//TODO: make check whether server is reachable
	this->serverUrl = serverUrl;
	return true;
}
std::string BasicProvider::ToString(){
	return std::string("BasicDataProvider");
}

bool BasicProvider::SettingsLoaded(){
	//this provider needs only a server url, so return whether it was set
	return this->serverUrl != std::string();
}

