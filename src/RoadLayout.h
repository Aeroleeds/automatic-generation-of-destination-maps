#pragma once
#include "pipelinepart.h"
#include "LayoutMove.h"
#include "CostFunction.h"
#include "BorderLineControl.h"
///PipelinePart for improving the data layout after the correct data set has been determined in the previous steps
/**	Will try lots of different configurations and looks for improvement according to pre-determined heuristics
	Uses the CostFunction and the CostTerms as representation for these heuristics
	Additionally snaps border nodes back to the border and shrinks the layout back if it grows outside of the BoundingBox by using the BorderLineControl class
*/
class RoadLayout :
	public PipelinePart
{
public:
	RoadLayout(void);
	~RoadLayout(void);
	///Will improve the layout of data object by applying and evaluating multiple LayoutMove executions
	/**	If a LayoutMove improved the layout according to the heuristics it is taken and used as new step for more improvement
		The improvement is measured every 500-ndreth step, if the improvement is less than 1% of the previous cost the weights of the cost function are switched or the execution is terminated (if the last set of weights has been used)
		During the process of improvement RoadLayoutData objects are used, the final result is then inserted and returned as MyData object
		The LayoutMove class handles the changes to the layout while the CostFunction class evaluates the quality of the layout*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	///The object evaluating proposed layouts
	CostFunction func;
	///The object manifacturing new layout propositions
	LayoutMove mover;
	//
	//bool initialised;
	///The object necessary for snapping borderline nodes back to the border
	BorderLineControl blc;
};

