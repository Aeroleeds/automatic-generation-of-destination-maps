#pragma once
#include "pipelinepart.h"

//will make the dangling ends of taken streets (see 4.1.3)
///Will extend all dangling ends of taken streets for up to 1.5 km
/**	This is to provide more context, for if an extended street is crossing another taken street their streets connection is maintained during the later stages*/
class RoadExtender : public PipelinePart
{
public:
	RoadExtender(void);
	~RoadExtender(void);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
	///Extends all streets in both directions, starting at the last Node with still one taken Edge of the given street
	virtual std::string PrepDoc(MyData * data);
	///Extends the given edge up to 1.5km away from the node and returns the encountered edge IDs along the way
	/**	Follows all edges with the same name and streettype until either the distance is reached or no more edge is found*/
	std::set<unsigned int> FollowAndReturnRoad(Edge * edge, Node * node, MyData * data);
private:
	//std::map<unsigned int, std::string> endIDToStreetName;
	std::set<Node *> endNodes;
	std::set<Node *> startNodes;
	//std::set<std::string> visitedStreetNames;

	/**will follow the way from n to e until one of the following occurs
		-> The street changes name
		-> The edge leads to nodes not set as Taken
		-> the Edge leads to a Node in either endNodes or startNodes
		-> The edges lead back to the original node (in this case the original Node is returned again
		will then return the last still taken node along the path ,or a Node that is in end/startNodes, or a Node where the original path does not continue */
	Node * FollowTakenEdges(Node * n, Edge * e, MyData * data);
	///follows the node along the given streetname and will return the last Node it reached (either for the roadname changes or for 1,5 km have been traveled)
	Node * FollowAndTakeEdges(Node * n, std::string streetName, MyData * data);
	///will return the next edge from n following the path of e; or nullptr if none exists
	/**	DEPRECATED*/
	Edge * GetNextEdge(Node * n, Edge * e, MyData * data);
	///Will return ALL edges from n following the path of e; or an empty vector if none exists
	std::vector<Edge *> GetNextEdges(Node * n, Edge * e, MyData * data);

};

