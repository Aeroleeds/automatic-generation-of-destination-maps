#include "StdAfx.h"
#include "RoundAboutKiller.h"


RoundAboutKiller::RoundAboutKiller(void)
{
}


RoundAboutKiller::~RoundAboutKiller(void)
{
}


std::string RoundAboutKiller::PrepDoc(MyData * data){
	std::vector<RoundaboutHelper::Roundabout> roundabouts;
	//look for the roundabouts and write them into their structs
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		if(!data->GetEdgeList().at(i)->IsRoundabout()) continue;
		bool alreadyChecked = false;
		for(unsigned int x = 0; x < roundabouts.size(); x++){
			if(roundabouts.at(x).ContainsEdge(data->GetEdgeList().at(i))){
				alreadyChecked = true;
				break;
			}
		}
		if(alreadyChecked) continue;
		roundabouts.push_back(RoundaboutHelper::ConstructRoundabout(data->GetEdgeList().at(i), data));
	}
	//save the new intersection nodes
	std::vector<Node *> centres;
	//then simplify it
	for(unsigned int i = 0; i < roundabouts.size(); i++){
		//this->SimplifyRoundabout(roundabouts.at(i), data);
		std::vector<Node *> borderNodes;
		for(unsigned int x = 0; x < roundabouts.at(i).nodes.size(); x++){
			if(RoundaboutHelper::HasNonRoundaboutEdges(roundabouts.at(i).nodes.at(x), data)){
				borderNodes.push_back(roundabouts.at(i).nodes.at(x));
			}
		}
		//after we obtained all the bordernodes we replace the actual roundabout with a simple intersection node
		Node * temp = this->ReplaceRoundabout(roundabouts.at(i), borderNodes, data);
		if(temp != nullptr){
			centres.push_back(temp);
		}
	}
	std::vector<Edge *> edgesToAdd;
	std::vector<unsigned int> eIDsToDelete;
	for(unsigned int i = 0; i < centres.size(); i++){
		//vector to put all the val 3 nodes into, that are leading into the roundabout
		std::vector<Node *> borderNodes;
		for(unsigned int x = 0; x < data->GetEdgesByNodeID(centres.at(i)->GetNodeID()).size(); x++){
			Node * temp = this->GetNextImportantNode(centres.at(i), data->GetEdgesByNodeID(centres.at(i)->GetNodeID()).at(x), data);
			//if the found node is a roundabout node, keep following it, and add the outer nodes
			if(temp->IsRoundabout()){
				for(unsigned int z = 0; z < data->GetEdgesByNodeID(temp->GetNodeID()).size(); z++){
					Node * tempNode = this->GetNextImportantNode(temp, data->GetEdgesByNodeID(temp->GetNodeID()).at(z), data);
					if(*tempNode == *centres.at(i)) continue;
					else{
						borderNodes.push_back(tempNode);
					}
				}
			}else{
				borderNodes.push_back(temp);
			}
		}
		//now that we found all the bordernodes for the current centre, delete ways if they appear more than once
		for(unsigned int x = 0; x < borderNodes.size(); x++){
			//count keeps count of how many times the centre node has been reached from this specific border node
			int count = 0;
			std::vector<unsigned int> tempDelete;
			//then look through all the edges starting at that bordernode
			for(unsigned int z = 0; z < data->GetEdgesByNodeID(borderNodes.at(x)->GetNodeID()).size(); z++){
				Node * temp = this->GetNextImportantNode(borderNodes.at(x), data->GetEdgesByNodeID(borderNodes.at(x)->GetNodeID()).at(z), data);
				//if this path lead to the centre, save it into the potentially to-be-deleted tempDelete vector and inc the count
				if(*temp == *centres.at(i)){
					count++;
					std::vector<unsigned int> vec = this->GeteIDs(borderNodes.at(x), data->GetEdgesByNodeID(borderNodes.at(x)->GetNodeID()).at(z), data);
					//walk the path again, to get the eIDs
					tempDelete.insert(tempDelete.end(), vec.begin(), vec.end());
				}else if(temp->IsRoundabout()){
					//if it is a roundabout we follow all edges starting here again, in order to find potential connections to our centre
					for(unsigned int a = 0; a < data->GetEdgesByNodeID(temp->GetNodeID()).size(); a++){
						Node * tempN = this->GetNextImportantNode(temp, data->GetEdgesByNodeID(temp->GetNodeID()).at(a), data);
						if(*tempN == *centres.at(i)){
							count++;
							//add the path that lead to the tempnode	
							std::vector<unsigned int> vec = this->GeteIDs(borderNodes.at(x), data->GetEdgesByNodeID(borderNodes.at(x)->GetNodeID()).at(z), data);
							tempDelete.insert(tempDelete.end(), vec.begin(), vec.end());
							//and the path from temp to centre node
							vec = this->GeteIDs(tempN, data->GetEdgesByNodeID(temp->GetNodeID()).at(a), data);
							tempDelete.insert(tempDelete.end(), vec.begin(), vec.end());
						}
					}
				}
			}
			//after all the computations, if count is larger than two, get serious about deleting the edges and connecting the borderNodes instead
			if(count > 1){
				//create the new edge
				//we use the edge at 0 as ref, for it is the one directly connected to our border node and thus most likely carries the correct information
				Edge * edge = new Edge(borderNodes.at(x), centres.at(i), data->GetEdgeByID(tempDelete.at(0))->GetMaxSpeed(), data->GetEdgeByID(tempDelete.at(0))->GetStreetName()); 
				edge->SetStreetType(data->GetEdgeByID(tempDelete.at(0))->GetStreetType());
				edgesToAdd.push_back(edge);
				//and delete the unneccessary ones
				eIDsToDelete.insert(eIDsToDelete.end(), tempDelete.begin(), tempDelete.end());
			}
		}
	}
	//delete the edges
	for(unsigned int i = 0; i < eIDsToDelete.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(eIDsToDelete.at(i)));
	}
	std::vector<Edge *> cleanedAddedEdges;
	//and add the new edges, if the two nodes are not already connected
	for(unsigned int i = 0; i < edgesToAdd.size(); i++){
		Node * start = edgesToAdd.at(i)->GetStartNode();
		bool merge = true;
		for(unsigned int x = 0; x < data->GetEdgesByNodeID(start->GetNodeID()).size(); x++){
			//they are already connected when we find an edge in the data set that connects the two edges of edgesToAdd.at(i)
			if(*data->GetEdgesByNodeID(start->GetNodeID()).at(x)->GetOtherNode(start) == *edgesToAdd.at(i)->GetEndNode()){
				merge = false;
				break;
			}
		}
		//then loop through the edges yet-to-be added, and check for dupes
		for(unsigned int x = 0; x < cleanedAddedEdges.size(); x++){
			Edge * soonEdge = cleanedAddedEdges.at(x);
			if(*soonEdge->GetStartNode() == *edgesToAdd.at(i)->GetStartNode() && *soonEdge->GetEndNode() == *edgesToAdd.at(i)->GetEndNode()){
				merge = false;
				break;
			}else if(*soonEdge->GetEndNode() == *edgesToAdd.at(i)->GetStartNode() && *soonEdge->GetStartNode() == *edgesToAdd.at(i)->GetEndNode()){
				merge = false;
				break;
			}
		}
		if(merge){
			cleanedAddedEdges.push_back(edgesToAdd.at(i));
		}else{
			//if we do not add it, delete it (NOT THE NODES, they are alive and well)
			delete(edgesToAdd.at(i));
		}
	}
	data->AddEdgeList(cleanedAddedEdges);
	//then cleanup all nodes with val == 0
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		if(data->GetNodeList().at(i)->GetValence() == 0){
			data->RemoveAndDeleteNode(data->GetNodeList().at(i));
		}
	}

	/*
	//then merge any two edges leading from a centre node to the same other node
 	std::vector<Edge *> toAdd;
	for(unsigned int i = 0; i < centres.size(); i++){
		//save a bunch of stuff for easy deletion
		//std::vector<Node *> startNodes;
		std::vector<Node *> endNodes;
		std::vector<Edge *> edgesToRem;

		std::vector<Edge *> edges = data->GetEdgesByNodeID(centres.at(i)->GetNodeID());
		for(unsigned int x = 0; x < edges.size(); x++){
			std::vector<Node *> goals;
			Node * goal = GetNextImportantNode(centres.at(i), edges.at(x), data);
			//if the first val 3 node is still part of the roundabout, keep following
			goals.push_back(goal);
			if(goal->IsRoundabout()){
				for(unsigned int z = 0; z < data->GetEdgesByNodeID(goal->GetNodeID()).size(); z++){
					Node * tempGoal = this->GetNextImportantNode(goal, data->GetEdgesByNodeID(goal->GetNodeID()).at(z), data);
					if(*tempGoal == *goal) continue;
					goals.push_back(tempGoal);
				}
			}else{
				goals.push_back(goal);
			}
			for(unsigned int y = x + 1; y < edges.size(); y++){
				Node * tempGoal = GetNextImportantNode(centres.at(i), edges.at(y), data);
				if(tempGoal->IsRoundabout()){
					for(unsigned int z = 0; z < data->GetEdgesByNodeID(goal->GetNodeID()).size(); z++){
						Node * goalNode = this->GetNextImportantNode(goal, data->GetEdgesByNodeID(goal->GetNodeID()).at(z), data);
						if(*tempGoal == *goalNode) continue;
						if(std::find(goals.begin(), goals.end(), tempGoal) != goals.end()){
							//if we found a match, add it to our vectors
							endNodes.push_back(goal);
							edgesToRem.push_back(edges.at(x));
							endNodes.push_back(goal);
							edgesToRem.push_back(edges.at(y));
							//and create the new edge leading directly to our new goalNode node
							Edge * newEdge = new Edge(centres.at(i), goalNode, data->GetEdgesByNodeID(goal->GetNodeID()).at(z)->GetMaxSpeed(), data->GetEdgesByNodeID(goal->GetNodeID()).at(z)->GetStreetName());
							newEdge->SetStreetType(data->GetEdgesByNodeID(goal->GetNodeID()).at(z)->GetStreetType());
							toAdd.push_back(newEdge);
						}
					}
				}
				else if(std::find(goals.begin(), goals.end(), tempGoal) != goals.end()){
					//if we found a match, add it to our vectors
					endNodes.push_back(goal);
					edgesToRem.push_back(edges.at(x));
					endNodes.push_back(goal);
					edgesToRem.push_back(edges.at(y));
					//and create the new edge leading directly to our intersection node
					Edge * newEdge = new Edge(centres.at(i), tempGoal, edges.at(x)->GetMaxSpeed(), edges.at(x)->GetStreetName());
					newEdge->SetStreetType(edges.at(x)->GetStreetType());
					toAdd.push_back(newEdge);
				}
				//check if the edges at x and y lead to the same node
				/*if(*goal == *GetNextImportantNode(centres.at(i), edges.at(y), data)){
					//if we found a match, add it to our vectors
					endNodes.push_back(goal);
					edgesToRem.push_back(edges.at(x));
					endNodes.push_back(goal);
					edgesToRem.push_back(edges.at(y));
					//and create the new edge leading directly to our intersection node
					Edge * newEdge = new Edge(centres.at(i), goal, edges.at(x)->GetMaxSpeed(), edges.at(x)->GetStreetName());
					newEdge->SetStreetType(edges.at(x)->GetStreetType());
					toAdd.push_back(newEdge);
				}*/
		/*	}
		}
		//we delete by eID to allow one edge to be deleted "twice"
		std::vector<unsigned int> edgesToDel;
		/*for(unsigned int x = 0; x < endNodes.size(); x++){
			this->RemoveEdgeInbetween(centres.at(i), endNodes.at(x), edgesToRem.at(x), data);
		}*/
		//get the eIDs for the way from the centre nodes to the goal nodes
	/*	for(unsigned int x = 0; x < endNodes.size(); x++){
			std::vector<unsigned int> tempVec = this->GetEIDsInbetween(centres.at(i), endNodes.at(x), edgesToRem.at(x), data);
			edgesToDel.insert(edgesToDel.begin(), tempVec.begin(), tempVec.end());
		}
		//remove all the edges belonging to the IDs (this is save, since if an ID is appearing twice, getEdge will return a nullptr, and no harm will be done)
		for(unsigned int x = 0; x < edgesToDel.size(); x++){
			data->RemoveAndDeleteEdge(data->GetEdgeByID(edgesToDel.at(x)));
		}
		//and make a cleanup run for all the nodes that now have a val of 0
	}
	//and add the newly created connections
	data->AddEdgeList(toAdd);*/
	return std::string("Splendid");
}

std::vector<unsigned int> RoundAboutKiller::GeteIDs(Node * start, Edge * edge, MyData * data){
	std::vector<unsigned int> ret;
	Node *temp = edge->GetOtherNode(start);
	Edge * tempEdge = edge;
	while(temp->GetValence() == 2){
		ret.push_back(tempEdge->GetEdgeID());
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
		tempEdge = *tempEdge == *edges.at(0) ? edges.at(1) : edges.at(0); 
		temp = tempEdge->GetOtherNode(temp);
	}
	ret.push_back(tempEdge->GetEdgeID());
	return ret;
}
/*
std::vector<unsigned int> RoundAboutKiller::GetEIDsInbetween(Node * start, Node * goal, Edge * edge, MyData * data){
	std::vector<unsigned int> ret;
	Node * temp = edge->GetOtherNode(start);
	Edge * tempEdge = edge;
	while(*temp != *goal){
		ret.push_back(tempEdge->GetEdgeID());
		if(temp->GetValence() == 2){
			std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
			tempEdge = *tempEdge == *edges.at(0) ? edges.at(1) : edges.at(0);
			temp = tempEdge->GetOtherNode(temp);
		}else{
			for(unsigned int i = 0; i < data->GetEdgesByNodeID(temp->GetNodeID()).size(); i++){
				if(*goal == *this->GetNextImportantNode(temp, data->GetEdgesByNodeID(temp->GetNodeID()).at(i), data)){
					tempEdge = data->GetEdgesByNodeID(temp->GetNodeID()).at(i);
					temp = tempEdge->GetOtherNode(temp);
				}
			}
		}
	}
	ret.push_back(tempEdge->GetEdgeID());
	return ret;
}
*/

/*
void RoundAboutKiller::RemoveEdgeInbetween(Node * start, Node * goal, Edge * edge, MyData * data){
	//if they do, replace the two edges by a simple edge to the centre
	Node * temp = edge->GetOtherNode(start);
//	Node * temp = edges.at(x)->GetOtherNode(centres.at(i));
//	Node * tempTwo = centres.at(i);
	Node * tempTwo = start;
//	Edge * tempEdge = edges.at(x);
	Edge * tempEdge = edge;
	while(*temp != *goal){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(tempEdge->GetEdgeID()));
		//if we leave a node behind without a single edge, delete it
		if(*tempTwo != *start && tempTwo->GetValence() == 0){
			data->RemoveAndDeleteNode(data->GetNodeByID(tempTwo->GetNodeID()));
		}
		tempEdge = data->GetEdgesByNodeID(temp->GetNodeID()).at(0);
		tempTwo = temp;
		temp = tempEdge->GetOtherNode(temp);
	}
	data->RemoveAndDeleteEdge(data->GetEdgeByID(tempEdge->GetEdgeID()));
	if(*tempTwo != *start && tempTwo->GetValence() == 0){
		data->RemoveAndDeleteNode(data->GetNodeByID(tempTwo->GetNodeID()));
	}

}
*/

std::string RoundAboutKiller::ToString(){
	return std::string("RoundAboutKiller");
}
/*
bool RoundAboutKiller::HasNonRoundaboutEdges(Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(!data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout()) return true;
	}
	return false;
}*/

Node * RoundAboutKiller::GetNextImportantNode(Node * start, Edge * edge, MyData * data){
	Node * temp = edge->GetOtherNode(start);
	Edge * tempEdge = edge;
	while(temp->GetValence() == 2){
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());		
		tempEdge = *edges.at(0) == *tempEdge ? edges.at(1) : edges.at(0);
		temp = tempEdge->GetOtherNode(temp);
	}
	return temp;
}

/*
RoundaboutHelper::Roundabout RoundAboutKiller::ConstructRoundabout(Edge * e, MyData * data){
	RoundaboutHelper::Roundabout roundy;
	Node * temp = e->GetEndNode();
	Edge * tempEdge = e;
	roundy.nodes.push_back(e->GetStartNode());
	while(*temp != *e->GetStartNode()){
		roundy.edges.push_back(tempEdge);
		roundy.nodes.push_back(temp);
		tempEdge = this->GetNextRoundEdge(tempEdge, temp, data);
		if(tempEdge == nullptr){
			return roundy;
		}
		temp = tempEdge->GetOtherNode(temp);
	}
	//we still need to add the last edge to the roundabout
	roundy.edges.push_back(tempEdge);
	return roundy;
}*/

void RoundAboutKiller::SetNodeToAverage(Node * toBeAveraged, RoundaboutHelper::Roundabout roundy){
	double lat = 0, lon = 0;
	//averaging in mercator values should be fine
	for(unsigned int i = 0; i < roundy.nodes.size(); i++){
		lat += roundy.nodes.at(i)->GetLat();
		lon += roundy.nodes.at(i)->GetLon();
	}
	if(roundy.nodes.size() != 0){
		//if we set them as mercator values as well
		toBeAveraged->SetLat(lat / roundy.nodes.size());
		toBeAveraged->SetLon(lon / roundy.nodes.size());
	}
}
/*
Node * RoundAboutKiller::RemoveConnectedEdges(Edge * one, Edge * two, MyData * data){
	Node * start = (*one->GetStartNode() == *two->GetStartNode() || *one->GetStartNode() == *two->GetEndNode()) ? one->GetStartNode() : one->GetEndNode();
	std::vector<Node *> toDelete;
	std::vector<Edge *> edgesToDelete;
	//Node * temp = one->GetOtherNode(start);
	Edge * tempEdge = one;
	for(unsigned int i = 0; i < 2; i++){
		Edge * tempEdge = nullptr;
		if(i == 0){
			tempEdge = one;
		}else if(i == 1){
			tempEdge = two;
		}
		Node * temp = tempEdge->GetOtherNode(start);
		while(!temp->IsRoundabout() && temp->GetValence() == 2){
			edgesToDelete.push_back(tempEdge);
			toDelete.push_back(temp);
			tempEdge = *data->GetEdgesByNodeID(temp->GetNodeID()).at(0) == *tempEdge ? data->GetEdgesByNodeID(temp->GetNodeID()).at(1) : data->GetEdgesByNodeID(temp->GetNodeID()).at(0); 
			temp = tempEdge->GetOtherNode(temp);
		}
		edgesToDelete.push_back(tempEdge);
	}
	//then delete all the found stuffings
	for(unsigned int i = 0; i < edgesToDelete.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(edgesToDelete.at(i)->GetEdgeID()));
	}
	for(unsigned int i = 0; i < toDelete.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(toDelete.at(i)->GetNodeID()));
	}
	//and null the deleted edges
	one = nullptr;
	two = nullptr;
	return start;
}
*/
/*Edge * RoundAboutKiller::GetNextRoundEdge(Edge * edge, Node * node, MyData * data){
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(*data->GetEdgesByNodeID(node->GetNodeID()).at(i) == *edge){
			continue;
		}else if(!data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRoundabout()){
			continue;
		}else return data->GetEdgesByNodeID(node->GetNodeID()).at(i);
	}
	return nullptr;
}*/

/*void RoundAboutKiller::SimplifyRoundabout(RoundaboutHelper::Roundabout roundy, MyData * data){
	std::vector<Edge *> borderEdges;
	std::vector<Node *> borderNodes;
	for(unsigned int i = 0; i < roundy.nodes.size(); i++){
		if(roundy.nodes.at(i)->GetValence() > 2){
			Node * tempNode = roundy.nodes.at(i);
			Edge * tempEdge = nullptr;
			for(unsigned int i = 0; i < data->GetEdgesByNodeID(tempNode->GetNodeID()).size(); i++){
				if(data->GetEdgesByNodeID(tempNode->GetNodeID()).at(i)->IsRoundabout()) continue;
				tempEdge = data->GetEdgesByNodeID(tempNode->GetNodeID()).at(i);
			}
			if(tempEdge == nullptr) continue;
			else borderEdges.push_back(tempEdge);
		}
	}
	std::vector<std::pair<Edge *, Edge *>> toRem;
	//maybe make sure we do not check for everything twice?
	for(unsigned int i = 0; i < borderEdges.size(); i++){
		Edge * tempEdge = this->StreetNameOccuringTwice(borderEdges.at(i), borderEdges);
		if(tempEdge == nullptr) continue;
		Node * tempNode = (tempEdge->GetStartNode()->IsRoundabout() ? tempEdge->GetEndNode() : tempEdge->GetStartNode());
		Node * borderNode = (borderEdges.at(i)->GetStartNode()->IsRoundabout() ? borderEdges.at(i)->GetEndNode() : borderEdges.at(i)->GetStartNode());
		Edge * bEdge = borderEdges.at(i);
		while(tempNode->GetValence() == 2){
			tempEdge = *data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) == *tempEdge ? data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0);
			tempNode = tempEdge->GetOtherNode(tempNode);
		}
		while(borderNode->GetValence() == 2){
			bEdge = *data->GetEdgesByNodeID(borderNode->GetNodeID()).at(0) == *bEdge ? data->GetEdgesByNodeID(borderNode->GetNodeID()).at(1) : data->GetEdgesByNodeID(borderNode->GetNodeID()).at(0);
			borderNode = bEdge->GetOtherNode(borderNode);
		}
		if(*borderNode == *tempNode){
			//borderNodes.push_back(this->RemoveConnectedEdges(bEdge, tempEdge, data));
			borderNodes.push_back((*bEdge->GetStartNode() == *tempEdge->GetStartNode() || *bEdge->GetStartNode() == *tempEdge->GetEndNode()) ? bEdge->GetStartNode() : bEdge->GetEndNode());
			toRem.push_back(std::pair<Edge *, Edge *>(bEdge, tempEdge));
		}
		else borderNodes.push_back(borderEdges.at(i)->GetStartNode()->IsRoundabout() ? borderEdges.at(i)->GetStartNode() : borderEdges.at(i)->GetEndNode()); 
	}
	std::set<unsigned int> removedIDs;
	std::vector<std::pair<Edge *, Edge*>> uniqueRemove;
	for(unsigned int i = 0; i < toRem.size(); i++){
		if(removedIDs.count(toRem.at(i).first->GetEdgeID()) == 0 && removedIDs.count(toRem.at(i).second->GetEdgeID()) == 0){
			uniqueRemove.push_back(toRem.at(i));
			removedIDs.insert(toRem.at(i).first->GetEdgeID());
			removedIDs.insert(toRem.at(i).second->GetEdgeID());
		}
	}
	for(unsigned int i = 0; i < uniqueRemove.size(); i++){
		this->RemoveConnectedEdges(uniqueRemove.at(i).first, uniqueRemove.at(i).second, data);
	}

	this->ReplaceRoundabout(roundy, borderNodes, data);	
}*/

Node * RoundAboutKiller::ReplaceRoundabout(RoundaboutHelper::Roundabout roundy, std::vector<Node *> nodes, MyData * data){
	if(roundy.edges.size() == 0 || roundy.nodes.size() == 0) return nullptr;
	//We need to assign a correct ID here
	Node * centreNode = new Node(2, 2, data->GetUniqueNodeID());
	this->SetNodeToAverage(centreNode, roundy);
	StreetType refType = roundy.edges.at(0)->GetStreetType();
	std::string refName = roundy.edges.at(0)->GetStreetName();

	for(unsigned int i = 0; i < roundy.edges.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(roundy.edges.at(i)->GetEdgeID()));
	}
	for(unsigned int i = 0; i < roundy.nodes.size(); i++){
		//we already deleted the edges, we should delete all nodes without a connection here
		if(roundy.nodes.at(i)->GetValence() == 0){
			data->RemoveAndDeleteNode(data->GetNodeByID(roundy.nodes.at(i)->GetNodeID()));
		}
	}
	std::vector<Edge *> edgesToAdd;
	//connect all nodes to the centre Node
	for(unsigned int i = 0; i < nodes.size(); i++){
		Edge * referenceEdge = nullptr;
		if(data->GetEdgesByNodeID(nodes.at(i)->GetNodeID()).size() > 0){
			referenceEdge = data->GetEdgesByNodeID(nodes.at(i)->GetNodeID()).at(0);
		}
		Edge * edge = nullptr;
		if(referenceEdge != nullptr){
			edge = new Edge(nodes.at(i), centreNode, referenceEdge->GetMaxSpeed(), referenceEdge->GetStreetName());
			edge->SetStreetType(referenceEdge->GetStreetType());
		}else{
			edge = new Edge(nodes.at(i), centreNode, 10, refName);
			edge->SetStreetType(refType);
		}
		edgesToAdd.push_back(edge);
	}
	data->AddEdgeList(edgesToAdd);
	return centreNode;
}

/*
Edge * RoundAboutKiller::StreetNameOccuringTwice(Edge * edge, std::vector<Edge *> edges){
	for(unsigned int i = 0; i < edges.size(); i++){
		if(edges.at(i) != nullptr && edges.at(i)->GetStreetName() == edge->GetStreetName() && *edges.at(i) != *edge) return edges.at(i);
	}
	return nullptr;
}*/

