#pragma once
#include "pipelinepart.h"

///Class to reduce all Edges with a val == 1 Node (that are not at the boundary) until they reach another Edge
/**	Reduces clutter while maintaining the information about the crossings (which roads go off and which one to take)*/
class RoadStumper :
	public PipelinePart
{
public:
	RoadStumper(void);
	~RoadStumper(void);
	///Removes all Edges with a val == 1 Node (that are not at the boundary) until they reach another Edge
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
};

