#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#pragma once

//#include <qdialog.h>
#include <QDialog>
#include "ui_ProgressDialog.h"
#include "stdafx.h"
///QDialog to show the progress made when the Pipeline is doing its work
class ProgressDialog : public QDialog
{

	Q_OBJECT

public:
	ProgressDialog(QWidget *parent = 0);
	~ProgressDialog(void);
	///Sets the progressdialog's main text overwriting the previous main text and deleting possibly existing sub texts
	void SetMainText(QString text);
	///Sets the progressdialog's sub text overwriting the previous sub text 
	void SetSubText(QString text);		
private:
	///The ui associated with the progress dialog
	Ui::ProgressDialog ui;
};

#endif // PROGRESSDIALOG_H
