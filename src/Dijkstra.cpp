#include "StdAfx.h"
#include "Dijkstra.h"
#include <set>
#include "Vec2.h"
#include <io.h>


Dijkstra::Dijkstra(void){
	//some default values for penalty n factor
	this->penalty = 10;
	this->factor = 1;
}

Dijkstra::~Dijkstra(void){

}
/*
std::vector<Node *> Dijkstra::DijkstraPath(MyData * data, Node * start, Node * goal){

	std::multiset<std::pair<double, Node *>, Dijkstra::comparator> queue;
	dijkData dijk;

	//fill the meta data with default values
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		//write the node into a variable to prevent the lookup from happening four times
		Node * node = data->GetNodeList().at(i);
		if(node->GetValence() != 2){
			//predecessor is initialised to 0, which means no prev edge
			dijk.idToPred.insert(std::pair<unsigned int, unsigned int>(node->GetNodeID(), 0));
			dijk.idToStatus.insert(std::pair<unsigned int, unsigned int>(node->GetNodeID(), 0));
			dijk.idToDist.insert(std::pair<unsigned int, unsigned int>(node, DBL_MAX));
		}
	}
	//and setup a goal distance (will only be important if a goal node has been specified by the user, in which case the algorithm stops if the best goal distance is unbeatable
	double goalDist = DBL_MAX;
	//start at the start
	dijk.idToDist[start->GetNodeID()] = 0;
	dijk.idToStatus[start->GetNodeID()] = 1;
	queue.insert(std::pair<double, Node *>(0, start->GetNodeID()));
	//while there is still a node in the queue (the loop may be interrupted, in case goal node has been defined)
	while(!queue.empty()){
		//check whether the goaldistance is shorter than the current distance (if so, stop right here)
		if(goal != nullptr && queue.begin()->first > goalDist){
			//return sth here
			//seriously
			//do it
		}
		//get the node with the shortest distance and remove it from our queue
		Node * temp = (*queue.begin()).second;
		queue.erase(queue.begin());
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
		for(unsigned int i = 0; i < edges.size(); i++){
			//get the weight associated with the edge leading to the returned node
			Dijkstra::WayQuery result = this->GetWeightAndNode(temp, edges.at(i), data->GetEdgeByID(dijk.idToPred.at(temp->GetNodeID())), data);
			//if that new distance is smaller than the old one, replace them
			if(dijk.idToDist.at(temp->GetNodeID()) + result.wayWeight < dijk.idToDist.at(result.resultingNodeID)){
				//save the old key, in case the node is to be replaced (i.e. the nodes status is 1, meaning already in the queue)
				double oldKey = dijk.idToDist.at(result.resultingNodeID);
				dijk.idToDist[result.resultingNodeID] = (dijk.idToDist.at(temp->GetNodeID()) + result.wayWeight);
				dijk.idToPred[result.resultingNodeID] = temp->GetNodeID();
				//if the node is not yet in queue, insert it
				if(dijk.idToStatus[result.resultingNodeID] == 0){
					dijk.idToStatus[result.resultingNodeID] = 1;
					queue.insert(std::pair<double, Node *>(dijk.idToDist[result.resultingNodeID], data->GetNodeByID(result.resultingNodeID)));
				//if the node is already in the queue, replace the old value with the new one
				}else if(dijk.idToStatus[result.resultingNodeID] == 1){
					std::multiset<std::pair<double, Node *>, Dijkstra::comparator>::iterator it;
					Node * currentNode = data->GetNodeByID(result.resultingNodeID);
					it = queue.find(std::pair<double, Node *>(oldKey, currentNode));
					//while we are at another node with the same distance, skip one by one until we find the correct node
					//the it != queue.end() is just a fail-safe, should never be important, so possibly delete it
					while(*it->second != *currentNode && it != queue.end()){
						it++;
					}
					//after we found it, erase it
					queue.erase(it);
					//And insert the new pair (same node w/diff distance)
					queue.insert(std::pair<double, Node *>(dijk.idToDist.at(result.resultingNodeID), currentNode));
				}
				//if the node has already been visited, dont do a thing (i.e. status == 2)
				//but in any case, check whether the goal has been (b)reached
				if(result.resultingNodeID == goal->GetNodeID() && dijk.idToDist.at(result.resultingNodeID) < goalDist){
					goalDist = dijk.idToDist.at(result.resultingNodeID);
				}

			}
		}
		dijk.idToStatus[temp->GetNodeID()] = 2;
	}

	//then return the meta data (dijk)




	//the vector to be returned; will be filled at the bottom of it all
	std::vector<Node *> ret;
	//in case the start node is already pathed out (from a way starting elsewhere), return
	if(this->nodeToGoal.count(start->GetNodeID()) != 0){
		return ret;
	}
	//this is the place to write the way to the already found way to the goal into (i.e. the node that was taken from the nodeToGoal map that has already had a valid shortest path to the goal)
	Node * shortcutGoal = nullptr;

	//map to save the travel time to each node (map<Node ID, distance>)
	std::map<unsigned int, double> dist;
	//fill the dist map w/infinties
	int size = data->GetNodeList().size();
	for(int i = 0; i < size; i ++){
		//we do only save the distance at junctions and the start node (all the others are just trotted along, since they do not matter in the routing algorithm) (this should speed up our routing)
		if(data->GetNodeList().at(i)->GetValence() == 2){
			continue;
		}
		dist.insert(std::pair<unsigned int, double>(data->GetNodeList().at(i)->GetNodeID(), UINT_MAX));
	}
	dist[start->GetNodeID()] = 0;
	dist[goal->GetNodeID()] = DBL_MAX;
	//a map, so we can follow our shortest path back to the start, so we get a path out of the whole thing (map<NodeID, EdgeToGetHereID))
	std::map<unsigned int, unsigned int> previousEdgeID;
	//a vector for saving all the nodes we have already visited
	std::set<unsigned int> visitedNodeID;
	//and a vector for saving the nodes to be visited
	std::vector<Node *> nodeQueue;
	//we start w/the start
	//nodeQueue.insert(start);
	nodeQueue.push_back(start);
	//goalDist will save the distance traveled when reaching the goal, for paths longer than the already found goalDist will not need be followed any further
	double goalDist = -1;
	Node * n;
	Node * shortestNode;
	double shortestDist;
	/*
		GET THE LooP STARTED
	*/
/*
	bool finished = false;
	//TODO:we never clear nodes from our nodeQueue
	while(!finished){
		finished = true;
		shortestDist = DBL_MAX;
		size = nodeQueue.size();
		/*
			find the node w/the smallest traveled distance and write it into shortestNode
		*/
/*
		for(int i = 0; i < size; i ++){
			n = nodeQueue[i];
			//if the node has already been visited in a previous runthrough, but not yet this runthrough
			if(this->nodeToGoal.count(n->GetNodeID()) != 0 && visitedNodeID.count(n->GetNodeID() == 0)){
				visitedNodeID.insert(n->GetNodeID());
				//get the distance from the known node to the goal node
				double distance = nodeToGoal.at(n->GetNodeID());
				//set the goal distance if it is shorter than the current one (DO NOT RECOMPUTE THAT PATH)
				//if no way to the goal has been found so far, simply set the path as the way to our goal
				if(goalDist == -1 || goalDist > dist.at(n->GetNodeID()) + nodeToGoal.at(n->GetNodeID())){
					goalDist = dist.at(n->GetNodeID()) + nodeToGoal.at(n->GetNodeID());
					shortcutGoal = n;
				}
				//no matter whether the newly proposed path is shorter or longer than the currently existing goalDist, this node needs not be examined further (its shortest dist has already been found)
				continue;
			}
			//if we have already visited the current node
			if(visitedNodeID.count(n->GetNodeID()) != 0){
				//TODO: remove current node for performance reasons; actually dont, we wont revisit a node anyway, since we add new nodes at the end of our vector (w/push_back)
				continue;
			}
			//if we have already found a way to our goal, and our current distance is larger than the distance to the goal we have already found, do not follow the node
			else if(goalDist != -1 && dist.at(n->GetNodeID()) > goalDist){
				continue;
			}
			if(dist.at(n->GetNodeID()) < shortestDist){
				shortestDist = dist.at(n->GetNodeID());
				shortestNode = n;
				//if we found a new node, we have not yet visited and that can result in a shorter path to our goal, set finished to false
				finished = false;
			}
		}
		//then set our node to the shortest found node
		n = shortestNode;

		//if we did not find any new nodes to be considered, break and thus end the outmost loop
		if(finished){
			break;
		}
		//add the new node to the visited nodes, since we hereby take all the edges from the node into consideration
		visitedNodeID.insert(n->GetNodeID());
		/*	then loop through the edges that contain the current node
			making the connections through all the edges, and writing new distances into the dist map
		*/
/*
		std::vector<Edge *> edges;
		edges = data->GetEdgesByNodeID(n->GetNodeID());
		size = edges.size();
		for(int i = 0; i < size; i++){
			Edge * e = edges[i];
			//if e is a one way road, one shall not pass this way
			if(!e->IsTraversable(shortestNode)){
				continue;
			}
			double timeTravel = 0;
			//if we took an edge to get here (true for anything but the start node), check if we made a turn and apply the penalty if so
			if(previousEdgeID.count(n->GetNodeID()) != 0){
				//check by getting the edge we took to get to this point and the current edge e (do this only once, for turns are no possibility while we travel through the valence 2 nodes in the while{} below
				timeTravel += this->GetPenalty(data->GetEdgeByID(previousEdgeID.at(n->GetNodeID())), e);
			}
			Node * otherNode = e->GetOtherNode(n);
			/*while we follow the road w/out any junctions, simply follow until a Node != 2 is reached
			the otherNode != nullptr is for nodes along the border, that may simply end w/a valence == 2 node*/
			//prevEdge to not accidentally go back
/*
			Edge * prevEdge = e;
			while(otherNode->GetValence() == 2){
				timeTravel += e->GetTimeTravel() * this->GetFactor(e);
				//check whether we have reached our beloved goal (and w/a better time than previously)
				if(otherNode->Equals(*goal) && (goalDist == -1 || dist.at(n->GetNodeID()) + timeTravel < goalDist)){
					previousEdgeID[otherNode->GetNodeID()] = e->GetEdgeID();
					goalDist = dist.at(n->GetNodeID()) + timeTravel;
				}
				//add otherNode to the nodes we already visited
				visitedNodeID.insert(otherNode->GetNodeID());
				//we know v will have exactly two entries, for the valence is two
				std::vector<Edge*> v = data->GetEdgesByNodeID(otherNode->GetNodeID());
				//and we assign the next edge to the one that is not the one we just took
				e = v.at(0)->Equals(*e) ? v.at(1) : v.at(0);
				//and we assign the node to the next Node, recheck if it has valence of two, and repeat
				otherNode = e->GetOtherNode(otherNode);
			}
			//Do not forget to add the travel time for the last node (our next junction/or end) if there is one (not the case if we reach the border of our map, and run out of data
			timeTravel += e->GetTimeTravel() * this->GetFactor(e);
			//once again, check for possible reaching of the destination
			if(otherNode->Equals(*goal) && (goalDist == -1 || dist.at(n->GetNodeID()) + timeTravel < goalDist)){
				//previousEdgeID.insert(std::pair<unsigned int, unsigned int>(otherNode->GetNodeID(), e->GetEdgeID()));
				//previousEdgeID[otherNode->GetNodeID()] = e->GetEdgeID();
				//The above will be done below anyway, so we can skip it
				goalDist = dist.at(n->GetNodeID()) + timeTravel;
			}

			//save the newly acquired values if the previous value is bigger than the new value
			if(dist.at(otherNode->GetNodeID()) > dist.at(n->GetNodeID()) + timeTravel){
				dist.at(otherNode->GetNodeID()) = dist.at(n->GetNodeID()) + timeTravel;
				previousEdgeID[otherNode->GetNodeID()] = e->GetEdgeID();
			}

			nodeQueue.push_back(otherNode);

			/*

						OLD SOLUTION


			//The Node is not visited when we just barely reached it w/out checking the edges going out from the node
			visitedNodeID.insert(otherNode->GetNodeID());
			

			//and add any neighbouring nodes to the queue
			std::vector<Edge *> ve = data->GetEdgesByNodeID(otherNode->GetNodeID());
			int size = ve.size();
			for(int i = 0; i < size; i++){
				//if the edge is the one we just came from, skip it; same if the edge leads to an already visited Node
				if(ve.at(i)->Equals(*e) || visitedNodeID.count(ve.at(i)->GetOtherNode(otherNode)->GetNodeID()) != 0){
					continue;
				}
				else{
					nodeQueue.push_back(ve.at(i)->GetOtherNode(otherNode));
				}
			}



			
		}
	}
	
	/*
		after all the work, reconstruct the way
		if none was found, return an empty vec
	*/
/*
	//if a shortcut has been taken, simply set the shortcutGoal as goal Node and the still-untaken part of the path will be returned for the taking
	if(shortcutGoal != nullptr){
		goal = shortcutGoal;
	}
	if(previousEdgeID.count(goal->GetNodeID()) != 0){
		Edge * edge = data->GetEdgeByID(previousEdgeID.at(goal->GetNodeID()));
		Node * node = goal;
		while(!node->Equals(*start)){
			ret.push_back(node);
			node = edge->GetOtherNode(node);
			//insert the node into our nodeToGoal map
			//the distance to use equals the goal distance minus the part of that distance that was already traveled in between the start node and the current one
			this->nodeToGoal.insert(std::pair<unsigned int, double>(node->GetNodeID(), goalDist - dist[node->GetNodeID()]));
			//if we are at a junction, look up our taken path
			if(previousEdgeID.count(node->GetNodeID()) != 0){
				edge = data->GetEdgeByID(previousEdgeID.at(node->GetNodeID()));
			//else if we already are at our start do not follow the path anymore
			}else if(node != start){
				//we know v will have exactly two entries, for the valence is two (otherwise there would be an entry in the previousEdgeID thingy
				std::vector<Edge*> v = data->GetEdgesByNodeID(node->GetNodeID());
				//and we assign the next edge to the one that is not the one we just took
				edge = v.at(0)->Equals(*edge) ? v.at(1) : v.at(0);
			}
		}
		ret.push_back(start);
	}
	return ret;
}*/

Dijkstra::dijkData Dijkstra::FullDijkstraPath(MyData * data, Node * start, Node * goal){
	
	std::multiset<std::pair<double, Node *>, Dijkstra::comparator> queue;
	dijkData dijk;

	//fill the meta data with default values
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		//write the node into a variable to prevent the lookup from happening four times
		Node * node = data->GetNodeList().at(i);
		if(node->GetValence() != 2){
			//predecessor is initialised to 0, which means no prev edge
			dijk.idToPred.insert(std::pair<unsigned int, unsigned int>(node->GetNodeID(), 0));
			dijk.idToStatus.insert(std::pair<unsigned int, unsigned int>(node->GetNodeID(), 0));
			dijk.idToDist.insert(std::pair<unsigned int, double>(node->GetNodeID(), DBL_MAX));
		}
	}
	//and setup a goal distance (will only be important if a goal node has been specified by the user, in which case the algorithm stops if the best goal distance is unbeatable
	double goalDist = DBL_MAX;
	//together with data entries for the goal node(if they are already there, no worries, nothing bad shall happen)
	if(goal != nullptr){
		dijk.idToPred[goal->GetNodeID()] = 0;
		dijk.idToStatus[goal->GetNodeID()] = 0;
		dijk.idToDist[goal->GetNodeID()] = DBL_MAX;
	}
	//start at the start
	dijk.idToPred[start->GetNodeID()] = 0;
	dijk.idToDist[start->GetNodeID()] = 0;
	dijk.idToStatus[start->GetNodeID()] = 1;
	queue.insert(std::pair<double, Node *>(0, start));
	//while there is still a node in the queue (the loop may be interrupted, in case goal node has been defined)
	while(!queue.empty()){
		//check whether the goaldistance is shorter than the current distance (if so, stop right here)
		if(goal != nullptr && queue.begin()->first > goalDist){
			return dijk;
		}
		//get the node with the shortest distance and remove it from our queue
		Node * temp = (*queue.begin()).second;
		queue.erase(queue.begin());
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
		for(unsigned int i = 0; i < edges.size(); i++){
			//get the weight associated with the edge leading to the returned node
			Dijkstra::WayQuery result = this->GetWeightAndNode(temp, edges.at(i), data->GetEdgeByID(dijk.idToPred.at(temp->GetNodeID())), data, goal);
			//if that new distance is smaller than the old one, replace them
			if(dijk.idToDist.at(temp->GetNodeID()) + result.wayWeight < dijk.idToDist.at(result.resultingNodeID)){
				//save the old key, in case the node is to be replaced (i.e. the nodes status is 1, meaning already in the queue)
				double oldKey = dijk.idToDist.at(result.resultingNodeID);
				dijk.idToDist[result.resultingNodeID] = (dijk.idToDist.at(temp->GetNodeID()) + result.wayWeight);
				dijk.idToPred[result.resultingNodeID] = result.leadingEdgeID;
				//if the node is not yet in queue, insert it
				if(dijk.idToStatus[result.resultingNodeID] == 0){
					dijk.idToStatus[result.resultingNodeID] = 1;
					queue.insert(std::pair<double, Node *>(dijk.idToDist[result.resultingNodeID], data->GetNodeByID(result.resultingNodeID)));
				//if the node is already in the queue, replace the old value with the new one
				}else if(dijk.idToStatus[result.resultingNodeID] == 1){
					std::multiset<std::pair<double, Node *>, Dijkstra::comparator>::iterator it;
					Node * currentNode = data->GetNodeByID(result.resultingNodeID);
					it = queue.find(std::pair<double, Node *>(oldKey, currentNode));
					//while we are at another node with the same distance, skip one by one until we find the correct node
					//the it != queue.end() is just a fail-safe, should never be important, so possibly delete it
					while(*it->second != *currentNode && it != queue.end()){
						it++;
					}
					//after we found it, erase it
					queue.erase(it);
					//And insert the new pair (same node w/diff distance)
					queue.insert(std::pair<double, Node *>(dijk.idToDist.at(result.resultingNodeID), currentNode));
				}
				//if the node has already been visited, dont do a thing (i.e. status == 2)
				//but in any case, check whether the goal has been (b)reached
				if(goal != nullptr && result.resultingNodeID == goal->GetNodeID() && dijk.idToDist.at(result.resultingNodeID) < goalDist){
					goalDist = dijk.idToDist.at(result.resultingNodeID);
				}

			}
		}
		//since we hereby visited the node, set its status to already visited (so it wont be added to the queue again)
		dijk.idToStatus[temp->GetNodeID()] = 2;
	}

	//then return the meta data (dijk)
	return dijk;
}

std::vector<Edge *> Dijkstra::DijkstraPath(MyData * data, Node * start, Node * goal){
	//simply computes the path with the fulldijk and then returns a vector of all the edges in between the start and goal nodes
	Dijkstra::dijkData dijk = this->FullDijkstraPath(data, start, goal);
	return this->GetEdges(start, goal, dijk, data);

}

void Dijkstra::ConnectAllToGoal(MyData * data, Node * goal, std::vector<Node *> toConnect){
	Dijkstra::dijkData dijk = this->FullDijkstraPath(data, goal);
	for(unsigned int i = 0; i < toConnect.size(); i++){
		this->FollowAndTakeEdges(toConnect.at(i), dijk, data);
	}
}

std::vector<Node *> Dijkstra::GetUnconnectedNodes(MyData * data, Node * goal){
	Dijkstra::dijkData dijk = this->FullDijkstraPath(data, goal);
	std::vector<Node *> ret;
	std::vector<Node *> nodes = data->GetNodeList();
	for(unsigned int i = 0; i < nodes.size(); i++){
		//the dijk data only contains val != 2 nodes
		if(nodes.at(i)->GetValence() == 2) continue;
		//if the distance value is still at the default value of DBL_MAX, the node is unreachable from the defined goal node
		if(dijk.idToDist.at(nodes.at(i)->GetNodeID()) == DBL_MAX) ret.push_back(nodes.at(i));
	}
	return ret;
}

std::vector<Edge *> Dijkstra::GetEdges(Node * start, Node * goal, Dijkstra::dijkData dijk, MyData * data){
	std::vector<Edge *> toReturn;
	//we start at the goal and follow the predecessors until we reach the start node
	Node * temp = goal;
	//while we have not reached our goal and there is still a predecessor to our current node
	while(*temp != *start && dijk.idToPred.at(temp->GetNodeID()) != 0){
		//get the predecessor to the current node
		Edge * edge = data->GetEdgeByID(dijk.idToPred.at(temp->GetNodeID()));
		toReturn.push_back(edge);
		//And follow that string of edges until another val != 2 node is reached (for we only know the predecessor at junctions)
		Node * intermediate = edge->GetOtherNode(temp);
		while(intermediate->GetValence() == 2){
			//if however the intermediate node is the start node, quit 
			if(*intermediate == *start){
				return toReturn;
			}
			std::vector<Edge *> edges = data->GetEdgesByNodeID(intermediate->GetNodeID());
			edge = *edge == *edges.at(0) ? edges.at(1) : edges.at(0);
			toReturn.push_back(edge);
			intermediate = edge->GetOtherNode(intermediate);
		}
		//since intermediate is now a junction node, set it as the new tmep Node
		temp = intermediate;
	}
	//then return the accumulated edges
	return toReturn;
}

void Dijkstra::FollowAndTakeEdges(Node * start, Dijkstra::dijkData dijk, MyData * data){
	Node * temp = start;
	unsigned int oldID = start->GetNodeID();
	//the goal node has 0 as pred value
	while(dijk.idToPred.at(temp->GetNodeID()) != 0){
		temp = this->FollowAndTakeEdge(temp, data->GetEdgeByID(dijk.idToPred.at(temp->GetNodeID())), data);
		//once we have taken the path from that node, we do not need to take the same way again (SPOILER: it wont change), which should work and save quite some runtime (i do not believe we need the ID later on)
		dijk.idToPred[oldID] = 0;
		oldID = temp->GetNodeID();
	}
}

Node * Dijkstra::FollowAndTakeEdge(Node * start, Edge * edge, MyData * data){
	Node * temp = edge->GetOtherNode(start);
	Edge * tempEdge = edge;
	while(temp->GetValence() == 2){
		//set it to true
		tempEdge->SetTaken(true);
		//and get the next edge along the way
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
		tempEdge = *tempEdge == *edges.at(0) ? edges.at(1) : edges.at(0);
		temp = tempEdge->GetOtherNode(temp);
	}
	tempEdge->SetTaken(true);
	return temp;
}


Dijkstra::WayQuery Dijkstra::GetWeightAndNode(Node * start, Edge * leadingEdge, Edge * prevEdge, MyData * data, Node * goal){
	Node * temp = leadingEdge->GetOtherNode(start);
	Edge * prevEd = prevEdge; 
	Edge * tempEdge = leadingEdge;
	double sum = 0;
	while(temp->GetValence() == 2){
		//assign the penalty and factor in roundroads
		sum += this->GetPenalty(prevEd, tempEdge);
		sum += tempEdge->GetDistance() * this->GetFactor(tempEdge);
		//if our temp node is the goal, stop here
		if(goal != nullptr && *temp == *goal){
			return Dijkstra::WayQuery(sum, temp, tempEdge);
		}
		//correctly set the prevEd
		prevEd = tempEdge;
		std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
		tempEdge = *tempEdge == *edges.at(0) ? edges.at(1) : edges.at(0);
		temp = tempEdge->GetOtherNode(temp);
	}
	sum += this->GetPenalty(prevEd, tempEdge);
	sum += tempEdge->GetDistance() * this->GetFactor(tempEdge);
	return Dijkstra::WayQuery(sum, temp->GetNodeID(), tempEdge->GetEdgeID());
}


//smart getter, if we travel on a ringroad, apply factor, else return 1
double Dijkstra::GetFactor(Edge * e){
	return e->GetStartNode()->IsRingRoad() && e->GetEndNode()->IsRingRoad() ? this->factor : 1.0;
}

/*	checks whether the two edges are somewhat similar to straight (defined by the angle between them)
	if they are, do not apply a penalty; if, however, a turn is required, apply the designated penalty
*/
int Dijkstra::GetPenalty(Edge * prevEdge, Edge * nextEdge){
	//MAKE PENALTY DEPENDANT ON THE STREET NAME NOT ON THE ANGLE
	//double angle =  Vec2(prevEdge->GetStartNode(), prevEdge->GetEndNode()).GetAngle(Vec2(nextEdge->GetStartNode(), nextEdge->GetEndNode()));
	//return angle < OptionFile::straightBiggerAngle && angle > OptionFile::straightSmallerAngle ? 0 : this->penalty;
	//if we just started and do not have a prevEdge, no penalty is assessed
	if(prevEdge == nullptr) return 0;
	else return prevEdge->GetStreetName() == nextEdge->GetStreetName() ? 0 : this->penalty;
}

void Dijkstra::SetFactor(double fac){
	this->factor = fac;
}

void Dijkstra::SetPenalty(int pen){
	this->penalty = pen;
}
/*
std::vector<Edge *> Dijkstra::DijkToEdges(std::vector<Node *> nodes, MyData * data){
	std::vector<Edge *> edges;
	Node * current = nullptr;
	Node *prev = nullptr;
	//loop through all the nodes, and get the edges connecting them to return a series of edges instead of a series of nodes
	for(unsigned int i = 0; i < nodes.size(); i++){
		current = nodes.at(i);
		if(prev != nullptr && current != nullptr){
			Edge * temp = GetCommonEdge(prev, current, data);
			if(temp != nullptr){
				edges.push_back(temp);
			}
			else return edges;
		}
		prev = current;
	}
	return edges;
}

Edge * Dijkstra::GetCommonEdge(Node * one, Node * two, MyData * data){
	std::vector<Edge *> edgesOne = data->GetEdgesByNodeID(one->GetNodeID());
	std::vector<Edge *> edgesTwo = data->GetEdgesByNodeID(two->GetNodeID());
	unsigned int sizeOne = edgesOne.size();
	unsigned int sizeTwo = edgesTwo.size();
	for(unsigned int i = 0; i < sizeOne; i++){
		for(unsigned int j = 0; j < sizeTwo; j++){
			if(edgesOne.at(i) == edgesTwo.at(j)){
				return edgesOne.at(i);
			}
		}
	}
	return nullptr;
}

void Dijkstra::ResetDijkstra(){
	nodeToGoal.clear();
}
*/
