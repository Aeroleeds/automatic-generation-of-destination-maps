#pragma once
///Interface for classes that grab the tinyxml2::XMLDocument from some arbitrary data source
/**	class to execute queries in the xapi format (i.e., by providing a key, value pair, and a bounding box in which the request is to be executed)
	note that these queries may be mapped to any other format by the data provider
	if the data is to be taken from another source, one can implement this interface and link it in the Datagrabber to use that data source instead
*/
class DataProvider
{
public:
	///Contains the key to be looked for in the SETTINGS.xml, all found settings under the given key are loaded during construction
	DataProvider(std::string settingsKey);
	virtual ~DataProvider(void) {};
	///will grab osm data and the result into a tinyxml2 document for further usage
	/**	The key and value pair restrict the output to certain nodes and ways that fulfill the given criteria
		(e.g. the commonly used pair in this algorithm are of the "highway=xyz" type, to restrict the output to only motorways, primary roads, or similar)
		The BoundingBox limits the output to a certain area of interest, this area is the one being portayed in the output
		After grabbing the data it will be loaded as tinyxml2::XMlDocument and returned*/
	virtual tinyxml2::XMLDocument * GetData(std::string key, std::string value, BoundingBox bb) = 0;
	///Not yet implemented way of switching the data privider
	/**	the way of providing the data provider w/a server to reach for
		will return true if the server is up and running, false if we encountered an issue along the way*/
	virtual bool SetServer(std::string serverUrl) = 0;
	///Returns true if all the settings have been loaded correctly
	virtual bool SettingsLoaded() = 0;
	///Get a string representation of the data provider
	virtual std::string ToString() = 0;
	///Do any setup work here
	/**	To be called before using the GetData method*/
	virtual void SetUp(){};
	///If the desired server needs an API key, this method is to be implemented
	virtual void SetAPIKey(std::string key){};
};

