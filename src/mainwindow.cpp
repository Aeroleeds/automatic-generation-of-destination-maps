#pragma once
#include "stdafx.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qstring.h"
#include "Pipeline.h"
#include "OutputBroker.h"
#include <QThread>
#include <QWebView>
#include <QWebFrame>
#include <QWebElement>
#include <QWebPage>
#include <QMessageBox>
#include <QKeyEvent>

//will turn into an unused import
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	this->mapper = new QSignalMapper(this);
	valid.setNotation(QDoubleValidator::StandardNotation);

	//if the view finished loading, initialise the map
	connect( ui.mapView, SIGNAL( loadFinished( bool ) ), this, SLOT( InitialiseMap() ) );

	QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
	ui.mapView->setUrl(QUrl("qrc:/html/google.html"));
	//connect the two buttons
	connect(ui.settingsButton, SIGNAL(clicked()), this, SLOT(OpenAdvancedSettings()));
	connect(ui.StartButton, SIGNAL(clicked()), this, SLOT(CheckInputAndExecute()));
	//and the line edits to update methods
	connect(ui.DestinationLatitudeInput, SIGNAL(editingFinished()), this, SLOT(UpdateMarker()));
	connect(ui.DestinationLongitudeInput, SIGNAL(editingFinished()), this, SLOT(UpdateMarker()));
	//bounding box update methods
	connect(ui.negLatInput, SIGNAL(editingFinished()), this, SLOT(UpdateRect()));
	connect(ui.negLonInput, SIGNAL(editingFinished()), this, SLOT(UpdateRect()));
	connect(ui.posLatInput, SIGNAL(editingFinished()), this, SLOT(UpdateRect()));
	connect(ui.posLonInput, SIGNAL(editingFinished()), this, SLOT(UpdateRect()));
	//and connect the output stuff w/the input mapper, in order to forward the int value
	connect(ui.OutputHeightInput, SIGNAL(editingFinished()), mapper, SLOT(map()));
	connect(ui.OutputWidthInput, SIGNAL(editingFinished()), mapper, SLOT(map()));

	mapper->setMapping(ui.OutputHeightInput, 1);
	mapper->setMapping(ui.OutputWidthInput, -1);

	connect(mapper, SIGNAL(mapped(int)), this, SLOT(FixRatio(int)));
	/*
	Directly assigning a value does not work
	connect(ui.OutputHeightInput, SIGNAL(editingFinished()), this, SLOT(FixRatio(1)));
	connect(ui.OutputWidthInput, SIGNAL(editingFinished()), this, SLOT(FixRatio(-1)));*/

	//this removes the close and max/min buttons
	prog.setWindowFlags(Qt::Dialog | Qt::Desktop | Qt::WindowStaysOnTopHint);
//	prog.setWindowFlags(Qt::WindowStaysOnTopHint);
	this->workerThread = nullptr;
	this->ui.WarningLabel->setText("");

	//setup the double validators for the input fields
	ui.DestinationLatitudeInput->setValidator(&valid);
	ui.DestinationLongitudeInput->setValidator(&valid);
	ui.negLatInput->setValidator(&valid);
	ui.negLonInput->setValidator(&valid);
	ui.posLatInput->setValidator(&valid);
	ui.posLonInput->setValidator(&valid);
	ui.OutputHeightInput->setValidator(&valid);
	ui.OutputWidthInput->setValidator(&valid);
	//default output values
	ui.OutputHeightInput->setText(QString(std::to_string((long double) OptionFile::outputHeight).c_str()));
	ui.OutputWidthInput->setText(QString(std::to_string((long double) OptionFile::outputWidth).c_str()));
}

MainWindow::~MainWindow()
{
	//i dont think i need to close em
	//TODO: make sure we neednt close em
	this->settings.hide();
	this->term.hide();
	this->prog.hide();

	delete mapper;
}


void MainWindow::focusInEvent(QFocusEvent *event){
}

void MainWindow::DisplayProgress(QString text, bool isSubProg){
	if(isSubProg){
		this->prog.SetSubText(text);
	}else{
		this->prog.SetMainText(text);
	}
}

void MainWindow::CheckInputAndExecute(){
	//if the worker thread is running, do not meddle with any options
	if(this->workerThread != nullptr && this->workerThread->isRunning()){
		ui.WarningLabel->setText("Currently computing, please have patience");	
		return;
	}
	this->settings.HideSettings();
	this->term.HideSettings();
	//OptionFile::serverKey = "overpass";
	//OptionFile::loadFile = "NewDir\\xapi_nodes.xml";
	//OptionFile::saveAs = "LoadTest";
	//OptionFile::pipeSet = OptionFile::OnlyDL;

	bool correctInput;
	double lat, lon, width, height, negLat, negLon, posLat, posLon;

	lon = ui.DestinationLongitudeInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	lat = ui.DestinationLatitudeInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	width = ui.OutputWidthInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	height = ui.OutputHeightInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	negLat = ui.negLatInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	posLat = ui.posLatInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	negLon = ui.negLonInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}
	posLon = ui.posLonInput->text().toDouble(& correctInput);
	if(!correctInput){
		ui.WarningLabel->setText("Please enter valid numbers");
		return;
	}

	if(lat > posLat || lat < negLat || lon > posLon || lon < negLon){
		ui.WarningLabel->setText("The destinations coordinates need be within the specified bounding box");
		return;
	}
	//if the input is fine, write it into the OptionFile
	OptionFile::bb.negLat = negLat;
	OptionFile::bb.negLong = negLon;
	OptionFile::bb.posLat = posLat;
	OptionFile::bb.posLong = posLon;
	OptionFile::destination.lat = lat;
	OptionFile::destination.longi = lon;
	OptionFile::outputHeight = height;
	OptionFile::outputWidth = width;
	//adjust ratio if necessary
	/*RatioHelper ratione;
	BoundingBox tempBox = ratione.ReconfigureBB(OptionFile::bb, OptionFile::outputWidth, OptionFile::outputHeight);
	if(std::abs(tempBox.negLat-OptionFile::bb.negLat) > 0.01 || std::abs(tempBox.posLong - OptionFile::bb.posLong) > 0.01|| std::abs(tempBox.negLong - OptionFile::bb.negLong) > 0.01 || std::abs(tempBox.posLat - OptionFile::bb.posLat) > 0.01){
		ui.WarningLabel->setText("Adjusted BoundingBox to fit correct aspect ratio");
		ui.posLatInput->setText(std::to_string(tempBox.posLat).c_str());
		ui.posLonInput->setText(std::to_string(tempBox.posLong).c_str());
		ui.negLatInput->setText(std::to_string(tempBox.negLat).c_str());
		ui.negLonInput->setText(std::to_string(tempBox.negLong).c_str());
		return;
	}*/

	//clear the warning
	ui.WarningLabel->setText("");

	//if the input is fine, write it into the OptionFile
	OptionFile::bb.negLat = negLat;
	OptionFile::bb.negLong = negLon;
	OptionFile::bb.posLat = posLat;
	OptionFile::bb.posLong = posLon;
	OptionFile::destination.lat = lat;
	OptionFile::destination.longi = lon;
	OptionFile::outputHeight = height;
	OptionFile::outputWidth = width;
	//compute the factor later used during layouting
	RatioHelper::ComputeRLFactor();
	//pipe();
	Pipeline * pipe = new Pipeline();
	pipe->SetUp();
	OutputBroker * broke = new OutputBroker(this);
	pipe->SetBroker(broke);
	//finally make sure the updates sent from the other thread are received properly
	connect(broke, SIGNAL(ProgressChangedSignal(QString, bool)), this, SLOT(DisplayProgress(QString, bool)));
	this->prog.showNormal();
	//pipe.DoWork();
	connect(pipe, &Pipeline::ResultReady, this, &MainWindow::HandleResults);
	connect(pipe, &Pipeline::finished, pipe, &QObject::deleteLater);
	workerThread = pipe;
	workerThread->start();
	//pipe.start();
	return;
}

void MainWindow::HandleResults(const QString & msg){
		this->prog.hide();
		//and release the memory used up by our thread
		//delete workerThread;
		//this should be safe right? QT knows we want the workerThread gone, and then it doesnt matter whether we still have a pointer to it, right?!
		workerThread->deleteLater();
		workerThread = nullptr;
		ui.WarningLabel->setText(msg);
}

void MainWindow::OpenAdvancedSettings(){
	//if the worker thread is running, do not meddle with any options
	if(this->workerThread != nullptr && this->workerThread->isRunning()){
		ui.WarningLabel->setText("Currently computing, please have patience");	
		return;
	}
	this->term.ShowSettings();
	this->settings.ShowSettings();
}

void MainWindow::ApplyChanges(){
	//set all the fields accordign to the OptionFile values
	ui.DestinationLatitudeInput->setText(QString(std::to_string((long double) OptionFile::destination.lat).c_str()));
	ui.DestinationLongitudeInput->setText(QString(std::to_string((long double) OptionFile::destination.longi).c_str()));
	ui.negLatInput->setText(QString(std::to_string((long double) OptionFile::bb.negLat).c_str()));
	ui.posLatInput->setText(QString(std::to_string((long double) OptionFile::bb.posLat).c_str()));
	ui.negLonInput->setText(QString(std::to_string((long double) OptionFile::bb.negLong).c_str()));
	ui.posLonInput->setText(QString(std::to_string((long double) OptionFile::bb.posLong).c_str()));
	ui.OutputHeightInput->setText(QString(std::to_string((long double) OptionFile::outputHeight).c_str()));
	ui.OutputWidthInput->setText(QString(std::to_string((long double) OptionFile::outputWidth).c_str()));
	//update the positions
	this->UpdateMarker();
	this->UpdateRect();
	this->CenterOnRectangle();
}

void MainWindow::CenterOnRectangle(){
	QString str = QString("centerOnRect();");
	ui.mapView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void MainWindow::InitialiseMap(){
	//register the webview as javascript enabled object
	ui.mapView->page()->currentFrame()->addToJavaScriptWindowObject("MainWindow", this );
}
/// @cond
void MainWindow::keyPressEvent(QKeyEvent * event){
	if(prev == ""){
		if(event->key() == Qt::Key_Up){ prev = "upOne";}
		else prev = "";
	}
	else if(prev == "upOne"){
		if(event->key() == Qt::Key_Up){ prev = "upTwo";}
		else prev = "";
	}else if(prev == "upTwo"){
		if(event->key() == Qt::Key_Down){ prev = "downOne";}
		else prev = "";
	}else if(prev == "downOne"){
		if(event->key() == Qt::Key_Down){ prev = "downTwo";}
		else prev = "";
	}else if(prev == "downTwo"){
		if(event->key() == Qt::Key_Left){ prev = "leftOne";}
		else prev = "";
	}else if(prev == "leftOne"){
		if(event->key() == Qt::Key_Right){ prev = "rightOne";}
		else prev = "";
	}else if(prev == "rightOne"){
		if(event->key() == Qt::Key_Left){ prev = "leftTwo";}
		else prev = "";
	}else if(prev == "leftTwo"){
		if(event->key() == Qt::Key_Right){ prev = "rightTwo";}
		else prev = "";
	}else if(prev == "rightTwo"){
		if(event->key() == Qt::Key_B){ prev = "b";}
		else prev = "";
	}else if(prev == "b"){
		if(event->key() == Qt::Key_A){
			prev = "";
			ui.DestinationLatitudeInput->setText("51.512481");
			ui.DestinationLongitudeInput->setText("-0.128684");
			this->UpdateMarker();
			ui.negLatInput->setText("51.5109");
			ui.posLatInput->setText("51.5137");
			ui.negLonInput->setText("-0.131654");
			ui.posLonInput->setText("-0.125517");
			this->UpdateRect();
			this->CenterOnRectangle();
			ui.WarningLabel->setText("\"There's only one place we're going to get all this.\" - M.W.");
			return;
		}
		else prev = "";
	}else prev = "";
	QWidget::keyPressEvent(event);
}
/// @endcond

PointInSpace MainWindow::ParseMapString(const QString & pos){
	QRegExp rx("(\\ |\\,|\\(|\\))");
	QStringList numbers = pos.split( rx, QString::SkipEmptyParts );
	//numbers.at 0 is the longitude, numbers at 1 is the latitude
	//return PointInSpace( numbers[0].toDouble(), numbers[1].toDouble() );
	//it is the other way around, fucked up the first time around
	return PointInSpace( numbers[1].toDouble(), numbers[0].toDouble() );
}

void MainWindow::JavaScriptEventMarker(const QString &coordinates){
	//parse the point and update the input fields accordingly
	PointInSpace p = this->ParseMapString(coordinates);
	//I really should figure out a better way to transform the values into sth QString compatible
	this->ui.DestinationLatitudeInput->setText(QString(std::to_string(p.lat).c_str()));
	this->ui.DestinationLongitudeInput->setText(QString(std::to_string(p.longi).c_str()));
}

void MainWindow::JavaScriptEventRectangle(const QString &one, const QString & two){
	//we manually check for lower values, despite the fact that the QStrings should always be in the correct order
	PointInSpace p = this->ParseMapString(one);
	long double lowLon = p.longi;
	long double lowLat = p.lat;
	long double upLon = lowLon;
	long double upLat = lowLat;
	p = this->ParseMapString(two);
	if(p.lat > lowLat){
		upLat = p.lat;
	}else{
		lowLat = p.lat;
	}
	if(p.longi > lowLon){
		upLon = p.longi;
	}else{
		lowLon = p.longi;
	}
	this->ui.negLatInput->setText(QString(std::to_string(lowLat).c_str()));
	this->ui.negLonInput->setText(QString(std::to_string(lowLon).c_str()));
	this->ui.posLatInput->setText(QString(std::to_string(upLat).c_str()));
	this->ui.posLonInput->setText(QString(std::to_string(upLon).c_str()));
	//and fix the output
	this->FixRatio();
}

void MainWindow::UpdateMarker(){
	//get the new marker values
	double lat = ui.DestinationLatitudeInput->text().toDouble();
	double lon = ui.DestinationLongitudeInput->text().toDouble();
	//and use the update marker method w/the new values
	QString str = QString("updateMarker('%1', '%2');").arg(ui.DestinationLatitudeInput->text().toDouble()).arg(ui.DestinationLongitudeInput->text().toDouble());
	ui.mapView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
}

void MainWindow::UpdateRect(){
	bool correctInput = true;
	long double lowLat = ui.negLatInput->text().toDouble(&correctInput);
	long double lowLon = ui.negLonInput->text().toDouble(&correctInput);
	long double upLat = ui.posLatInput->text().toDouble(&correctInput);
	long double upLon = ui.posLonInput->text().toDouble(&correctInput);
	//the input checking is necessary, for the signal is sent once A SINGLE input field is changed to a valid dbl-value, we thus need to check whether the other three values are valid as well
	if(!correctInput){
		return;
	}
	//if the user made an honest mistake, fix it for him/her (in the UI as well)
	if(upLat < lowLat){
		ui.negLatInput->setText(QString(std::to_string(upLat).c_str()));
		ui.posLatInput->setText(QString(std::to_string(lowLat).c_str()));
		//and switch the values
		/*long double temp = lowLat;
		lowLat = upLat;
		upLat = temp;*/
	}
	//same for the longitude values
	if(upLon < lowLon){
		ui.negLonInput->setText(QString(std::to_string(upLon).c_str()));
		ui.posLonInput->setText(QString(std::to_string(lowLon).c_str()));
		//and switch the values
		/*long double temp = lowLon;
		lowLon = upLon;
		upLon = temp;*/
	}
	//first pass the southwest corner, then the northeast one; always start with the latitude
	QString str = QString("changeRect('%1', '%2', '%3', '%4');").arg(ui.negLatInput->text().toDouble()).arg(ui.negLonInput->text().toDouble()).arg(ui.posLatInput->text().toDouble()).arg(ui.posLonInput->text().toDouble());
	ui.mapView->page()->currentFrame()->documentElement().evaluateJavaScript(str);
	//and fix the output
	this->FixRatio();
	this->CenterOnRectangle();
}

void MainWindow::FixRatio(int heightAdjusted){
	bool correctInput = true;
	long double lowLat = ui.negLatInput->text().toDouble(&correctInput);
	long double lowLon = ui.negLonInput->text().toDouble(&correctInput);
	long double upLat = ui.posLatInput->text().toDouble(&correctInput);
	long double upLon = ui.posLonInput->text().toDouble(&correctInput);
	if(!correctInput){
		return;
	}
	//two edges to get the distances along both edges
	Node * sw = new Node(lowLon, lowLat, 0);
	Node * se = new Node(lowLon, upLat, 0);
	Node * ne = new Node(upLon, upLat, 0);
	Edge * one = new Edge(sw, se, 12, "horizontal street");
	Edge * two = new Edge(se, ne, 12, "vertical street");
	//get the ratio
	double longSide = one->GetDistance() > two->GetDistance() ? one->GetDistance() : two->GetDistance();
	double shortSide = one->GetDistance() < two->GetDistance() ? one->GetDistance() : two->GetDistance();
	double ratio = shortSide / longSide;
	//cleanup
	delete one;
	delete two;
	delete sw;
	delete se;
	delete ne;
	//check whether there are valid values in the output fields
	bool correctHigh = true;
	long double high = ui.OutputHeightInput->text().toDouble(&correctHigh);
	//if we got a double value smaller 0, set the bool to false
	correctHigh = correctHigh ? high > 0 : false;
	correctInput = correctInput && correctHigh;
	bool correctWide = true;
	long double wide = ui.OutputWidthInput->text().toDouble(&correctWide);
	correctWide = correctWide ? wide > 0 : false;
	correctInput = correctInput && correctWide;
	//If invalid values were entered, provide default ones
	if(!correctInput){
		if(correctHigh){
			wide = high / ratio;
		}else if(correctWide){
			high = wide/ratio;
		}else{
			wide = 210;
			high = wide/ratio;
		}
	//otherwise check for the heightAdjusted value
	}else if(heightAdjusted == 0){
		//the long side is given by dividing the short side through the ratio
		if(high > wide){
			high = wide / ratio;
		}else{
			wide = high / ratio;
		}
	}else if(heightAdjusted > 0){
		if(high > wide){
			//if we want to compute the smaller value, multiply with the ratio
			wide = high * ratio;
		}else{
			wide = high / ratio;
		}
	}else if(heightAdjusted < 0){
		if(wide > high){
			//if we want to compute the smaller value, multiply with the ratio
			high = wide * ratio;
		}else{
			high = wide / ratio;
		}
	}
	//and set the values in the optionfile and the ui
	OptionFile::outputHeight = high;
	OptionFile::outputWidth = wide;
	ui.OutputHeightInput->setText(QString(std::to_string((long double) OptionFile::outputHeight).c_str()));
	ui.OutputWidthInput->setText(QString(std::to_string((long double) OptionFile::outputWidth).c_str()));
}
