#include "StdAfx.h"
#include "CostFunction.h"
#include "RelativeLengthTerm.h"
#include "MinimumLengthTerm.h"
#include "OrientationTerm.h"
#include "FalseIntersectionTerm.h"
#include "RelativeOrientationTerm.h"
#include "AreaControlTerm.h"
#include "RelativeRelativeLengthTerm.h"

CostFunction::CostFunction()
{
}


CostFunction::~CostFunction(void)
{
	//clean the terms up
	for(unsigned int i = 0; i < costTerms.size(); i++){
		delete costTerms.at(i);
	}
}

void CostFunction::SetUp(MyData * data){
	//Initialising the cost terms here
	CostTerm * term = new MinimumLengthTerm();
	CostTerm * rel = nullptr;
	//choose the correct relative length term here
	if(OptionFile::useRelRelLeng){
		rel = new RelativeRelativeLengthTerm();
	}else{
		rel = new RelativeLengthTerm();
	}
	CostTerm * orient = new OrientationTerm();
	CostTerm * falseInsec = new FalseIntersectionTerm();
	CostTerm * relOrien = new RelativeOrientationTerm(data);
	//and putting them in the containers
	this->costTerms.push_back(falseInsec);
	this->costTerms.push_back(term);
	this->costTerms.push_back(rel);
	this->costTerms.push_back(orient);
	this->costTerms.push_back(relOrien);
	//the experimental AreaControl
	if(OptionFile::useAreaControl){
		CostTerm * ac = new AreaControlTerm();
		this->costTerms.push_back(ac);
	}

	this->suffix = 1;
}

bool CostFunction::SwitchWeights(){
	if(this->costTerms.at(0)->GetWeight(suffix + 1) == -1){
		return false;
	}else{
		this->suffix++;
		return true;
	}
}

double CostFunction::GetEnergy(MyData * data, RoadLayoutData * tempData){
/*	std::vector<double> debugSums;
	for(unsigned int i = 0; i < costTerms.size(); i++){
		debugSums.push_back(0);
	}*/
	double sums = 0;
	//loop through the edges and get the weighted energy
	for(unsigned int i = 0; i < tempData->GetEdgeList().size(); i++){
		for(unsigned int j = 0; j < this->costTerms.size(); j++){
			//if the current weight has an input of 0, do not compute it
			if(this->costTerms.at(j)->GetWeight(suffix) == 0){
				continue;
			}
			double cost = this->costTerms.at(j)->GetCost(tempData->GetEdgeList().at(i), tempData, data);
			//if we have a false intersection in our data(or any other future term returns MAX_VALUE) then we can interrupt the process for it will not result in a good value anyway
			if(cost == DBL_MAX){
				for(unsigned int x = 0; x < this->costTerms.size(); x++){
					costTerms.at(x)->ResetTerm();
				}
				return DBL_MAX;
				//do not return straight from here, since some CostTerms might have resetting functions in their PostCost (so execute the PostCosts aswell)
				//break;
			}
			cost *= this->costTerms.at(j)->GetWeight(suffix);
			sums += cost;
			//debugSums[j] += cost;
		}
	}
	//and sum up the potential post costs associated with the terms (i.e. get the values from the minimumlengthterm and the relativeOrientationTerm) and weigh them 
	for(unsigned int i = 0; i < this->costTerms.size(); i++){
		//if the weight is set to 0, do not call the postCost, since it		
		if(costTerms.at(i)->GetWeight(this->suffix) == 0){
			continue;
		}
		sums += costTerms.at(i)->PostCost() * costTerms.at(i)->GetWeight(this->suffix);
	}
	//and do any potentially necessary resetting here
	for(unsigned int x = 0; x < this->costTerms.size(); x++){
		costTerms.at(x)->ResetTerm();
	}

	return sums;
}
/*
void CostFunction::ResetValues(){
	this->suffix = 1;
}*/

/*
std::string CostFunction::ToString(){
	return std::string("Cost Function");
}

bool CostFunction::PrepDoc(MyData * data){
	//THINK ABOUT MAKING THIS CLASS ONLY RETURN A VALUE FOR THE ENERGY, AND MAKING AN EXTRA CLASS DO THE LOOPINT TODO:
	double originalCost = DBL_MAX;
	bool finished = false;
	//while the reduction in cost is bigger than 1 percent, keep rolling, else skip to the next weights
	while(!finished){
		//first off, make a move


		//then compute the cost function, and possibly accept the data


	}
	return true;
}*/