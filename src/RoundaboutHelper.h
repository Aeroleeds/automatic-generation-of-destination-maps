#pragma once
#include "stdafx.h"
#include "MyData.h"
#include <algorithm>

///Helps with dealing w/Roundabouts by providing a struct for it as well as convenience methods for creating and working with Roundabouts
class RoundaboutHelper
{
public:
	RoundaboutHelper(void);
	~RoundaboutHelper(void);

	///Defines a Roundabout by saving all edges and nodes belonging to a single roundabout
	struct Roundabout{
		///All edges of the current Roundabout
		std::vector<Edge *> edges;
		///All nodes of the current Roundabout
		std::vector<Node *> nodes;
		///Returns true if the given edge is part of the Roundabout
		bool ContainsEdge(Edge * edge){
			return std::find(edges.begin(), edges.end(), edge) != edges.end();
		};
	};
	///Creates a Roundabout of all the roundabout edges and nodes adjacent to the Edge e
	static Roundabout ConstructRoundabout(Edge * e, MyData * data);
	///returns true if the node has at least one edge associated with it that is not a roundabout edge
	static bool HasNonRoundaboutEdges(Node * node, MyData * data);

	///Returns an edge leading from node that is a roundabout edge and not the edge passed on as param
	/**	If no such edge is found, a nullptr is returned (even though this should never happen)*/
	static Edge * GetNextRoundEdge(Edge * edge, Node * node, MyData * data);
	///Returns an edge leading from node that is not a roundabout edge 
	/**	If no such edge is found, a nullptr is returned*/
	static Edge * GetNonRoundEdge(Node * node, MyData * data);
};

