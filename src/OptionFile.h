#pragma once
#include "stdafx.h"
#include "Node.h"

///The file containing all the settings set by the user and any information needed at multiple points in the program
/**	Contains no Logic whatsoever, only stores settings*/
class OptionFile
{


public:
	OptionFile(void);
	~OptionFile(void);

	///String representations of the PipelineSetup enums
	static std::string pipelineSetupStrings[];//= { "All parts", "Already selected data", "Pre-layout data", "Only download" };
	///The enumerations to described which parts of the Pipeline are to be executed
	enum PipelineSetup{All, Selected, PreLayout, OnlyDL, UpToVisRings, UpToTravisRoute, OnlyRender, NumPipeValues};
	///The current value for the Pipeline (defaults to PipelineSetup::All)
	static PipelineSetup pipeSet;
	

	//TODO: cleanup unneccessary flags

	///static variables for the options to be set by the user
	/**	Not all are actually used by the program*/
	static std::string dataPath, imagePath, styleSheetPath, serverLocation;
	///the angles to be used for defining a straight road (w/regards to the penalty assigned by the Dikstra algorithm); used in Dijkstra::GetPenalty(...)
	/**	In order to be considered straight, the angle needs be in between straightSmallerAngle and straightBiggerAngle*/
	static double straightSmallerAngle, straightBiggerAngle;
	/*
		The destination to be reached by our algorithm
	*/
	///The nearest node to our destination (which is entered as lat/lon coordinates)
	/**	Is assigned by the DataGrabber */
	static Node destinationNode;
	///The definition of the destination through the lat/lon coordinates
	/**	Mainly used to find the destinationNode*/
	static PointInSpace destination;
	///The BoundingBox for our area of interest
	/**	Rectangle defined by low and high lat and lon values*/
	static BoundingBox bb;
	///The middle lon value (of the bounding box)
	static double midLon;
	//TODO: Do we use this???	
	//TODO: set the streetnames accordingly for every taken Node
	static std::set<std::string> streetNames;
	///the width and height of the ouput, in mili metres (e.g. A4 = 210mm * 297mm)
	static double outputWidth, outputHeight;

	///The amount of edges between each pair of the eight cardinal directions (which will always be there anyway) in VisibilityRingCreator
	static double stepsAtVisibilityRings;

	///The # of steps when building neighbouring nodesets at RampRemover
	static unsigned int stepsAtNeighbourBuilding;
	///The first streettype not to be considered for the layout at DataGrabber
	/**	Defaults to service
		(i.e. service roads are NOT in the final layout	*/
	static int nonUsedType;
	//basic booleans for extended functionality
	///Defines whether streets are to be considered for a merger when they change their name (in RoadMerger)
	/**	Relevant for RoadMerger, improves consistency of the algorithm especially with regards to possible ramps, for they (normally) do not carry names
		In some situations this results in weird merge points, which can be resolved with looking at nodes where the name changes as potential merge nodes */
	static bool mergeOnNameChange;
	//true if all link roads are to be taken in the first run through, results in more consistent output, as unneccessary links are removed anyway, but it is guaranteed, that all necessary links are in the data set
	//static bool takeAllLinks;
	///true if streets that do not possess a name are not to be merged just like other streets 
	/**	if can be problematic, for many streets are unnamed, and weird results may happen 
		especially ramps are often times merged despite not really being a merge target */
	static bool doNotMergeDefaultNames;
	///True if special treatment for roundabouts is requested
	/**	Highly recommended*/
	static bool useRoundaboutOptimiser;
	///The name of the files to be saved
	/**	Subsequent saves under the same name will be equipped with an integer to differentiate between consecutive saves*/
	static std::string saveAs;
	///Determine whether false intersections that are already part of the original data are to be allowed during the layouting stage
	/**	HIGHLY recommended, for otherwise no layouting will happen at all for there will be a false intersection penalty of positive infinity on all layouts*/
	static bool allowExistingFalseIntersections;
	///Used to use the specific entered values for testing for visibility rings
	/**	Thus enabling to choose a side of the street that is deemed the correct one (otherwise self-intersections at the destination are prevented, which might lead to weird results (mainly multiple visibility rings around the destination))*/
	static bool useExactDestinationForVisRings;
	///The key associated with the server to be reached out for
	/**	The key must be the name of the tag containing the url in the SETTINGS.xml */
	static std::string serverKey;
	///The name of the file to be used as input (instead of downloading it)
	/**	The file must be in the correct dir (the same one where all the output data files are saved)*/
	static std::string loadFile;
	///Controls whether the experimental AreaControlTerm is to be used during layouting
	static bool useAreaControl;
	///Controls whether the experimental RelativeRelativeLengthTerm is to be used during layouting
	/**	The RelRelLengTerm will REPLACE the normal RelativeLengthTerm*/
	static bool useRelRelLeng;

	//Setup the weights
	///The weights associated w/the FalseIntersectionTerm used during the RoadLayout at the CostFunction
	static double falseInsecOne, falseInsecTwo;
	///The weights associated w/the MinimumLengthTerm used during the RoadLayout at the CostFunction
	static double minLengOne, minLengTwo;
	///The weights associated w/the RelativeLengthTerm used during the RoadLayout at the CostFunction
	static double relLengOne, relLengTwo;
	///The weights associated w/the OrientationTerm used during the RoadLayout at the CostFunction
	static double orientOne, orientTwo;
	///The weights associated w/the RelativeOrientation used during the RoadLayout at the CostFunction
	static double relOrientOne, relOrientTwo;
	///The weights associated w/the AreaControlTerm used during the RoadLayout at the CostFunction
	static double acOne, acTwo;
	///The weights associated w/the RelativeRelativeLengthTerm used during the RoadLayout at the CostFunction
	static double relRelLengOne, relRelLengTwo;

	///suffix for the rendering output
	static int renderSuffix;

	///Method to update the middle lon value
	static void updateMidLon(){
		double delta = OptionFile::bb.posLong - OptionFile::bb.negLong;
		delta = delta / 2;
		OptionFile::midLon = OptionFile::bb.negLong + delta;
	}
};

