#include "StdAfx.h"
#include "FalseIntersectionTerm.h"
#include "FalseIntersectionHelper.h"
#include "Line.h"

FalseIntersectionTerm::FalseIntersectionTerm(void)
{
	BoundingBox b = OptionFile::bb;
	//make a diagonal edge, and get its distance
	Node * one = new Node(b.negLong, b.negLat, 0);
	Node * two = new Node(b.posLong, b.posLat, 1);
	Edge * edge = new Edge(one, two, 1, "df");
	/*	fixing the latitude and just using the longitude values we get the distance over the width of the area
		out of this we can compute the equivalent of 25 mm in metres distance
		which is then used to compare the edge length against
	*/
	//TODO: edges are used here the same way osm uses them (i.e. everything between two val != 2 nodes belongs to the same edge)
	//this->minDist = edge->GetDistance() / OptionFile::outputWidth * 25;
	//the diagonal of our output format
	double outputDia = sqrt(OptionFile::outputHeight * OptionFile::outputHeight + OptionFile::outputWidth * OptionFile::outputWidth);
	this->rlToMapFactor = outputDia / edge->GetDistance();
	//a control distance of 8mm
	this->controlDist = 8;
	delete edge;
	delete one;
	delete two;

	knownFalseSecsInit = false;
}


FalseIntersectionTerm::~FalseIntersectionTerm(void)
{
}


double FalseIntersectionTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	if(!this->knownFalseSecsInit){
		this->InitKnownFalseInsecs(data);
	}
	//first off set up a set w/the edges that are connected to the current one (including the current one); they need not be checked
	/*std::set<unsigned int> neighboursID;
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(e->GetStartNode()->GetNodeID()).size(); i++){
		neighboursID.insert(data->GetEdgesByNodeID(e->GetStartNode()->GetNodeID()).at(i)->GetEdgeID());
	}
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(e->GetEndNode()->GetNodeID()).size(); i++){
		neighboursID.insert(data->GetEdgesByNodeID(e->GetEndNode()->GetNodeID()).at(i)->GetEdgeID());
	}*/

	double shortestDist = DBL_MAX;
	for(unsigned int i = 0; i < tempData->GetEdgeList().size(); i++){
		if(this->eIDToNeighbours.at(e->GetEdgeID()).count(tempData->GetEdgeList().at(i)->GetEdgeID()) != 0){
			continue;
		}
		if(help.IsFalseIntersection(e, tempData->GetEdgeList().at(i))){
			//if there are knownfalse intersections, and this is one of them, do not penalise
			//(if knownFalseIntersections is not desired, then its not initialised and all .count() calls will return 0, thus nothing unexpected will happen)
			if(knownFalseIntersections.count(e->GetEdgeID()) != 0 && knownFalseIntersections[e->GetEdgeID()].count(tempData->GetEdgeList().at(i)->GetEdgeID()) != 0){
				continue;
			}else if(knownFalseIntersections.count(tempData->GetEdgeList().at(i)->GetEdgeID()) != 0 && knownFalseIntersections[tempData->GetEdgeList().at(i)->GetEdgeID()].count(e->GetEdgeID())){
				continue;
			}
			return DBL_MAX;
		}
		//get the  absolute shortest distance between the two edges
	//	double dist = help.GetAbsoluteShortestDistance(e, tempData->GetEdgeList().at(i));
	//	always taking the normal distance does not work, so lets try a simpler distance metric (the cases where it performs poorly should be few and far between)
	//	double dist = this->GetShortestDistance(e, tempData->GetEdgeList().at(i));
		double dist = this->GetSimpleShortestDistance(e, tempData->GetEdgeList().at(i));
		if(dist < shortestDist){
			shortestDist = dist;
		}
	}
	if(shortestDist * this->rlToMapFactor >= controlDist){
		return 0;
	}else{
		double ret = (controlDist - shortestDist * this->rlToMapFactor) / controlDist;
		return ret * ret;
	}
}

//the False intersection term has no postCost term, thus return 0
double FalseIntersectionTerm::PostCost(){
	return 0;
}

double FalseIntersectionTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::falseInsecOne;
	case 2:
		return OptionFile::falseInsecTwo;
	default:
		return -1;
	}
}

std::string FalseIntersectionTerm::ToString(){
	return std::string("Cost Function : False Intersection term");
}

std::string FalseIntersectionTerm::GetIdentifier(){
	return std::string("falseInsec");
}
/*
double FalseIntersectionTerm::GetShortestDistance(Edge * edge, Edge * otherEdge){
	double shortestDist = DBL_MAX;
	Line line(edge->GetStartNode(), edge->GetEndNode());
	Node * intersection = line.IntersectionPoint(otherEdge->GetStartNode(), 0);
	Edge * e = new Edge(intersection, otherEdge->GetStartNode(), 0, "");
	shortestDist = shortestDist < e->GetDistance() ? shortestDist : e->GetDistance();
	delete e;
	delete intersection;
	intersection = line.IntersectionPoint(otherEdge->GetEndNode(), 0);
	e = new Edge(intersection, otherEdge->GetEndNode(), 0, "");
	shortestDist = shortestDist < e->GetDistance() ? shortestDist : e->GetDistance();
	delete e;
	delete intersection;

	line = Line(otherEdge->GetStartNode(), otherEdge->GetEndNode());
	intersection = line.IntersectionPoint(edge->GetStartNode(), 0);
	e = new Edge(intersection, edge->GetStartNode(), 0, "");
	shortestDist = shortestDist < e->GetDistance() ? shortestDist : e->GetDistance();
	delete e;
	delete intersection;
	intersection = line.IntersectionPoint(edge->GetEndNode(), 0);
	e = new Edge(intersection, edge->GetEndNode(), 0, "");
	shortestDist = shortestDist < e->GetDistance() ? shortestDist : e->GetDistance();
	delete e;
	delete intersection;

	return shortestDist;
}
*/

double FalseIntersectionTerm::GetSimpleShortestDistance(Edge * edge, Edge * otherEdge){
	double min = 0;
	Edge * e = new Edge(edge->GetStartNode(), otherEdge->GetStartNode(), 12, "");
	min = e->GetDistance();
	delete e;

	e = new Edge(edge->GetStartNode(), otherEdge->GetEndNode(), 12, "");
	min = min < e->GetDistance() ? min : e->GetDistance();
	delete e;

	e = new Edge(edge->GetEndNode(), otherEdge->GetStartNode(), 12, "");
	min = min < e->GetDistance() ? min : e->GetDistance();
	delete e;

	e = new Edge(edge->GetEndNode(), otherEdge->GetEndNode(), 12, "");
	min = min < e->GetDistance() ? min : e->GetDistance();
	delete e;

	return min;
}

void FalseIntersectionTerm::InitKnownFalseInsecs(MyData * data){
	this->knownFalseSecsInit = true;
	std::vector<Edge *> edges = data->GetEdgeList();
	for(unsigned int i = 0; i < edges.size(); i++){
		Edge * e = edges.at(i);
		//first off set up a set w/the edges that are connected to the current one (including the current one); they need not be checked
		std::set<unsigned int> neighboursID;
		for(unsigned int j = 0; j < data->GetEdgesByNodeID(e->GetStartNode()->GetNodeID()).size(); j++){
			neighboursID.insert(data->GetEdgesByNodeID(e->GetStartNode()->GetNodeID()).at(j)->GetEdgeID());
		}
		for(unsigned int j = 0; j < data->GetEdgesByNodeID(e->GetEndNode()->GetNodeID()).size(); j++){
			neighboursID.insert(data->GetEdgesByNodeID(e->GetEndNode()->GetNodeID()).at(j)->GetEdgeID());
		}
		this->eIDToNeighbours.insert(std::pair<unsigned int, std::set<unsigned int>>(e->GetEdgeID(), neighboursID));
		for(unsigned int x = 0; x < edges.size(); x++){
			if(neighboursID.count(edges.at(x)->GetEdgeID()) != 0){
				continue;
			}
			//if the false intersection is already registered but in the other order, continue
			else if(knownFalseIntersections.count(edges.at(x)->GetEdgeID()) != 0 && knownFalseIntersections.at(edges.at(x)->GetEdgeID()).count(e->GetEdgeID()) != 0){
				continue;
			}
			if(help.IsFalseIntersection(e, edges.at(x))){
				//either insert a new std::set or insert the value into the already existing set
				if(knownFalseIntersections.count(e->GetEdgeID()) != 0){
					knownFalseIntersections[e->GetEdgeID()].insert(edges.at(x)->GetEdgeID());
				}else{
					std::set<unsigned int> set;
					set.insert(edges.at(x)->GetEdgeID());
					this->knownFalseIntersections.insert(std::pair<unsigned int, std::set<unsigned int>>(e->GetEdgeID(), set));
				}
			}
		}
	}
}

