#pragma once
#include "PipelinePart.h"
#include "MyData.h"

///Uses the Mapnik library to create a rendered image from the MyData object passed on to it
/**	Reads the style.xml file for rendering guidance and prints the MyData object as RenderMe.osm file, to be used as input by the style sheet (as datasource)
	the style sheet may be changed by the user*/
class Renderer : public PipelinePart
{
public:
	Renderer(void);
	~Renderer(void);
	///Renders and saves the image with the given style sheet using mapnik
	/**	Will print the information into RenderMe.osm, thus always refer to RenderMe.osm as datasource in your style sheet*/
	std::string RenderAndSaveImage(tinyxml2::XMLDocument * xmlDoc);
	///Parses the MyData object into an osm-file and renders said file
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	///Saves the paths of all the fonts to be registered
	/**	Read during construction from the SETTINGS.xml*/
	std::vector<std::string> fontPaths;
	///Saves the paths of all the plugins to be used
	/**	Read during construction from the SETTINGS.xml*/
	std::vector<std::string> pluginPaths;
	///The resolution to be used for rendering
	/**	Read during construction from the SETTINGS.xml*/
	int resWidth, resHeight;
	///The path to the style sheet to be used
	std::string stylePath;
};

