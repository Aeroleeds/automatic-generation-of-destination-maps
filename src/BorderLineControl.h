#pragma once
#include "MyData.h"
#include "RoadLayoutData.h"

///Helper class to keep nodes that are at the border of our area of interest at that border and shrink the data if nodes that originally are inside the BoundingBox moved outside of it
/**	First analyses a MyData object for all nodes that lie close to the border
	then resets all nodes of a RoadLayoutData object that were on the border in the MyData object back to that border position
	Additionally shrinks the whole layout once a node that is not supposed to be outside of the border winds up there
	the shrinking is done uniformly towards the center-point of the layout until all nodes that are supposed to be inside th boundary are back inside the boundary	*/
class BorderLineControl
{
public:
	BorderLineControl(void);
	~BorderLineControl(void);
	///Use before anything else to obtain data for the nodes at the border that need resetting after a LayoutMove has been applied
	/**	Loops through all nodes checking whether their position is closer than some threshold to the border
		if it is the node's coordinate at the border is saved in one of the maps*/
	void ProcessReference(MyData * data);
	///forces the border-nodes extracted from the ProcessReference() method back to its border position
	/**	Movement along the non border sides is allowed (e.g. a node at the left border may move up or down)
		Will not do anything if ProcessReference has not been called beforehand*/
	void ApplyBorderLine(RoadLayoutData * data);
private:
//	bool OnBoundary(Node * node);
	///Saves the node IDs of nodes that are outside of the OptionFile::bb BoundingBox
	std::set<unsigned int> nodeIDsOutside;
	///The map saving the IDs of all nodes on a border with their latitude value
	/**	The saved value is the one they will be set to, after any movement
		(thus enabling the nodes to freely move in lon direction)*/
	std::map<unsigned int, double> nodeIDtoLat;
	///The map saving the IDs of all nodes on a border with their longitude value
	/**	The saved value is the one they will be set to, after any movement
		(thus enabling the nodes to freely move in lat direction)*/
	std::map<unsigned int, double> nodeIDtoLon;
	///The threshold used to determine which nodes are considered to be at the borderline
	/**	e.g. if the difference between the nodes lat and the boundary's negLat is smaller than the threshold it is considered to be a borderline node*/
	double threshold;
	///The BoundingBox containing all the data of the map
	/**	Is automatically set to the OptionFile::bb during construction */
	BoundingBox bb;
	///A temporary Node in the middle of the BoundingBox used to aid in shrinking the other nodes towards the centre
	/**	Automatically con- and destructed in the con- and destructor of BorderLineControl
		NOT part of the official data set*/
	Node * centre;
	///Will move all nodes from the tempData toward (or away from) the constructed centre Node depending on the scale value used
	/**	scale shall be < 1 here, since we want to shrink the layout toward the centre
		scale values > 1 should be possible (albeit being untested) but not what we want here
		Using a scale value means that nodes further away from the centre are moved more than nodes closer to the centre
		(if youre looking at the distance between the current node and the centre, the new location will be scale * the distance [in the same direction])
		Will recompute the edges distances (since they might obviously change once we shrink everything down)*/
	void MoveToCentre(RoadLayoutData * tempData, double scale);
	///Returns whether one of the nodes in the nodeList is outside the BoundingBox of the OptionFile despite the fact that it should not be
	/**	uses the previously created set of nodeIDs (of nodes which are outside) to verify which nodes are correctly outside the bounding box
		(Not that there should be any, for the Clipper should remove everything outside, however this provides us with the option to not use any clipping and still have a working BorderLineControl)
		The set of IDs is created during ProcessReference which should be called before using InvalidNodeOutside()*/
	bool InvalidNodeOutside(std::vector<Node *> nodeList);
};

