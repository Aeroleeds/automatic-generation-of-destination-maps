#pragma once
#include "CostTerm.h"
///Evaluates whether two Edges have the relatively same length as in the original data
/**	I.e. penalises edges which grow faster than the other edges, in order to maintain the order of longer edges remaining longer in the final layout
	This is ratio based, if the whole layout enlargens uniformly the total cost for that layout will be 0
	This is making sure that roads that are longer in the original data set remain longer in the optimised set
	(see: 4.3.2. #3)
*/
class RelativeLengthTerm :
	public CostTerm
{
public:
	RelativeLengthTerm(void);
	~RelativeLengthTerm(void);

	//the stuff we inherited from our costTerm
	///Compares the relative lengths between all edges in the original data and the proposed data
	/**	Computes the ratio between e and every other edge in the tempData
		Then compares that ratio with the ratio between the same edges in the original data
		Higher deviations in the ratio result in a higher cost	*/
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///PostCost for the RelativeLengthTerm is always 0
	//double PostCost();
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	/**	Is 1 in both stage one and stage two	*/
	double GetWeight(int suffix);
private:
	//The vector containing all the edge IDs from a single segment
	/*	Set once in the beginning according to the MyData set*
	std::vector<std::vector<unsigned int>> segmentEdgeIDs;
	// the costs of the edge segments from the MyData set
	std::vector<double> origSegmentCosts;
	//containing the edges accessed by their IDs
	/*	Reset every evaluation via the Resetterm method*
	std::map<unsigned int, Edge *> edgeIdToEdge;
	*/
};

