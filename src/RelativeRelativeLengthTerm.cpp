#include "StdAfx.h"
#include "RelativeRelativeLengthTerm.h"
#include <algorithm>

RelativeRelativeLengthTerm::RelativeRelativeLengthTerm(void)
{
}


RelativeRelativeLengthTerm::~RelativeRelativeLengthTerm(void)
{
}

double RelativeRelativeLengthTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	if(this->typeToCount.size() == 0){
		InitMaps(tempData);
	}
	double sum = 0;
	//we always sum up the values for the current edge and all other edges (apart from the already visited ones)
	//[e.g. if we are @edge 2 we do not need to consider the rel. length of edge 2 and edge 1 since they have been covered while we were @edge 1]
	for(unsigned int i = 0; i < tempData->GetEdgeList().size(); i++){
		Edge * tempEdge = tempData->GetEdgeList().at(i);
		double multi = this->GetMultiplier(e->GetStreetType(), tempEdge->GetStreetType());
		if(multi == 0){
			continue;
		}
		double r0, r, square;
		//the original distance of both e and the tempedge
		double oEDist = data->GetEdgeByID(e->GetEdgeID())->GetDistance();
		double oTDist = data->GetEdgeByID(tempEdge->GetEdgeID())->GetDistance();
		//and in case the first layouting step reduced edges quite a lot, prevent them from being 0
		oEDist = oEDist > 0.00001 ? oEDist : 0.00001;
		oTDist = oTDist > 0.00001 ? oTDist : 0.00001;
		//this should work for the edgelist in data should have the exact same size as the edgelist in the roadlayoutdata
		r0 = oEDist > oTDist ? (oEDist / oTDist) : (oTDist / oEDist);
		double nEDist = e->GetDistance();
		double nTDist = tempEdge->GetDistance();
		nEDist = nEDist > 0.00001 ? nEDist : 0.00001;
		nTDist = nTDist > 0.00001 ? nTDist : 0.00001;
		r = oEDist > oTDist ? (nEDist / nTDist) : (nTDist / nEDist);
		//simply apply the multiplier before squaring
		square = (r0 - r)/r * multi;
		sum += square * square;
	}
	return sum;
}

void RelativeRelativeLengthTerm::InitMaps(RoadLayoutData * data){
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		StreetType type = data->GetEdgeList().at(i)->GetStreetType();
		//if the type is not yet present, insert it anew
		if(typeToCount.count(type) == 0){
			typeToCount.insert(std::pair<StreetType, unsigned int>(type, 1));
		}else{
			//else inc the count
			typeToCount[type] = typeToCount.at(type) + 1;
		}
		//since its a set it will automatically check for dupes, thus we can safely add all of em
		this->help.typesPresent.insert(type);
	}
}

double RelativeRelativeLengthTerm::GetMultiplier(StreetType mainType, StreetType otherType){
	if(mainType == otherType){
		//60 percent divided by the amount of edges (since 60 percent need be divided onto all the edges of the type)
		return 60 / this->typeToCount.at(mainType);
	}else{
		std::vector<StreetType> neighbours = this->help.GetNeighbouringTypes(mainType);
		if(std::find(neighbours.begin(), neighbours.end(), otherType) != neighbours.end()){
			//count up all the neighbouring edges
			double count = 0;
			for(unsigned int i = 0; i < neighbours.size(); i++){
				count += this->typeToCount.at(neighbours.at(i));
			}
			//and divide the 40 percent by all the neighbouring edges
			return 40 / count;
		}

	}
	//if the otherType is neither the same nor a neighbouring type, return 0 (for that type has no influence on our cost)
	return 0;
}

std::string RelativeRelativeLengthTerm::ToString(){
	return std::string("AreaControlTerm");
}

std::string RelativeRelativeLengthTerm::GetIdentifier(){
	return std::string("AreaControlTerm");
}

double RelativeRelativeLengthTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::relRelLengOne;
	case 2:
		return OptionFile::relRelLengTwo;
	default:
		return -1;
	}
}