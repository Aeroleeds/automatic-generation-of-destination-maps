#pragma once
#include <vector>
#include "OutputBroker.h"
#include "pipelinePart.h"
#include "Renderer.h"
#include "DebugOptions.h"
#include "RoadExtender.h"
#include "TraversableRoute.h"
#include "DataWallE.h"
#include "DataGrabber.h"
#include "VisibilityRingCreator.h"
#include "RoadMerger.h"
#include "RampRemover.h"
#include "PrintXML.h"
#include "GeometrySimplifier.h"
#include "RoadLayout.h"
#include "RatioHelper.h"
#include "Clipper.h"
#include "RoadStumper.h"
#include "MotorwayTurnRemover.h"
#include "RoundAboutKiller.h"
#include "RoundaboutTaker.h"
#include <qthread>
#include <qstring.h>

/*
	The pipeline we go through in order to reach the final destination map.
	Consists of multiple pipelineParts each doing a small bit of work.
	Here components can be added/removed from the workflow
*/
///The heart piece of the algorithm, creates and works with a MyData object by using PipelinePart objects
/** Simply calling the setup and run() (or alternatively the DoWork() method, if no threading is required) will perform the whole algorithm
	Additionally sets the OutputBroker at the PipelineParts in order to forward progress messages
	
	The different PipelineSettings are as follows (in the typical switch/case style, a break is needed to prevent later additions):
		
		ALL settings use the DataGrabber first

		All parts:

			PrintXML (suffix: 1)
			VisibilityRingCreator
			TraversableRoute
			RoadExtender
			RoundaboutTaker
			DataWallE
			PrintXML (suffix: 2)
		
		Selected data:
			
			RoundAboutKiller
			RoadStumper
			RoadMerger
			RampRemover
			MotorwayTurnRemover
			PrintXML (suffix: 3)
			GeometrySimplifier
			PrintXML (suffix: 4)
			Clipper
			PrintXML (suffix: 5)

		Pre-Layout data:

			RoadLayout
			PrintXML (suffix: 6)
			break;

		Only Render:

			Renderer
			break;

		Only Download:
			
			PrintXML (suffix: 1)
			break;

		Up to visibility rings:

			VisibilityRingCreator
			DataWallE
			PrintXML (suffix: 1)

		Up to traversable route:

			VisibilityRingCreator
			TraversableRoute
			DataWallE
			PrintXML (suffix: 1)
			break;


	*/
class Pipeline : public QThread
{
	Q_OBJECT


signals:
	///The signal to the mainwindow that the latest computation has finished, the returned QString is either true or false depending on how the computation finished
	void ResultReady(const QString &res);
public:
	Pipeline(void);
	~Pipeline(void);
	///Needs be called before every call to another method of the pipeline
	/** will build the pipeline and do all the necesseties for the well-being of our system
		returns true if system was setup correctly, false otherwise
	*/
	virtual bool SetUp();
	///Not to be called manually, use run() instead
	/**	will simply go through all the specified pipeline parts and will let them work their magic
		is additionally responsible for responding to eventual failures
		returns true if the result has been saved as desired, false if anything out of the ordinary happened
		//TODO: probably should make this private
	*/
	virtual std::string DoWork();
	///The method to be called from the outside to get the pipeline running
	void run() Q_DECL_OVERRIDE{
		QString	res = QString(this->DoWork().c_str());
		emit ResultReady(res);
	}
	///Method to display progress messages on the sub level of the progress dialog
	/**	Set an OutputBroker first before relying on this method (via SetBroker)*/
	virtual void DisplaySubProg(std::string text);
	///The OutputBroker is the one handling where the output provided to the DisplaySubProg is to be lead
	/**	Its safe to leave the OutputBroker unset, DisplaySubProg will then simply swallow the output to be displayed*/
	virtual void SetBroker(OutputBroker * outputBroker);

private:
	///The vector containing all PipelinePart objects to be used during the algorithm
	/**	Is initialised in the SetUp method according to the OptionFile::pipeSet parameter (see: OptionFile::PipelineSetup*/
	std::vector<PipelinePart *> pipeParts;
	///Saves the MyData object during and even after the DoWork() method has ended, for possible further use (not yet in the algorithm)
	MyData * data;
	///Used for showing the output in the dialog
//	MainWindow *windowOutput;
	///The OutputBroker to assign to the PipelinePart objects
	OutputBroker *output;
	///PipelinePart to print the MyData as xml document
	PrintXML printXML;
	
	//DebugOptions debugOptions;
	///PipelinePart to get the data and write it into a MyData object from either a data file or the internet
	DataGrabber dataGrabber;
	///PipelinePart to create the visibility rings
	VisibilityRingCreator visRing;
	///PipelinePart to connect the visibility rings to the destination Node
	TraversableRoute traversRout;
	///PipelinePart to extend taken Edges
	RoadExtender roadTender;
//	LinkRoadTaker linkRoker;
	///PipelinePart to clean up non-taken Nodes and Edges
	DataWallE wallEE;
	///PipelinePart to reduce the extended roads back to the last intersection
	RoadStumper roadStum;
	///PipelinePart to merge parallel edges that have the same name (mainly motorways)
	RoadMerger roadMer;
	///PipelinePart to remove all link roads and replace them with simple intersections
	RampRemover ramRem;
	///PipelinePart to remove streets that are just there to turn around on the motorway
	MotorwayTurnRemover moTuRe;
	///PipelinePart to simplify our geometry by removing as many val == 2 Nodes as possible
	GeometrySimplifier geoSim;
	///PipelinePart to reduce the MyData to only include Nodes and Edges that are in the OptionFile::bb BoundingBox
	Clipper clip;
	///PipelinePart to change the layout of the selected MyData edges and nodes
	RoadLayout roadLa;
	///PipelinePart to replace Roundabouts with simple intersections
	RoundAboutKiller kill;
	///PipelinePart to take all Edges and Nodes of a roundabout if one of the nodes of a roundabout is taken (provides us with a clean roundabout to work with)
	RoundaboutTaker roundTaky;
	///PipelinePart to implement
	Renderer lux;
};

