#include "StdAfx.h"
#include "RelativeOrientationTerm.h"
#include "Vec2.h"
#include "Line.h"

//PI definition according to cplusplus.com
#define PI 3.14159265

RelativeOrientationTerm::RelativeOrientationTerm(MyData * data)
{
	std::vector<Edge *> edges = data->GetEdgeList();
	for(unsigned int i = 0; i < edges.size(); i++){
		//this is where we write all the ids of edges neighbouring our current edge (these cannot be considered parallel)
		std::set<unsigned int> neighboursID;
		//first off, look for any neighbouring edges that are straight or orthogonal to our current edge
		std::vector<Edge *> otherEdges = this->NeighbourEdges(edges.at(i), data);
		for(unsigned int j = 0; j < otherEdges.size(); j++){
			neighboursID.insert(otherEdges.at(j)->GetEdgeID());
			double angle = this->GetAngle(edges.at(i), otherEdges.at(j));
			//is the neighbouring edge straight?
			if(angle < 5){
				if(straightPairs.count(std::make_pair(edges.at(i)->GetEdgeID(), otherEdges.at(j)->GetEdgeID())) == 0 && straightPairs.count(std::make_pair(otherEdges.at(j)->GetEdgeID(), edges.at(i)->GetEdgeID())) == 0){
					this->straightPairs.insert(std::pair<unsigned int, unsigned int>(edges.at(i)->GetEdgeID(), otherEdges.at(j)->GetEdgeID()));
					this->interestingEdges.insert(edges.at(i)->GetEdgeID());
					this->interestingEdges.insert(otherEdges.at(j)->GetEdgeID());
				}
			}
			//is the neighbouring edge perpendicular to our edge?
			if(angle > 85 && angle < 95){
				if(orthogonalPairs.count(std::make_pair(edges.at(i)->GetEdgeID(), otherEdges.at(j)->GetEdgeID())) == 0 && orthogonalPairs.count(std::make_pair(otherEdges.at(j)->GetEdgeID(), edges.at(i)->GetEdgeID())) == 0){
					this->orthogonalPairs.insert(std::pair<unsigned int, unsigned int>(edges.at(i)->GetEdgeID(), otherEdges.at(j)->GetEdgeID()));
					this->interestingEdges.insert(edges.at(i)->GetEdgeID());
					this->interestingEdges.insert(otherEdges.at(j)->GetEdgeID());
				}

			}
		}
		//after checking the neighbours check for potentially parallel edges
		//TODO: use the shortest distance thingy of Edge to get the nearest parallel lines and save them here
		Edge * left = nullptr, *right = nullptr;
		double distLeft = DBL_MIN, distRight = DBL_MAX;
		for(unsigned int j = 0; j < edges.size(); j++){
			if(neighboursID.count(edges.at(j)->GetEdgeID()) != 0 || i == j){
				continue;
			}
			double angle = this->GetAngle(edges.at(i), edges.at(j));
			//an edge is considered parallel if it differs less than 5 percent from either 0 or 180 degrees
			if((angle < 5 || (angle <= 180 && angle > 175)) && this->Intersects(edges.at(i), edges.at(j))){
				double res = help.GetDistance(edges.at(i), edges.at(j));
				if(res < 0 && res > distLeft){
					left = edges.at(j);
					distLeft = res;
				}else if(res > 0 && res < distRight){
					right = edges.at(j);
					distRight = res;
				}
			}
		}
		//take the absolute distances from here on out
		distLeft = std::abs(distLeft);
		//we actually should not need to abs the distRight, but hey
		distRight = std::abs(distRight);
		//now we need to check whether an edge perpendicular to our current edge leading to edgesLeft and edgeRight are intersecting any other edges, if so, do not save them
		Edge * eLeft = nullptr, *eRight = nullptr;
		if(left != nullptr){
			eLeft = this->GetPerpendicularEdge(edges.at(i), left);
		}
		if(right != nullptr){
			eRight = this->GetPerpendicularEdge(edges.at(i), right);
		}
		//if we found possible parallel edges
		if(right != nullptr || left != nullptr){
			//check whether they intersect something, if they do set the corresponding pointers back to nullptr

			//THERE WERE DISTANCES IN HERE, I DEEMED THEM UNNECCESSARY, FOR IF SOMETHING INTERSECTS THATS A NO-NO NO MATTER WHERE THE INTERSECTION IS

			for(unsigned int j = 0; j < edges.size(); j++){
				if(*edges.at(j) == *edges.at(i)){
					continue;
				}
				if(left != nullptr && *edges.at(j) != *left){
					if(help.IsFalseIntersection(eLeft, edges.at(j))){
					//	double res = std::abs(help.GetDistance(edges.at(j), edges.at(i)));
						//the res should be negative, if it is to the same side as the left edge
						//if the res is additionally larger (closer to 0) than the distLeft (the one from our current edge) than theres something in between our left edge and the potential parallel edge
						//this is for the distance is actually the absolute distance paired with the corresponding sign (so lower absolute value means closer)
						//if(res < 0 && res > distLeft){
						//no need for sides, for the edge we check for falseintersections against only spans between the two corresponding edges, thus ALL intersections are relevant
						//if(res < distLeft){
						Node * toDel = eLeft->GetStartNode();
						delete eLeft;
						delete toDel;
						eLeft = nullptr;
						left = nullptr;
						//}
					}
				}
				if(right != nullptr && *edges.at(j) != *right){
					if(help.IsFalseIntersection(eRight, edges.at(j))){
					//	double res = std::abs(help.GetDistance(edges.at(j), edges.at(i)));
						//the res should be positive, if it is to the same side as the right edge
						//if the res is additionally smaller (closer to 0) than the distRight (the one from our current edge) than theres something in between our right edge and the potential parallel edge
						//this is for the distance is actually the absolute distance paired with the corresponding sign (so lower absolute value means closer)
						//if(res > 0 && res < distRight){
						//if(res < distRight){
						Node * toDel = eRight->GetStartNode();
						delete eRight;
						delete toDel;
						eRight = nullptr;
						right = nullptr;
						//}
					}
				}
				if(left == nullptr && right == nullptr){
					break;
				}
			}
			//if any edges survived, add them to the parallel pairs
			if(left != nullptr){
				Node * toDel = eLeft->GetStartNode();
				delete eLeft;
				delete toDel;
				this->parallelPairs.insert(std::pair<unsigned int, unsigned int>(edges.at(i)->GetEdgeID(), left->GetEdgeID()));
				this->interestingEdges.insert(edges.at(i)->GetEdgeID());
				this->interestingEdges.insert(left->GetEdgeID());
			}
			if(right != nullptr){
				Node * toDel = eRight->GetStartNode();
				delete eRight;
				delete toDel;
				this->parallelPairs.insert(std::pair<unsigned int, unsigned int>(edges.at(i)->GetEdgeID(), right->GetEdgeID()));
				this->interestingEdges.insert(edges.at(i)->GetEdgeID());
				this->interestingEdges.insert(right->GetEdgeID());
			}
		}
	}
}


RelativeOrientationTerm::~RelativeOrientationTerm(void)
{
}

double RelativeOrientationTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data){
	//here we just build our map out of the interesting edges of tempData
	if(this->interestingEdges.count(e->GetEdgeID()) != 0){
		this->edgeIDToEdge.insert(std::make_pair(e->GetEdgeID(), e));
	}
	return 0;
}

//the computation takes place in here!
double RelativeOrientationTerm::PostCost(){
	double sum = 0;
	std::set<std::pair<unsigned int, unsigned int>>::iterator it;
	for(it = orthogonalPairs.begin(); it != orthogonalPairs.end(); ++it){
		//since an orthogonal angle is supposedly PI/2 we get the differnce from the original angle by subtracting PI/2
		double ret = this->GetAngle(*it) - PI/2;
		sum += ret * ret;
	}
	for(it = straightPairs.begin(); it != straightPairs.end(); ++it){
		double ret = this->GetAngle(*it);
		sum += ret * ret;
	}
	for(it = parallelPairs.begin(); it != parallelPairs.end(); ++it){
		double ret = this->GetAngle(*it);
		sum += ret * ret;
	}
	this->edgeIDToEdge.clear();
	return sum;
}

std::string RelativeOrientationTerm::ToString(){
	return std::string("Cost Function : Relative Orientation term");
}

std::string RelativeOrientationTerm::GetIdentifier(){
	return std::string("relOrient");
}

double RelativeOrientationTerm::GetWeight(int suffix){
	switch(suffix)
	{
	case 1:
		return OptionFile::relOrientOne;
	case 2:
		return OptionFile::relOrientTwo;
	default:
		return -1;
	}
}

void RelativeOrientationTerm::ResetTerm(){
	this->edgeIDToEdge.clear();
}

//quick helper to instantly get the angle for the edges in a pair
double RelativeOrientationTerm::GetAngle(std::pair<unsigned int, unsigned int> pair){
	Vec2 vec(this->edgeIDToEdge.at(pair.first)->GetStartNode(), this->edgeIDToEdge.at(pair.first)->GetEndNode());
	Vec2 other(this->edgeIDToEdge.at(pair.second)->GetStartNode(), this->edgeIDToEdge.at(pair.second)->GetEndNode());	
	return vec.GetAngleRadians(other);
}

double RelativeOrientationTerm::GetAngle(Edge * e, Edge * other){
	Vec2 vec(e->GetStartNode(), e->GetEndNode());
	Vec2 vec322(other->GetStartNode(), other->GetEndNode());
	return vec.GetAngle(vec322);
}

std::vector<Edge *> RelativeOrientationTerm::NeighbourEdges(Edge * edge, MyData * data){
	std::vector<Edge *> ret;
	std::vector<Edge *> edges = data->GetEdgesByNodeID(edge->GetStartNode()->GetNodeID());
	for(unsigned int i = 0; i < edges.size(); i++){
		if(*edges.at(i) == *edge){
			continue;
		}
		ret.push_back(edges.at(i));
	}
	edges = data->GetEdgesByNodeID(edge->GetEndNode()->GetNodeID());
	for(unsigned int i = 0; i < edges.size(); i++){
		if(*edges.at(i) == *edge){
			continue;
		}
		ret.push_back(edges.at(i));
	}
	return ret;
}

bool RelativeOrientationTerm::Intersects(Edge * e, Edge * other){
	Line line = Line(e->GetStartNode(), e->GetEndNode());
	Node * insec = line.IntersectionPoint(other->GetStartNode(), 0);
	if(line.IntersectionOnSegment(insec , e)){
		delete insec;
		return true;
	}
	delete insec;
	insec = line.IntersectionPoint(other->GetEndNode(), 0);
	if(line.IntersectionOnSegment(insec, e)){
		delete insec;
		return true;
	}
	delete insec;
	line = Line(other->GetStartNode(), other->GetEndNode());
	insec = line.IntersectionPoint(e->GetStartNode(), 0);
	if(line.IntersectionOnSegment(insec, other)){
		delete insec;
		return true;
	}
	delete insec;
	insec = line.IntersectionPoint(e->GetEndNode(), 0);
	if(line.IntersectionOnSegment(insec, other)){
		delete insec;
		return true;
	}
	delete insec;
	return false;
}

Edge * RelativeOrientationTerm::GetPerpendicularEdge(Edge * e, Edge * other){
	Line line = Line(e->GetStartNode(), e->GetEndNode());
	//if the intersection is between the IntersectionPoint (that is automatically lying on edge e) and other->GetStartNode() return an edge consisting of those two (and proceed similarly w/the other possibilites)
	Node * insec = line.IntersectionPoint(other->GetStartNode(), 0);
	if(line.IntersectionOnSegment(insec, e)){
		delete insec;
		return new Edge(line.IntersectionPoint(other->GetStartNode(), 0), other->GetStartNode(), 12, "efes");
	}
	delete insec;
	insec = line.IntersectionPoint(other->GetEndNode(), 0);
	if(line.IntersectionOnSegment(insec, e)){
		delete insec;
		return new Edge(line.IntersectionPoint(other->GetEndNode(), 0), other->GetEndNode(), 12, "guinness");
	}
	delete insec;
	line = Line(other->GetStartNode(), other->GetEndNode());
	insec = line.IntersectionPoint(e->GetStartNode(), 0);
	if(line.IntersectionOnSegment(insec, other)){
		delete insec;
		return new Edge(line.IntersectionPoint(e->GetStartNode(), 0), e->GetStartNode(), 12, "kilkenny");
	}
	delete insec;
	insec = line.IntersectionPoint(e->GetEndNode(), 0);
	if(line.IntersectionOnSegment(insec, other)){
		delete insec;
		return new Edge(line.IntersectionPoint(e->GetEndNode(), 0), e->GetEndNode(), 12, "murauer");
	}
	delete insec;
	return nullptr;
}
