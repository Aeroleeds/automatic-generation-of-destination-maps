#include "StdAfx.h"
#include "TraversableRoute.h"


TraversableRoute::TraversableRoute(void)
{
	dijk = Dijkstra();
	dijk.SetFactor(0.3);
	dijk.SetPenalty(10);
}


TraversableRoute::~TraversableRoute(void)
{
}

std::string TraversableRoute::PrepDoc(MyData * data){
	//reset the dijkstra
	//dijk.ResetDijkstra();
	//first off, get a pointer to our destination node
	Node * goal = data->GetNodeByID(OptionFile::destinationNode.GetNodeID());
	//all the nodes to be connected to the goal node
	std::vector<Node *> interestingNodes;
	//loop through the nodes and find all the interesting ones (i.e. val == 3, taken and a ringroad)
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		//if the node is either not taken or not a ringroad, then we do not intend to get a path from that node
		if(!data->GetNodeList().at(i)->IsTaken() || !data->GetNodeList().at(i)->IsRingRoad()) continue;
		//if the node is at an intersection, or an ending node (i.e. has only edge leading from it that is taken and a a ring road)
		//TODO: we automatically extend the samples to a val == 3 node, so we should not need the IsEndingNode(..) part
		if(data->GetNodeList().at(i)->GetValence() != 2){	// || this->IsEndingNode(data->GetNodeList().at(i), data)){
			interestingNodes.push_back(data->GetNodeList().at(i));
		}
	}
	//now that we have all the interesting nodes, may the dijk connect them
	dijk.ConnectAllToGoal(data, goal, interestingNodes);
	return std::string("Splendid");
}
std::string TraversableRoute::ToString(){
	return std::string("TraversableRoute");
}
/*
bool TraversableRoute::IsEndingNode(Node * node, MyData * data){
	int count = 0;
	for(unsigned int i = 0; i < data->GetEdgesByNodeID(node->GetNodeID()).size(); i++){
		if(data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsRingRoad() && data->GetEdgesByNodeID(node->GetNodeID()).at(i)->IsTaken()){
			count++;
		}
	}
	return count == 1;
}*/

/*
Edge * TraversableRoute::GetCommonEdge(Node * one, Node * two, MyData * data){
	std::vector<Edge *> edgesOne = data->GetEdgesByNodeID(one->GetNodeID());
	std::vector<Edge *> edgesTwo = data->GetEdgesByNodeID(two->GetNodeID());
	unsigned int sizeOne = edgesOne.size();
	unsigned int sizeTwo = edgesTwo.size();
	for(unsigned int i = 0; i < sizeOne; i++){
		for(unsigned int j = 0; j < sizeTwo; j++){
			if(*edgesOne.at(i) == *edgesTwo.at(j)){
				return edgesOne.at(i);
			}
		}
	}
	return nullptr;
}*/