#pragma once
#include "CostTerm.h"

///Computes the sums of energy for the layouting of our data, using the CostTerm objects
/**	Used on newly created layouts of the data, to measure their quality (better layouts are immediately taken, while worse layouts are discarded)
	The metrics used to reach the value of the quality are the CostTerms (for details, look at the CostTerm children)
	The used strategy to change the layout is described by the Layout Move objects
	For reference see: 4.3.2. */
class CostFunction
{
public:
	//We will need the original data for initialising the stuff for our costterms
	CostFunction(void);
	~CostFunction(void);
	
	///Setup initialises all the values, NEEDS be called before doing anything else
	/**	Defines the CostTerms to be used and initialises the suffix (used for the weights)*/
	void SetUp(MyData * data);
	///returns the energy for the given tempData
	/**	The MyData object is to be the object before the first layouting step, and it is necessary as reference for some CostTerms (e.g. the RelativeLengthTerm)
		Feeds all edges of the tempData to all Costterms and sums and weighs the results according to their weight and to the stage we are in
		use for evaluation after a move has been made*/
	double GetEnergy(MyData * data, RoadLayoutData * tempData);
	///changes the weights associated w/the terms (increases the suffix by one)
	/**	returns whether the change was successful
		and thus will return false if we have already been at our last set of weights*/
	bool SwitchWeights();
	
	//Resets the suffix (use with caution)
	/*	Any potentially */
	//void ResetValues();
private:
	///the vector containing all the terms used for layouting (see: 4.3.2.)
	std::vector<CostTerm *> costTerms;
	///Suffix to determine which weights need be used
	int suffix;
};

