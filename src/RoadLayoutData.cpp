#include "StdAfx.h"
#include "RoadLayoutData.h"


RoadLayoutData::RoadLayoutData(std::vector<Node *> nodes, std::vector<Edge *> edges)
{
	std::map<unsigned int, Node *> nodeIDtoNode;
	for(unsigned int i = 0; i < nodes.size(); i++){
		Node * n = new Node(nodes.at(i)->GetLon(false), nodes.at(i)->GetLat(false), nodes.at(i)->GetNodeID());
		n->SetRingRoad(nodes.at(i)->IsRingRoad());
		n->SetValence(nodes.at(i)->GetValence());
		nodeIDtoNode.insert(std::pair<unsigned int, Node *>(n->GetNodeID(), n));
		this->nodeList.push_back(n);
	}
	for(unsigned int i = 0; i < edges.size(); i++){
		Edge * e = new Edge(nodeIDtoNode.at(edges.at(i)->GetStartNode()->GetNodeID()),nodeIDtoNode.at(edges.at(i)->GetEndNode()->GetNodeID()), edges.at(i)->GetMaxSpeed(), edges.at(i)->GetStreetName(), edges.at(i)->GetEdgeID());
		e->SetOneWay(edges.at(i)->IsOneWay());
		e->SetRingRoad(edges.at(i)->IsRingRoad());
		e->SetRoundabout(edges.at(i)->IsRoundabout());
		e->SetStreetType(edges.at(i)->GetStreetType());
		this->edgeList.push_back(e);
	}
}

RoadLayoutData::~RoadLayoutData(void)
{
	for(unsigned int i = 0; i < edgeList.size(); i++){
		delete edgeList.at(i);
	}
	this->edgeList.clear();
	for(unsigned int i = 0; i < nodeList.size(); i++){
		delete nodeList.at(i);
	}
	this->nodeList.clear();
}

RoadLayoutData * RoadLayoutData::GetDeepCopy(){
	return new RoadLayoutData(this->nodeList, this->edgeList);
}

void RoadLayoutData::DeleteAll(){
	for(unsigned int i = 0; i < edgeList.size(); i++){
		delete edgeList.at(i);
	}
	this->edgeList.clear();
	for(unsigned int i = 0; i < nodeList.size(); i++){
		delete nodeList.at(i);
	}
	this->nodeList.clear();
}

std::vector<Edge *> RoadLayoutData::GetEdgeList(){return this->edgeList;}

std::vector<Node *> RoadLayoutData::GetNodeList(){return this->nodeList;}