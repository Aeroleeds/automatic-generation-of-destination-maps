#include "StdAfx.h"
#include "OutputBroker.h"

OutputBroker::OutputBroker(MainWindow * output)
{
	this->outputWindow = output;
}


OutputBroker::~OutputBroker(void)
{
}

void OutputBroker::DisplayProgress(std::string text, bool isSubProg){
	if(this->outputWindow == nullptr) return;
	emit(ProgressChangedSignal(QString(text.c_str()), isSubProg));

	//mutex.lock();
	//outputWindow->DisplayProgress(text, isSubProg);
	//mutex.unlock();
}
