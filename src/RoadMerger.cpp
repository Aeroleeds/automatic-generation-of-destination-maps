#include "StdAfx.h"
#include "RoadMerger.h"
#include "Vec2.h"
#include "Line.h"
#include <algorithm>

RoadMerger::RoadMerger(void)
{
}


RoadMerger::~RoadMerger(void)
{
}

std::string RoadMerger::ToString(){
	return std::string("Road Merger");
}

void RoadMerger::SetUp(){
	nameToEdge.clear();
}

std::string RoadMerger::PrepDoc(MyData * data){
	//Setup the name to edge map
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		Edge * edge = data->GetEdgeList().at(i);
		if(this->nameToEdge.count(edge->GetStreetName()) == 0){
			std::vector<Edge *> tempEdges;
			tempEdges.push_back(edge);
			this->nameToEdge.insert(std::pair<std::string, std::vector<Edge *>>(edge->GetStreetName(), tempEdges));
		}else{
			std::vector<Edge *> tempEdges = this->nameToEdge.at(edge->GetStreetName());
			tempEdges.push_back(edge);
			this->nameToEdge[edge->GetStreetName()] = tempEdges;
		}
	}
	//first off, get all the nodes w/valence != 2 (we can use a vector here, since duplicates will not appear since we go through all nodes only once)
	std::vector<Node *> markedNodes;
	std::vector<std::pair<Node *, std::vector<Node *>>> nodesToMerge;
	std::vector<Node *> allNodes = data->GetNodeList();
	//only do the lookup once
	bool lookUp = OptionFile::mergeOnNameChange;
	for(unsigned int i = 0; i < allNodes.size(); i++){
		if(allNodes.at(i)->GetValence() != 2){
			markedNodes.push_back(allNodes.at(i));
		}else if(lookUp){
			//additionally look at all val == 2 nodes where a name change occurs
			std::vector<Edge *> tempEdges = data->GetEdgesByNodeID(allNodes.at(i)->GetNodeID());
			if(tempEdges.at(0)->GetStreetName() != tempEdges.at(1)->GetStreetName()){
				markedNodes.push_back(allNodes.at(i));
			}
		}
	}
	//save the names of all the found parallel streets here
	std::set<std::string> parallelStreetNames;
	//and save all the nodes that were merged
	std::vector<Node *> mergedNodes;
	//then loop through all the marked nodes, possible nearby edges, and if so, merge the edges (and thereby nodes) together
	for(unsigned int i = 0; i < markedNodes.size(); i++){
		std::vector<Edge *> edges = data->GetEdgesByNodeID(markedNodes.at(i)->GetNodeID());
		std::set<std::string> visitedEdges;
		std::vector<Node *> tempNodes;
		for(unsigned int j = 0; j < edges.size(); j++){
			//do not merge link roads, they are handled separately
			if(edges.at(j)->IsLinkRoad()){
				continue;
			}
			if(visitedEdges.count(edges.at(j)->GetStreetName()) == 0){
				visitedEdges.insert(edges.at(j)->GetStreetName());

				Edge * splittedEdge = this->FindNearbyParallelEdge(data, edges.at(j), markedNodes.at(i));
				Node * temp = nullptr;
				//if we found a valid edge, get the corresponding node on it (the one that we will split the edge against
				if(splittedEdge != nullptr){
					Line line = Line(splittedEdge->GetStartNode(), splittedEdge->GetEndNode());
					temp = line.IntersectionPoint(markedNodes.at(i), markedNodes.at(i)->GetNodeID());
				}
				//if we found a new Node on a parallel edge, save it for computation done later
				if(temp != nullptr){
					tempNodes.push_back(temp);
					//this seems redundant for we only search for parallel streets with the same name anyway
					parallelStreetNames.insert(edges.at(j)->GetStreetName());
					this->SplitEdge(splittedEdge, temp, data);
				}
			}
		}
		if(tempNodes.size() == 0){
			continue;
		}
		nodesToMerge.push_back(std::pair<Node *, std::vector<Node *>>(markedNodes.at(i), tempNodes));
	}

	for(unsigned int i = 0; i < nodesToMerge.size(); i++){
			/*	now we need to merge all the newly created nodes into a single one
			and additionally relink the edges so they all lead to the same node
		*/
		mergedNodes.push_back(this->MergeAndDeleteOtherNodes(nodesToMerge.at(i).first, nodesToMerge.at(i).second, data));
	}

	//recompute the edge length for the edges at merged nodes
	for(unsigned int i = 0; i < mergedNodes.size(); i++){
		std::vector <Edge *> edgars = data->GetEdgesByNodeID(mergedNodes.at(i)->GetNodeID());
		for(unsigned int j = 0; j < edgars.size(); j++){
			edgars.at(j)->RecomputeDistance();
		}
	}

	//return std::string("Splendid");

	//then merge any connections appearing more than once between two merged edges
	//redid this part to merge any connections appearing more than once, no matter whether the two edges (and nodes) were merged or not
	for(unsigned int i = 0; i < mergedNodes.size(); i++){
		std::vector<Edge *> edges = data->GetEdgesByNodeID(mergedNodes.at(i)->GetNodeID());
		//bool found = false;
		for(unsigned int j = 0; /*!found &&*/ j < edges.size(); j++){
			//our edge pointer may be a nullptr, if it was already merged into a new edge (obviously dont proceed further w/nullptrs)
			if(edges.at(j) == nullptr){
				continue;
			}
			if(parallelStreetNames.count(edges.at(j)->GetStreetName()) != 0){
				/**
					follow the edge until we reach a node w/val != 2
					check whether its in our marked nodes
				*/
				Edge * lead = nullptr;
				Edge * temp = edges.at(j);
				//get the next node with val != 2
				Node * tempNode = this->GetNextNode(mergedNodes.at(i), temp, data);
				//if the node is not one of the merged ones, keep looking elsewhere
				//IGNORE WHETHER THE NODE WAS MERGED PREVIOUSLY (TODO: make sure that's what we want) (fairly confident 'bout it though)
				/*if(std::find(mergedNodes.begin(), mergedNodes.end(), tempNode) == mergedNodes.end()){
					continue;
				}*/
				//save the edge we came from
				lead = temp;
				//and get all the edges leading from our original node
				std::vector<Edge *> dgs = data->GetEdgesByNodeID(mergedNodes.at(i)->GetNodeID());
				for(unsigned int k = 0; k < dgs.size(); k++){
					if(dgs.at(k)->GetStreetName() != lead->GetStreetName()){
						continue;
					}
					if(*dgs.at(k) == *lead){
						continue;
					}
					//if the edge has the correct name and is not the one we just came from, follow it!
					Node * res = GetNextNode(mergedNodes.at(i), dgs.at(k), data);
					//if we end up at the other merged nodes, we need to merge the two paths
					if(*res == *tempNode){
						//collapsing some edges
						CollapseEdges(mergedNodes.at(i), tempNode, edges.at(j), dgs.at(k), data);
						//found = true;
						//reset the data used (to avoid null ptr conflicts)
						edges = data->GetEdgesByNodeID(mergedNodes.at(i)->GetNodeID());
						//and reset the running variable (is set to -1, for it is automatically inc'd to 0)
						j = -1;
						//if we found what we were looking for, keep on keeping on
						break;
					}
				}
			}
		}
	}
	return std::string("Splendid");
}

void RoadMerger::CollapseEdges(Node * one, Node * two, Edge * eOne, Edge * eTwo, MyData * data){
	Edge * toRemove = nullptr;
	Node * first = nullptr;
	//and some pointers so we can easily remove possibly existing one-way roads
	/*Edge * noOneWay = nullptr;
	Node * noOneStart = nullptr;*/
	if(eOne->GetStreetType() >= eTwo->GetStreetType()){
		toRemove = eTwo;
		first = one;
//		noOneWay = eOne;
	//	noOneStart = one;
	}else{
		toRemove = eOne;
		first = one;
		//noOneWay = eTwo;
		//noOneStart = two;
		if(this->GetDistanceToNextNode(one, eOne, data) > this->GetDistanceToNextNode(two, eTwo, data)){
			toRemove = eOne;
			first = one;
		//	noOneWay = eTwo;
		//	noOneStart = two;
		}else{
			toRemove = eTwo;
			first = two;
		//	noOneWay = eOne;
		//	noOneStart = one;
		}
		
	}
	std::vector<Edge *> remEd;
	std::vector<Node *> remNd;
	first = toRemove->GetOtherNode(first);
	while((first->GetValence() == 2) || this->OnlyTwoRealEdges(first, data)){
		//add them to the list to be removed
		remNd.push_back(first);
		remEd.push_back(toRemove);
		//then reassign them to the next values
		if(first->GetValence() == 2){
			toRemove = (*data->GetEdgesByNodeID(first->GetNodeID()).at(0) == *toRemove) ? data->GetEdgesByNodeID(first->GetNodeID()).at(1) : data->GetEdgesByNodeID(first->GetNodeID()).at(0);
			first = toRemove->GetOtherNode(first);
		}else{
			//if theres a (or even more than one) link road here, reconnect the link road and then follow the other "real" edge
			std::vector<Edge *> links = data->GetEdgesByNodeID(first->GetNodeID());
			//in order to not mess w/our GetOtherNode() method, we save the new values for first and toRemove, and assign them only after the looping
			Edge * newEdge = nullptr;
			Node * newNode = nullptr;
			for(unsigned int i = 0; i < links.size(); i++){
				//since the node w/the link road is to be replaced, we need to delete and re-add the link road to preserve connectivity
				if(links.at(i)->IsLinkRoad()){
					Node * relinkMe = links.at(i)->GetOtherNode(first);
					//find the shortest connection for the link edge
					Edge * tempEdgeOne = nullptr;
					Edge * tempEdgeTwo = nullptr;
					if(*links.at(i)->GetStartNode() == *relinkMe){
						tempEdgeOne = new Edge(relinkMe, one, links.at(i)->GetMaxSpeed(), links.at(i)->GetStreetName());
						tempEdgeTwo = new Edge(relinkMe, two, links.at(i)->GetMaxSpeed(), links.at(i)->GetStreetName());
					}else{
						tempEdgeOne = new Edge(one, relinkMe, links.at(i)->GetMaxSpeed(), links.at(i)->GetStreetName());
						tempEdgeTwo = new Edge(two, relinkMe, links.at(i)->GetMaxSpeed(), links.at(i)->GetStreetName());
					}
					
					//delete the longer one
					if(tempEdgeOne->GetDistance() > tempEdgeTwo->GetDistance()){
						delete tempEdgeOne;
						tempEdgeOne = nullptr;
						//relink the first tempEdge to be used after the if/else
						tempEdgeOne = tempEdgeTwo;
					}else{
						delete tempEdgeTwo;
						tempEdgeTwo = nullptr;
					}
					tempEdgeOne->SetStreetType(links.at(i)->GetStreetType());
					tempEdgeOne->SetTaken(true);
					//this works for we gave the edge the ID of the to-be-replaced edge
					data->RemoveAndDeleteEdge(data->GetEdgeByID(links.at(i)->GetEdgeID()));
					//and add the new link edge
					std::vector<Edge *> toBeAddedEdges;
					toBeAddedEdges.push_back(tempEdgeOne);
					data->AddEdgeList(toBeAddedEdges);
				}else if(*links.at(i) == *toRemove){
					continue;
				}else{
					//this should be executed exactly once, for theres only two non-link-road edges at that point
					newEdge = links.at(i);
					newNode = newEdge->GetOtherNode(first);
				}
			}
			toRemove = newEdge;
			first = newNode;
		}
	}
	//and add the edge once more
	remEd.push_back(toRemove);
	//the first val != 2 node has to be our merge node
	//then do the removal
	for(unsigned int i = 0; i < remEd.size(); i++){
		data->RemoveAndDeleteEdge(remEd.at(i));
	}
	for(unsigned int i = 0; i < remNd.size(); i++){
		data->RemoveAndDeleteNode(remNd.at(i));
	}
	/*
	//and remove possible one-way tags
	noOneStart = noOneWay->GetOtherNode(noOneStart);
	while(noOneStart->GetValence() == 2){
		noOneWay->SetOneWay(false);

	}*/
}


//here we replace any potentially existing nodes that are the mirror image of our firstNode any other edge from the data set
Node * RoadMerger::MergeAndDeleteOtherNodes(Node * firstNode, std::vector<Node *> otherNodes, MyData * data){
	double latAvg = firstNode->GetLat(), lonAvg = firstNode->GetLon();
	//loop through all positions, and add them up
	for(unsigned int i = 0;i < otherNodes.size(); i++){
		latAvg += otherNodes.at(i)->GetLat();
		lonAvg += otherNodes.at(i)->GetLon();
	}
	firstNode->SetLat(latAvg / (otherNodes.size() + 1));
	firstNode->SetLon(lonAvg / (otherNodes.size() + 1));
	for(unsigned int i = 0; i < otherNodes.size(); i++){
		//get all edges leading from our node
		std::vector<Edge *> edges = data->GetEdgesByNodeID(otherNodes.at(i)->GetNodeID());
		//and check whether one of them uses the fake node
		for(unsigned int j = 0; j < edges.size(); j++){
			//replace the temporary node w/our first Node; and increase the valence to preserve our correct values
			if(*edges.at(j)->GetStartNode() == *otherNodes.at(i)){
				edges.at(j)->SetStartNode(firstNode);
				firstNode->SetValence(firstNode->GetValence() + 1);
			}else if(*edges.at(j)->GetEndNode() == *otherNodes.at(i)){
				edges.at(j)->SetEndNode(firstNode);
				firstNode->SetValence(firstNode->GetValence() + 1);
			}
			//dont do anything if the edge belongs to our original node (because the edge settings are already perfectly fine in that case)
		}
		//no need to delete anything for the fake node will NOT be added to the dataset, since the node id is already in the system
		//data->RemoveAndDeleteNode(otherNodes.at(i));
		//but still free the space
		delete otherNodes.at(i);
		otherNodes.at(i) = nullptr;
	}
	return firstNode;
}
//DEPRECATED
/*
Node * RoadMerger::FindLinkedAndMergedNodes(Node * firstNode, Edge * edgeOne, Edge * edgeTwo, std::vector<Node *> mergedNodes, MyData * data){
	Node * tempNode = edgeOne->GetOtherNode(firstNode);
	//start the traveling along the edges#
	//TODO: we might need to find the *tempNode
	while(std::find(mergedNodes.begin(), mergedNodes.end(), tempNode) == mergedNodes.end()){
		//if we find another node w/valence != 2 we return, we therefore could not find a valid node
		if(tempNode->GetValence() != 2){
			return nullptr;
		}
		else if(data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0) == edgeOne){
			edgeOne = data->GetEdgesByNodeID(tempNode->GetNodeID()).at(0);
		}else{
			edgeOne = data->GetEdgesByNodeID(tempNode->GetNodeID()).at(1);
		}
		tempNode = edgeOne->GetOtherNode(tempNode);
	}
	return tempNode;
}*/
//DEPRECATED
/*
void RoadMerger::MergeEdges(Node * firstNode, Node * secondNode, Edge * edgeOne, Edge * edgeTwo, MyData * data){
	if(secondNode == nullptr){
		return;
	}
	//first off, define which edge is getting deleted, and which one is kept (take the higher class or the shorter one)
	bool takeFirstEdge = false;
	bool tiebreaker = false;
	Edge * toRemove;
	
	if(edgeOne->GetStreetType() == edgeTwo->GetStreetType()){
		//if both share a streettype we need to sum up the travel time since we keep the edge w/the shortest traveling time
		tiebreaker = true;
	}else if(edgeOne->GetStreetType() > edgeTwo->GetStreetType()){
		takeFirstEdge = true;
	}else{
		//this else path is just for clarity, since takeFirstEdge is false by default, however for clarity reasons, I left it there
		takeFirstEdge = false;
	}

	if(tiebreaker == false && takeFirstEdge == true){
		toRemove = edgeTwo;
	}else if(tiebreaker == false && takeFirstEdge == false){
		toRemove = edgeOne;
	}else if(tiebreaker){
		unsigned int timeOne = 0, timeTwo = 0;
		Node * temp = firstNode;
		Edge * tempEdge = edgeOne;
		while(temp != secondNode){
			temp = tempEdge->GetOtherNode(temp);
			timeOne += tempEdge->GetDistance();
			std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
			//on the rare occasion we have a second node w/valence 1, just break here, since we already computed the whole distance
			if(edges.size() >= 2){
				tempEdge = (edges.at(0) == tempEdge ? edges.at(1) : edges.at(0));
			}else break;
		}
		temp = firstNode;
		tempEdge = edgeTwo;
		while(temp != secondNode){
			temp = tempEdge->GetOtherNode(temp);
			timeTwo += tempEdge->GetDistance();
			std::vector<Edge *> edges = data->GetEdgesByNodeID(temp->GetNodeID());
			//on the rare occasion we have a second node w/valence 1, just break here, since we already computed the whole distance
			if(edges.size() >= 2){
				tempEdge = (edges.at(0) == tempEdge ? edges.at(1) : edges.at(0));
			}else break;
		}
		if(timeOne > timeTwo){toRemove = edgeOne;}
		else{toRemove = edgeTwo;}
	}
	//delete the to Remove edge
	Node * otherNode = toRemove->GetOtherNode(firstNode);
	std::vector<Node *> toRemoveNodes;
	while(otherNode != secondNode && otherNode->GetValence() != 1){
		data->RemoveAndDeleteEdge(toRemove);
		toRemove = data->GetEdgesByNodeID(otherNode->GetNodeID()).at(0);
		toRemoveNodes.push_back(otherNode);
		otherNode = toRemove->GetOtherNode(otherNode);
	}
	//clean up the last double edge
	data->RemoveAndDeleteEdge(toRemove);
	//and keep on cleaning up
	for(unsigned int i = 0; i < toRemoveNodes.size(); i++){
		data->RemoveAndDeleteNode(toRemoveNodes.at(i));
	}
}
*/
//IMPORTANT NOTE: the edge passed as e WILL be deleted by this method, do not call the pointer again!
void RoadMerger::SplitEdge(Edge * e, Node * n, MyData * data){
	//first create the two new edges that will replace the one old edge
	Node * firstNode = e->GetStartNode(), * secondNode = e->GetEndNode();
	Edge * newOne = new Edge(firstNode, n, e->GetMaxSpeed(), e->GetStreetName());
	Edge * newTwo = new Edge(n, secondNode, e->GetMaxSpeed(), e->GetStreetName());
	//we only need some of the values, e.g. isTaken needs not be set, since from this point in the program on, all nodes are assumed to be taken automatically
	//TODO: we possibly dont even need oneway, make sure!
	newOne->SetOneWay(e->IsOneWay());	newTwo->SetOneWay(e->IsOneWay());
	newOne->SetStreetType(e->GetStreetType()); newTwo->SetStreetType(e->GetStreetType());
	//delete the edge from our nameToEdge map
	std::vector<Edge *> nameEdges = this->nameToEdge.at(e->GetStreetName());
	//we are just checking for funsies here, actually not necessary
	if(std::find(nameEdges.begin(), nameEdges.end(), e) != nameEdges.end()){
		//remove it
		nameEdges.erase(std::find(nameEdges.begin(), nameEdges.end(), e));
		//and add the new ones instead
		nameEdges.push_back(newOne);
		nameEdges.push_back(newTwo);
		this->nameToEdge[e->GetStreetName()] = nameEdges;
	}
	//then delete the old edge, and write the new ones into our data set
	data->RemoveAndDeleteEdge(e);
	std::vector<Edge *> edges;
	edges.push_back(newOne); edges.push_back(newTwo);
	data->AddEdgeList(edges);
}

//make sure the edge e is of the desired streetType before calling this
Edge * RoadMerger::FindNearbyParallelEdge(MyData * data, Edge * e, Node * n){
	//this is where we guarantee to only look @ streets w/the correct name
	std::vector<Edge *> edges = this->nameToEdge.at(e->GetStreetName());
	int size = edges.size();
	for(int i = 0; i < size; i++){
		if(e->GetStreetType() != edges.at(i)->GetStreetType()) continue;
		if(!WithinBounds(n, edges.at(i))){
			continue;
		}

		if(!OptionFile::doNotMergeDefaultNames && e->GetStreetName() == ""){
			continue;
		}
		if(!OptionFile::doNotMergeDefaultNames && edges.at(i)->GetStreetName() == ""){
			continue;
		}
		//TODO: bind it to an OptionFile value
		//do not merge small streets (it will not be a necessity anyway, afai-think)
		/*if(edges.at(i)->GetStreetType() > 7){
			continue;
		}*/
			
		//check whether the edge has our Node as start or endpoint
		if(*edges.at(i)->GetStartNode() == *n || *edges.at(i)->GetEndNode() == *n){
			continue;
		}
		//check whether the edge is parallel (i.e. the angle between the two edges differs by less than 5 degrees)
		Vec2 vec = Vec2(e->GetStartNode(), e->GetEndNode());
		Vec2 otherVec = Vec2(edges.at(i)->GetStartNode(), edges.at(i)->GetEndNode());
		double angle = vec.GetAngle(otherVec);
		if(angle >= 5 && (angle < 175 || angle > 185)){
			continue;
		}
		Line line = Line(edges.at(i)->GetStartNode(), edges.at(i)->GetEndNode());
		Node * temp = line.IntersectionPoint(n, n->GetNodeID());
		Edge * ed = new Edge(temp, n, 20000, "Who CAERS?J");
		//if the intersection point is not on the edge, it is not a valid road to merge
		if(!line.IntersectionOnSegment(temp, edges.at(i))){
			delete ed;
			delete temp;
		}
		//distance should be smaller or equal 200
		else if(ed->GetDistance() <= 200){
			//if it is delete the edge and return our found Edge! Yay!
			delete ed;
			return edges.at(i);
		}else{
			//if its far, far away, delete both the edge and the node
			delete temp;
			delete ed;	
		}
	}
	//if we didn't find any valid edges, return nullptr
	return nullptr;
}

//check whether the node's coordinates is in between the edgeNodes' coordinates
bool RoadMerger::WithinBounds(Node * n, Edge * e){
	//Need to add some wiggle room here
	double wiggleRoom = 0.002;
	if(n->GetLat() <= e->GetStartNode()->GetLat() + wiggleRoom && n->GetLat() >= e->GetEndNode()->GetLat() - wiggleRoom){
		return true;
	}else if(n->GetLat() >= e->GetStartNode()->GetLat() - wiggleRoom && n->GetLat() <= e->GetEndNode()->GetLat() + wiggleRoom){
		return true;
	}else if(n->GetLon() <= e->GetStartNode()->GetLon() + wiggleRoom && n->GetLon() >= e->GetEndNode()->GetLon() - wiggleRoom){
		return true;
	}else if(n->GetLon() >= e->GetStartNode()->GetLon() - wiggleRoom && n->GetLon() <= e->GetEndNode()->GetLon() + wiggleRoom){
		return true;
	}else return false;
}

Node * RoadMerger::GetNextNode(Node * start, Edge * edge, MyData * data){
	Node * tempNode = edge->GetOtherNode(start);
	Edge * temp = edge;
	while((tempNode->GetValence() == 2) || (this->OnlyTwoRealEdges(tempNode, data))){
		//if we only have two edges, take the other one
		if(tempNode->GetValence() == 2){
			std::vector<Edge *> dgs = data->GetEdgesByNodeID(tempNode->GetNodeID());
			temp = (*dgs.at(0) == *temp ? dgs.at(1) : dgs.at(0));
			//if the other node is a link-road, stop right there (link roads do not matter)
			if(temp->IsLinkRoad()){
				return tempNode;
			}
			tempNode = temp->GetOtherNode(tempNode);
		}else{
			//otherwise find the other non-link-road edge
			Edge * otherEdge = nullptr;
			std::vector<Edge *> edges = data->GetEdgesByNodeID(tempNode->GetNodeID());
			for(unsigned int i = 0; i < edges.size(); i++){
				if((edges.at(i)->IsLinkRoad()) || (*edges.at(i) == *temp)) continue;
				else{
					otherEdge = edges.at(i);
				}
			}
			temp = otherEdge;
			tempNode = temp->GetOtherNode(tempNode);
		}
	}
	return tempNode;
}

bool RoadMerger::OnlyTwoRealEdges(Node * node, MyData * data){
	std::vector<Edge *> edges = data->GetEdgesByNodeID(node->GetNodeID());
	unsigned int count = 0;
	for(unsigned int i = 0; i < edges.size(); i++){
		if(!edges.at(i)->IsLinkRoad()){
			count++;
		}
	}
	return count == 2;
}

//TODO: verify this works as intended
unsigned int RoadMerger::GetDistanceToNextNode(Node * start, Edge * toTravel, MyData * data){
	Node * first = toTravel->GetOtherNode(start);
	Edge * edge = toTravel;
	unsigned int distance = toTravel->GetDistance();
	//during computation of the shortest path, ignore potentially existing link roads (as before)
	while((first->GetValence() == 2) || (this->OnlyTwoRealEdges(first, data))){
		if(first->GetValence() == 2){
			edge = (*data->GetEdgesByNodeID(first->GetNodeID()).at(0) == *edge) ? data->GetEdgesByNodeID(first->GetNodeID()).at(1) : data->GetEdgesByNodeID(first->GetNodeID()).at(0);
			first = edge->GetOtherNode(first);
		}else{
			Edge * otherEdge = nullptr;
			std::vector<Edge *> edges = data->GetEdgesByNodeID(first->GetNodeID());
			for(unsigned int i = 0; i < edges.size(); i++){
				if((edges.at(i)->IsLinkRoad()) || (*edges.at(i) == *edge)) continue;
				else{
					otherEdge = edges.at(i);
				}
			}
			edge = otherEdge;
			first = edge->GetOtherNode(first);
		}
		distance += edge->GetDistance();
	}
	return distance;
}
