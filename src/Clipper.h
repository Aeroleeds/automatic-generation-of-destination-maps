#pragma once
#include "pipelinepart.h"

///Class to reduce the data in the MyData set to be only within the OptionFile::bb BoundingBox
/**	Removes all edges and nodes that are outside the BoundingBox and replaces edges that have one node within the BoundingBox and one outside of it with a new edge that ends just at the border */
class Clipper :
	public PipelinePart
{
public:
	Clipper(void);
	~Clipper(void);

	///The clipper clips the data against the BoundingBox defined under OptionFile::bb
	/**	All nodes outside the BoundingBox will be deleted, all edges completely outside will be deleted as well
		Edges with one node inside one node outside the bounding box have the outside node replaced by a node barely within the bounding boxes border
		The outer parts of that edge and the outside node will then be deleted*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	///Used to move border nodes that were created by clipping slightly into the boundingbox, in order to have all nodes inside the box
	void MoveNodeToNode(Node * toMove, Node * goal);
};

