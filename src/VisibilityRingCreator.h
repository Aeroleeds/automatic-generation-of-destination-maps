#pragma once
#include "pipelinePart.h"
#include "Dijkstra.h"
///class for defining rings around the destination for easy navigation
/**	The rings are marked as RingRoads and taken (their effect will only be visible after running a DataWallE instance)
	these ring roads will create a hierachical structure to travel
	Ringroads are created by sampling along multiple halflines, starting at the destination node
	we then check for the nearest edge of a given streetType that intersects the half-line
	we loop through the streetTypes and roads of higher classification will block roads with lesser classification
	(e.g. if a motorway is 200 metres to the north of our destination and the closest primary is 500 metres to the north, the primary will not be selected as ring road for the motorway blocks its visibility)
	The samples are then connected to create the ring roads
	All edges of the highest present streettype that is at least of type tertiary and the corresponding link edges (e.g. trunk and trunk_link) are automatically taken*/

class VisibilityRingCreator : public PipelinePart
{
public:
	VisibilityRingCreator(void);
	~VisibilityRingCreator(void);

	///RingSamples are containing the edges that we hit when sampling for our ring roads
	/**	They are organised in a vector, if no edge was hit in a given direction, a nullptr is stored in its stead
		Additionally possess a method for easy access to the next Edge Index (skipping possible nullptrs)*/
	struct RingSamples{
		RingSamples(){
		//	edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);edges.push_back(nullptr);
			for(unsigned int i = 0; i < (8 + 8 * OptionFile::stepsAtVisibilityRings); i++){
				edges.push_back(nullptr);
			}
		}
		///The vector containing all the edges
		/**	Is initialised as 8 nullptrs, do NOT push_back, but overwrite the nullptrs*/
		std::vector<Edge *> edges;
		///Returns the index of the next edge in the RingSamples object, or UINT_MAX if none exist
		unsigned int NextEdgeIndex(unsigned int curreIndex){
			if(edges.at((curreIndex + 1 )% edges.size()) == nullptr){
				return NextEdgeIndex(curreIndex + 1, 1);
			}else return (curreIndex + 1) % edges.size();
		}
	private:
		//this is here, so we do not loop forever if the samples are empty
		///a method counting the steps taken in trying to find a non-nullptr entry (if the steps are > the size of our Ringsamples, no edge could be found)
		unsigned int NextEdgeIndex(unsigned int curreIndex, unsigned int stepsTaken){
			if(stepsTaken > edges.size()){
				return UINT_MAX;
			}
			if(edges.at((curreIndex + 1 )% edges.size()) == nullptr){
				return NextEdgeIndex(curreIndex + 1, stepsTaken + 1);
			}else return (curreIndex + 1) % edges.size();
		}
	};
	///Samples the visibility rings at specific points, and tries to connect the samples afterwards
	/**	Sampling is done with halflines starting at the destination Node and going up until the border
		Out of all Edges intersecting the halfline, the one closest to the destination Node is taken as sample
		Samples are connected to their neighbours, with the edges in between receiving the ringroad tag as well as the taken tag
		Smaller roads are hidden by larger roads, e.g. if we the closest sample of a primary road is at distance 322m to the north of the destination node, but there is a trunk sample at 112m, the primary sample is ignored (blocked by the trunk sample)*/
	virtual std::string PrepDoc(MyData * data);
	///Returns a string to identify this PipelinePart
	virtual std::string ToString();
	///SetUp function needs be called before using this Object
	virtual void SetUp();
private:
	///will draw samples for our visibility ring in eight directions; ALWAYS call this in descending order of streetTypes (largest first)
	/**will return the nearest edge in all the defined directions (8 sample points)
		creates a halfline for each of these intersections through the destination node and returns all the found closest edges
		larger, already found edges will however block the next edges
		NEEDS be called in order, for checking whether the current edge is blocked*/
	RingSamples GetSamples(std::vector<Edge *> edges, MyData * data);
	///will take a ringsamples struct and connect any missing pieces if possible setting them as ring roads
	/**will follow roads outward if they have no neighbour, in order to hopefully provide better results */
	void ConnectSamples(RingSamples samples, MyData * data);
	///connects the two edges passed on as param
	/**if the two streets cross at some point, this will simply extend both edges, so they connect there
		if not, a dijkstra path search will be used on the dataset, in order to find a good connection
		automatically sets the edges along the way to RingRoads and as Taken in data*/
	void ConnectAndTakeEdges(Edge * one, Edge * two, MyData * data);
	//Will compute the squared distance between the edge and the node
	/*The distance is barely there for comparison's sake
		and will be computed by taking the squared delta latitude plus the squared delta longitude of the node and the (would be) intersection on the edge*/
	//double GetDistance(Edge * edge, Node * node);
	///Returns the distance between the Node node and the intersection point obtained by intersecting the two Edges
	/**	This is used to get the distance of samples, with the Node set as destination node, the Edge set to one of the sample edges (from compareAgainst) and the otherEdge being an Edge from the MyData object*/
	double GetDistance(Edge * edge, Edge * otherEdge, Node * node);
	///Follows the edge along its streetname until it traveled too long or it reaches a node where an edge w/the specified street name is present
	/** Too long here is defined as 1.5 km
		Stops when the road name changes
		Returns the node where the specified road takes off*/
	Node * GetJunction(Node * start, Edge * edge, Edge * otherEdge, MyData * data);
	///If no way was found an empty vector is returned, else the vector contains all edges along the found path
	std::vector<Edge *> FoundWayToNode(Node * start, Edge * edge, Node * goal, MyData * data);
	///Follows the Edge edge at the start Node until the end Node is reached and sets all encountered edges to taken and as ringroad
	void FollowAndTake(Node * start, Node * end, Edge * edge, MyData * data);
	///Checks whether the returned Dijkstra path is longer than the referenceLength, if it is not, all Edges in the path are set as taken and ring roads; returns whether the path was taken or not
	bool CheckAndTakeDijkstraPath(std::vector<Edge *> path, MyData * data, double referenceLength);
	//returns whether the two indices are close enough to one another to warrant a connection between them
	/**	E.g. we do not want to connect samples on opposite sides (east and west) to one another for the connection will cut the data in half*/
	bool ConnectIt(RingSamples samples, unsigned int ind, unsigned int nextInd);

	///the values for the shortest found distance in each direction we sample
	std::vector<double> shortestDistances;
	///containing the edges to check against for possible intersections, set up once in the constructor
	std::vector<Edge *> compareAgainst;
	///The Dijkstra object to compute shortest paths, if a connection is to be made
	Dijkstra dijk;
	///A map to get all edges of a specific StreetType
	std::map<StreetType, std::vector<Edge *>> edgeSets;
	///A map to get the RingSamples of a specific StreetType
	std::map<StreetType, RingSamples> sampleSets;
};

