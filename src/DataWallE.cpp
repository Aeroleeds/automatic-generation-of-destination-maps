#include "StdAfx.h"
#include "DataWallE.h"
#include "RoadLayoutData.h"

DataWallE::DataWallE(void)
{
}


DataWallE::~DataWallE(void)
{
}

std::string DataWallE::PrepDoc(MyData * data){
	std::vector<unsigned int> edgesToDelete;
	for(unsigned int i = 0; i < data->GetEdgeList().size(); i++){
		Edge * e = data->GetEdgeList().at(i);
		if(!e->IsTaken()){
			edgesToDelete.push_back(e->GetEdgeID());
			//data->RemoveAndDeleteEdge(e);
			//delete e;
		}else{
			//remove possible one-way tags, for they do not matter anymore, but may interfere with our MyDataToXML
			e->SetOneWay(false);
		}
	}
	std::vector<unsigned int> nodesToDelete;
	for(unsigned int i = 0; i < data->GetNodeList().size(); i++){
		Node * n = data->GetNodeList().at(i);
		if(!n->IsTaken()){
			nodesToDelete.push_back(n->GetNodeID());
			//data->RemoveAndDeleteNode(n);
			//delete n;
		}
	}
	for(unsigned int i = 0; i < edgesToDelete.size(); i++){
		data->RemoveAndDeleteEdge(data->GetEdgeByID(edgesToDelete.at(i)));
	}
	for(unsigned int i = 0; i < nodesToDelete.size(); i++){
		data->RemoveAndDeleteNode(data->GetNodeByID(nodesToDelete.at(i)));
	}
	return std::string("Splendid");
}
std::string DataWallE::ToString(){
	return std::string("DataWallE");
}

