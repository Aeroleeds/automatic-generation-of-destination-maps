#pragma once
#include "CostTerm.h"
#include "Vec2.h"
///Will return the angle between the newly proposed edge from the RoadLayoutData and the equivalent old edge in the MyData object
/**	The class implementing the orientation term (see: 4.3.2. #4)
	This shall penalise edges that differ too far from the original orientation
*/
class OrientationTerm :
	public CostTerm
{
public:
	OrientationTerm(void);
	~OrientationTerm(void);

	//the stuff we inherited from our costTerm
	///Returns the squared angle in radians between the Edge e and its equivalent in the MyData set
	/**	Provides the layouts with correct-ish angles
		The reference angles are the ones from the original data (i.e. before all the layout steps happen, not from the unchanged downloaded data)
		The angle is measured by creating two normalised vectors from the edges and using the acos on them	*/
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///The OrientationTerm has no PostCost()
	double PostCost();
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	/**	Note: the weight quintuples during the second stage of layouting (i.e. the weight is 1000 in stage one and 5000 in stage two)*/
	double GetWeight(int suffix);

private:
	///Saves the vec2s associated w/the given edgeIDs of the original data
	std::map<unsigned int, Vec2> eIDToVec;
	///Initialises the eIDToVec map
	void InitMap(MyData * data);
	///The boolean to check whether we already initialised the map
	bool alreadyInit;

};

