#pragma once
#include "Node.h"

///Class for normalised vectors, w/a value in lon and lat
/**	values are automatically normalised on construction
	this is done, for we are only really into the angles between two vectors
	DO NOT USE IF UNNORMALISED VECTORS ARE DESIRED
	additionally contains a method for computing the dot product in order to get the angle between two Vec2s
	easy constructor taking the start and end nodes is readily available
*/
class Vec2
{
public:
	///constructor using lo and la as the directions the vec2 is pointing to
	/**	Will normalise the vector, for its use is to compute angles*/
	Vec2(double lo, double la);
	///constructor computing the direction of the vector by subtracting the position of the start node from the position of the end node
	/**	Will normalise the vector, for its use is to compute angles*/
	Vec2(Node * start, Node * end);
	~Vec2(void);
	///returns the angle between this vector and another vector
	/**	The returned angle will be in degrees*/
	double GetAngle(Vec2 otherVec);
	///returns the angle between this vector and another vector
	/**	The returned angle will be in radians*/
	double GetAngleRadians(Vec2 otherVec);
	///compute the dot product between this vector and the otherVec
	double DotProduct(Vec2 otherVec);
private:
	///Returns the length of the vector for normalising purposes
	/**	Used during the construction of the vector to obtain normalised vectors*/
	double GetLength(double lo, double la);
	///The two attributes needed to describe a vector
	double lon, lat;
	
};
