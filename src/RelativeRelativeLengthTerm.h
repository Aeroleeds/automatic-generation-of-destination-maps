#pragma once
#include "costterm.h"

///An experimental alternative to the default relative length term, that only checks whether edges with similar street type maintain their relative length
/**	E.g. a the cost assessed for a motorway edge is not anymore dependant on how large residential roads have become, but simply tries to maintain the size relations between other edges of the same or a similar streettype
	this should enable smaller roads to grow more, since they are no longer forced to remain smaller than the motorways but are instead only compared to their small road counterparts
	The argument for this is that small and large roads are differentiable by their rendering alone and we thus may distort the relation between them
	so long as we maintain the length within a streettype it should be easily navigable
	60 percent of the weights go to the same streettype, these are then divided evenly among all edges of that type
	40 percent to the neighbouring ones, which are divided evenly among all edges of BOTH neighbouring types (or just one if only one exists; e.g. motorway only has a lower neighbour, for it is the biggest streettype)*/
class RelativeRelativeLengthTerm :
	public CostTerm
{
public:
	RelativeRelativeLengthTerm(void);
	~RelativeRelativeLengthTerm(void);
	///Returns the cost of e compared to all edges in tempData
	/**	Does the same computation as RelaiveLengthTerm, but applies a weight to it depending on which streettype the pair of edges belong to*/
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	double GetWeight(int suffix);
private:
	///Initialises the two necessary maps for usage of the RelRelLengTerm (typeToCount and TypeHelper.typesPresent)
	void InitMaps(RoadLayoutData * data);
	///Returns the multiplier to be used for the two given streettypes (depends on how close these two types are, will return 0 if there is one or more streettypes between the main and other type)
	double GetMultiplier(StreetType mainType, StreetType otherType);
	///A map maintaining the count of edges for each given type
	std::map<StreetType, unsigned int> typeToCount;
	///A helper to aid in finding the neighbouring streettypes
	struct TypeHelper{
		///contains all StreetTypes found in the data
		std::set<StreetType> typesPresent;
		///Returns a vector with the neighbouring StreetTypes for the given StreetType (if none exist an empty vector is returned)
		/**	Neighbouring here means the first present type that is part of the data (a motorway may be considered neighbouring to tertiaries if no prim, second or trunk roads exist)*/
		std::vector<StreetType> GetNeighbouringTypes(StreetType currentType){
			std::vector<StreetType> toReturn;
			//look for bigger streettypes that might be present
			int temp = currentType - 1;
			while(temp >= 0){
				if(IsLinkType((StreetType) temp)){
					temp--;
				}else if(typesPresent.count((StreetType) temp) == 0){
					temp --;
				}else{
					toReturn.push_back((StreetType) temp);
					break;
				}
			}
			//then look for smaller streettypes
			temp = currentType + 1;
			//while we are at a type that is still used
			while(temp < OptionFile::nonUsedType){
				if(IsLinkType((StreetType) temp)){
					temp++;
				}else if(typesPresent.count((StreetType) temp) == 0){
					temp++;
				}else{
					toReturn.push_back((StreetType) temp);
					break;
				}
			}
			return toReturn;
		}
	private:
		///A little helper function to ignore link roads (this is just a preventive measure, since no link roads will go through to the layouting stage, but we keep it as safety measure)
		bool IsLinkType(StreetType type){
			return streetStrings[type].find("_link") != std::string::npos;
		}
	};
	///The typehelper object to help with finding neighbouring streettypes
	TypeHelper help;
};

