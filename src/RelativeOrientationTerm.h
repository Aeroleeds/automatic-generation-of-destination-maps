#pragma once
#include "costterm.h"
#include "FalseIntersectionHelper.h"
///Evaluates whether segments that were either parallel, orthogonal or straight in the original data remain that way
/**	Evaluates a MyData set during construction for edges that are one of the three cases and saves their IDs in pairs
	Then constructs a map for edge IDs and their corresponding new edges in the GetCost step
	And computes the final cost in PostCost using the squared deviation from the perfect orientation in radians
	Is not applied during the first stage of layouting, but is introduced in the second one to reposition the data similarly to the way it was originally*/
class RelativeOrientationTerm :
	public CostTerm
{
public:
	//set the orientations from the original MyData here!
	RelativeOrientationTerm(MyData * data);
	~RelativeOrientationTerm(void);

	//the stuff we inherited from our costTerm
	///GetCost always returns 0
	/**	And builds a map for Edge ID to the Edge e in the process*/
	double GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///Sums up the difference in orientation for all marked edges
	/**	Loops through all marked pairs and determines whether the angle between the two edges is still orthogonal/parallel/straight
		Assesses a penalty depending on how much the angle deviates from the expected value*/
	double PostCost();
	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	/**	Has a weight of 0 during the first stage of layouting*/
	double GetWeight(int suffix);

	void ResetTerm();
private:
	///returns the angle between the two edges found under the given IDs
	/**	In radians
		Needs a set up edgeIDToEdge map in order to function
		Will NOT check if such an Edge exists, for it should if this method is only called at the appropriate point in time*/
	double GetAngle(std::pair<unsigned int, unsigned int> pair);
	///returns the angle between the two edges e and other
	/**	In radians*/
	double GetAngle(Edge * e, Edge * other);
	///Returns all edges that share a node with Edge e
	/**	In the given MyData object*/
	std::vector<Edge *> NeighbourEdges(Edge * e, MyData * data);
	/*
		checks if the two edges intersect if we lay a perpendicular line through one of em
	*/
	///Checks if the perpendicular line through one of the edges intersects with the other
	/**	Used for finding parallel edges*/
	bool Intersects(Edge * e, Edge * other);
	///Returns an Edge through the intersection between the perpendicular line through one of the edges and through one of the edge's nodes
	/**	The returned edge itself lies on the perpendicular line as well
		The Edge needs be deleted by the caller as well as the starting Node (and ONLY the starting Node) of given Edge*/
	Edge * GetPerpendicularEdge(Edge * e, Edge * other);
	//to map the oriented edges to the new edges
	///Map to easily access all interesting edges by their ID
	std::map<unsigned int, Edge *> edgeIDToEdge;
	///Map to save all edge IDs of the orthogonal pairs in the original data set
	/**	Each ID of the pair represents its own edge
		i.e. std::pair< 4, 3 > in the set means that edge w/ID 4 is perpendicular to edge w/ID 3*/
	std::set<std::pair<unsigned int, unsigned int>> orthogonalPairs;
	///Map to save all edge IDs of the straight pairs in the original data set
	/**	Each ID of the pair represents its own edge
		i.e. std::pair< 4, 3 > in the set means that edge w/ID 4 is straight to edge w/ID 3*/
	std::set<std::pair<unsigned int, unsigned int>> straightPairs;
	///Map to save all edge IDs of the parallel pairs in the original data set
	/**	Each ID of the pair represents its own edge
		i.e. std::pair< 4, 3 > in the set means that edge w/ID 4 is parallel to edge w/ID 3*/
	std::set<std::pair<unsigned int, unsigned int>> parallelPairs;
	///Helps find intersections between two potential parallel edges
	/**	If there is an edge between two considered edges, they are not anymore considered,
		for only edges which are directly visible to one another are considered as being parallel*/
	FalseIntersectionHelper help;
	///A set to contain all edgeIDs that are in at least one of the pairs
	std::set<unsigned int> interestingEdges;
};

