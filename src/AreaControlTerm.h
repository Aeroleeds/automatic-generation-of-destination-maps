#pragma once
#include "costterm.h"
///Custom cost term placing five BoundingBoxes into the data and increasing them until a certain number of nodes is in each rectangle
/** The penalty depends on how often an increase operation had to be executed in order to get the desired number of nodes in every rect
	Divide like this:
		_________
		| A|  |B |
		|__|__|__|
		|  |C |  |
		|__|__|__|
		| D|  |E |
		|__|__|__|

	and increase the areas if they are too small
	The # of nodes needed for no penalty is a fraction of the total amount of nodes (i.e. tempData->GetNodeList().size() / x)
	The increments of enlargenement for the rectangles is depending on the total size of the layout*/
class AreaControlTerm :
	public CostTerm
{
public:
	///Simple constructor
	AreaControlTerm(void);
	~AreaControlTerm(void);
	///Saves all nodes into the nodes container to be used in the PostCost()
	/**	Will only save if nodes is empty, thus the first time this is used or after the nodes are cleaned via ResetTerm*/
	double AreaControlTerm::GetCost(Edge * e, RoadLayoutData * tempData, MyData * data);
	///Checks for the nodes inside the rectangles and assesses a penalty for how often the rects needed to be enlargened until the desired number of nodes is in it
	double AreaControlTerm::PostCost();

	///Returns a string representation of this CostTerm
	std::string ToString();
	///Returns the unique among CostTerms ID
	std::string GetIdentifier();
	///Returns the weight depending on the current suffix
	double GetWeight(int suffix);
	///Cleans the Node container used in the post cost, always must be called before a new RoadLayoutData is to be evaluated
	virtual void ResetTerm();
private:
	///the six BoundingBox objects dividing our space in equal parts
	//BoundingBox a, b, c, d, e, f;
	///The container for all the nodes of the RoadLayoutData, is filled during GetCost()
	std::vector<Node *> nodes;
	//std::vector<BoundingBox> boxes;
	///Contains the starting values for the rectangles, that are to be copied and then grown until the right # of nodes is present
	std::vector<BoundingBox> startingBoxes;
	///the increments by which the rectangles are enlargened
	double incLat, incLon;
	///Takes the toSmall box and returns an enlargened version
	/**Enlargement takes place in all directions according to the incLat / incLon values*/
	BoundingBox EnlargenBox(BoundingBox toSmall);
};

