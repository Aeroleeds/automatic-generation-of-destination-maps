// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <set>

//forward declarations
struct PointInSpace;
struct BoundingBox;


// TODO: reference additional headers your program requires here
#include <iostream>
#include <map>

#include <tinyxml2.h>
#include <curlpp\cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#include "Node.h"
//#include "OptionFile.h"

/*
	StreetTypes in correct order, according to http://wiki.openstreetmap.org/wiki/Map_Features#Highway
	make sure numValues is the last elem in the enum
	TODO: need to include the links; e.g. motorway_link
*/

enum StreetType{motorway, motorway_link, trunk, trunk_link, primary, primary_link, secondary, secondary_link, tertiary, tertiary_link, unclassified, residential, service, living_street, pedestrian, track, numValues};

static const std::string streetStrings[] = { "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "secondary_link", "tertiary", "tertiary_link", "unclassified", "residential", "service", "living_street", "pedestrian", "track" , "NOT A VALUE"};
//these are default values that might sadly be off a little (e.g. primary roads have 100kmh outside of cities and 50kmh within cities, I took 100...)
static const unsigned int defaultSpeed[] = {130, 20, 100, 20, 80, 20, 60, 20, 50, 20, 30, 30, 30, 5, 5, 5, 0};

/*
	Struct for the bounding box, necessary to set the borders of the map
*/
///Struct for a rectangle within the data
/**	Possesses methods for checking whether a Node or position (lat/lon pair) is inside the given box
	As well as a method to produce a string representation of the BoundingBox that is compatible with xapi requests (e.g. [bbox=negLong/LEFT,negLat/BOT,posLong/RIGHT,posLat/TOP])
	The boxes sides are ALWAYS parallel to the two axes (latitude and longitude)
	In all cases the negative values need to be smaller than positive values*/
struct BoundingBox{
	///The border values defining the bounding box
	long double posLat, negLat, posLong, negLong;
	///empty constructor, initialises all values to 0
	BoundingBox(){
		this->posLat = 0; this->negLat = 0; this->posLong =0; this->negLong = 0;}
	//NOTE:the neg values need to be smaller than the corresponding pos Values
	///Constructor taking all four values, n(egative) values need be smaller than p(ositive) values
	BoundingBox(long double pLat, long double nLat, long double pLong, long double nLong){
		this->posLat = pLat; this->negLat = nLat;
		this->posLong = pLong; this->negLong = nLong;}
	///Copy constructor to produce a second BoundingBox just like the other BoundingBox
	BoundingBox(const BoundingBox& other){
		this->posLat = other.posLat;
		this->negLat = other.negLat;
		this->posLong = other.posLong;
		this->negLong = other.negLong;
	}
	///will return the bounding box in a string suitable for the xappy request (e.g. [bbox=negLong/LEFT,negLat/BOT,posLong/RIGHT,posLat/TOP])
	std::string toString(){
		return std::string("[bbox=").append(std::to_string(negLong)).append(",").append(std::to_string(negLat)).append(",")
			.append(std::to_string(posLong)).append(",").append(std::to_string(posLat)).append("]");
	}
	///Returns true when the position (lat and lon) is inside this BoundingBox
	bool IsInside(double lat, double lon){
		if(posLat >= lat && negLat <= lat && posLong >= lon && negLong <= lon){
			return true;
		}else return false;
	}
	///Returns true when the Node n is inside this BoundingBox
	bool IsInside(Node * n){
		//The bounding box checks are in lat/lon
		return IsInside(n->GetLat(false), n->GetLon(false));
	}
};
/*
	Struct for points in the map, simply containg longitude and latitude values
*/
///Struct for points in the map, as described by their latitude and longitude values
struct PointInSpace{
	PointInSpace(long double lon, long double lat){
		this->longi = lon;
		this->lat = lat;
	}
	PointInSpace(){
		this->longi = 0;
		this->lat = 0;
	}
	///The latitude and longitude values of that given point)
	long double longi, lat;
};