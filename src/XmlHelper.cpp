#include "stdafx.h"
#include "XmlHelper.h"
#include <iostream>
#include <fstream>


tinyxml2::XMLDocument * XmlHelper::ReadFile(const std::string &filePath){
	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
	tinyxml2::XMLError err;
	err = doc->LoadFile(filePath.c_str());
	if(err != 0){
		delete doc;
		return nullptr;
	}
	return doc;
}

void XmlHelper::SaveFile(tinyxml2::XMLDocument * doc, std::string name){
	//std::string tempName = name.append(".xml");
	/*std::string tempName = "FolderForStuff\\test.xml";
	FILE * file = std::fopen(tempName.c_str(), "w");
	//tinyxml2::XMLPrinter * print = new tinyxml2::XMLPrinter(file);
	//doc->Print(print);
	//delete print;
	doc->SaveFile(file);
	std::fclose(file);*/
	std::string fileName = std::string("Data\\").append(name).append(".xml");
	doc->SaveFile(fileName.c_str());

}

