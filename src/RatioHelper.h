#pragma once
#include "stdafx.h"

///Class to maintain the correct ratio between the BoundingBox and the output width and height
/**	If the ratio is off, the smaller/larger side of the BoundingBox is enlargened until the ratio is close to the output ratio
	Whether the smaller or larger side of the BoundingBox is made larger is depending on the difference in ratios
	The output width and height is not changed by this algorithm*/
class RatioHelper
{
public:
	RatioHelper(void);
	~RatioHelper(void);
	///Configures the bounding box so the ratios between the orig BB and the output are the same
	/**	Additionally computes the rlToMapFactor to be later used by the cost terms*/
	//static BoundingBox ReconfigureBB(BoundingBox orig, int outputWidth, int outputHeight);
	static void ComputeRLFactor();
	///Returns the factor used to get the equivalent map distance to the real life distance
	/**	ReconfigureBB NEEDS be called before calling GetRLtoMapFactor*/
	static double GetRLtoMapFactor();

private:
	///The factor used to multiply the edge->GetDistance() values with in order to get their distance on the output paper
	static double rlToMapFactor;
	///Small function to prevent the need for the ratios to be the exact same number (instead providing a margin of error in which the two ratios are considered equal)
	static bool EqualEnough(double one, double two);
};

